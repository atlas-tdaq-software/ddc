#ifndef _AUTO_SYMULATOR_H_
#define _AUTO_SYMULATOR_H_

//
// This header file contains definition of AutoSimulator object class. 
// An object of this class is destined for sending a pre-defined DAQ
// data and handle the DCS response for the automatic test of DDC-DT 
// while building OSW release 
//							S. Khomoutnikov 
//							2001
// Modifications:
//		S.Kh	26.11.03	Ported for omniORB4 CORBA broker
//
//////////////////////////////////////////////////////////////////////

typedef struct
{
	int 	status;
	std::string  name;
} SystemStatus;



class AutoSimulator
{
public:
	AutoSimulator(DdcDaqsimConfig* dcsImportConfig, DdcDaqsimConfig* dcsExportConfig,
				  IPCPartition& p) 
		: exportConfig(dcsImportConfig), importConfig(dcsExportConfig),
		  partition(p) 
		{ doExit = 0; }
	~AutoSimulator() { delete importConfig; delete exportConfig; }
	bool run(ISInfoReceiver *);
	bool exportDataToIS(SystemStatus* exportData);
	IPCPartition&	getPartition() { return(partition); }
	
	static int doExit;
private:
	DdcDaqsimConfig*	exportConfig;
	DdcDaqsimConfig*	importConfig;
	IPCPartition	partition;
};

#endif
