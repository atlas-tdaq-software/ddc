#include <string>
#include <vector>
#include <time.h>
#include <math.h>
#include <unistd.h>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <is/namedinfo.h>
#include <is/inforeceiver.h>
#include <cmdl/cmdargs.h>

using namespace std;

#include "DdcData.hxx"
#include "DdcDaqSimConfig.hxx"
#include "DdcDaqSim_auto.hxx"

static CmdArgStr config_file('C', "DDC_config", "data-transfer-config-path", "path of configuration file");

int AutoSimulator::doExit = 0;

void callback(ISCallbackInfo* cbInfo)
{
	AutoSimulator* 	autoSimulator;
	time_t			justNow;
	autoSimulator = (AutoSimulator*)(cbInfo->parameter());
	
	justNow	= time(&justNow);
	DcsSingleData<float>	dataFlt((time_t)0, autoSimulator->getPartition(),
						    (char*)cbInfo->name());
	
	if(cbInfo->reason() != ISInfo::Deleted) {
		if(cbInfo->type() == dataFlt.type()) {
			cbInfo->value(dataFlt);
			if(fabs(dataFlt.getValue() - 55.55) <= 0.0001) {
				cerr << "SUCCESS: DdcDt is working properly" << endl;
				autoSimulator->doExit = 1;
			}
			else {
				if(fabs(dataFlt.getValue() - 0.23) >= 0.0001) {
					cerr << "ERROR: Improper value " << dataFlt.getValue()
						 << " has been set in PVSS" << endl;
					autoSimulator->doExit = -1;
				}
				else
					cerr << "INFO: Proper response is received "
						 << ctime((const time_t*)&justNow) << endl;
			}
		}
  		// Here handling some other types may be inserted
		// ...

	}
	else {
		cerr << "INFO: Data is deleted from IS" << endl;
		autoSimulator->doExit = -1;
	}
}

bool AutoSimulator::exportDataToIS(SystemStatus* exportData)
{
	time_t	currTime;
	string 	isObName;
	bool	success = true;

	isObName = exportConfig->findIsServer(exportData->name.c_str());
	isObName += ".";
	isObName += exportData->name;

	DcsSingleData<int>* isExportData = new DcsSingleData<int>(time(&currTime), partition, (char*)isObName.c_str());
	isExportData->setValue(exportData->status);
	try {
		isExportData->checkin();
		cerr << "INFO: DAQ data sent " << ctime((const time_t*)&currTime) << endl;
	}
	catch(daq::is::Exception) {
		cerr << "ERROR: " << ctime((const time_t*)&currTime) << " IS server "
			 << exportConfig->findIsServer(exportData->name.c_str()) << " for " << isObName 
			 << " in partition " << partition.name() << " does not run!" << endl;
		success = false;
	}
	delete isExportData;
	return(success);
}


bool AutoSimulator::run(ISInfoReceiver* testReceiver)
{
	SystemStatus	exportData;
	string			dpName;
	bool			success;
	int 			clock = 0;
	
  // First subscribe for 'v1.response.opening'
	dpName = importConfig->findIsServer("v1.response.opening");
	dpName += ".";
	dpName += "v1.response.opening";

	try {
		testReceiver->subscribe(dpName.c_str(), callback, (void*)this);
		cerr << "DAQ_sim: DCS data subscribed for" << endl;
	}
	catch(daq::is::RepositoryNotFound) {
		cerr << "DAQ_sim_ERROR: "
			 << " DT_autotest subscription fault RepositoryNotFound for DPE "
			 << dpName << endl;
		return(false);
	}
	catch(daq::is::InvalidCriteria) {
		cerr << "DAQ_sim_ERROR: "
			 << " DT_autotest subscription fault InvalidCriteria for DPE "
			 << dpName << endl;
		return(false);
	}
						 
  // Now build the SystemStatus 'DAQ1.status' and
  // export it into the proper IS server 
	exportData.status = 1;
	exportData.name = "DAQ1.status";
	
	cerr << "DAQ_sim: Exporting data" << endl;
	success = exportDataToIS(&exportData);
	if(!success)
		return(false);
	
	while(1) {
		if(!doExit) {
			clock += 1;
			if(clock >= 20) {
				cerr << "ERROR: No response from DDC-DT" << endl;
				return(false);
			}
			sleep(1);
		}
		else {
			cerr << "DAQ_sim: Doing unsubscribe" << endl;
			testReceiver->unsubscribe(dpName.c_str());
			cerr << "Unsubscribed" << endl;
			break;
		}
	}
	return(true);
}


int main(int argc, char *argv[])
{
	AutoSimulator* myDaq = NULL;
	DdcDaqsimConfig* dcsImportConfig = 0;
	DdcDaqsimConfig* dcsExportConfig = 0;
	string 	ddc_conf;

    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);
    CmdArgStr	appl_name('n', "daqsim_appl", "application-name", "name of appl.to test.", CmdArg::isREQ);

//
// Initialize command line parameter with default value
//
	config_file = "";

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, &appl_name, &config_file, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmdLine.parse(arg_iter);
	
	if(config_file == "") {
		cerr << appl_name << "_ERROR: Configuration is unavailable. Exiting ..." << endl;
		exit(1);
	}
	else
		cerr << appl_name << "_INFO: Test application started" << endl;

	dcsImportConfig = new DdcDaqsimConfig((const string)config_file, (const string)appl_name, TO_DCS);
	dcsExportConfig = new DdcDaqsimConfig((const string)config_file, (const string)appl_name, FROM_DCS);
	
	if(dcsExportConfig->getItemNum() == 0) {
		cerr << appl_name << "_ERROR: No DCS->DAQ data defined";
		exit(1);
	}
	if(dcsImportConfig->getItemNum() == 0) {
		cerr << appl_name << "_ERROR: No DAQ->DCS data defined";
		exit(1);
	}
	
	IPCCore::init(argc, argv);
	
	cerr << appl_name << "_INFO: IPCCore::init done" << endl;
	IPCPartition testPartition((const char*)partition_name);
	ISInfoReceiver* testReceiver = new ISInfoReceiver(testPartition);
	cerr << appl_name << "_INFO: Receiver created" << endl;
	
	myDaq = new AutoSimulator(dcsImportConfig, dcsExportConfig, testPartition);
	cerr << appl_name << "_INFO: myDaq created" << endl;
	bool success = myDaq->run(testReceiver);
	
	int exitCode = myDaq->doExit;
	cerr << appl_name << "_INFO: deleting objects" << endl;
	delete testReceiver;	
	delete myDaq;
	
	if(success && (exitCode > 0)) {
		cerr << appl_name << "_INFO: Returning 0" << endl;
		return 0;
	}
	else {
		cerr << appl_name << "_ERROR: Returning 1" << endl;
		return 1;
	}
}
