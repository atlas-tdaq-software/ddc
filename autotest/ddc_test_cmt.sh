#!/bin/sh
#################################################################
#
#	Test Script for the DDC package
#	Created by S.Khomoutnikov 23.10.01 
#	Last modified 21.01.05 	by S.Kh.
#
#################################################################

# Environment
export PATH="$1/bin:$PATH"
#export LD_LIBRARY_PATH="$1/bin:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH="$TDAQ_INST_PATH/../external/$CMTCONFIG/lib:$LD_LIBRARY_PATH"
#export LD_LIBRARY_PATH="$1/api/$CMTCONFIG/lib:$LD_LIBRARY_PATH"
export PVSS_PATH="$1"
echo "PVSS_PATH = $PVSS_PATH"
export TDAQ_PARTITION="integ_ddc_test"
export TDAQ_DB_DATA="../data/ddc_check_partition.data.xml"
export TDAQ_DB=rdbconfig:ddcDb
# to address to pvss registry pvssInst.conf
export PVSS_II_ROOT="/afs/cern.ch/atlas/project/tdaq/dcs"

#source /afs/cern.ch/atlas/project/tdaq/cmt/bin/memcheck.sh

# Initial settings
s_ref="server.ref"
err_file="server.err"
pvss_ref_dt="pvss_dt.ref"
pvss_err_dt="pvss_dt.err"
pvss_ref_ct="pvss_ct.ref"
pvss_err_ct="pvss_ct.err"
pvss_ref_mt="pvss_mt.ref"
pvss_err_mt="pvss_mt.err"
ddcdt_ref="ddc_dt.ref"
ddcmt_ref="ddc_mt.ref"
ddcct_ref="ddc_ct.ref"
daqsim_err="ddc_daq_sim.err"
msg_err="ddc_msg_receiver.err"
mrs_ref="mrs_server.ref"
mrs_err="mrs_server.err"

project="ddcAutoTestProj"
project_dir="/tmp/ddc_$CMTCONFIG"

partition="ipc_server -p $TDAQ_PARTITION"
runCtrl="is_server -p $TDAQ_PARTITION -n RunCtrl "
runParams="is_server -p $TDAQ_PARTITION -n RunParams "

mrs_srv="mrs_server -p $TDAQ_PARTITION"

pvss="start_pvss2"

ddcmt="ddc_mt -p $TDAQ_PARTITION -n ddcmt1 "

test_receiver="ddc_msg_receiver -p $TDAQ_PARTITION "

ddcdt="ddc_dt -M 2 -p $TDAQ_PARTITION -n ddcdt1 "

daqsim="./ddc_daqsim_auto -p $TDAQ_PARTITION -n DdcDaqSim -C ddc_daqsim.config "

ddcct="ddc_ct -p $TDAQ_PARTITION -n ddcct1 -P RootController -s ddc"

ipcmainid=0
ipcsrvid=0
mrssrvid=0
srvid1=0
srvid2=0
srvid3=0
rcvid=0
rootid=0

already_exists=0
srv_error=0
mt_test_error=0
ct_test_error=0

TestMsg1="DAQ1.status ERROR"
TestMsg1a="DAQ1.status FATAL"
TestMsg2="INVALID ANY MORE"
TestMsg2a="DAQ1.status SUCCESS"
TestMsg3="DDC_MT_message.response.text ERROR"
TestMsg4="DDC_MT_message.response.text SUCCESS"

check_message()
{
	count=0
	Message=`grep "$1" $msg_err`
	while test -z "$Message" -a $count -lt 15
	do
		sleep 1
		count=`expr $count + 1`
		Message=`grep "$1" $msg_err`
	done
	if test $count -eq 15
	then
		mt_test_error=1
	else
		mt_test_error=0
	fi
}

deactivate_mrs()
{
    id=$mrssrvid
	if test $id -ne 0
    then
        kill -2 $id
		mrssrvid=0
    fi
}


deactivate_is()
{
    id=$1
	if test $id -ne 0
    then
        kill -2 $id
    fi
}

deactivate_all_is()
{
    deactivate_is $1
	deactivate_is $2
}

deactivate_ipc()
{
    if test $1 -ne 0
    then
        kill $1
    fi
}

deactivate_2ipc()
{
    	deactivate_ipc $1
	deactivate_ipc $2
	rm -f *backup
	export IPC_REF_FILE="$ipc_ref_file_prev"
}

on_server_fault()
{
		echo "Failed!"
		echo "$1"
		deactivate_all_is $srvid1 $srvid2
		deactivate_2ipc $ipcsrvid $ipcmainid
}

check_server()
{
	count=0
	while test ! -s $s_ref -a ! -s $err_file -a $count -lt 64
	do
	    sleep 1
 		count=`expr $count + 1`
	done
	
	if test -s $err_file
	then
		if test "$1" == "Partition IPC Server is running"
		then
			echo "Partition IPC server's errlog: "
			echo `more $err_file`
		fi
		AlreadyExist=`grep "$3" $err_file`
		if test -n "$AlreadyExist"
		then
			echo "OK, $1"
			already_exists=1
		else
			on_server_fault "$2"
			srv_error=1
		fi
	else
		if test "$1" == "Partition IPC Server is running"
		then
			echo "Partition IPC server's out: "
			echo `more $s_ref`
		fi
		if test ! -s $s_ref
		then
    		on_server_fault "$2"
			srv_error=1
		else
   			echo "OK, $1"
		fi
	fi
}

ddcid=0
deactivate_ddc()
{
    if test $ddcid -ne 0
    then
        kill $ddcid
		sleep 2
    fi
}

deactivate_pvss()
{
	kill_pvss2 >> $1 2>&1
}

on_interrupt()
{
	if test $rcvid -ne 0
	then
		kill -9 $rcvid
		rcvid=0
	fi
	if test $rootid -ne 0
	then 
		kill $rootid
	fi
	deactivate_all_is $srvid1 $srvid2
	deactivate_mrs
	deactivate_2ipc $ipcsrvid $ipcmainid
	deactivate_ddc
	kill_pvss2 > interruption.log 2>&1
	rm -rf $project_dir
	echo 
	echo "************* Test interrupted **************"
	killall -9 lm_ip
	exit 1
}

trap on_interrupt 1 2 3 4 5 6 7 8 10 12 13 14 15

on_ddc_fault()
{
	cp -f $TEST_PROJECT_PATH/log/PVSS_II.log ./PVSS_$2.$CMTCONFIG.log

	echo "Failed"
	echo "$1"
	echo "See PVSS_$2.$CMTCONFIG.log and ddc_$2.ref in working directory for details"
}

exit_on_ddcdt_fault()
{
	on_ddc_fault "$1" "$2"
	deactivate_all_is $srvid1 $srvid2
	killall mrs_server
	deactivate_2ipc $ipcsrvid $ipcmainid
	deactivate_ddc
	deactivate_pvss $pvss_ref_dt
	
	if test -f "$TEST_PROJECT_PATH/log/PVSS_II.log"
	then
		cp -f $TEST_PROJECT_PATH/log/PVSS_II.log ./PVSS_dt.$CMTCONFIG.log
	else
		echo "There is no $TEST_PROJECT_PATH/log/PVSS_II.log"
	fi
	rm -rf $project_dir
	export PVSS_II=$old_pvssII
	killall lm_ip
	exit 1
}

on_pvss_fault()
{
	echo "Failed"
	echo "PVSS has not been started successfully"
	deactivate_mrs
	deactivate_all_is $srvid1 $srvid2
	srvid1=0
	srvid2=0
	srvid3=0
}

echo "####################################################"
echo "DDC test started at `date`"
echo "####################################################"

rm -f *.log *.ref *.err

#############################################
# Build PVSS test application
#############################################
echo
echo "Waiting for building PVSS test application... "

test_err=0

if ! test -f "../autotest/$project.tar.gz"
then 
	echo "Failed"
	echo "NO test project file $project.tar.gz"
	test_err=1
else
	if test -f $project_dir/$project.tar.gz
	then
		rm -f $project_dir/$project.tar.gz
	else
		if ! test -d $project_dir
		then
			mkdir $project_dir
		fi
	fi
	cp ../autotest/$project.tar.gz $project_dir
	
	this_dir=`pwd`
	rm -rf $project_dir/ddc_test $project_dir/$project.tar
	cd $project_dir
	gunzip $project.tar.gz
	if [ $? -ne 0 ]
	then
		echo "Unzipping $project.tar.gz failed"
		test_err=1
	fi
	
	tar -xf $project.tar
	if [ $? -ne 0 ]
	then
		echo "tar -xf $project.tar failed"
		test_err=1
	fi
	cd $this_dir
fi

rm -f $project_dir/$project.tar $project_dir/$project.tar.gz

if test $test_err -ne 0
then
	rm -rf $project_dir
    exit 1
else
	rm -f $project_dir/ddc_test/log/*
# */
	if test -f $project_dir/ddc_test_$CMTCONFIG
	then 
		rm -rf $project_dir/ddc_test_$CMTCONFIG
	fi
	mv $project_dir/ddc_test $project_dir/ddc_test_$CMTCONFIG
	echo "OK, PVSS test application is built in $project_dir"
fi


old_pvssII=`printenv PVSS_II`
export TEST_PROJECT_PATH="$project_dir/ddc_test_$CMTCONFIG"
pvssII="$TEST_PROJECT_PATH/config/config"

#==============================================
# Building the project configuration file

rm -f $pvssII
echo "[general]" > $pvssII
echo "pvss_path = \"$PVSS_PATH\"" >> $pvssII
echo "proj_path = \"$TEST_PROJECT_PATH\"" >> $pvssII
echo "proj_version = \"3.0\"" >> $pvssII
echo "langs = \"en_US.iso88591\"" >> $pvssII
echo "connectDelay = 2" >> $pvssII
echo "projectKey = 1" >> $pvssII
echo "userName = \"root\"" >> $pvssII
echo "password = \"\"" >> $pvssII
echo "[data]" >> $pvssII
echo "dataBgName = \"\"" >> $pvssII

echo "PVSS config file built"

export PVSS_II="$pvssII"

killall -9 PVSS00pmon
#==============================================

#############################################
# Start IPC servers
#############################################
echo
echo "Waiting for IPC server to start up... "

ipc_ref_file_prev=$IPC_REF_FILE
this_directory=`pwd`
export IPC_REF_FILE="$this_directory/ipc_root.ref"

rm -f $s_ref $err_file
ipc_server > $s_ref 2>$err_file&
ipcmainid=$!
	
check_server "General IPC Server is running" "General IPC Server was not started" "Reason: Already"
if test $already_exists -ne 0
then
	already_exists=0
	ipcmainid=0
else
	if test $srv_error -ne 0
	then
		rm -rf $project_dir
		export PVSS_II=$old_pvssII
		exit 1
	fi
fi

rm -f $s_ref $err_file
$partition > $s_ref 2> $err_file&
ipcsrvid=$!

srv_error=0
already_exists=0
check_server "Partition IPC Server is running" "IPC Server was not started" "Reason: Already"
if test $already_exists -ne 0
then
	already_exists=0
	ipcsrvid=0
else
	if test $srv_error -ne 0
	then
		deactivate_ipc $ipcmainid
		rm -rf $project_dir
		export PVSS_II=$old_pvssII
		exit 1
	fi
fi

rdb_server -p $TDAQ_PARTITION -d ddcDb -D $TDAQ_DB_DATA &
rdbid=$!

echo
echo "TDAQ_DB_DATA=$TDAQ_DB_DATA"
echo

#=======================================================================
# Building the API manager configuration file to address to PVSS project

rm -f pvss.config
echo "[general]" > pvss.config
echo "pvss_path = \"$PVSS_PATH\"" >> pvss.config
echo "proj_version = \"3.0\"" >> pvss.config
echo "langs = \"en_US.iso88591\"" >> pvss.config
echo "connectDelay = 3" >> pvss.config
echo "connectRetries = 20" >> pvss.config
echo "projectKey = 11" >> pvss.config
		
PVSS_PROGS="$TEST_PROJECT_PATH/config/progs"
export PVSS_PROGS

####################################################################################
#############################################
# Performing test of DDC message transfer	#
#############################################
#############################################

echo
echo "`date`"
echo "Testing DDC-MT"
echo "--------------"

#############################################
# Start MRS server
#############################################

echo
echo "Waiting for MRS server to start up... "

rm -f $mrs_ref $mrs_err
$mrs_srv > $mrs_ref 2> $mrs_err &
mrssrvid=$!

srv_error=0
count=0
sleep 2
while test ! -s $mrs_ref -a ! -s $mrs_err -a $count -lt 128
do
	sleep 1
 	count=`expr $count + 1`
done
	
if test -s $mrs_ref
then
	MrsStarted=`grep "MRS server started" $mrs_ref`
	count=0
	while test -z "$MrsStarted" -a $count -lt 64
	do
		sleep 1
		MrsStarted=`grep "MRS server started" $mrs_ref`
		count=`expr $count + 1`
	done
	if test $count -lt 64
	then
		echo "OK, MRS server is running"
	else
		if test -s $mrs_err
		then
			AlreadyExists=`grep "MRS server already" $mrs_err`
			if test -n "$AlreadyExists"
			then
				echo "OK, MRS server is running"
				mrssrvid=0
			else
				mt_test_error=1
			fi
		else
			mt_test_error=1
		fi
		if test $mt_test_error -ne 0
		then
			echo "Failed"
			echo "MRS server has not started properly"
			echo "Look into log files:"
			cat $mrs_ref $mrs_err
		fi
	fi
else
	echo "Failed"
	echo "MRS server has not started"
	mt_test_error=1
fi

if test $mt_test_error -eq 0
then
#################################################
#	 Start test receiver application
#################################################
	echo
	echo "Waiting for test application to start up... "
	rm -f $msg_err

	$test_receiver > $msg_err 2>&1 &
	rcvid=$!

	count=0
	while test ! -s $msg_err -a $count -lt 15
	do
		sleep 1
   		count=`expr $count + 1`
	done

	if test ! -s $msg_err
	then
		mt_test_error=1
	else
  		MtResponse=`grep "is subscribed for DCS messages" $msg_err`
		count=0
		while test -z "$MtResponse" -a $count -lt 16
		do
			sleep 1
			count=`expr $count + 1`
			MtResponse=`grep "is subscribed for DCS messages" $msg_err`
		done
		
		if test $count -lt 16
		then
			echo "OK, Test receiver has started"
		else
			mt_test_error=1
		fi
	fi

	if test $mt_test_error -ne 0
	then
   		echo "Failed!"
    	echo "Test message receiver could not start properly"
	fi
	
	if test $mt_test_error -eq 0
	then
#############################################
# 	 	Start DDC-MT
#############################################
		echo
		echo "Waiting for DDC-MT to start up... "
		rm -f $ddcmt_ref

		# Copying the DDC-MT configuration file
		#cp -f ../data/ddc_mt.config .		# Commented 04.11.03
	
		#==============================================

		killall -9 ddc_mt
		$ddcmt > $ddcmt_ref 2>&1 &
		ddcid=$!

		count=0
		while test ! -s $ddcmt_ref -a $count -lt 64
		do
 	 		sleep 1
  	 		count=`expr $count + 1`
		done
		if test ! -s $ddcmt_ref
		then
 	 	  	on_ddc_fault "DDC-MT has not been started successfully" "mt"
			mt_test_error=1
		else
			DDCStarted=`grep "API manager constructed" $ddcmt_ref`
 	  		count=0
			while test $count -lt 16 -a ! -n "$DDCStarted"
			do
				sleep 1
				count=`expr $count + 1`
				DDCStarted=`grep "API manager constructed" $ddcmt_ref`
			done
			if test -n "$DDCStarted"
			then
				DDCExited=`grep "Exiting" $ddcmt_ref`
				if test -n "$DDCExited"
				then
					on_ddc_fault "DDC-MT has not been started successfully (just exited)" "mt"
					mt_test_error=1
				else
					echo "OK, DDC-MT is started"
				fi
			else
				on_ddc_fault "DDC-MT has not been started successfully (no Manager construction)" "mt"
				mt_test_error=1
			fi
		fi
		
		if test $mt_test_error -eq 0
		then
#################################################
#	 		Start PVSS test application
#################################################
			echo
			echo "Waiting for PVSS to start up... "
				
			# Building the 'progs' file for starting PVSS v 3.0
				
			rm -f $PVSS_PROGS
			
			echo "version 1" > $PVSS_PROGS
			echo "" >> $PVSS_PROGS
			echo "auth \"\" \"\"" >> $PVSS_PROGS
			echo "#Manager         | Start  | SecKill | Restart# | ResetMin | Options" >> $PVSS_PROGS
			echo "PVSS00pmon       | manual |      30 |        3 |        1 |" >> $PVSS_PROGS
			echo "PVSS00data       | always |      30 |        3 |        1 |" >> $PVSS_PROGS
			echo "PVSS00valarch    | always |      30 |        3 |        1 |-num 0" >> $PVSS_PROGS
			echo "PVSS00event      | always |      30 |        3 |        1 |" >> $PVSS_PROGS
			echo "PVSS00sim        | always |      30 |        3 |        1 |" >> $PVSS_PROGS
			echo "PVSS00ctrl       | always |      30 |        3 |        1 |-f ddc_test_mt.lst -log +stderr" >> $PVSS_PROGS
			
			
			$pvss > $pvss_ref_mt 2>&1

			count=0
			pvssManagers=`ps`
			echo "$pvssManagers" > processes.lst
			pvssEvent=`grep "PVSS00event" processes.lst`
			pvssData=`grep "PVSS00data" processes.lst`
			pvssCtrl=`grep "PVSS00ctrl" processes.lst`
			
			while test ! -n "$pvssEvent" -o ! -n "$pvssData" -o ! -n "$pvssCtrl"
			do
				if test $count -lt 64
				then
			 		sleep 1
					count=`expr $count + 1`
					pvssManagers=`ps`
					echo "$pvssManagers" > processes.lst
					pvssEvent=`grep "PVSS00event" processes.lst`
					pvssData=`grep "PVSS00data" processes.lst`
					pvssCtrl=`grep "PVSS00ctrl" processes.lst`
				else
					break
				fi
			done
			
			if test  $count -lt 64
			then
				echo "OK, PVSS application started"
				echo "Starting PVSS took $count sec." 
				rm -f processes.lst
			else
				mt_test_error=1
				echo "PVSS has not started properly"
				more processes.lst
			fi
			
			if test $mt_test_error -eq 0
			then
#################################################
#	 			Check connection to PVSS
#################################################
				echo
				echo "Waiting for PVSS to be connected... "
				
				count=0
				connected=0
				while test $connected -eq 0 -a $count -lt 100
				do
					PVSSConnected=`grep "Connection to PVSS application" $ddcmt_ref`
					PVSSConnected_alt=`grep "Connected to, (SYS: 1 Api" $TEST_PROJECT_PATH/log/PVSS_II.log`
					if test -n "$PVSSConnected" -o -n "$PVSSConnected_alt"
					then
						connected=1
						echo "OK, PVSS is connected"
					else
						sleep 1
						count=`expr $count + 1`
					fi
				done
				if test $connected -eq 0
				then
					echo
					on_ddc_fault "No connection to PVSS" "mt"
					mt_test_error=1
				else
#################################################
#	 				Check receiving messages
#################################################
					echo
					echo "Waiting for receiving test messages..."
					
					check_message "$TestMsg1"
					if test $mt_test_error -ne 0
					then
						mt_test_error=0
						check_message "$TestMsg1a"
					fi
					if test $mt_test_error -eq 0
					then
						check_message "$TestMsg2"
						if test $mt_test_error -ne 0
						then
							check_message "$TestMsg2a"
						fi
					else
						echo "There is neither Msg 1 nor Msg 1a"
					fi
					if test $mt_test_error -eq 0
					then
						check_message "$TestMsg3"
						if test $mt_test_error -eq 0
						then
							check_message "$TestMsg4"
							if test $mt_test_error -eq 0
							then
								echo "OK, DDC-MT has worked properly"
								echo "=============================="
							else
								echo "There is no Msg 4"
							fi
						else
							echo "There is no Msg 3"
						fi
					else
						echo "There is neither Msg 2 nor Msg 2a"
					fi
				fi	
			else
				 echo "PVSS project has NOT started successfully"
			fi
		fi
	fi
fi

if test -f "$TEST_PROJECT_PATH/log/PVSS_II.log"
then
	cp -f $TEST_PROJECT_PATH/log/PVSS_II.log ./PVSS_mt.$CMTCONFIG.log
else
	echo "There is no $TEST_PROJECT_PATH/log/PVSS_II.log"
fi
		
if test $mt_test_error -ne 0
then
	echo 
	echo "DDC-MT test was unsuccessful"
	echo "============================"
fi

echo 
echo "Deactivating DDC-MT..."
deactivate_ddc
echo "Deactivating PVSS..."
deactivate_pvss $pvss_ref_mt
if test $rcvid -ne 0
then
	kill -9 $rcvid
	rcvid=0
fi
sleep 5

####################################################################################
#############################################
# Performing test of DDC command transfer	#
#############################################
#############################################

echo
echo "`date`"
echo "Testing DDC-CT"
echo "--------------"

#############################################
# Start PVSS test application
#############################################
echo
echo "Waiting for PVSS to start up... "
	
# Building the progs file for starting PVSS

rm -f $PVSS_PROGS
			
echo "version 1" > $PVSS_PROGS
echo "" >> $PVSS_PROGS
echo "auth \"\" \"\"" >> $PVSS_PROGS
echo "PVSS00pmon       | manual |      30 |        3 |        1 |" >> $PVSS_PROGS
echo "PVSS00data       | always |      30 |        3 |        1 |" >> $PVSS_PROGS
echo "PVSS00valarch    | always |      30 |        3 |        1 |-num 0" >> $PVSS_PROGS
echo "PVSS00event      | always |      30 |        3 |        1 |" >> $PVSS_PROGS
echo "PVSS00sim        | always |      30 |        3 |        1 |" >> $PVSS_PROGS
echo "PVSS00ctrl       | always |      30 |        3 |        1 |-f ddc_test_dt.lst" >> $PVSS_PROGS
			
test_err=0
$pvss > $pvss_ref_dt 2>&1

count=0
pvssManagers=`ps`
echo "$pvssManagers" > processes.lst
pvssEvent=`grep "PVSS00event" processes.lst`
pvssData=`grep "PVSS00data" processes.lst`
pvssCtrl=`grep "PVSS00ctrl" processes.lst`

while test ! -n "$pvssEvent" -o ! -n "$pvssData" -o ! -n "$pvssCtrl"
do
	if test $count -lt 64
	then
		sleep 1
		count=`expr $count + 1`
		pvssManagers=`ps`
		echo "$pvssManagers" > processes.lst
		pvssEvent=`grep "PVSS00event" processes.lst`
		pvssData=`grep "PVSS00data" processes.lst`
		pvssCtrl=`grep "PVSS00ctrl" processes.lst`
	else
		break
	fi
done

if test  $count -lt 64
then
	echo "OK, PVSS application started"
	echo "Starting PVSS took $count sec." 
	rm -f processes.lst
else
	test_err=1
	echo "PVSS has not started properly"
	more processes.lst
fi
			
if test $test_err -ne 0
then
	on_pvss_fault
	deactivate_2ipc $ipcsrvid $ipcmainid

	if test -f "$TEST_PROJECT_PATH/log/PVSS_II.log"
	then
		cp -f $TEST_PROJECT_PATH/log/PVSS_II.log ./PVSS_ct.$CMTCONFIG.log
	fi
	rm -rf $project_dir
	deactivate_pvss $pvss_ref_dt
	export PVSS_II=$old_pvssII
	killall lm_ip
	exit 1
fi

#############################################
# 		Starting RunCtrl IS server
#############################################

echo
echo "Waiting for RunCtrl IS server to start up... "

rm -f $s_ref $err_file
$runCtrl > $s_ref 2> $err_file&
srvid1=$!

srv_error=0
check_server "$runCtrl is running" "$runCtrl was not started" "already running"
if test $already_exists -ne 0
then
	already_exists=0
	srvid2=0
fi

if test $srv_error -ne 0
then
	deactivate_all_is $srvid1 $srvid2
	deactivate_2ipc $ipcsrvid $ipcmainid
	deactivate_pvss $pvss_ref_dt
	rm -rf $project_dir
	export PVSS_II=$old_pvssII
	killall lm_ip
	exit 1
fi


ready="NO"
if test $ct_test_error -eq 0
then
#################################################
	# Start DDC-CT
	#############################################
	echo
	echo "Waiting for DDC-CT to start up... "
	rm -f $ddcct_ref
	rm -rf /tmp/ddcct*
	
	killall -9 ddc_ct
	
	$ddcct > $ddcct_ref 2>&1 &
	ddcid=$!
	
	count=0
	rm -f ct_test.log
	echo "Starting ddc_ct" > ct_test.log
	while test "$ready" != "YES" -a $count -lt 32
	do
		rc_isread -p $TDAQ_PARTITION -n ddcct1 >> ct_test.log 2>&1
		echo "Count = \"$count\"" >> ct_test.log
		LifeState=`grep "Ok" ct_test.log`
		ControllerState=`grep "INITIAL" ct_test.log`

		if test -n "$LifeState" -a -n "$ControllerState"
		then
			ready="YES"
		else
			sleep 1
			count=`expr $count + 1`
		fi
	done
	
	if test "$ready" != "YES"
	then
 	 	  on_ddc_fault "DDC-CT has not been started successfully" "ct"
		  ct_test_error=1
	else
		echo "OK, DDC-CT started"
	fi
fi

#if test $ct_test_error -eq 0
#then
#################################################
	#############################################
	# Start Root controller
	#############################################
	
#	rc_empty_controller -p $TDAQ_PARTITION -n RootController -P "" -s setup -v 2 -R :Root --rdb > rootCtrl.log 2>&1 &
#	rootid=$!
#	
#	count=0
#	booted="NO"
#	echo
#	echo "Waiting for root controller started"
#	ready="NO"
#
#	while test "$booted" != "YES" -a $count -lt 32
#	do
#		rc_isread -p $TDAQ_PARTITION -s RunCtrl -n RootController > rootCtrl_test.log 2>&1
#		echo "Count = \"$count\"" >> rootCtrl_test.log
#		LifeState=`grep "Ok" rootCtrl_test.log`
#		if test -n "$LifeState"
#		then
#			booted="YES"
#		else
#			sleep 1
#			count=`expr $count + 1`
#		fi
#	done
#	
#	if test "$booted" != "YES"
#	then
#		echo "Failed"
#		echo "Boot for Root has not been successful"
#		echo "See rootCtrl.log and rootCtrl_test.log in working directory for details"
#		killall -9 ddc_ct
#		ct_test_error=1
#	else
#		rm -f sendCommand.log 
#		rc_sendcommand -p $TDAQ_PARTITION -n RootController boot >> sendCommand.log 2>&1
#	fi
#
#	if test $ct_test_error -eq 0
#	then
#		count=0
#		while test "$ready" != "YES" -a $count -lt 32
#		do
#			rc_isread -p $TDAQ_PARTITION -s RunCtrl -n RootController > rootCtrl_test.log 2>&1
#			echo "Count = \"$count\"" >> rootCtrl_test.log
#			LifeState=`grep "Ok" rootCtrl_test.log`
#
#			ControllerState=`grep "INITIAL" rootCtrl_test.log`
#			if test -n "$LifeState" -a -n "$ControllerState"
#			then
#				ready="YES"
#			else
#				sleep 1
#				count=`expr $count + 1`
#			fi
#		done
#	fi
#		
#	if test "$ready" != "YES"
#	then
#		echo "Failed"
#		echo "Root controller has not been started"
#		echo "See rootCtrl.log and rootCtrl_test.log in working directory for details"
#		killall -9 ddc_ct
#		ct_test_error=1
#	else
#		echo "OK, Root controller started"
#	fi
#fi
	
if test $ct_test_error -eq 0
then
#################################################
	#############################################
	# Testing the ddc_ct controller
	#############################################
	
	echo
	echo "Testing the ddc_ct controller (`date`)"
	comm_result=""

	rc_sendcommand -p $TDAQ_PARTITION -n ddcct1 load > sendCommand.log 2>&1

	count=0
	echo "Loading ddc_ct configuration" >> ct_test.log
	while test "$comm_result" != "OK" -a $count -lt 32
	do
#		rc_isread -p $TDAQ_PARTITION -s RunCtrl -n RootController >> rootCtrl_test.log 2>&1
		rc_isread -p $TDAQ_PARTITION -s RunCtrl -n ddcct1 >> ct_test.log 2>&1
		echo "Count = \"$count\"" >> ct_test.log
		LifeState=`grep "Ok" ct_test.log`
		ControllerState=`grep "LOADED" ct_test.log`
		if test -n "$LifeState" -a -n "$ControllerState"
		then
			comm_result="OK"
		else
			sleep 1
			count=`expr $count + 1`
		fi
	done
	if test "$comm_result" != "OK"
	then
		ct_test_error=1
	fi
fi
	
if test $ct_test_error -ne 0
then
	cp -f $TEST_PROJECT_PATH/log/PVSS_II.log ./PVSS_ct.$CMTCONFIG.log
	echo 
	echo "DDC-CT has not worked properly"
	test_err=1
else
	echo "OK, DDC-CT has worked properly"
fi
	echo "=============================="

#rm -f $TEST_PROJECT_PATH/log/PVSS_II.log

	
#############################################
# Wait for deactivation of DDC-CT processes
#############################################
echo
echo "Deactivating DDC-CT and root controller..."
if test $rootid -ne 0
then 
	kill $rootid
fi

rc_sendcommand -p $TDAQ_PARTITION -n ddcct1 quit > sendCommand.log 2>&1
echo "Command FINAL is sent"
sleep 2

ddcid=0
	
	
####################################################################################
#############################################
# Performing test of DDC data transfer		#
#############################################
#############################################

echo
echo "`date`"
echo "Testing DDC-DT"
echo "--------------"

#############################################
# Start IS servers
#############################################

echo
echo "Waiting for RunParams server to start up... "

rm -f $s_ref $err_file
$runParams > $s_ref 2> $err_file&
srvid2=$!

srv_error=0
check_server "$runParams is running" "$runParams was not started" "already running"
if test $already_exists -ne 0
then
	already_exists=0
	srvid2=0
fi

if test $srv_error -ne 0
then
	deactivate_all_is $srvid1 $srvid2
	deactivate_2ipc $ipcsrvid $ipcmainid
	rm -rf $project_dir
	export PVSS_II=$old_pvssII
	killall lm_ip
	exit 1
fi


#############################################
# Start DDC-DT
#############################################
echo
echo "Waiting for DDC-DT to start up... "
rm -f $ddcdt_ref

# Copying the DDC-DT configuration file
#cp ../data/ddc_dt.config .			# Commented 04.11.03

#==============================================

killall -9 ddc_dt
#memcheck $ddcdt > $ddcdt_ref 2>&1 &
$ddcdt > $ddcdt_ref 2>&1 &
ddcid=$!

count=0
while test ! -s $ddcdt_ref -a $count -lt 64
do
    sleep 1
    count=`expr $count + 1`
done
if test ! -s $ddcdt_ref
then
    exit_on_ddcdt_fault "DDC-DT has not been started successfully" "dt"
else
	DDCStarted=`grep "API manager constructed" $ddcdt_ref`
 	count=0
	while test $count -lt 16 -a ! -n "$DDCStarted"
	do
		sleep 1
		count=`expr $count + 1`
		DDCStarted=`grep "API manager constructed" $ddcdt_ref`
	done
	if test -n "$DDCStarted"
	then
		DDCExited=`grep "Exiting" $ddcdt_ref`
		if test -n "$DDCExited"
		then
			exit_on_ddcdt_fault "DDC-MT has not been started successfully (just exited)" "dt"
		else
			echo "OK, DDC-DT is started"
			count=0
			connected=0
			echo "Waiting for PVSS to be connected"
			while test $connected -eq 0 -a $count -lt 120
			do
				PVSSConnected=`grep "Connection to PVSS application" $ddcdt_ref`
				PVSSConnected_alt=`grep "Connected to, (SYS: 1 Api -num 2" $TEST_PROJECT_PATH/log/PVSS_II.log`
				if test -n "$PVSSConnected" -o -n "$PVSSConnected_alt"
				then
					connected=1
					echo "OK, PVSS is connected"
				else
					sleep 1
					count=`expr $count + 1`
				fi
			done
			if test $connected -eq 0
			then
				echo
				killall -9 ddc_dt
				ddcid=0
				echo "DDC-DT killed due to no connection to PVSS" >> $ddcdt_ref
				exit_on_ddcdt_fault "No connection to PVSS" "dt"
			fi	
		fi
	else
		exit_on_ddcdt_fault "DDC-DT has not been started successfully (no Manager construction)" "dt"
	fi
fi

#############################################
# Start DAQ simulation
#############################################

echo
echo "Starting up the test application"
echo
rm -f $daqsim_err
cp -f ../data/ddc_daqsim.config .

$daqsim > $daqsim_err 2>&1

test_err=0
if test ! -s $daqsim_err
then
    echo "Failed!"
    echo "DAQ simulation could not be performed"
	test_err=1
else
    DAQResult=`grep "SUCCESS" $daqsim_err`
	if ! test -n "$DAQResult"
	then
		echo "Failed!"
		echo "DDC-DT test has not finished properly"
		test_err=1
	else
		echo "OK, DDC-DT has worked properly"
		echo "=============================="
	fi
fi

#############################################
# Wait for deactivation of all processes
#############################################
echo
echo "Deactivating DDC-DT..."
deactivate_ddc
echo "Deactivating MRS..."
deactivate_mrs
echo "Deactivating IS and partition..."
deactivate_all_is $srvid1 $srvid2
if test $rdbid -ne 0
then
	kill $rdbid
fi
deactivate_2ipc $ipcsrvid $ipcmainid
echo "Deactivating PVSS..."
deactivate_pvss $pvss_ref_dt
export PVSS_II=$old_pvssII

echo "Removing test project"
cp -f $TEST_PROJECT_PATH/log/PVSS_II.log ./PVSS_dt.$CMTCONFIG.log
rm -rf $project_dir

if test $test_err -eq 0
then
	rm $s_ref
#	rm -f $err_file
fi

killall lm_ip
echo
if test $mt_test_error -eq 0 -a $ct_test_error -eq 0 -a $test_err -eq 0
then
	echo "OK! DDC checking is completed at `date`"
	echo "#########################################################"
else
	echo "DDC check finished with error(s) at `date`"
	echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	exit 1
fi

