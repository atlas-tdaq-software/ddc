#ifndef _DDC_DAL_H_
#define _DDC_DAL_H_

///////////////////////////////////////////////////////////////////////////
// This file contains definition of objects and methods to access
// configuration database object specific for DDC configuration
//					
//									Created 19.02.03
//									by S.Khomutnikov
// Modifications:
//		S.Kh.	17.10.03	Hande-made dal methods removed: generated methods
//							are to be used instead
//		S.Kh.	30.10.06	Since now this is only the collection of 
//							generated headers for DAL.
//							DdcDal.cxx does not exist any more. 	
///////////////////////////////////////////////////////////////////////////

// Generated headers for ddc-dal
#include <ddc/DdcTargetList.h>
#include <ddc/DdcDataTargetList.h>
#include <ddc/DdcTextMessage.h>
#include <ddc/DdcCommandDef.h>
#include <ddc/PvssWorkstation.h>
#include <ddc/DdcDtApplication.h>
#include <ddc/DdcMtApplication.h>
#include <ddc/DdcCtApplication.h>
#include <ddc/PvssDatapoint.h>

#endif
