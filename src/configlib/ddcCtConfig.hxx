#ifndef _DDC_CT_EXTERNHDL_H_
#define _DDC_CT_EXTERNHDL_H_

#include <BaseExternHdl.hxx>

class ddccCtExternHdl : public BaseExternHdl
{
public:
    // List of user defined functions
    // Internal use only
    enum 
    {
		F_ddccGetDdcCt = 0,
		F_ddccCreateDdcCt,
		F_ddccReplaceDdcCt,
		F_ddccGetDdcCtAttr,
		F_ddccSetDdcCtAttr,
		F_ddccGetDdcCommands,
		F_ddccGetDcsAlarmDp,
		F_ddccNontransCmdToTrans,
		F_ddccTransCmdToNontrans,
		F_ddccGetCommandAttr,
		F_ddccSetCommandAttr,
		F_ddccCreateDdcCommand,
		F_ddccDeleteDdcCommandDef,
		F_ddccAddTransitionCommand,
		F_ddccAssignDcsAlarm,
    };

    // Descritption of user defined functions. 
    // Used by libCtrl to identify function calls
    static FunctionListRec  fnList[];

    ddccCtExternHdl(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : BaseExternHdl(nextHdl, funcCount, fnList) {}
    
    // Execute user defined function
    virtual const Variable *execute(ExecuteParamRec &param);

private:
	void 	showError(ExecuteParamRec &param, std::string& function);
	
	bool	getDdcCt(Configuration *, const daq::core::Segment *, daq::ddc::DdcCtApplication**, std::string& name, 
							std::string& pvssName, std::string& errMsg);
	bool	createDdcCt(Configuration *, daq::core::Segment *, const daq::ddc::PvssWorkstation *, std::string name, 
							daq::ddc::DdcCtApplication**, std::string& errMsg);
	bool	replaceDdcCt(Configuration *, daq::core::Segment *, daq::ddc::DdcCtApplication**, std::string name, std::string& errMsg);
	bool	getDdcCtAttr(daq::ddc::DdcCtApplication*, std::vector<std::string>& names, std::string& errMsg);
	bool	setDdcCtAttr(daq::ddc::DdcCtApplication*, std::string param, std::string& errMsg);
	bool	getDdcCommands(Configuration *, daq::ddc::DdcCtApplication*, std::vector<std::string>& names, 
							std::vector<std::string>& targets, std::string& errMsg);
	bool	getDcsAlarmDp(daq::ddc::DdcCtApplication*, std::string& name, std::string& errMsg);
	bool	nontransCmdToTrans(Configuration *, daq::ddc::DdcCtApplication*, std::string name, 
								std::string fName, std::string& errMsg);
	bool	transCmdToNontrans(daq::ddc::DdcCtApplication*, std::string name, std::string& errMsg);
	bool	getCommandAttr(Configuration *, std::string name, std::vector<std::string>& names, std::string& errMsg);
	bool	setCommandAttr(Configuration *, std::string name, std::vector<std::string> names, std::string& errMsg);
	bool	createDdcCommand(Configuration *, daq::core::Segment *, std::string name, 
								daq::ddc::DdcCommandDef**, std::string& errMsg);
	bool	deleteDdcCommandDef(Configuration *, daq::ddc::DdcCtApplication*, std::string name, 
								bool transCmdFlag, std::string& errMsg);
	bool	addTransitionCommand(daq::ddc::DdcCtApplication*, daq::ddc::DdcCommandDef*, std::string& errMsg);
	bool	assignDcsAlarm(Configuration *, daq::ddc::DdcCtApplication*, std::string name, std::string poll, std::string& errMsg);
};


// Create new ExternHdl. This must be global function named newExternHdl
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl);

#endif 
