// Ctrl extension for DDC configuration

#include <stdio.h>
#include <CharString.hxx>
#include <BlobVar.hxx>
#include <CtrlExpr.hxx>
#include <ExprList.hxx>
#include <CtrlThread.hxx>
#include <IdExpr.hxx>
#include <Variable.hxx>
#include <AnyTypeVar.hxx>
#include <BitVar.hxx>
#include <CharVar.hxx>
#include <IntegerVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>
#include <TextVar.hxx>
#include <TimeVar.hxx>
#include <DynVar.hxx>
#include <ErrorVar.hxx>
#include <ExternData.hxx>
#include <ErrClass.hxx>
#include <BCTime.h>

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/Segment.h>
#include <dal/RunControlApplication.h>
#include <dal/Binary.h>
#include <dal/Tag.h>

using namespace std;

#include <ddc/PvssWorkstation.h>
#include <ddc/DdcCtApplication.h>
#include <ddc/DdcDtApplication.h>
#include <ddc/DdcMtApplication.h>
#include <ddc/PvssDatapoint.h>
#include <ddc/DdcCommandDef.h>

#include "ddcConfig-Defs.hxx"
#include "ddcCtConfig.hxx"


// Function description
FunctionListRec ddccCtExternHdl::fnList[] = 
{
  //  Return-Value    function name        parameter list               true == thread-save
  //  -------------------------------------------------------------------------------------
	{BIT_VAR, "ddccGetDdcCt", "(int conf, int segmentRef, int& ctRef, string& name, string& pvssName, string& errMsg)", true},
	{BIT_VAR, "ddccCreateDdcCt", "(int conf, int segmentRef, int pvssWsRef, string name, int& ctRef, string& errMsg)", true},
	{BIT_VAR, "ddccReplaceDdcCt", "(int conf, int segmentRef, int& ctRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccGetDdcCtAttr", "(int ctRef, vector<string>& attrs, string& errMsg)", true},
	{BIT_VAR, "ddccSetDdcCtAttr", "(int ctRef, string params, string& errMsg)", true},
	{BIT_VAR, "ddccGetDdcCommands", "(int conf, int ctRef, vector<string>& com, vector<string>& ntcom, string& errMsg)", true},
	{BIT_VAR, "ddccGetDcsAlarmDp", "(int ctRef, string& name, string& errMsg)", true},
	{BIT_VAR, "ddccNontransCmdToTrans", "(int conf, int ctRef, string name, string trans, string& errMsg)", true},
	{BIT_VAR, "ddccTransCmdToNontrans", "(int ctRef, string name, string errMsg)", true},
	{BIT_VAR, "ddccGetCommandAttr", "(int conf, string name, vector<string>& attr, string& errMsg)", true},
	{BIT_VAR, "ddccSetCommandAttr", "(int conf, string name, vector<string> attr, string& errMsg)", true},
	{BIT_VAR, "ddccCreateDdcCommand", "(int conf, int segmentRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccDeleteDdcCommandDef", "(int conf, int ctRef, string name, bool transition, string& errMsg)", true},
	{BIT_VAR, "ddccAddTransitionCommand", "(int ctRef, int cmdRef, string& errMsg)", true},
	{BIT_VAR, "ddccAssignDcsAlarm", "(int conf, int ctRef, string name, string& errMsg)", true}
};


BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
  // Calculate number of functions defined
  PVSSulong funcCount = sizeof(ddccCtExternHdl::fnList)/sizeof(ddccCtExternHdl::fnList[0]);  
  
  // now allocate the new instance
  ddccCtExternHdl *hdl = new ddccCtExternHdl(nextHdl, funcCount, ddccCtExternHdl::fnList);

  return hdl;
}

void ddccCtExternHdl::showError(ExecuteParamRec &param, std::string& function)
{
	ErrClass  errClass(
			ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, ErrClass::ARG_MISSING,
			"ddccCtExternHdl", function.c_str(), "missing or wrong argument data");

	// Remember error message. 
	// In the Ctrl script use getLastError to retreive the error message
	param.thread->appendLastError(&errClass);
	return;
}

//------------------------------------------------------------------------------
// This function is called every time we use ddccXXX in a Ctrl script.
// The argument is an aggregaton of function name, function number, 
// the arguments passed to the Ctrl function, the "thread" context 
// and user defined data.
const Variable *ddccCtExternHdl::execute(ExecuteParamRec &param)
{
  // We return a pointer to a static variable. 
  static IntegerVar integerVar;
  static BitVar 	bitError;
	bool 	err;
	std::string		function;
	const std::string wordF("Function ");
  // A pointer to one "argument". Any input argument may be an exprssion
  // like "a ? x : y" instead of a simple value.
  CtrlExpr   *expr;
	
	const Variable*		objRef;
	const Variable* 	errMsg;
	const Variable*		dynStringRef;
	const Variable*		dynStringRef2;

	const Variable*		dbPtrVar;
	const Variable*		objNameVar;
	const Variable*		objNameVar2;
	
	Configuration*		myDb;
	daq::core::Segment*		mySegment;
	daq::ddc::PvssWorkstation*	myPvssWs;
	daq::ddc::DdcCtApplication*	myDdcCt;
	daq::ddc::DdcCommandDef*		myCmd;
	
	std::string			msg;
	std::string			dbName;
	std::string			objName;
	std::string			fName;
	std::vector<std::string>	names;
	std::vector<std::string>	targets;
	bool				yesNo;
	unsigned int	vectorLen;
	
  // switch / case on function numbers
  switch (param.funcNum)
  {
	case F_ddccGetDdcCt:
	{	function = "GetDdcCt";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// reference to DdcCt
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for DdcCt name
		 || !(objNameVar = expr->getTarget(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for pvssWs name
		 || !(objNameVar2 = expr->getTarget(param.thread))
		 ||	(objNameVar2->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDdcCt(myDb, mySegment, &myDdcCt, objName, fName, msg);
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDdcCt);
			((TextVar *)objNameVar)->setValue(objName.c_str());
			((TextVar *)objNameVar2)->setValue(fName.c_str());
		}
		else
			((TextVar *)objNameVar)->setValue("");
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateDdcCt:
	{	function = "CreateDdcCt";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssWs = (daq::ddc::PvssWorkstation *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to DdcCt
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createDdcCt(myDb, mySegment, myPvssWs, objName, &myDdcCt, msg);
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDdcCt);
			cerr << "DdcCt obj created for " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccReplaceDdcCt:
	{	function = "ReplaceDdcCt";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// reference to DdcCt
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)expr->evaluate(param.thread))->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = replaceDdcCt(myDb, mySegment, &myDdcCt, objName, msg);
		if(!err) {
			cerr << "DdcCt replaced for " << objName << endl;
			((IntegerVar *)objRef)->setValue((int)myDdcCt);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDdcCtAttr:
	{	function = "GetDdcCtAttr";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());
		cerr << "In getAttr " << (int)myDdcCt << endl;

		if (!param.args || !(expr = param.args->getNext())		// refernce for data target attrs
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDdcCtAttr(myDdcCt, names, msg);
		if(!err) {
			cerr << "Attributes retrieved for " << myDdcCt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetDdcCtAttr:
	{	function = "SetDdcCtAttr";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();


		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setDdcCtAttr(myDdcCt, objName, msg);
		if(!err) {
			cerr << "Attributes set for " << myDdcCt->get_Name() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDdcCommands:
	{	function = "GetDdcCommands";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for transition command list
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for non-transition command list
		 || !(dynStringRef2 = expr->getTarget(param.thread))
		 ||	(dynStringRef2->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		names.erase(names.begin(), names.end());
		targets.erase(targets.begin(), targets.end());
		
		err = getDdcCommands(myDb, myDdcCt, names, targets, msg);
		unsigned int 	i;
		unsigned int 	len = names.size();
		TextVar			toJoin;
		if(!err) {
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
			len = targets.size();
			for(i=0; i<len; i++) {
				toJoin.setValue(targets[i].c_str());
				((DynVar *)dynStringRef2)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDcsAlarmDp:
	{	function = "GetDcsAlarmDp";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for DP name
		 || !(objNameVar = expr->getTarget(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDcsAlarmDp(myDdcCt, fName, msg);
		if(!err) {
			cerr << "Alarm DP retrieved for " << myDdcCt->get_Name() << endl;
			((TextVar *)objNameVar)->setValue(fName.c_str());
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccNontransCmdToTrans:
	{	function = "NontransCmdToTrans";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// command ID
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// target transition
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = nontransCmdToTrans(myDb, myDdcCt, objName, fName, msg);
		if(!err) {
			cerr << objName << " moved for transition " << fName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccTransCmdToNontrans:
	{	function = "TransCmdToNontrans";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// command ID
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = transCmdToNontrans(myDdcCt, objName, msg);
		if(!err) {
			cerr << "Moved to Non-transition: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetCommandAttr:
	{	function = "GetCommandAttr";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// command ID
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for attribute list
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getCommandAttr(myDb, objName, names, msg);
		if(!err) {
			cerr << "Attributes read for " << objName << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetCommandAttr:
	{	function = "SetCommandAttr";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for Db
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// command ID
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// array of attributes
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)objNameVar)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)objNameVar)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)objNameVar)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setCommandAttr(myDb, objName, names, msg);
		if(!err) {
			cerr << "Attributes set for " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateDdcCommand:
	{	function = "CreateDdcCommand";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// command ID
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to DdcCommand
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createDdcCommand(myDb, mySegment, objName, &myCmd, msg);
		if(!err) {
			cerr << "Created DdcCommand: " << objName << endl;
			((IntegerVar *)objRef)->setValue((int)myCmd);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccDeleteDdcCommandDef:
	{	function = "DeleteDdcCommandDef";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// command ID
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to DdcCt
		 || !(objRef = expr->evaluate(param.thread))
		 ||	(objRef->isA() != BIT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		yesNo = ((BitVar *)objRef)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		cerr << "TransCmd = " << (yesNo ? "true" : "false") << endl;
		err = deleteDdcCommandDef(myDb, myDdcCt, objName, yesNo, msg);
		if(!err) {
			cerr << "Definition of " << objName << " removed from the DB" << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccAddTransitionCommand:
	{	function = "AddTransitionCommand";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for Cmd
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myCmd = (daq::ddc::DdcCommandDef *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = addTransitionCommand(myDdcCt, myCmd, msg);
		if(!err) {
			cerr << "Transition comand " << myCmd->UID() << " assigned for " << myDdcCt->get_Name() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccAssignDcsAlarm:
	{	function = "AssignDcsAlarm";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for Db
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for Ct
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DP name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// Poll interval
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = assignDcsAlarm(myDb, myDdcCt, objName, fName, msg);
		if(!err) {
			cerr << "DCS alarm assigned: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

  }		// end switch()
  
  return &bitError;
}


bool	ddccCtExternHdl::getDdcCt(Configuration* db, const daq::core::Segment* s, daq::ddc::DdcCtApplication** ct, 
							std::string& name, std::string& pvssName, std::string& errMsg)
{
	const daq::core::RunControlApplicationBase* ctrl = s->get_IsControlledBy();
	if(ctrl) {
//		const daq::ddc::DdcCtApplication* myCt = db->cast<daq::ddc::DdcCtApplication, daq::core::RunControlApplication>(s->get_IsControlledBy());
		const daq::ddc::DdcCtApplication* myCt = db->cast<daq::core::RunControlApplication>(s->get_IsControlledBy());
		if(myCt) {
			*ct = (daq::ddc::DdcCtApplication *)myCt;
			name = myCt->get_Name();
			pvssName = myCt->get_DCSStationToConnect()->get_Name();
		}
	}
	else
		*ct = 0;
	errMsg = "OK";
	return false;
}

bool	ddccCtExternHdl::createDdcCt(Configuration* db, daq::core::Segment* s, const daq::ddc::PvssWorkstation* ws, 
								std::string name, daq::ddc::DdcCtApplication** ct, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcCtApplication *> ctList;
	std::vector<const daq::ddc::DdcCtApplication *>::iterator	it;
	db->get(ctList);
	for(it = ctList.begin(); it != ctList.end(); it++)
		if(((*it)->get_Name() == name) || ((*it)->UID() == name)) {
			errMsg = "DdcCtApplication with this name already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcCtApplication* newCt = db->create<daq::ddc::DdcCtApplication>(*s, name);
	if(newCt) {
		if(!((daq::ddc::DdcCtApplication*)newCt)->set_Name(name)) {
			errMsg = "Cannot set Name for " + name;
			return true;
		}
		else {
			if(((daq::ddc::DdcCtApplication*)newCt)->set_DCSStationToConnect(ws)) {
				*ct = (daq::ddc::DdcCtApplication *)newCt;
				if(s->set_IsControlledBy(newCt)) {
					const daq::core::Binary* exe = db->get<daq::core::Binary>(DDCCT_PROGRAM);
					if(!exe) {
						errMsg = "There is no Binary ";
						errMsg += DDCCT_PROGRAM;
						return true;
					}
					if(!((*ct)->set_Program(exe))) {
						errMsg = "Cannot set Program for " + name;
						return true;
					}
//					const daq::core::Tag* tag = db->get<daq::core::Tag>(EXPLICIT_TAG);
//					if(!tag) {
//						errMsg = "There is no Tag ";
//						errMsg += EXPLICIT_TAG;
//						return true;
//					}
					const daq::core::Tag* tag = 0;
					if(!((*ct)->set_ExplicitTag(tag))) {
						errMsg = "Cannot set Tag for " + name;
						return true;
					}
					else 
						cerr << name << ": Explicit tag is cleaned" << endl;
					const std::vector<const daq::core::Application *>& applList = s->get_Applications();
					std::vector<const daq::core::Application *>::const_iterator it;
					for(it = applList.begin(); it != applList.end(); it++) {
						if(((*it)->class_name() == "DdcDtApplication") 
						|| ((*it)->class_name() == "DdcMtApplication") ){
//							std::vector<const daq::core::Application *> aa;
							std::vector<const daq::core::BaseApplication *> aa;
//							aa.push_back((const daq::core::Application *)(*ct));
							aa.push_back((const daq::core::BaseApplication *)(*ct));
//							((daq::core::Application *)(*it))->set_InitializationDependsFrom(aa);
							((daq::core::BaseApplication *)(*it))->set_InitializationDependsFrom(aa);
						}
					}
					errMsg = "OK";
				}
				else {
					errMsg = "Cannot set reference for CT from Segment " + s->get_Name();
					cerr << errMsg << endl;
					return true;
				}
			}
			else {
				errMsg = "Cannot set reference for PVSS workstation from " + newCt->get_Name();
				cerr << errMsg << endl;
				return true;
			}
		}
	}
	else {
		errMsg = "Cannot create DdcCtApplication " + name;
		return true;
	}
	return false;
}

bool	ddccCtExternHdl::replaceDdcCt(Configuration* db, daq::core::Segment* s, daq::ddc::DdcCtApplication** ct,
										std::string newName, std::string& errMsg)
{
	daq::ddc::DdcCtApplication* oldCt = *ct;
	const daq::ddc::PvssWorkstation* ws = oldCt->get_DCSStationToConnect();
	if(createDdcCt(db, s, ws, newName, ct, errMsg))
		return true;
	
	const std::string oldName = oldCt->get_Name();
	if(!(db->destroy(*oldCt))) {
		errMsg = "Cannot destroy DdcCtApplication object for " + oldName;
		return true;
	}
	errMsg = "OK";
	return false;
}

bool	ddccCtExternHdl::getDdcCtAttr(daq::ddc::DdcCtApplication* ct, std::vector<std::string>& attrs, std::string& errMsg)
{
	errMsg = "OK";
	
	cerr << "In get function " << (int)ct << endl;
	attrs.push_back(ct->get_Name());
	attrs.push_back(ct->get_Parameters());
	return false;
}

bool	ddccCtExternHdl::setDdcCtAttr(daq::ddc::DdcCtApplication* ct, std::string param, std::string& errMsg)
{
	if(ct->set_Parameters(param))
		errMsg = "OK";
	else {
		errMsg = "Cannot set Parameters for " + ct->get_Name();
		return true;
	}
	return false;
}

bool	ddccCtExternHdl::getDdcCommands(Configuration* db, daq::ddc::DdcCtApplication* ct, std::vector<std::string>& names, 
										std::vector<std::string>& targets, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcCommandDef *>&	tr_comList = ct->get_TransitionCommands();
	std::vector<const daq::ddc::DdcCommandDef *>::const_iterator	it;
	
	if(tr_comList.size() == 0)
		std::cerr << "No transition commands defined for " << ct->get_Name() << std::endl;
	else {
		for(it = tr_comList.begin(); it != tr_comList.end(); it++)
			names.push_back((*it)->UID());
	}	
	
	std::vector<const daq::ddc::DdcCommandDef *>	allCmd;
	std::vector<const daq::ddc::DdcCommandDef *>::iterator	itc;
	
	db->get(allCmd);
	for(itc = allCmd.begin(); itc != allCmd.end(); itc++) {
		if(((*itc)->get_RunControlTransition() == "None") 
		|| ((*itc)->get_RunControlTransition() == "none") 
		|| ((*itc)->get_RunControlTransition() == "NONE")) {
			targets.push_back((*itc)->UID());
		}
	}	
	errMsg = "OK";
	return false;
}

bool	ddccCtExternHdl::getDcsAlarmDp(daq::ddc::DdcCtApplication* ct, std::string& name, std::string& errMsg)
{
	const daq::ddc::PvssDatapoint* dp = ct->get_detectorAlarmDef();
	if(dp) {
		name = dp->UID();
	}
	else {
		name = "";
	}
	errMsg = "OK";
	return false;
}

bool	ddccCtExternHdl::nontransCmdToTrans(Configuration* db, daq::ddc::DdcCtApplication* ct, std::string name, 
											std::string fName, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcCommandDef *>& comList = ct->get_TransitionCommands();
	std::vector<const daq::ddc::DdcCommandDef *>::const_iterator it;
	for(it = comList.begin(); it != comList.end(); it++)
		if((*it)->get_RunControlTransition() == fName) {
			errMsg = "The DDC command for transition " + fName + " already exists";
			return true;
		}
		
	const daq::ddc::DdcCommandDef* comm = db->get<daq::ddc::DdcCommandDef>(name);
	if(!comm) {
		errMsg = "Cannot retrieve command " + name;
		cerr << errMsg << endl;
		return true;
	}
	if(((daq::ddc::DdcCommandDef *)comm)->set_RunControlTransition(fName)) {
		std::vector<const daq::ddc::DdcCommandDef *>	newList(comList);
		newList.push_back((daq::ddc::DdcCommandDef *)comm);
		if(ct->set_TransitionCommands(newList)) {
			errMsg = "OK";
			return false;
		}
		else {
			errMsg = "Cannot set new command for " + ct->get_Name();
			cerr << errMsg << endl;
			return true;
		}
	}
	errMsg = "Cannot set new transition for " + name;
	cerr << errMsg << endl;
	return true;
}

bool	ddccCtExternHdl::transCmdToNontrans(daq::ddc::DdcCtApplication* ct, std::string name, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcCommandDef *>& comList = ct->get_TransitionCommands();
	std::vector<const daq::ddc::DdcCommandDef *>	newList;

	std::vector<const daq::ddc::DdcCommandDef *>::const_iterator	it;
	for(it = comList.begin(); it != comList.end(); it++) {
		if((*it)->UID() == name) {
			if(((daq::ddc::DdcCommandDef *)(*it))->set_RunControlTransition("None")) {
				continue;
			}
			else {
				errMsg = "set_RunControlTransition failed";
				return true;
			}
		}
		else {
			newList.push_back(*it);
		}
	}

	if(ct->set_TransitionCommands(newList)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot remove transition command for " + ct->get_Name();
	cerr << errMsg << endl;
	return true;
}

bool	ddccCtExternHdl::getCommandAttr(Configuration* db, std::string name, std::vector<std::string>& names, std::string& errMsg)
{
	const daq::ddc::DdcCommandDef* comm = db->get<daq::ddc::DdcCommandDef>(name);
	
	if(!comm) {
		errMsg = "Cannot retrieve command " + name;
		cerr << errMsg << endl;
		return true;
	}
	names.push_back(comm->get_RunControlTransition());
	names.push_back(comm->get_DcsCommand());
	names.push_back(comm->get_CommandParameters());
	char	chartime[8];
	sprintf(chartime, "%d", comm->get_Timeout());
	string	strtime(chartime);
	names.push_back(strtime);
	
	errMsg = "OK";
	return false;
}

bool	ddccCtExternHdl::setCommandAttr(Configuration* db, std::string name, std::vector<std::string> names, std::string& errMsg)
{
	const daq::ddc::DdcCommandDef* comm = db->get<daq::ddc::DdcCommandDef>(name);
	
	if(((daq::ddc::DdcCommandDef *)comm)->set_RunControlTransition(names[0])) 
		if(((daq::ddc::DdcCommandDef *)comm)->set_DcsCommand(names[1]))
			if(((daq::ddc::DdcCommandDef *)comm)->set_CommandParameters(names[2]))
				if(((daq::ddc::DdcCommandDef *)comm)->set_Timeout(atoi(names[3].c_str()))) {
					errMsg = "OK";
					return false;
				}
	errMsg = "Cannot set command attributes for " + name;
	cerr << errMsg;
	return true;
}

bool	ddccCtExternHdl::createDdcCommand(Configuration* db, daq::core::Segment* s, std::string name, 
										daq::ddc::DdcCommandDef** cmd, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcCommandDef *> cmdList;
	std::vector<const daq::ddc::DdcCommandDef *>::iterator	it;
	db->get(cmdList);
	for(it = cmdList.begin(); it != cmdList.end(); it++)
		if((*it)->UID() == name) {
			errMsg = "DdcCommand with this name already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcCommandDef* newCmd = db->create<daq::ddc::DdcCommandDef>(*s, name);
	if(newCmd) {
		*cmd = (daq::ddc::DdcCommandDef *)newCmd;
		errMsg = "OK";
		return false;
	}
	errMsg = "Impossible to create in DB the DdcCommand " + name;
	return true;
}

bool	ddccCtExternHdl::deleteDdcCommandDef(Configuration* db, daq::ddc::DdcCtApplication* ct, std::string name, 
											bool transCmdFlag, std::string& errMsg)
{
	const daq::ddc::DdcCommandDef* comm = db->get<daq::ddc::DdcCommandDef>(name);
	if(!comm) {
		errMsg = "No in database the command " + name;
		return true;
	}
	
	if(transCmdFlag) {
//		cerr << "Removing transition command" << endl;
		const std::vector<const daq::ddc::DdcCommandDef *>& comList = ct->get_TransitionCommands();
		std::vector<const daq::ddc::DdcCommandDef *>	newList(comList);
		std::vector<const daq::ddc::DdcCommandDef *>::iterator	it;
		for(it = newList.begin(); it != newList.end(); it++)
			if((*it)->UID() == name) {
				newList.erase(it);
				break;
			}
		if(!(ct->set_TransitionCommands(newList))) {
//			errMsg = "Cannot set TransitionCommands for " + ct->get_Name();
			return true;
		}
	}
	else 
		cerr << "No need to remove transition command" << endl;
	
	daq::ddc::DdcCommandDef*	cDef = const_cast<daq::ddc::DdcCommandDef *>(comm);
	if(db->destroy(*cDef)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot destroy " + name;
	cerr << errMsg << endl;
	return true;
}

bool	ddccCtExternHdl::addTransitionCommand(daq::ddc::DdcCtApplication* ct, daq::ddc::DdcCommandDef* cmd, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcCommandDef *>& comList = ct->get_TransitionCommands();
	std::vector<const daq::ddc::DdcCommandDef *>::const_iterator it;
	for(it = comList.begin(); it != comList.end(); it++)
		if((*it)->get_RunControlTransition() == cmd->get_RunControlTransition()) {
			errMsg = "DDC command for this transition already exists: " + (*it)->UID();
			return true;
		}
	
	std::vector<const daq::ddc::DdcCommandDef *>	newList(comList);
	newList.push_back(cmd);
	if(ct->set_TransitionCommands(newList)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot set new command for " + ct->get_Name();
	cerr << errMsg << endl;
	return true;
}

bool	ddccCtExternHdl::assignDcsAlarm(Configuration* db, daq::ddc::DdcCtApplication* ct, std::string name, 
										std::string poll, std::string& errMsg)
{
	daq::ddc::PvssDatapoint* myDp;
	
	const daq::ddc::PvssDatapoint* existDp = db->get<daq::ddc::PvssDatapoint>(name);
	if(!existDp) {
		const daq::ddc::PvssDatapoint* newDp = db->create<daq::ddc::PvssDatapoint>(*ct, name);
		if(newDp) {
			myDp = const_cast<daq::ddc::PvssDatapoint *>(newDp);
			myDp->set_DpName(name);
		}
		else {
			errMsg = "Cannot create PvssDatapoint object";
			return true;
		}
	}
	else 
		myDp = const_cast<daq::ddc::PvssDatapoint *>(existDp);
	
	myDp->set_PollInterval(atoi(poll.c_str()));
	
	const daq::ddc::PvssDatapoint* oldDp = ct->get_detectorAlarmDef();
	
	if(!(ct->set_detectorAlarmDef(myDp))) {
		errMsg = "Cannot set new Not_Ready_for_DAQ flag";
		return true;
	}
	if(oldDp) {
		daq::ddc::PvssDatapoint* dp = const_cast<daq::ddc::PvssDatapoint *>(oldDp);
		cerr << "Deleting old Alarm DP..." << endl;
		if(db->destroy(*dp))
			cerr << " ...done" << endl;
		else
			cerr << " ... WARNING: Could not destroy old Alarm DP" << endl;		
	}
	errMsg = "OK";
	return false;
}
