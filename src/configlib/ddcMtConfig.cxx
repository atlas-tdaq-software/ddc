// Ctrl extension for DDC-MT configuration

#include <stdio.h>
#include <CharString.hxx>
#include <BlobVar.hxx>
#include <CtrlExpr.hxx>
#include <ExprList.hxx>
#include <CtrlThread.hxx>
#include <IdExpr.hxx>
#include <Variable.hxx>
#include <AnyTypeVar.hxx>
#include <BitVar.hxx>
#include <CharVar.hxx>
#include <IntegerVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>
#include <TextVar.hxx>
#include <TimeVar.hxx>
#include <DynVar.hxx>
#include <ErrorVar.hxx>
#include <ExternData.hxx>
#include <ErrClass.hxx>
#include <BCTime.h>

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/RunControlApplication.h>
#include <dal/Binary.h>
#include <dal/Tag.h>

using namespace std;

#include <ddc/PvssWorkstation.h>
#include <ddc/DdcCtApplication.h>
#include <ddc/DdcMtApplication.h>
#include <ddc/DdcTargetList.h>
#include <ddc/DdcTextMessage.h>

#include "ddcConfig-Defs.hxx"
#include "ddcMtConfig.hxx"


// Function description
FunctionListRec ddccMtExternHdl::fnList[] = 
{
  //  Return-Value    function name        parameter list               true == thread-save
  //  -------------------------------------------------------------------------------------
    {BIT_VAR, "ddccCreateMt","(int conf, int segRef, string name, int wsRef, int ctRef, int& mtRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetMtAttrs", "(int mtRef, vector<string>& attrs, string& errMsg)", true},
	{BIT_VAR, "ddccSetMtAttrs", "(int mtRef, vector<string>& attrs, string& errMsg)", true},
	{BIT_VAR, "ddccSetAllAlarms", "(int mtRef, bool yesNo, string& errMsg)", true},
	{BIT_VAR, "ddccGetAlarms", "(int mtRef, vector<string>& names, string& errMsg)", true},
	{BIT_VAR, "ddccGetTextMsg", "(int mtRef, vector<string>& names, string& errMsg)", true},
	{BIT_VAR, "ddccGetTargetObj", "(int conf, string name, int& objRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetMsgObj", "(int conf, string name, int& objRef, string& errMsg)", true},
	{BIT_VAR, "ddccCreateTargetObj", "(int conf, int mtRef, string name, int& objRef, string& errMsg)", true},
	{BIT_VAR, "ddccCreateMsgObj", "(int conf, int mtRef, string name, int& objRef, string& errMsg)", true},
	{BIT_VAR, "ddccDeleteTargetObj", "(int conf, int mtRef, int objRef, string& errMsg)", true},
	{BIT_VAR, "ddccDeleteMsgObj", "(int conf, int mtRef, int objRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetTargetAttrs", "(int mtRef, vector<string>& attrs, string& errMsg)", true},
	{BIT_VAR, "ddccSetTargetAttrs", "(int mtRef, vector<string>& attrs, string& errMsg)", true},
	{BIT_VAR, "ddccGetMsgAttrs", "(int mtRef, vector<string>& attrs, string& errMsg)", true},
	{BIT_VAR, "ddccSetMsgAttrs", "(int mtRef, vector<string>& attrs, string& errMsg)", true},
};


BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
  // Calculate number of functions defined
  PVSSulong funcCount = sizeof(ddccMtExternHdl::fnList)/sizeof(ddccMtExternHdl::fnList[0]);  
  
  // now allocate the new instance
  ddccMtExternHdl *hdl = new ddccMtExternHdl(nextHdl, funcCount, ddccMtExternHdl::fnList);

  return hdl;
}


void ddccMtExternHdl::showError(ExecuteParamRec &param, std::string& function)
{
	ErrClass  errClass(
			ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, ErrClass::ARG_MISSING,
			"ddccExternHdl", function.c_str(), "missing or wrong argument data");

	// Remember error message. 
	// In the Ctrl script use getLastError to retreive the error message
	param.thread->appendLastError(&errClass);
	return;
}


//------------------------------------------------------------------------------
// This function is called every time we use tcpXXX in a Ctrl script.
// The argument is an aggregaton of function name, function number, 
// the arguments passed to the Ctrl function, the "thread" context 
// and user defined data.
const Variable *ddccMtExternHdl::execute(ExecuteParamRec &param)
{
  // We return a pointer to a static variable. 
  static IntegerVar integerVar;
  static BitVar 	bitError;
	std::string		function;
	const std::string wordF("Function ");
	bool 	err;
  // A pointer to one "argument". Any input argument may be an exprssion
  // like "a ? x : y" instead of a simple value.
  CtrlExpr   *expr;
	
	const Variable*		objRef;
	const Variable* 	errMsg;
	const Variable*		dynStringRef;

	const Variable*		dbPtrVar;
	const Variable*		objNameVar;
	
	Configuration*		myDb;
	daq::core::Segment*		mySegment;
	daq::ddc::PvssWorkstation*	myPvssWs;
	daq::ddc::DdcCtApplication*	myDdcCt;
	daq::ddc::DdcMtApplication*	myDdcMt;
	daq::ddc::DdcTargetList*	myTarget;
	daq::ddc::DdcTextMessage*	myMsg;
	
	std::string			msg;
	std::string			dbName;
	std::string			objName;
	std::string			fName;
	std::vector<std::string>	names;
	std::vector<std::string>	targets;
	bool				yesNo;
	unsigned int	vectorLen;
	
  // switch / case on function numbers
  switch (param.funcNum)
  {
	case F_ddccCreateMt:
	{	function = "CreateMt";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();

		
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference for PvssWs
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssWs = (daq::ddc::PvssWorkstation *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// reference to DDC-MT appl object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 6" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createMtAppl(myDb, mySegment, objName, myPvssWs, myDdcCt, &myDdcMt, msg);
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDdcMt);
			cerr << "DDC-MT obj created for " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetMtAttrs:
	{	function = "GetMtAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC-MT application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for attribute List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getMtAttrs(myDdcMt, names, msg);
		if(!err) {
			cerr << "Attributes retrieved for " << myDdcMt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetMtAttrs:
	{	function = "SetMtAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC-MT application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// array of attributes
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)objNameVar)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)objNameVar)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)objNameVar)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setMtAttrs(myDdcMt, names, msg);
		if(!err) {
			cerr << "Attributes set for " << myDdcMt->get_Name() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetAllAlarms:
	{	function = "SetAllAlarms";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// reference to DdcCt
		 || !(objRef = expr->evaluate(param.thread))
		 ||	(objRef->isA() != BIT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		yesNo = ((BitVar *)objRef)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setAllAlarms(myDdcMt, yesNo, msg);
		if(!err) {
			cerr << "Flag of all alarms " << (yesNo ? "set" : "removed") << " for " << myDdcMt->get_Name() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetAlarms:
	{	function = "GetAlarms";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// 		reference for lists of alerts
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getAlarms(myDdcMt, names, msg);
		if(!err) {
			cerr << "Alarm lists retrieved for " << myDdcMt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetTextMsg:
	{	function = "GetTextMsg";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// 		reference for list of msg
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getTextMsg(myDdcMt, names, msg);
		if(!err) {
			cerr << "Text messages retrieved for " << myDdcMt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetTargetObj:
	{	function = "GetTargetObj";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// target list name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for list obj
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getTargetObj(myDb, objName, &myTarget, msg);
		if(!err) {
			cerr << "Alarm list object retrieved for " << objName << endl;
			((IntegerVar *)objRef)->setValue((int)myTarget);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetMsgObj:
	{	function = "GetMsgObj";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DcsTextMessage name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for list obj
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getMsgObj(myDb, objName, &myMsg, msg);
		if(!err) {
			cerr << "DCS text message object retrieved for " << objName << endl;
			((IntegerVar *)objRef)->setValue((int)myMsg);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateTargetObj:
	{	function = "CreateTargetObj";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DcsTextMessage name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for target obj
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createTargetObj(myDb, myDdcMt, objName, &myTarget, msg);
		if(!err) {
			cerr << "Target object created for " << objName << endl;
			((IntegerVar *)objRef)->setValue((int)myTarget);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateMsgObj:
	{	function = "CreateMsgObj";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for MT
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DcsTextMessage name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for target obj
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createMsgObj(myDb, myDdcMt, objName, &myMsg, msg);
		if(!err) {
			cerr << "DCS message object created for " << objName << endl;
			((IntegerVar *)objRef)->setValue((int)myMsg);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccDeleteTargetObj:
	{	function = "DeleteTargetObj";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for MT
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for Alarm list
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myTarget = (daq::ddc::DdcTargetList *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		objName = myTarget->UID();
		err = deleteTargetObj(myDb, myDdcMt, myTarget, msg);
		if(!err) {
			cerr << "Alarm list deleted: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccDeleteMsgObj:
	{	function = "DeleteMsgObj";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for MT
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcMt = (daq::ddc::DdcMtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DCS message
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myMsg = (daq::ddc::DdcTextMessage *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		objName = myMsg->UID();
		err = deleteMsgObj(myDb, myDdcMt, myMsg, msg);
		if(!err) {
			cerr << "DCS text message deleted: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetTargetAttrs:
	{	function = "GetTargetAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for Alarm list
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myTarget = (daq::ddc::DdcTargetList *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for List' attributes
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getTargetAttrs(myTarget, names, msg);
		if(!err) {
			cerr << "Attributes retrieved for " << myTarget->UID() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetTargetAttrs:
	{	function = "SetTargetAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for Alarm list
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myTarget = (daq::ddc::DdcTargetList *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for List' attributes
		 || !(dynStringRef = expr->evaluate(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)dynStringRef)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)dynStringRef)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)dynStringRef)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setTargetAttrs(myTarget, names, msg);
		if(!err) {
			cerr << "Attributes set for " << myTarget->UID() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetMsgAttrs:
	{	function = "GetMsgAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for Msg obj
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myMsg = (daq::ddc::DdcTextMessage *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for List' attributes
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getMsgAttrs(myMsg, names, msg);
		if(!err) {
			cerr << "Attributes retrieved for " << myMsg->UID() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetMsgAttrs:
	{	function = "SetMsgAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for Alarm list
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myMsg = (daq::ddc::DdcTextMessage *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for Msg' attributes
		 || !(dynStringRef = expr->evaluate(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)dynStringRef)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)dynStringRef)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)dynStringRef)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setMsgAttrs(myMsg, names, msg);
		if(!err) {
			cerr << "Attributes set for " << myMsg->UID() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

  }		// end switch()
  
  return &bitError;
}

bool	ddccMtExternHdl::createMtAppl(Configuration* db, const daq::core::Segment* s, std::string name, 
							const daq::ddc::PvssWorkstation* ws, const daq::ddc::DdcCtApplication* ct, 
							daq::ddc::DdcMtApplication** mt, std::string& errMsg)
{
	bool	success = true;
	std::vector<const daq::ddc::DdcMtApplication *> mtList;
	std::vector<const daq::ddc::DdcMtApplication *>::iterator	it;
	db->get(mtList);
	for(it = mtList.begin(); it != mtList.end(); it++)
		if((*it)->UID() == name) {
			errMsg = "DDC-Mt application with this name already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcMtApplication* newMt = db->create<daq::ddc::DdcMtApplication>(*s, name);
	if(newMt) {
		*mt = (daq::ddc::DdcMtApplication *)newMt;
		(*mt)->set_Name(name);
		(*mt)->set_DCSStationToConnect(ws);
		std::vector<const daq::core::BaseApplication *> aa;
		aa.push_back((const daq::core::BaseApplication *)ct);
		(*mt)->set_InitializationDependsFrom(aa);
		
		const daq::core::Binary* exe = db->get<daq::core::Binary>(DDCMT_PROGRAM);
		if(!exe) {
			errMsg = "There is no Binary ";
			errMsg += DDCMT_PROGRAM;
			return true;
		}
		if(!((*mt)->set_Program(exe))) {
			errMsg = "Cannot set Program for " + name;
			return true;
		}
//		const daq::core::Tag* tag = db->get<daq::core::Tag>(EXPLICIT_TAG);
//		if(!tag) {
//			errMsg = "There is no Tag ";
//			errMsg += EXPLICIT_TAG;
//			return true;
//		}
		const daq::core::Tag* tag = 0;
		if(!((*mt)->set_ExplicitTag(tag))) {
			errMsg = "Cannot set Tag for " + name;
			return true;
		}
		else 
			cerr << name << ": Explicit tag is cleaned" << endl;
		if(!((*mt)->set_InitTimeout(0))) {
			errMsg = "Cannot clear InitTimeout " + name;
			return true;
		}
		
		const std::vector<const daq::core::Application *>&	objRefList = s->get_Applications();
		// refer to DDC appl from Segment
		std::vector<const daq::core::Application *> new_appls(objRefList);
		new_appls.push_back(newMt);
		success = ((daq::core::Segment *)s)->set_Applications(new_appls);
		if(!success) 
			errMsg = "Cannot modify Applications relationship of " + s->get_Name();
		else 
			errMsg = "OK";
	}
	else {
		errMsg = "Cannot create DdcMtApplication " + name;
		success = false;
	}
	
	return(!success);
}


bool	ddccMtExternHdl::getMtAttrs(const daq::ddc::DdcMtApplication* apMt, std::vector<std::string>& attr, std::string& errMsg)
{
	errMsg = "OK";
	
	attr.push_back(apMt->UID());
	attr.push_back(apMt->get_Name());
	attr.push_back(apMt->get_Parameters());
	return false;
}

bool	ddccMtExternHdl::setMtAttrs(daq::ddc::DdcMtApplication* apMt, std::vector<std::string> attr, std::string& errMsg)
{
	bool	success = true;
	errMsg = "OK";
	
	success = apMt->set_Name(attr[0]);
	if(!success) {
		errMsg = "Cannot set name for " + apMt->get_Name();
		return true;
	}
	else {
		success = apMt->set_Parameters(attr[1]);
		if(!success) {
			errMsg = "Cannot set Parameters for " + apMt->get_Name();
			return true;
		}
	}
	return false;
}

bool	ddccMtExternHdl::setAllAlarms(daq::ddc::DdcMtApplication* mt, bool yesNo, std::string& errMsg)
{
	if(mt->set_AllAlarms(yesNo)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Impossible to define AllAlarms for " + mt->get_Name();
	return true;
}

bool	ddccMtExternHdl::getAlarms(daq::ddc::DdcMtApplication* mt, std::vector<std::string>& names, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcTargetList *>&	targets = mt->get_DcsAlarms();
	std::vector<const daq::ddc::DdcTargetList *>::const_iterator	it;
	
	for(it = targets.begin(); it != targets.end(); it++)
		names.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}

bool	ddccMtExternHdl::getTextMsg(daq::ddc::DdcMtApplication* mt, std::vector<std::string>& names, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcTextMessage *>&	targets = mt->get_DcsTextMessages();
	std::vector<const daq::ddc::DdcTextMessage *>::const_iterator	it;
	
	for(it = targets.begin(); it != targets.end(); it++)
		names.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}

bool	ddccMtExternHdl::getTargetObj(Configuration* db, std::string name, daq::ddc::DdcTargetList** t, std::string& errMsg)
{
	const daq::ddc::DdcTargetList* target = db->get<daq::ddc::DdcTargetList>(name);
	
	if(target) {
		*t = (daq::ddc::DdcTargetList *)target;
		errMsg = "OK";
		return false;
	}
	errMsg = "No TargetList " + name + " defined";
	return true;
}

bool	ddccMtExternHdl::getMsgObj(Configuration* db, std::string name, daq::ddc::DdcTextMessage** m, std::string& errMsg)
{
	const daq::ddc::DdcTextMessage* msg = db->get<daq::ddc::DdcTextMessage>(name);
	
	if(msg) {
		*m = (daq::ddc::DdcTextMessage *)msg; 
		errMsg = "OK";
		return false;
	}
	errMsg = "No DcsTextMessage " + name + " defined";
	return true;
}

bool	ddccMtExternHdl::createTargetObj(Configuration* db, const daq::ddc::DdcMtApplication* mt, std::string name, 
							daq::ddc::DdcTargetList** t, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcTargetList *> tList;
	std::vector<const daq::ddc::DdcTargetList *>::iterator	it;
	db->get(tList);
	for(it = tList.begin(); it != tList.end(); it++)
		if((*it)->UID() == name) {
			errMsg = "Alarm list with this name already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcTargetList* newAl = db->create<daq::ddc::DdcTargetList>(*mt, name);
	if(newAl) {
		*t = (daq::ddc::DdcTargetList *)newAl;
		
		const std::vector<const daq::ddc::DdcTargetList *>&	objRefList = mt->get_DcsAlarms();
		// refer to alarm lists from DdcMtApplication
		std::vector<const daq::ddc::DdcTargetList *> new_set(objRefList);
		new_set.push_back(newAl);
		if(((daq::ddc::DdcMtApplication *)mt)->set_DcsAlarms(new_set)) {
			errMsg = "OK";
			return false;
		}
		else {
			errMsg = "Cannot modify DcsAlarms relationship of " + mt->get_Name();
			return true;
		}
	}
	errMsg = "Cannot create new alarm list " + name;
	return true;
}

bool	ddccMtExternHdl::createMsgObj(Configuration* db, const daq::ddc::DdcMtApplication* mt, std::string name, 
							daq::ddc::DdcTextMessage** t, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcTextMessage *> tList;
	std::vector<const daq::ddc::DdcTextMessage *>::iterator	it;
	db->get(tList);
	for(it = tList.begin(); it != tList.end(); it++)
		if((*it)->UID() == name) {
			errMsg = "Text message with this ID already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcTextMessage* newMsg = db->create<daq::ddc::DdcTextMessage>(*mt, name);
	if(newMsg) {
		*t = (daq::ddc::DdcTextMessage *)newMsg;
		
		const std::vector<const daq::ddc::DdcTextMessage *>&	objRefList = mt->get_DcsTextMessages();
		// refer to alarm lists from DdcMtApplication
		std::vector<const daq::ddc::DdcTextMessage *> new_set(objRefList);
		new_set.push_back(newMsg);
		if(((daq::ddc::DdcMtApplication *)mt)->set_DcsTextMessages(new_set)) {
			errMsg = "OK";
			return false;
		}
		else {
			errMsg = "Cannot modify DcsTextMessages relationship of " + mt->get_Name();
			return true;
		}
	}
	errMsg = "Cannot create DdcTextMessage " + name;
	return true;
}

bool	ddccMtExternHdl::deleteTargetObj(Configuration* db, const daq::ddc::DdcMtApplication* mt, 
										daq::ddc::DdcTargetList* t, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcTargetList *>&	objRefList = mt->get_DcsAlarms();
	std::vector<const daq::ddc::DdcTargetList *>::iterator	it;
	// remove target list from MT appl
	std::vector<const daq::ddc::DdcTargetList *> new_set(objRefList);
	for(it = new_set.begin(); it != new_set.end(); it++)
		if((*it) == t) {
			new_set.erase(it);
			break;
		}
	if(!(((daq::ddc::DdcMtApplication *)mt)->set_DcsAlarms(new_set))) {
		errMsg = "Cannot remove Alarm list from DDC-MT";
		return true;
	}
	if(!(db->destroy(*t))) {
		errMsg = "Cannot destroy alarm list";
		return true;
	}
	errMsg = "OK";
	return false;
}


bool	ddccMtExternHdl::deleteMsgObj(Configuration* db, const daq::ddc::DdcMtApplication* mt, 
										daq::ddc::DdcTextMessage* msg, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcTextMessage *>&	objRefList = mt->get_DcsTextMessages();
	std::vector<const daq::ddc::DdcTextMessage *>::iterator	it;
	// remove message from MT appl
	std::vector<const daq::ddc::DdcTextMessage *> new_set(objRefList);
	for(it = new_set.begin(); it != new_set.end(); it++)
		if((*it) == msg) {
			new_set.erase(it);
			break;
		}
	if(!(((daq::ddc::DdcMtApplication *)mt)->set_DcsTextMessages(new_set))) {
		errMsg = "Cannot remove TextMessage from DDC-MT";
		return true;
	}
	if(!(db->destroy(*msg))) {
		errMsg = "Cannot destroy DCS text message";
		return true;
	}
	errMsg = "OK";
	return false;
}

bool	ddccMtExternHdl::getTargetAttrs(daq::ddc::DdcTargetList* tl, std::vector<std::string>& attrs, std::string& errMsg)
{
	const std::vector<std::string>&	objRefList = tl->get_Datapoints();
	std::vector<std::string>::const_iterator it;
	
	attrs.erase(attrs.begin(), attrs.end());
	for(it = objRefList.begin(); it != objRefList.end(); it++)
		attrs.push_back(*it);
	errMsg = "OK";
	return false;
}

bool	ddccMtExternHdl::setTargetAttrs(daq::ddc::DdcTargetList* tl, const std::vector<std::string>& attrs, std::string& errMsg)
{
	if(!(tl->set_Datapoints(attrs))) {
		errMsg = "Cannot set Datapoints for " + tl->UID();
		return true;
	}
	errMsg = "OK";
	return false;
}

bool	ddccMtExternHdl::getMsgAttrs(daq::ddc::DdcTextMessage* tm, std::vector<std::string>& attrs, std::string& errMsg)
{
	attrs.erase(attrs.begin(), attrs.end());
	
	attrs.push_back(tm->get_PvssDpeName());
	attrs.push_back(tm->get_MessageSeverity());
	errMsg = "OK";
	return false;
}

bool	ddccMtExternHdl::setMsgAttrs(daq::ddc::DdcTextMessage* tm, const std::vector<std::string>& attrs, std::string& errMsg)
{
	if(!(tm->set_PvssDpeName(attrs[0]))) {
		errMsg = "Cannot set PvssDpeName for " + tm->UID();
		return true;
	}
	if(!(tm->set_MessageSeverity(attrs[1]))) {
		errMsg = "Cannot set MessageSeverity for " + tm->UID();
		return true;
	}
	errMsg = "OK";
	return false;
}
