#ifndef _DDC_DEFAULTS
#define _DDC_DEFAULTS
#define HWTAG_DEFAULT 		"i686-rh73"
#define RLOGIN_DEFAULT 		"ssh"
#define EVENTPORT_DEFAULT 	4998
#define DATAPORT_DEFAULT 	4897
#define DATASERVER_DEFAULT "RunParams"

#define DDCCT_PROGRAM	"ddc_ct"
#define DDCDT_PROGRAM	"ddc_dt"
#define DDCMT_PROGRAM	"ddc_mt"
#endif

