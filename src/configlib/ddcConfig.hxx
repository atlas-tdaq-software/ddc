#ifndef _DDCEXTERNHDL_H_
#define _DDCEXTERNHDL_H_

#include <BaseExternHdl.hxx>

class ddccExternHdl : public BaseExternHdl
{
public:
    // List of user defined functions
    // Internal use only
    enum 
    {
	 	F_ddccCreateDbKernel = 0, 
	  	F_ddccOpenDb,
		F_ddccGetDbEnvironment, 
		F_ddccGetPartitions,
		F_ddccGetPartObj,
		F_ddccGetDdcSegments,
		F_ddccGetSegmentObj,
		F_ddccCreateNewDdcSegment,
		F_ddccAddDdcSegment,
		F_ddccGetSegmentTag,
		F_ddccSetSegmentTag,
		F_ddccGetPvssWs,
		F_ddccCreatePvssWs,
		F_ddccRemoveSegment,
		F_ddccGetPvssWsAttributes,
		F_ddccSetPvssWsAttributes,
		F_ddccDeletePvssWs,
		F_ddccGetDdcWs,
		F_ddccGetDdcWsAttributes,
		F_ddccCreateDdcWs,
		F_ddccDeleteDdcWs,
		F_ddccSaveDb,
		F_ddccGetApplList,
		F_ddccGetDdcApplObject,
		F_ddccSetPvssWs,
		F_ddccGetConnOps,
		F_ddccSetConnOps,
		F_ddccRemoveDdcAppl,
		F_ddccGetApplDdcWs,
		F_ddccAssignDdcWs,
		F_ddccCloseDb
    };

    // Descritption of user defined functions. 
    // Used by libCtrl to identify function calls
    static FunctionListRec  fnList[];

    ddccExternHdl(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : BaseExternHdl(nextHdl, funcCount, fnList) {}
    
    // Execute user defined function
    virtual const Variable *execute(ExecuteParamRec &param);

private:
	void 	showError(ExecuteParamRec &param, std::string& function);
	
	bool	openDb(::Configuration **, std::string& errMsg);
	bool	getDbEnv(Configuration *, std::string mainDb, daq::core::Variable**, 
								bool* newEnv, std::string& errMsg);
	bool	getPartitions(Configuration *, std::vector<std::string>& partitions, std::string& errMsg);
	bool	getPartitionObj(Configuration *, std::string name, daq::core::Partition ** p, string& errMsg);
	bool	getDdcSegments(daq::core::Partition* p, std::vector<std::string>& segments, std::string& errMsg);
	bool	getSegmentObj(Configuration *, std::string name, daq::core::Segment ** s, string& errMsg);
	bool	createDdcSegment(Configuration *, std::string mainDb, daq::core::Partition* partition,
							const daq::core::Variable* pvss, std::string name, 
							std::string& fileName, daq::core::Segment** s, std::string& errMsg);
	bool	addDdcSegment(Configuration *, std::string mainFile, daq::core::Partition* p, 
							std::string name, std::vector<std::string>& s, std::string& errMsg);
	bool	getSegmentTag(const daq::core::Partition *, const daq::core::Segment *, std::string& tag, std::string& errMsg);
	bool	setSegmentTag(Configuration *, daq::core::Segment *, std::string tag, std::string& errMsg);
	bool	getPvssWs(Configuration *, std::vector<std::string>& s, std::string name,
						daq::ddc::PvssWorkstation**, std::string& errMsg); 
	bool	createPvssWs(Configuration *, const daq::core::Segment *, std::string name, 
							daq::ddc::PvssWorkstation**, std::string& errMsg);    
	bool	removeSegment(Configuration *, std::string dbFile, const daq::core::Partition *, 
									std::string sName,std::string& erMsg);
	bool	getPvssWsAttr(const daq::ddc::PvssWorkstation*, std::vector<std::string>&, std::string& errMsg); 
	bool	setPvssWsAttr(const daq::ddc::PvssWorkstation*, const std::vector<std::string>&, std::string& errMsg); 
	bool	deletePvssWs(Configuration *, std::string name, std::string& errMsg); 
	bool	getDdcWs(Configuration *, std::vector<std::string>& s, std::string& errMsg); 
	bool	getDdcWsAttr(Configuration *, const std::string name, std::vector<std::string>&, std::string& errMsg); 
	bool	createDdcWs(Configuration *, const daq::core::Segment *, std::string name, std::string& errMsg);    
	bool	deleteDdcWs(Configuration *, std::string name, std::string& errMsg); 
	bool	getApplList(Configuration *, const daq::core::Segment *, const std::string, std::vector<std::string>& names,
						std::vector<std::string>& targetWs, std::string& errMsg);
	bool	getDdcApObj(Configuration *, const daq::core::Segment *, const std::string apClass, const std::string, 
						void **, std::string& errMsg);
	bool	setStationToConnect(void * ddcAp, const std::string apClass,
								const daq::ddc::PvssWorkstation* myPvssWs, std::string& errMsg);
	bool	getConnOps(void* ddcAp, const std::string apClass, std::vector<std::string>& ops, std::string& errMsg);
	bool	setConnOps(void* ddcAp, const std::string apClass, std::vector<std::string>& ops, std::string& errMsg);
	bool	removeDdcAppl(Configuration *, daq::core::Segment*, void * ddcAp, std::string apClass, std::string& errMsg);
	bool	getApplDdcWs(void * ddcAp, std::string apClass, std::string& name, std::string& errMsg);
	bool	assignDdcWs(Configuration *, daq::core::Application *, std::string name, std::string& errMsg);
	bool	isUsedByApplications(Configuration* db, const daq::core::Computer* ws);
	bool	isDefaultHost(Configuration* db, const daq::core::Computer* ws, std::string& pName);
};


// Create new ExternHdl. This must be global function named newExternHdl
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl);

#endif 
