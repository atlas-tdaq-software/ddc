// Ctrl extension for DDC configuration

#include <stdio.h>
#include <CharString.hxx>
#include <BlobVar.hxx>
#include <CtrlExpr.hxx>
#include <ExprList.hxx>
#include <CtrlThread.hxx>
#include <IdExpr.hxx>
#include <Variable.hxx>
#include <AnyTypeVar.hxx>
#include <BitVar.hxx>
#include <CharVar.hxx>
#include <IntegerVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>
#include <TextVar.hxx>
#include <TimeVar.hxx>
#include <DynVar.hxx>
#include <ErrorVar.hxx>
#include <ExternData.hxx>
#include <ErrClass.hxx>
#include <BCTime.h>

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/Segment.h>
#include <dal/Binary.h>
#include <dal/Tag.h>

using namespace std;

#include <ddc/PvssWorkstation.h>
#include <ddc/DdcDtApplication.h>
#include <ddc/DdcCtApplication.h>
#include <ddc/DdcDataTargetList.h>

#include "ddcConfig-Defs.hxx"
#include "ddcDtConfig.hxx"


// Function description
FunctionListRec ddccDtExternHdl::fnList[] = 
{
  //  Return-Value    function name        parameter list               true == thread-save
  //  -------------------------------------------------------------------------------------
	{BIT_VAR, "ddccGetDtAttrs", "(int dtRef, vector<string>& attr, string& errMsg)", true},
	{BIT_VAR, "ddccSetDtAttrs", "(int dtRef, vector<string> attr, string& errMsg)", true},
	{BIT_VAR, "ddccCreateDt", "(int conf, int segRef, string name, int wsRef, int ctRef, int& dtRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetAllDataTargets", "(int conf, vector<string>& names, string& errMsg)", true},
	{BIT_VAR, "ddccGetExport", "(int conf, vector<string>& names, string& errMsg)", true},
	{BIT_VAR, "ddccGetImport", "(int conf, vector<string>& names, string& errMsg)", true},
	{BIT_VAR, "ddccAddExportList", "(int dtRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccRemoveExportList", "(int dtRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccAddImportList", "(int dtRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccRemoveImportList", "(int dtRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccCreateDataTarget", "(int conf, int segRef, string name, int& dataTargetRef, string& errMsg)", true},
	{BIT_VAR, "ddccDeleteDataTarget", "(int conf, string name, string& errMsg)", true},
	{BIT_VAR, "ddccGetDataTargetAttr", "(int conf, string name, int& tagetRef, vector<string>& names, string& errMsg)", true},
	{BIT_VAR, "ddccSetDataTargetAttr", "(int tagetRef, vector<string>& names, string& errMsg)", true}
};


BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
  // Calculate number of functions defined
  PVSSulong funcCount = sizeof(ddccDtExternHdl::fnList)/sizeof(ddccDtExternHdl::fnList[0]);  
  
  // now allocate the new instance
  ddccDtExternHdl *hdl = new ddccDtExternHdl(nextHdl, funcCount, ddccDtExternHdl::fnList);

  return hdl;
}

void ddccDtExternHdl::showError(ExecuteParamRec &param, std::string& function)
{
	ErrClass  errClass(
			ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, ErrClass::ARG_MISSING,
			"ddccDtExternHdl", function.c_str(), "missing or wrong argument data");

	// Remember error message. 
	// In the Ctrl script use getLastError to retreive the error message
	param.thread->appendLastError(&errClass);
	return;
}

//------------------------------------------------------------------------------
// This function is called every time we use ddccXXX in a Ctrl script.
// The argument is an aggregaton of function name, function number, 
// the arguments passed to the Ctrl function, the "thread" context 
// and user defined data.
const Variable *ddccDtExternHdl::execute(ExecuteParamRec &param)
{
  // We return a pointer to a static variable. 
  static IntegerVar integerVar;
  static BitVar 	bitError;
	bool 	err;
	std::string		function;
	const std::string wordF("Function ");
  // A pointer to one "argument". Any input argument may be an exprssion
  // like "a ? x : y" instead of a simple value.
  CtrlExpr   *expr;
	
	const Variable*		objRef;
	const Variable* 	errMsg;
	const Variable*		dynStringRef;

	const Variable*		dbPtrVar;
	const Variable*		objNameVar;
	
	Configuration*		myDb;
	daq::core::Segment*		mySegment;
	daq::ddc::PvssWorkstation*	myPvssWs;
	daq::ddc::DdcDtApplication*	myDdcDt;
	daq::ddc::DdcCtApplication*	myDdcCt;
	daq::ddc::DdcDataTargetList*	myDataTarget;
	
	std::string			msg;
	std::string			dbName;
	std::string			objName;
	std::string			fName;
	std::vector<std::string>	names;
	std::vector<std::string>	targets;
	unsigned int	vectorLen;
	
  // switch / case on function numbers
  switch (param.funcNum)
  {
	case F_ddccGetDtAttrs:
	{	function = "GetDtAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC-DT application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for attribute List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDtAttrs(myDdcDt, names, msg);
		if(!err) {
			cerr << "Attributes retrieved for " << myDdcDt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetDtAttrs:
	{	function = "SetDtAttrs";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC-DT application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());
		cerr << "ddcDt ref: " << (int)myDdcDt << endl;

		if (!param.args || !(expr = param.args->getNext())			// array of attributes
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)objNameVar)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)objNameVar)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)objNameVar)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setDtAttrs(myDdcDt, names, msg);
		if(!err) {
			cerr << "Attributes set for " << myDdcDt->get_Name() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateDt:
	{	function = "CreateDt";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference for PvssWs
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssWs = (daq::ddc::PvssWorkstation *)(((IntegerVar *)dbPtrVar)->getValue());


		if (!param.args || !(expr = param.args->getNext())			// reference for DdcCt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcCt = (daq::ddc::DdcCtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// reference to DDC-DT appl object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 6" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createDtAppl(myDb, mySegment, objName, myPvssWs, myDdcCt, &myDdcDt, msg);
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDdcDt);
			cerr << "DDC-DT obj created for " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetAllDataTargets:
	{	function = "GetAllDataTargets";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for data target Lists
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getAllDataTargets(myDb, names, msg);
		if(!err) {
			cerr << "All data targets retrieved" << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetExport:
	{	function = "GetExport";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for data target Lists
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getExport(myDdcDt, names, msg);
		if(!err) {
			cerr << "Data targets retrieved for " << myDdcDt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetImport:
	{	function = "GetImport";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for data target Lists
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getImport(myDdcDt, names, msg);
		if(!err) {
			cerr << "Data targets retrieved for " << myDdcDt->get_Name() << endl;
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}


	case F_ddccAddExportList:
	{	function = "AddExportList";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DdcDt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// List name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = addExportList(myDb, myDdcDt, objName, msg);
		if(!err) {
			cerr << "Export list added: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccRemoveExportList:
	{	function = "RemoveExportList";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcDt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// List name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = removeExportList(myDdcDt, objName, msg);
		if(!err) {
			cerr << "Export list removed: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccAddImportList:
	{	function = "AddImportList";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DdcDt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// List name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = addImportList(myDb, myDdcDt, objName, msg);
		if(!err) {
			cerr << "Import list added: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccRemoveImportList:
	{	function = "RemoveImportList";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DdcDt
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcDt = (daq::ddc::DdcDtApplication *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// List name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = removeImportList(myDdcDt, objName, msg);
		if(!err) {
			cerr << "Import list removed: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateDataTarget:
	{	function = "CreateDataTarget";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// list ID 
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to data target list
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createDataTarget(myDb, mySegment, objName, &myDataTarget, msg);
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDataTarget);
			cerr << "DataTarget obj created for " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccDeleteDataTarget:
	{	function = "DeleteDataTarget";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// list ID 
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = deleteDataTarget(myDb, objName, msg);
		if(!err) {
			cerr << "DataTarget deleted: " << objName << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDataTargetAttr:
	{	function = "GetDataTargetAttr";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// list ID 
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to data target list
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())		// refernce for data target attrs
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDataTargetAttr(myDb, objName, &myDataTarget, names, msg);
		if(!err) {
			cerr << "Attributes retrieved for " << myDataTarget->UID() << endl;
			((IntegerVar *)objRef)->setValue((int)myDataTarget);
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetDataTargetAttr:
	{	function = "SetDataTargetAttr";
		std::cerr << wordF << function << endl;
		param.thread->clearLastError();		
		
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDataTarget = (daq::ddc::DdcDataTargetList *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// list ID 
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// array of attributes
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)objNameVar)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)objNameVar)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)objNameVar)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setDataTargetAttr(myDataTarget, objName, names, msg);
		if(!err) {
			cerr << "Attributes set for " << myDataTarget->UID() << endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

  }		// end switch()
  
  return &bitError;
}


bool	ddccDtExternHdl::getDtAttrs(const daq::ddc::DdcDtApplication* apDt, std::vector<std::string>& attr, std::string& errMsg)
{
	errMsg = "OK";
	
	attr.push_back(apDt->UID());
	attr.push_back(apDt->get_Name());
	attr.push_back(apDt->get_Parameters());
	attr.push_back(apDt->get_DaqDataReqServer());
	return false;
}

bool	ddccDtExternHdl::setDtAttrs(daq::ddc::DdcDtApplication* apDt, std::vector<std::string> attr, std::string& errMsg)
{
	bool	success = true;
	errMsg = "OK";
	
	success = apDt->set_Name(attr[0]);
	if(!success) {
		errMsg = "Cannot set name for " + apDt->get_Name();
		return true;
	}
	else {
		success = apDt->set_Parameters(attr[1]);
		if(!success) {
			errMsg = "Cannot set Parameters for " + apDt->get_Name();
			return true;
		}
		else {
			success = apDt->set_DaqDataReqServer(attr[2]);
			if(!success) {
				errMsg = "Cannot set DaqDataReqServer for " + apDt->get_Name();
				return true;
			}
		}
	}
	return false;
}

bool	ddccDtExternHdl::createDtAppl(Configuration* db, const daq::core::Segment* s, std::string name, 
						const daq::ddc::PvssWorkstation* ws, const daq::ddc::DdcCtApplication* ct, 
						daq::ddc::DdcDtApplication** dt, std::string& errMsg)
{
	bool	success = true;
	std::vector<const daq::ddc::DdcDtApplication *> dtList;
	std::vector<const daq::ddc::DdcDtApplication *>::iterator	it;
	db->get(dtList);
	for(it = dtList.begin(); it != dtList.end(); it++)
		if((*it)->UID() == name) {
			errMsg = "DDC-DT application with this name already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcDtApplication* newDt = db->create<daq::ddc::DdcDtApplication>(*s, name);
	if(newDt) {
		*dt = (daq::ddc::DdcDtApplication *)newDt;
		(*dt)->set_Name(name);
		(*dt)->set_DCSStationToConnect(ws);
		std::vector<const daq::core::BaseApplication *> aa;
		aa.push_back((const daq::core::BaseApplication *)ct);
		(*dt)->set_InitializationDependsFrom(aa);
		const daq::core::Binary* exe = db->get<daq::core::Binary>(DDCDT_PROGRAM);
		if(!exe) {
			errMsg = "There is no Binary ";
			errMsg += DDCDT_PROGRAM;
			return true;
		}
		if(!((*dt)->set_Program(exe))) {
			errMsg = "Cannot set Program for " + name;
			return true;
		}
//		const daq::core::Tag* tag = db->get<daq::core::Tag>(EXPLICIT_TAG);
//		if(!tag) {
//			errMsg = "There is no Tag ";
//			errMsg += EXPLICIT_TAG;
//			return true;
//		}
		const daq::core::Tag* tag = 0;
		if(!((*dt)->set_ExplicitTag(tag))) {
			errMsg = "Cannot set Tag for " + name;
			return true;
		}
		else 
			cerr << name << ": Explicit tag is cleaned" << endl;
		if(!((*dt)->set_InitTimeout(0))) {
			errMsg = "Cannot clear InitTimeout " + name;
			return true;
		}
		
		
		const std::vector<const daq::core::Application *>&	objRefList = s->get_Applications();
		// refer to DDC appl from Segment
		std::vector<const daq::core::Application *> new_appls(objRefList);
		new_appls.push_back(newDt);
		success = ((daq::core::Segment *)s)->set_Applications(new_appls);
		if(!success) 
			errMsg = "Cannot modify Applications relationship of " + s->get_Name();
		else 
			errMsg = "OK";
	}
	else {
		errMsg = "Cannot create DdcDtApplication " + name;
		success = false;
	}
	
	return(!success);
}


bool	ddccDtExternHdl::getAllDataTargets(Configuration* db, std::vector<std::string>& names, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcDataTargetList *>	targets;
	std::vector<const daq::ddc::DdcDataTargetList *>::iterator	it;
	
	names.erase(names.begin(), names.end());
	db->get(targets);
	for(it = targets.begin(); it != targets.end(); it++)
		names.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}


bool	ddccDtExternHdl::getExport(daq::ddc::DdcDtApplication* dt, std::vector<std::string>& names, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcDataTargetList *>&	targets = dt->get_DcsExport();
	std::vector<const daq::ddc::DdcDataTargetList *>::const_iterator	it;
	
	names.erase(names.begin(), names.end());
	for(it = targets.begin(); it != targets.end(); it++)
		names.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}


bool	ddccDtExternHdl::getImport(daq::ddc::DdcDtApplication* dt, std::vector<std::string>& names, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcDataTargetList *>&	targets = dt->get_DcsImport();
	std::vector<const daq::ddc::DdcDataTargetList *>::const_iterator	it;
	
	names.erase(names.begin(), names.end());
	for(it = targets.begin(); it != targets.end(); it++)
		names.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}


bool	ddccDtExternHdl::addExportList(Configuration* db, daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcDataTargetList *>&	lisOfRef = dt->get_DcsExport();
	
	std::vector<const daq::ddc::DdcDataTargetList *> newLists(lisOfRef);
	
	std::vector<const daq::ddc::DdcDataTargetList *>	targets;
	std::vector<const daq::ddc::DdcDataTargetList *>::iterator	it;
	
	db->get(targets);
	for(it = targets.begin(); it != targets.end(); it++) 
		if((*it)->UID() == name) {
			newLists.push_back(*it);
			break;
		}
	if(dt->set_DcsExport(newLists)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot set new DcsExport for " + dt->get_Name();
	return true;
}

bool	ddccDtExternHdl::removeExportList(daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcDataTargetList *>&	lisOfRef = dt->get_DcsExport();
	std::vector<const daq::ddc::DdcDataTargetList *>::iterator	it;
	
	std::vector<const daq::ddc::DdcDataTargetList *> newLists(lisOfRef);
	
	for(it = newLists.begin(); it != newLists.end(); it++)
		if((*it)->UID() == name) {
			newLists.erase(it);
			break;
		} 
	if(dt->set_DcsExport(newLists)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot remove DcsExportList " + name;
	return true;
}


bool	ddccDtExternHdl::addImportList(Configuration* db, daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcDataTargetList *>&	lisOfRef = dt->get_DcsImport();
	
	std::vector<const daq::ddc::DdcDataTargetList *> newLists(lisOfRef);
	
	std::vector<const daq::ddc::DdcDataTargetList *>	targets;
	std::vector<const daq::ddc::DdcDataTargetList *>::iterator	it;
	
	db->get(targets);
	for(it = targets.begin(); it != targets.end(); it++) 
		if((*it)->UID() == name) {
			newLists.push_back(*it);
			break;
		}
	if(dt->set_DcsImport(newLists)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot set new DcsImport for " + dt->get_Name();
	return true;
}

bool	ddccDtExternHdl::removeImportList(daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg)
{
	const std::vector<const daq::ddc::DdcDataTargetList *>&	lisOfRef = dt->get_DcsImport();
	std::vector<const daq::ddc::DdcDataTargetList *>::iterator	it;
	
	std::vector<const daq::ddc::DdcDataTargetList *> newLists(lisOfRef);
	
	for(it = newLists.begin(); it != newLists.end(); it++)
		if((*it)->UID() == name) {
			newLists.erase(it);
			break;
		} 
	if(dt->set_DcsImport(newLists)) {
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot remove DcsImportList " + name;
	return true;
}


bool	ddccDtExternHdl::createDataTarget(Configuration* db, const daq::core::Segment* s, std::string name, 
							daq::ddc::DdcDataTargetList** dtl, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcDataTargetList *> dtList;
	std::vector<const daq::ddc::DdcDataTargetList *>::iterator	it;
	db->get(dtList);
	for(it = dtList.begin(); it != dtList.end(); it++)
		if((*it)->UID() == name) {
			errMsg = "DataTargetList with this name already exists in DB";
			return true;
		}
	
	const daq::ddc::DdcDataTargetList* newList = db->create<daq::ddc::DdcDataTargetList>(*s, name);
	if(newList) {
		if(!((daq::ddc::DdcDataTargetList*)newList)->set_ISServer(DATASERVER_DEFAULT)) {
			errMsg = "Cannot set ISServer for " + name;
			return true;
		}
		else {
			*dtl = (daq::ddc::DdcDataTargetList *)newList;
			errMsg = "OK";
		}
	}
	else {
		errMsg = "Cannot create DataTargetList " + name;
		return true;
	}
	return false;
}

bool	ddccDtExternHdl::deleteDataTarget(Configuration* db, std::string name, std::string& errMsg)
{
	std::vector<const daq::ddc::DdcDataTargetList *>	targets;
	std::vector<const daq::ddc::DdcDataTargetList *>::const_iterator it;
	
	db->get(targets);
	for(it = targets.begin(); it != targets.end(); it++) {
		if((*it)->UID() == name)  	{		// list to be deleted
		    daq::ddc::DdcDataTargetList * l = const_cast<daq::ddc::DdcDataTargetList *>(*it);
			if(db->destroy(*l)) {
			    errMsg = "OK";
				return false;
			}
			else {
				errMsg = "Impossible to delete DataTargetList " + name;
				cerr << errMsg << endl;
				return true;
			}
		}
	}
	errMsg = "No " + name + " to delete it";
	return true;
}

bool	ddccDtExternHdl::getDataTargetAttr(Configuration* db, std::string name, daq::ddc::DdcDataTargetList** dtl, 
							std::vector<std::string>& attrs, std::string& errMsg)
{
	const daq::ddc::DdcDataTargetList* targetList = db->get<daq::ddc::DdcDataTargetList>(name);
	
	if(!targetList) {
		errMsg = "DataTargetList " + name + " absent in DB";
		cerr << errMsg << endl;
		return true;
	}
	else {
			*dtl = (daq::ddc::DdcDataTargetList *)targetList;
			attrs.erase(attrs.begin(), attrs.end());
			attrs.push_back(targetList->get_ISServer());
			const std::vector<std::string>& datapoints = targetList->get_Datapoints();
			for(unsigned int i=0; i < datapoints.size(); i++)
				attrs.push_back(datapoints[i]);
	}
	errMsg = "OK";
	return false;
}

bool	ddccDtExternHdl::setDataTargetAttr(daq::ddc::DdcDataTargetList* dtl, std::string server, 
								std::vector<std::string> attrs, std::string& errMsg)
{
	bool	success;
	
	success = dtl->set_ISServer(server);
	if(!success) {
		errMsg = "Cannot set ISServer for " + dtl->UID();
		return true;
	}
	success = dtl->set_Datapoints(attrs);
	if(!success) {
		errMsg = "Cannot set datapoints for " + dtl->UID();
		return true;
	}
	errMsg = "OK";
	return false;
}
