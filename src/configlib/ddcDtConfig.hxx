#ifndef _DDC_DT_EXTERNHDL_H_
#define _DDC_DT_EXTERNHDL_H_

#include <BaseExternHdl.hxx>

class ddccDtExternHdl : public BaseExternHdl
{
public:
    // List of user defined functions
    // Internal use only
    enum 
    {
		F_ddccGetDtAttrs = 0,
		F_ddccSetDtAttrs,
		F_ddccCreateDt,
		F_ddccGetAllDataTargets,
		F_ddccGetExport,
		F_ddccGetImport,
		F_ddccAddExportList,
		F_ddccRemoveExportList,
		F_ddccAddImportList,
		F_ddccRemoveImportList,
		F_ddccCreateDataTarget,
		F_ddccDeleteDataTarget,
		F_ddccGetDataTargetAttr,
		F_ddccSetDataTargetAttr,
   };

    // Descritption of user defined functions. 
    // Used by libCtrl to identify function calls
    static FunctionListRec  fnList[];

    ddccDtExternHdl(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : BaseExternHdl(nextHdl, funcCount, fnList) {}
    
    // Execute user defined function
    virtual const Variable *execute(ExecuteParamRec &param);

private:
	void 	showError(ExecuteParamRec &param, std::string& function);
	
	bool	getDtAttrs(const daq::ddc::DdcDtApplication *, std::vector<std::string>&, std::string& errMsg);
	bool	setDtAttrs(daq::ddc::DdcDtApplication *, std::vector<std::string>, std::string& errMsg);
	bool	createDtAppl(Configuration *, const daq::core::Segment *, std::string name, const daq::ddc::PvssWorkstation *,
						const daq::ddc::DdcCtApplication *, daq::ddc::DdcDtApplication**, std::string& errMsg);
	bool	removeDdcAppl(Configuration *, daq::core::Segment*, void * ddcAp, std::string apClass, std::string& errMsg);
	bool	getApplDdcWs(void * ddcAp, std::string apClass, std::string& name, std::string& errMsg);
	bool	getAllDataTargets(Configuration *, std::vector<std::string>&, std::string& errMsg);
	bool	getExport(daq::ddc::DdcDtApplication* dt, std::vector<std::string>& names, std::string& errMsg);
	bool	getImport(daq::ddc::DdcDtApplication* dt, std::vector<std::string>& names, std::string& errMsg);
	bool	assignDdcWs(Configuration *, daq::core::Application *, std::string name, std::string& errMsg);
	bool	addExportList(Configuration *, daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg);
	bool	removeExportList(daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg);
	bool	addImportList(Configuration *, daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg);
	bool	removeImportList(daq::ddc::DdcDtApplication* dt, std::string name, std::string& errMsg);
	bool	createDataTarget(Configuration *, const daq::core::Segment *, std::string name, 
							daq::ddc::DdcDataTargetList**, std::string& errMsg);
	bool	deleteDataTarget(Configuration *, std::string name, std::string& errMsg);
	bool	getDataTargetAttr(Configuration *, std::string name, daq::ddc::DdcDataTargetList**, 
							std::vector<std::string>& names, std::string& errMsg);
	bool	setDataTargetAttr(daq::ddc::DdcDataTargetList *, std::string name, 
							std::vector<std::string> names, std::string& errMsg);
};


// Create new ExternHdl. This must be global function named newExternHdl
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl);

#endif 
