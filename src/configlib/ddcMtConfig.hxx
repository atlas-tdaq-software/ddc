#ifndef _DDC_MT_EXTERNHDL_H_
#define _DDC_MT_EXTERNHDL_H_

#include <BaseExternHdl.hxx>

class ddccMtExternHdl : public BaseExternHdl
{
public:
    // List of user defined functions
    // Internal use only
    enum 
    {
	 	F_ddccCreateMt = 0,
		F_ddccGetMtAttrs, 
		F_ddccSetMtAttrs,
		F_ddccSetAllAlarms,
		F_ddccGetAlarms,
		F_ddccGetTextMsg,
		F_ddccGetTargetObj,
		F_ddccGetMsgObj,
		F_ddccCreateTargetObj,
		F_ddccCreateMsgObj,
		F_ddccDeleteTargetObj,
		F_ddccDeleteMsgObj,
		F_ddccGetTargetAttrs,
		F_ddccSetTargetAttrs,
		F_ddccGetMsgAttrs,
		F_ddccSetMsgAttrs
    };

    // Descritption of user defined functions. 
    // Used by libCtrl to identify function calls
    static FunctionListRec  fnList[];

    ddccMtExternHdl(BaseExternHdl *nextHdl, PVSSulong funcCount, FunctionListRec fnList[])
      : BaseExternHdl(nextHdl, funcCount, fnList) {}
    
    // Execute user defined function
    virtual const Variable *execute(ExecuteParamRec &param);

private:
	void 	showError(ExecuteParamRec &param, std::string& function);
	
	bool	createMtAppl(Configuration*, const daq::core::Segment*, std::string name, const daq::ddc::PvssWorkstation*, 
						const daq::ddc::DdcCtApplication*, daq::ddc::DdcMtApplication**, std::string& errMsg);
	bool	getMtAttrs(const daq::ddc::DdcMtApplication*, std::vector<std::string>& attr, std::string& errMsg);
	bool	setMtAttrs(daq::ddc::DdcMtApplication*, std::vector<std::string> attr, std::string& errMsg);
	bool	setAllAlarms(daq::ddc::DdcMtApplication*, bool yesNo, std::string& errMsg);
	bool	getAlarms(daq::ddc::DdcMtApplication*, std::vector<std::string>& names, std::string& errMsg);
	bool	getTextMsg(daq::ddc::DdcMtApplication*, std::vector<std::string>& names, std::string& errMsg);
	bool	getTargetObj(Configuration*, std::string name, daq::ddc::DdcTargetList**, std::string& errMsg);
	bool	getMsgObj(Configuration*, std::string name, daq::ddc::DdcTextMessage**, std::string& errMsg);
	bool	createTargetObj(Configuration*, const daq::ddc::DdcMtApplication*, std::string name, 
							daq::ddc::DdcTargetList**, std::string& errMsg);
	bool	createMsgObj(Configuration*, const daq::ddc::DdcMtApplication*, std::string name, 
							daq::ddc::DdcTextMessage**, std::string& errMsg);
	bool	deleteTargetObj(Configuration*, const daq::ddc::DdcMtApplication*, daq::ddc::DdcTargetList*, std::string& errMsg);
	bool	deleteMsgObj(Configuration*, const daq::ddc::DdcMtApplication*, daq::ddc::DdcTextMessage*, std::string& errMsg);
	bool	getTargetAttrs(daq::ddc::DdcTargetList*, std::vector<std::string>&, std::string& errMsg);
	bool	setTargetAttrs(daq::ddc::DdcTargetList*, const std::vector<std::string>&, std::string& errMsg);
	bool	getMsgAttrs(daq::ddc::DdcTextMessage*, std::vector<std::string>&, std::string& errMsg);
	bool	setMsgAttrs(daq::ddc::DdcTextMessage*, const std::vector<std::string>&, std::string& errMsg);
};


// Create new ExternHdl. This must be global function named newExternHdl
BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl);

#endif 
