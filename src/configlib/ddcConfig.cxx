// Ctrl extension for DDC configuration

#include <stdio.h>
#include <stdlib.h>
#include <CharString.hxx>
#include <BlobVar.hxx>
#include <CtrlExpr.hxx>
#include <ExprList.hxx>
#include <CtrlThread.hxx>
#include <IdExpr.hxx>
#include <Variable.hxx>
#include <AnyTypeVar.hxx>
#include <BitVar.hxx>
#include <CharVar.hxx>
#include <IntegerVar.hxx>
#include <UIntegerVar.hxx>
#include <FloatVar.hxx>
#include <TextVar.hxx>
#include <TimeVar.hxx>
#include <DynVar.hxx>
#include <ErrorVar.hxx>
#include <ExternData.hxx>
#include <ErrClass.hxx>
#include <BCTime.h>

#include <config/ConfigObject.h>
#include <config/Configuration.h>

#include <dal/Partition.h>
#include <dal/Segment.h>
#include <dal/Variable.h>
#include <dal/RunControlApplication.h>
#include <dal/Tag.h>

using namespace std;

#include <ddc/PvssWorkstation.h>
#include <ddc/DdcDtApplication.h>
#include <ddc/DdcMtApplication.h>
#include <ddc/DdcCtApplication.h>
#include <ddc/DdcDataTargetList.h>
#include <ddc/DdcTargetList.h>
#include <ddc/PvssDatapoint.h>
#include <ddc/DdcCommandDef.h>
#include <ddc/DdcTextMessage.h>

#include "ddcConfig-Defs.hxx"
#include "ddcConfig.hxx"


// Function description
FunctionListRec ddccExternHdl::fnList[] = 
{
  //  Return-Value    function name        parameter list               true == thread-save
  //  -------------------------------------------------------------------------------------
    {BIT_VAR, "ddccCreateDbKernel","(int& ref)"						, true},
	{BIT_VAR, "ddccOpenDb", "(int& conf, string& errMsg)", true},
	{BIT_VAR, "ddccGetDbEnvironment", "(int ref, int& refPvss, string& errMsg)", true},
	{BIT_VAR, "ddccGetPartitions", "(int conf, vector<string>& partitions, string& errMsg)", true},
	{BIT_VAR, "ddccGetPartObj", "(int conf, string partition, int& ref, string& errMsg)", true},
	{BIT_VAR, "ddccGetDdcSegments", "(int partitionRef, vector<string>& segments, string& errMsg)", true},
	{BIT_VAR, "ddccGetSegmentObject", "(int conf, string segment, int& ref, string& errMsg)", true},
	{BIT_VAR, "ddccCreateNewDdcSegment", "(int conf, string db, int partitionRef, int pvssRef, string sName, string fileName, int& segment, string& errMsg)", true},
	{BIT_VAR, "ddccAddDdcSegment", "(int conf, string db, int pRef, string name, vector<string>& sRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetSegmentTag", "(int partitionRef, int segmentRef, string& tag, string& errMsg)", true},
	{BIT_VAR, "ddccSetSegmentTag", "(int conf, int segmentRef, string tag, string& errMsg)", true},
	{BIT_VAR, "ddccGetPvssWs", "(int conf, vector<string>& sRef, string name, int& wsRef, string& errMsg)", true},
	{BIT_VAR, "ddccCreatePvssWs", "(int conf, int segmentRef, string name, int& wsRef, string& errMsg)", true},
	{BIT_VAR, "ddccRemoveSegment", "(int conf, string db, int pRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccGetPvssWsAttributes", "(int& wsRef, vector<string> attr, string& errMsg)", true},
	{BIT_VAR, "ddccSetPvssWsAttributes", "(int& wsRef, vector<string> attr, string& errMsg)", true},
	{BIT_VAR, "ddccDeletePvssWs", "(int conf, string name, string& errMsg)", true},
	{BIT_VAR, "ddccGetDdcWs", "(int conf, vector<string>& sRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetDdcWsAttributes", "(int conf, string name, vector<string>& sRef, string& errMsg)", true},
	{BIT_VAR, "ddccCreateDdcWs", "(int conf, int segmentRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccDeleteDdcWs", "(int conf, string name, string& errMsg)", true},
 	{BIT_VAR, "ddccSaveDb", "(int conf, string& errMsg)", true},
	{BIT_VAR, "ddccGetApplList", "(int conf, int segmentRef, string class, vector<string>& appls, vector<string>& ws, string& errMsg)", true},
	{BIT_VAR, "ddccGetDdcApplObject", "(int conf, int segmentRef, string class, string name, int& apRef, string& errMsg)", true},
	{BIT_VAR, "ddccSetPvssWs", "(int apRef, string class, int pvssWsRef, string& errMsg)", true},
	{BIT_VAR, "ddccGetConnOps", "(int apRef, string class, vector<string>& ops, string& errMsg)", true},
	{BIT_VAR, "ddccSetConnOps", "(int apRef, string class, vector<string>& ops, string& errMsg)", true},
	{BIT_VAR, "ddccRemoveDdcAppl", "(int conf, int segmentRef, int apRef, string class, string& errMsg)", true},
	{BIT_VAR, "ddccGetApplDdcWs", "(int apRef, string class, string& name, string& errMsg)", true},
	{BIT_VAR, "ddccAssignDdcWs", "(int conf, int apRef, string name, string& errMsg)", true},
	{BIT_VAR, "ddccCloseDb", "(int conf, string& errMsg)", true}
};


BaseExternHdl *newExternHdl(BaseExternHdl *nextHdl)
{
  // Calculate number of functions defined
  PVSSulong funcCount = sizeof(ddccExternHdl::fnList)/sizeof(ddccExternHdl::fnList[0]);  
  
  // now allocate the new instance
  ddccExternHdl *hdl = new ddccExternHdl(nextHdl, funcCount, ddccExternHdl::fnList);

  return hdl;
}

void ddccExternHdl::showError(ExecuteParamRec &param, std::string& function)
{
	ErrClass  errClass(
			ErrClass::PRIO_WARNING, ErrClass::ERR_PARAM, ErrClass::ARG_MISSING,
			"ddccExternHdl", function.c_str(), "missing or wrong argument data");

	// Remember error message. 
	// In the Ctrl script use getLastError to retreive the error message
	param.thread->appendLastError(&errClass);
	return;
}

//------------------------------------------------------------------------------
// This function is called every time we use ddccXXX in a Ctrl script.
// The argument is an aggregaton of function name, function number, 
// the arguments passed to the Ctrl function, the "thread" context 
// and user defined data.
const Variable *ddccExternHdl::execute(ExecuteParamRec &param)
{
  // We return a pointer to a static variable. 
  static IntegerVar integerVar;
  static BitVar 	bitError;
	bool 	err;
	std::string		function;
	const std::string wordF("Function ");
  // A pointer to one "argument". Any input argument may be an exprssion
  // like "a ? x : y" instead of a simple value.
  CtrlExpr   *expr;
	
	const Variable*		objRef;
	const Variable*		objRef2;
	const Variable* 	errMsg;
	const Variable*		dynStringRef;
	const Variable*		dynStringRef2;

	const Variable*		dbPtrVar;
	const Variable*		objNameVar;
	
	::Configuration*		myDb;
	daq::core::Variable*	myPvssPath;
	daq::core::Partition*	myPartition;
	daq::core::Segment*		mySegment;
	daq::core::Application*	myAppl;
	daq::ddc::PvssWorkstation*	myPvssWs;
	void*					myDdcAppl;
	
	std::string			msg;
	std::string			dbName;
	std::string			objName;
	std::string			fName;
	bool				yesNo;
	std::vector<std::string>	names;
	std::vector<std::string>	targets;
	unsigned int	vectorLen;
	int			dummyValue = 0;		// to drop together with case F_ddccCreateDbKernel
	
  // switch / case on function numbers
  switch (param.funcNum)
  {
    case F_ddccCreateDbKernel:
	{	function = "CreateDbKernel";
	// To be dropped after corresponding correction of Editor' script
		param.thread->clearLastError();
		if (!param.args || !(expr = param.args->getFirst())
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			// Return (true) to indicate error
			std::cerr << "Error 0" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		// Call function implementation
		((IntegerVar *)objRef)->setValue(dummyValue);
		std::cerr << "Dummy OKS Implementation built" << std::endl;
		bitError.setValue(false);
		return &bitError;
	}	
	
	case F_ddccOpenDb:
	{	function = "OpenDb";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();

		if (!param.args || !(expr = param.args->getFirst())	// refernce for Configuration
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		// Call function implementation
		err = openDb(&myDb, msg);

		// Pass result to Ctrl
		((TextVar *)errMsg)->setValue(msg.c_str());
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDb);
			std::cerr << "DB " << dbName << " is opened" << std::endl;
		}
		bitError.setValue(false);
		return &bitError;
	}

	case F_ddccGetDbEnvironment:
	{	function = "GetDbEnvironment";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
		if (!param.args || !(expr = param.args->getFirst())
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		dbName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for PVSS_PATH
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for bool newEnv
		 || !(objRef2 = expr->getTarget(param.thread))
		 ||	(objRef2->isA() != BIT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		// Call function implementation
		err = getDbEnv(myDb, dbName, &myPvssPath, &yesNo, msg);

		// Pass result to Ctrl
		((TextVar *)errMsg)->setValue(msg.c_str());
		if(!err) {
			std::cerr << "Variable retrieved" << std::endl;
			((IntegerVar *)objRef)->setValue((int)myPvssPath);
			((BitVar *)objRef2)->setValue(yesNo);
		}
		bitError.setValue(false);
		return &bitError;
	}

	case F_ddccGetPartitions:
	{	function = "GetPartitions";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
		if (!param.args || !(expr = param.args->getFirst())
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for Configuration
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		err = getPartitions(myDb, names, msg);

		// Pass result to Ctrl
		unsigned int len = names.size();
		unsigned int i;
		TextVar			toJoin;
		std::cerr << "Partitions presented:" << std::endl;
		for(i=0; i<len; i++) {
			toJoin.setValue(names[i].c_str());
			((DynVar *)dynStringRef)->append(toJoin);
			std::cerr << toJoin.getValue() << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		return &bitError;
	}

	case F_ddccGetPartObj:
	{	function = "GetPartObj";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
		if (!param.args || !(expr = param.args->getFirst())
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to Partition object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		err = getPartitionObj(myDb, objName, &myPartition, msg);
		
		((TextVar *)errMsg)->setValue(msg.c_str());
		if(!err) {
			std::cerr << "Partition object found for " << objName << std::endl;
			((IntegerVar *)objRef)->setValue((int)myPartition);
		}
		return &bitError;
	}
	
	case F_ddccGetDdcSegments:
	{	function = "GetDdcSegments";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
		if (!param.args || !(expr = param.args->getFirst())
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPartition = (daq::core::Partition *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for segment List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		err = getDdcSegments(myPartition, names, msg);

		// Pass result to Ctrl
		if(!err) {
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			cerr << "DDC segments found: ";
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
				cerr << toJoin.getValue() << " ";
			}
			std::cerr << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());

		return &bitError;
	}
	case F_ddccGetSegmentObj:
	{	function = "GetSegmentObj";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
		
		if (!param.args || !(expr = param.args->getFirst())
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to Segment object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		err = getSegmentObj(myDb, objName, &mySegment, msg);

		// Pass result to Ctrl
		if(!err) {
			cerr << "Segment object found for " << objName << std::endl;
			((IntegerVar *)objRef)->setValue((int)mySegment);
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
				
		return &bitError;
	}
	
	case F_ddccCreateNewDdcSegment:
	{	function = "CreateNewDdcSegment";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
		
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		dbName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference for partition
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPartition = (daq::core::Partition *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for PVSS_PATH
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssPath = (daq::core::Variable *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 6" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference to Segment object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 7" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())			// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 8" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		err = createDdcSegment(myDb, dbName, myPartition, myPvssPath, 
								objName, fName, &mySegment, msg);

		// Pass result to Ctrl
		if(!err) 
			((IntegerVar *)objRef)->setValue((int)mySegment);
		((TextVar *)errMsg)->setValue(msg.c_str());
				
		return &bitError;
	}

	case F_ddccAddDdcSegment:
	{	function = "AddDdcSegment";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		dbName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference for partition
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPartition = (daq::core::Partition *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for segment List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 6" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------

		err = addDdcSegment(myDb, dbName, myPartition, fName, names, msg);

		// Pass result to Ctrl
		if(!err) {
			unsigned int 	i;
			unsigned int 	len = names.size();
			TextVar			toJoin;
			std::cerr << "Segments presented now:";
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
				std::cerr << " " << toJoin.getValue();
			}
			std::cerr << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
				
		return &bitError;
	}

	case F_ddccGetSegmentTag:
	{	function = "GetSegmentTag";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for partition
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPartition = (daq::core::Partition *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())		// refernce for Tag value
		 || !(objNameVar = expr->getTarget(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getSegmentTag(myPartition, mySegment, objName, msg);

		if(!err) {
			std::cerr << "Found platform is " << objName << std::endl;
			((TextVar *)objNameVar)->setValue(objName.c_str());
		}
		else
			((TextVar *)objNameVar)->setValue("");
			
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetSegmentTag:
	{	function = "SetSegmentTag";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// Tag name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setSegmentTag(myDb, mySegment, objName, msg);

		if(!err) 
			std::cerr << "Platform " << objName << " is set for " << mySegment->get_Name() << std::endl;
			
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetPvssWs:
	{	function = "GetPvssWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for segment List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())			// connected Workstation name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to PvssWs object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getPvssWs(myDb, names, objName, &myPvssWs, msg);
		unsigned int 	i;
		unsigned int 	len = names.size();
		TextVar			toJoin;
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myPvssWs);
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreatePvssWs:
	{	function = "CreatePvssWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to PvssWs object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createPvssWs(myDb, mySegment, objName, &myPvssWs, msg);
		if(!err) {
			std::cerr << "Created PvssWorkstation " << objName << std::endl;
			((IntegerVar *)objRef)->setValue((int)myPvssWs);
		}
		
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccRemoveSegment:
	{	function = "RemoveSegment";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		dbName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference for partition
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPartition = (daq::core::Partition *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = removeSegment(myDb, dbName, myPartition, objName, msg);
		if(!err) {
			std::cerr << "Segment " << objName << " removed" << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetPvssWsAttributes:
	{	function = "GetPvssWsAttributes";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for PvssWs
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssWs = (daq::ddc::PvssWorkstation *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for attribute List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getPvssWsAttr(myPvssWs, names, msg);
		if(!err) {
			unsigned int len = names.size();
			unsigned int i;
			TextVar			toJoin;
			std::cerr << "PvssWS attributes:" << std::endl;
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
				std::cerr << toJoin.getValue() << std::endl;
			}
		}
		else {
			std::cerr << "Unsuccessful getting attributes for: " << myPvssWs->get_Name() << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetPvssWsAttributes:
	{	function = "SetPvssWsAttributes";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for PvssWs
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssWs = (daq::ddc::PvssWorkstation *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// array of attributes
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)objNameVar)->getArrayLength();
		names.erase(names.begin(), names.end());
		names.push_back(((TextVar*)((DynVar *)objNameVar)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			names.push_back(((TextVar*)((DynVar *)objNameVar)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setPvssWsAttr(myPvssWs, names, msg);
		if(!err) {
			std::cerr << "Attributes set for  " << myPvssWs->get_Name() << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccDeletePvssWs:
	{	function = "DeletePvssWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = deletePvssWs(myDb, objName, msg);
		if(!err)
			std::cerr << "PvssWorkstation deleted from DB: " << objName << std::endl;
		
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDdcWs:
	{	function = "GetDdcWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for segment List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDdcWs(myDb, names, msg);
		unsigned int 	i;
		unsigned int 	len = names.size();
		TextVar			toJoin;
		if(!err) {
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDdcWsAttributes:
	{	function = "GetDdcWsAttributes";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for segment List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDdcWsAttr(myDb, objName, names, msg);
		unsigned int 	i;
		unsigned int 	len = names.size();
		TextVar			toJoin;
		if(!err) {
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCreateDdcWs:
	{	function = "CreateDdcWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// segment file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = createDdcWs(myDb, mySegment, objName, msg);
		if(!err) 
			std::cerr << "Created DdcWorkstation: " << objName << std::endl;
		
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccDeleteDdcWs:
	{	function = "DeleteDdcWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = deleteDdcWs(myDb, objName, msg);
		if(!err)
			std::cerr << "DDC Workstation deleted from DB: " << objName << std::endl;
		
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSaveDb:
	{	function = "SaveDb";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = !(myDb->commit());
		if(!err) {
			std::cerr << "Changes commited" << std::endl;
			((TextVar *)errMsg)->setValue("OK");
		}
		else 
			((TextVar *)errMsg)->setValue("Error of commit changes");
		
		return &bitError;
	}

	case F_ddccGetApplList:
	{	function = "GetApplList";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DB file name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for application List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for segment List
		 || !(dynStringRef2 = expr->getTarget(param.thread))
		 ||	(dynStringRef2->isA() != DYNTEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 6" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getApplList(myDb, mySegment, objName, names, targets, msg);
		unsigned int 	i;
		unsigned int 	len = names.size();
		TextVar			toJoin;
		if(!err) {
			for(i=0; i<len; i++) {
				toJoin.setValue(names[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
			for(i=0; i<len; i++) {
				toJoin.setValue(targets[i].c_str());
				((DynVar *)dynStringRef2)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetDdcApplObject:
	{	function = "GetDdcApplObject";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl class name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// application name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// reference to DDC' appl object
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 6" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getDdcApObj(myDb, mySegment, fName, objName, &myDdcAppl, msg);
		if(!err) {
			((IntegerVar *)objRef)->setValue((int)myDdcAppl);
			std::cerr << "DDC application obj found for " << objName << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetPvssWs:
	{	function = "SetPvssWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcAppl = (void *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl class name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())			// reference for DDC application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myPvssWs = (daq::ddc::PvssWorkstation *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setStationToConnect(myDdcAppl, fName, myPvssWs, msg);
		if(!err) {
			std::cerr << "PVSS station set for " << fName << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetConnOps:
	{	function = "GetConnOps";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcAppl = (void *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl class name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for option List
		 || !(dynStringRef = expr->getTarget(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 		{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getConnOps(myDdcAppl, fName, targets, msg);
		if(!err) {
			std::cerr << "Connection options got for " << fName << std::endl;
			unsigned int 	i;
			unsigned int 	len = targets.size();
			TextVar			toJoin;
			for(i=0; i<len; i++) {
				toJoin.setValue(targets[i].c_str());
				((DynVar *)dynStringRef)->append(toJoin);
			}
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccSetConnOps:
	{	function = "SetConnOps";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC application
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcAppl = (void *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// appl class name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		fName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())		// refernce for option List
		 || !(dynStringRef = expr->evaluate(param.thread))
		 ||	(dynStringRef->isA() != DYNTEXT_VAR)  ) 		{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		vectorLen = ((DynVar *)dynStringRef)->getArrayLength();
		targets.erase(targets.begin(), targets.end());
		targets.push_back(((TextVar*)((DynVar *)dynStringRef)->getFirstVar())->getValue());
		for(unsigned int i=1; i < vectorLen; i++)
			targets.push_back(((TextVar*)((DynVar *)dynStringRef)->getNextVar())->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = setConnOps(myDdcAppl, fName, targets, msg);
		if(!err) {
			std::cerr << "Connection options set for " << fName << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccRemoveDdcAppl:
	{	function = "RemoveDdcAppl";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for segment
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		mySegment = (daq::core::Segment *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DDC appl
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcAppl = (void *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// class name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 5" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = removeDdcAppl(myDb, mySegment, myDdcAppl, objName, msg);
		if(!err) {
			std::cerr << "DDC application object removed" << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccGetApplDdcWs:
	{	function = "GetApplDdcWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DDC appl
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDdcAppl = (void *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// class name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for ws name
		 || !(objRef = expr->getTarget(param.thread))
		 ||	(objRef->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = getApplDdcWs(myDdcAppl, objName, fName, msg);
		if(!err) {
			std::cerr << "DdcWs found: " << fName << std::endl;
			((TextVar *)objRef)->setValue(fName.c_str());
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccAssignDdcWs:
	{	function = "AssignDdcWs";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myAppl = (daq::core::Application *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())			// DDC ws name
		 || !(objNameVar = expr->evaluate(param.thread))
		 ||	(objNameVar->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 3" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		objName = ((TextVar *)objNameVar)->getValue();

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 4" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		err = assignDdcWs(myDb, myAppl, objName, msg);
		if(!err) {
			std::cerr << "DdcWs assigned for " << myAppl->get_Name() << std::endl;
		}
		((TextVar *)errMsg)->setValue(msg.c_str());
		
		return &bitError;
	}

	case F_ddccCloseDb:
	{	function = "CloseDb";
		std::cerr << wordF << function << std::endl;
		param.thread->clearLastError();
	
		if (!param.args || !(expr = param.args->getFirst())			// reference for DB
		 || !(dbPtrVar = expr->evaluate(param.thread))
		 ||	(dbPtrVar->isA() != INTEGER_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 1" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}
		myDb = (::Configuration *)(((IntegerVar *)dbPtrVar)->getValue());

		if (!param.args || !(expr = param.args->getNext())	// refernce for errMsg
		 || !(errMsg = expr->getTarget(param.thread))
		 ||	(errMsg->isA() != TEXT_VAR)  ) 			{
			showError(param, function);
			std::cerr << "Error 2" << std::endl;
			bitError.setValue (true); 
			return &bitError;
		}

		// ----------------------------------------------------
		// Function call
		// -----------------------------------------------------
		
		delete myDb;
		std::cerr << "Database is closed" << std::endl;
		((TextVar *)errMsg)->setValue("OK");
		
		return &bitError;
	}

  }		// end switch()
  
  return &bitError;
}	

bool  ddccExternHdl::openDb(::Configuration** myDb, string& errMsg)
{
	*myDb = new Configuration("");
	if ( (*myDb)->loaded() == false ) {
		errMsg = "Cannot open " + (*myDb)->get_impl_spec();
		delete *myDb;
		return true;
	}
	errMsg = "OK";
	return false;
}

//bool	ddccExternHdl::getDbEnv(Configuration* db, string dbName, daq::core::Variable** ldPath, 
bool	ddccExternHdl::getDbEnv(Configuration* db, string dbName,
								daq::core::Variable** pvssPath, bool* newEnv, std::string& errMsg)
{
	*newEnv = false;
//	const string	ldName("LD_LIBRARY_PATH");
//	const daq::core::Variable* ldRef = db->get<daq::core::Variable>(ldName);
//	if(ldRef) 
//		*ldPath = (daq::core::Variable *)ldRef;
//	else {
//		ldRef = db->create<daq::core::Variable>(dbName, ldName);
//		if(ldRef == 0) {
//			errMsg = "Cannot create environment LD_LIBRARY_PATH";
//			return true;
//		}
//		else {
//			*newEnv = true;
//			*ldPath = (daq::core::Variable *)ldRef;
//			(*ldPath)->set_Variable(ldName);
//			(*ldPath)->set_Value("${"+ldName+"}");
//		}
//	}
	const string	pvName("PVSS_PATH");
	const daq::core::Variable* pvRef = db->get<daq::core::Variable>(pvName);
	if(pvRef) 
		*pvssPath = (daq::core::Variable *)pvRef;
	else {
		pvRef = db->create<daq::core::Variable>(dbName, pvName);
		if(pvRef == 0) {
			errMsg = "Cannot create environment PVSS_PATH";
			return true;
		}
		else {
			*newEnv = true;
			*pvssPath = (daq::core::Variable *)pvRef;
			(*pvssPath)->set_Name(pvName);
			(*pvssPath)->set_Value("${"+pvName+"}");
		}
	}
	errMsg = "OK";
	return false;
}


bool  ddccExternHdl::getPartitions(Configuration* db, vector<string>& partitions, string& errMsg)
{
	std::vector<const daq::core::Partition *>	objRefList;
	std::vector<const daq::core::Partition *>::const_iterator	it;
	
	db->get(objRefList);
	if(objRefList.size() == 0) {
		errMsg = "No partition defined in the database";
		return true;
	}
	partitions.erase(partitions.begin(), partitions.end());
	for(it=objRefList.begin(); it!=objRefList.end(); it++)
		partitions.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}


bool	ddccExternHdl::getPartitionObj(Configuration* db, string name, daq::core::Partition ** p, string& errMsg)
{
	const daq::core::Partition* partition = db->get<daq::core::Partition>(name);
	
	if(!partition) {
		errMsg = "No partition " + name + " defined";
		return true;
	}
	*p = (daq::core::Partition *)partition;
	errMsg = "OK";
	return false;
}


bool	ddccExternHdl::getDdcSegments(daq::core::Partition* p, vector<string>& segments, string& errMsg)
{
	std::vector<const daq::core::Segment *>::const_iterator it;
	
	const std::vector<const daq::core::Segment *>&	objRefList = p->get_Segments();
	
	segments.erase(segments.begin(), segments.end());
	for(it=objRefList.begin(); it!=objRefList.end(); it++)
		segments.push_back((*it)->UID());
	errMsg = "OK";
	return false;
}

bool	ddccExternHdl::getSegmentObj(Configuration* db, string name, daq::core::Segment ** s, string& errMsg)
{
	const daq::core::Segment* segment = db->get<daq::core::Segment>(name);

	if(!segment) {
		errMsg = "No segment " + name + " defined";
		return true;
	}
	*s = (daq::core::Segment *)segment;
	errMsg = "OK";
	return false;
}


bool	ddccExternHdl::createDdcSegment(Configuration* db, std::string dbFile, daq::core::Partition* p, 
										const daq::core::Variable* pvssPath,
										std::string name, std::string& fileName, 
										daq::core::Segment ** s, std:: string& errMsg)
{
	bool		success = true;
	FILE*		oldFile;
	std::list<std::string> includes;
	std::vector<const daq::core::Segment *>::const_iterator it;
	
	oldFile = fopen(fileName.c_str(), "r");
	if(oldFile) {
		errMsg = "Segment file " + fileName + " already exists";
		fclose(oldFile);
		success = false;
	}
	else {
		const std::vector<const daq::core::Segment *>&	objRefList = p->get_Segments();
	
		for(it = objRefList.begin(); it != objRefList.end(); it++)
			if((*it)->get_Name() == name) {
				success = false;
				break;
			}
		if(!success) 
			errMsg = "Segment " + name + " does already present!";
		else {
			string ddcSchemaPath = getenv((const char *)"TDAQ_INST_PATH");
			ddcSchemaPath += "/share/data/ddc/dcs_ddc.schema.xml";
			includes.push_back(ddcSchemaPath);
			success = db->create("", fileName, includes);
			if(!success) {
				errMsg = "Impossible to create database file " + fileName;
			}
			else {
				// create Segment
				std::cerr << "Creating segment " << name << " in " << fileName << std::endl;
				const daq::core::Segment* newSegment = db->create<daq::core::Segment>(fileName, name);
				if(!newSegment) {	// not successfully created
					errMsg = "Cannot create segment " + name;
					success = false;
				}
				else {
					((daq::core::Segment *)newSegment)->set_Name(name);
					success = db->commit();
					if(!success) {
						std::cerr << "Cannot save DB" << std::endl;
						errMsg = "Impossible to save new segment file " + fileName;
						return(true);
					}
					std::cerr << "Including segment " << name << " into partition file " << dbFile << std::endl;
					*s = (daq::core::Segment *)newSegment;
					// segment references for needed environment
					std::vector<const daq::core::Parameter *> env;
					env.push_back(pvssPath);
					(*s)->set_ProcessEnvironment(env);
					// refer segment file from partition file
					success = db->add_include(dbFile, fileName);
					if(!success) 
						errMsg = "Cannot include " + fileName + " into the database";
					else {
						std::cerr << "... done" << std::endl;
						// refer to Segment from Partition
						std::vector<const daq::core::Segment *> new_segments(objRefList);
						new_segments.push_back(newSegment);
						success = p->set_Segments(new_segments);
						if(!success) 
							errMsg = "Cannot modify Segments relationship of " + p->get_Name();
					}
				}
			}
		}
	}
	if(success)
		errMsg = "OK";
	return(!success);
}

bool	ddccExternHdl::addDdcSegment(Configuration *db, std::string dbFile, daq::core::Partition* p, 
			std::string ddcSegmentFile, std::vector<std::string>& allSegments, std::string& errMsg)
{
	bool	success = true;
	std::vector<const daq::core::Segment *>	newSegments;
	std::vector<const daq::core::Segment *>::iterator its;
	std::list<std::string>	includeList;
	std::list<std::string>::iterator it;
	
	success = db->get_includes(dbFile, includeList);
	if(!success)
		errMsg = "Impossible to get list of include files";
	else {
		for(it = includeList.begin(); it != includeList.end(); it++) {
			if((*it) == ddcSegmentFile) {
				success = false;
				break;
			}
		}
		if(!success)
			errMsg = ddcSegmentFile + " already loaded";
		else {
			success = db->add_include(dbFile, ddcSegmentFile);
			if(!success)
				errMsg = "Cannot include " + ddcSegmentFile + " into " + dbFile;
			else {
				db->get(newSegments);
				for(its = newSegments.begin(); its != newSegments.end(); its++) {
					if(((*its)->get_Name().find("nfrastructure") != std::string::npos)
					|| ((*its)->UID() == "setup")) {
						newSegments.erase(its);
						break;
					}
				}
				
				success = p->set_Segments(newSegments);
				if(!success) 
					errMsg = "Cannot modify Segments relationship of " + p->get_Name();
				else
					for(its = newSegments.begin(); its != newSegments.end(); its++)
						allSegments.push_back((*its)->get_Name());
			}
		}
	}
	if(success)
		errMsg = "OK";

	return(!success);
}

bool	ddccExternHdl::getSegmentTag(const daq::core::Partition* p, const daq::core::Segment* s, 
							std::string& tagName, std::string& errMsg)
{
	errMsg = "OK";
	const std::vector<const daq::core::Tag *>& 	tags = s->get_DefaultTags();
	std::vector<const daq::core::Tag *>::const_iterator	it;
	
	for(it = tags.begin(); it != tags.end(); it++) {
		if((*it)->get_HW_Tag() == HWTAG_DEFAULT) {
			tagName = (*it)->UID();
			return false;
		}
	}
	
	const std::vector<const daq::core::Tag *>& 	ptags = p->get_DefaultTags();
	for(it = ptags.begin(); it != ptags.end(); it++) {
		if((*it)->get_HW_Tag() == HWTAG_DEFAULT) {
			tagName = (*it)->UID();
			return false;
		}
	}
	
	tagName = "";
	return false;
}

bool	ddccExternHdl::setSegmentTag(Configuration* db, daq::core::Segment* s, std::string tagName, std::string& errMsg)
{
	const daq::core::Tag*	tag = db->get<daq::core::Tag>(tagName);
	
	if(!tag) {
		errMsg = "Tag " + tagName + " is not found in the database";
		return true;
	}
	
	const std::vector<const daq::core::Tag *>&	tags = s->get_DefaultTags();
	std::vector<const daq::core::Tag *>::iterator	it;
	std::vector<const daq::core::Tag *> newTags(tags);
	
	for(it = newTags.begin(); it != newTags.end(); it++) {
		if((*it)->get_HW_Tag() == HWTAG_DEFAULT) {
			newTags.erase(it);
			break;
		}
	}
	newTags.push_back(tag);
	if(!(s->set_DefaultTags(newTags))) {
		errMsg = "Impossible to set tags for " + s->get_Name();
		return true;
	}
	errMsg = "OK";
	return false;
}


bool	ddccExternHdl::getPvssWs(Configuration* db, std::vector<std::string>& s, std::string name,
								 daq::ddc::PvssWorkstation** ws, std::string& errMsg) 
{
	std::vector<const daq::ddc::PvssWorkstation *> wsList;
	std::vector<const daq::ddc::PvssWorkstation *>::const_iterator it;
	std::string	thisName;
	
	*ws = 0;
	db->get(wsList);
	s.erase(s.begin(), s.end());
	for(it = wsList.begin(); it != wsList.end(); it++) {
		thisName = (*it)->get_Name();
		s.push_back(thisName);
		if(thisName == name)  			// connected ws
			*ws = (daq::ddc::PvssWorkstation*)(*it);
	}
	
	errMsg = "OK";
	return(false);
}

bool	ddccExternHdl::createPvssWs(Configuration* db, const daq::core::Segment* s, std::string wsName, 
									daq::ddc::PvssWorkstation** ws, std::string& errMsg)
{
	const daq::ddc::PvssWorkstation* newWs = db->create<daq::ddc::PvssWorkstation>(*s, wsName);
	if(newWs) {
		((daq::ddc::PvssWorkstation*)newWs)->set_Name(wsName);
		*ws = (daq::ddc::PvssWorkstation *)newWs;
		errMsg = "OK";
		return false;
	}
	errMsg = "Cannot create PvssWorkstation " + wsName;
	return true;
}

bool	ddccExternHdl::removeSegment(Configuration* db, std::string dbFile, const daq::core::Partition* p, 
									std::string sName,std::string &errMsg)
{
	bool	success = true;
	bool	found = false;
	std::vector<const daq::core::Segment *>::iterator its;
	
	errMsg = "OK";
	const std::vector<const daq::core::Segment *>&	objRefList = p->get_Segments();
	std::vector<const daq::core::Segment *> new_segments(objRefList);
	for(its = new_segments.begin(); its != new_segments.end(); its++) {
		if((*its)->get_Name() == sName) {
			new_segments.erase(its);
			found = true;
			break;
		}
	}
	if(!found) {
		errMsg = "There is no " + sName + " segment in partition " + p->get_Name();
		success = false;
	}
	else {
		success = ((daq::core::Partition *)p)->set_Segments(new_segments);
		if(!success) {
			errMsg = "Impossible to set new Segments for " + p->get_Name();
			return(true);
		}

		std::list<std::string>	includeList;
		std::list<std::string>::iterator it;
		std::string	segmentFile = sName + ".data.xml";
			
		success = db->get_includes(dbFile, includeList);
		if(!success)
			errMsg = "Impossible to get list of include files";
		else {
			for(it = includeList.begin(); it != includeList.end(); it++) {
				if(it->find(segmentFile) != std::string::npos) {
					std::cerr << "Removing segment file " << (*it) << std::endl;
					success = db->remove_include(dbFile, *it);
					if(!success)
						errMsg = "Unsuccessful `remove_include`";
					break;
				}
			}
		}
	}
	return(!success);
}

bool	ddccExternHdl::setPvssWsAttr(const daq::ddc::PvssWorkstation* ws,
							const std::vector<std::string>& attrs, std::string& errMsg)
{
	bool	success = true;
	unsigned int i;
	unsigned int port;
	
	if(attrs.size() != 5) {
		errMsg = "Incorrect number of atributes";
		success = false;
	}
	else {
		i = 0;
		success = ((daq::ddc::PvssWorkstation *)ws)->set_Name(attrs[i++]);
		if(success) {
			success = ((daq::ddc::PvssWorkstation *)ws)->set_HW_Tag(attrs[i++]);
		}
		else {
			success = ((daq::ddc::PvssWorkstation *)ws)->set_HW_Tag(HWTAG_DEFAULT);
		}
		if(success)
			success = ((daq::ddc::PvssWorkstation *)ws)->set_RLogin(attrs[i++]);
		if(success) {
			port = atoi(attrs[i++].c_str());
			if(port == 0)
				port = EVENTPORT_DEFAULT;
			success = ((daq::ddc::PvssWorkstation *)ws)->set_PvssEventPort(port);
		}
		if(success) {
			port = atoi(attrs[i++].c_str());
			if(port == 0)
				port = DATAPORT_DEFAULT;
			success = ((daq::ddc::PvssWorkstation *)ws)->set_PvssDataPort(port);
		}
		if(!success) {
			char num[8];
			sprintf(num, "%d", i-1);
			string snum(num);
			errMsg = "Cannot set parameter " + snum;
		}
		else
			errMsg = "OK";
	}
	return(!success);
}

bool	ddccExternHdl::getPvssWsAttr(const daq::ddc::PvssWorkstation* ws,
									   std::vector<std::string>& attrs, std::string& errMsg)
{
	unsigned int port;
	char		 singleAttr[8];

	attrs.push_back(ws->get_Name());
	attrs.push_back(ws->get_HW_Tag());
	attrs.push_back(ws->get_RLogin());
	
	port = ws->get_PvssEventPort();
	sprintf(singleAttr, "%d", port);
	attrs.push_back(singleAttr);
	port = ws->get_PvssDataPort();
	sprintf(singleAttr, "%d", port);
	attrs.push_back(singleAttr);
	
	errMsg = "OK";
	
	return(false);
}

bool	ddccExternHdl::deletePvssWs(Configuration* db, std::string name, std::string& errMsg)
{
	std::vector<const daq::ddc::PvssWorkstation *>	wsList;
	std::vector<const daq::ddc::PvssWorkstation *>::const_iterator it;
	
	db->get(wsList);
	for(it = wsList.begin(); it != wsList.end(); it++) {
		if((*it)->get_Name() == name)  	{		// ws to be deleted
		    daq::ddc::PvssWorkstation * ws = const_cast<daq::ddc::PvssWorkstation *>(*it);
			if(db->destroy(*ws)) {
			    errMsg = "OK";
				return false;
			}
			else {
				errMsg = "Impossible to delete PvssWorkstation " + name;
				return true;
			}
		}
	}
	errMsg = "No " + name + " to delete it";
	return true;
}

bool	ddccExternHdl::getDdcWs(Configuration* db, std::vector<std::string>& s, std::string& errMsg)
{
	std::vector<const daq::core::Computer *> wsList;
	std::vector<const daq::core::Computer *>::const_iterator it;
	
	db->get(wsList);
	for(it = wsList.begin(); it != wsList.end(); it++) {
		if((*it)->class_name() != "PvssWorkstation") {
			s.push_back((*it)->get_Name());
		}
	}
	
	errMsg = "OK";
	return(false);
}

bool	ddccExternHdl::getDdcWsAttr(Configuration* db, std::string name, 
									std::vector<std::string>& attrs, std::string& errMsg)
{
	std::vector<const daq::core::Computer *> wsList;
	std::vector<const daq::core::Computer *>::const_iterator it;
	
	db->get(wsList);
	for(it = wsList.begin(); it != wsList.end(); it++) {
		if((*it)->get_Name() == name) {
			daq::core::Computer*	ws = const_cast<daq::core::Computer *>(*it);
			attrs.push_back(ws->get_Name());
			attrs.push_back(ws->get_HW_Tag());
			attrs.push_back(ws->get_RLogin());
			
			errMsg = "OK";
			return(false);
		}
	}
	
	errMsg = name + "DDC workstation is not found in DB";
	return(true);
}

bool	ddccExternHdl::createDdcWs(Configuration* db, const daq::core::Segment* s, 
									std::string name, std::string& errMsg)
{
	bool	success = true;
		
	std::vector<const daq::core::Computer *>	comps;
	std::vector<const daq::core::Computer *>::iterator	it;
	db->get(comps);
	for(it = comps.begin(); it != comps.end(); it++)
		if((*it)->get_Name() == name) {
			errMsg = "Workstation with this name already exists in DB";
			return true;
		}
	
	const daq::core::Computer* newWs = db->create<daq::core::Computer>(*s, name);
	if(newWs) {
		success = ((daq::core::Computer*)newWs)->set_Name(name);
		if(!success)
			errMsg = "Cannot set Name for " + name;
		else {
			success = ((daq::core::Computer*)newWs)->set_HW_Tag(HWTAG_DEFAULT);
			if(!success)
				errMsg = "Cannot set HW_Tag for " + name;
			else {
				success = ((daq::core::Computer*)newWs)->set_RLogin(RLOGIN_DEFAULT);
				if(!success) 
					errMsg = "Cannot set RLogin for " + name;
				else {
					errMsg = "OK";
				}
			}
		}
	}
	else {
		errMsg = "Cannot create DDC Linux Workstation " + name;
		success = false;
	}
	
	return(!success);
}

bool	ddccExternHdl::deleteDdcWs(Configuration* db, std::string name, std::string& errMsg)
{
	std::vector<const daq::core::Computer *>	wsList;
	std::vector<const daq::core::Computer *>::const_iterator it;

	db->get(wsList);
	for(it = wsList.begin(); it != wsList.end(); it++) {
		if((*it)->get_Name() == name)  	{		// ws to be deleted
		    if(!isUsedByApplications(db, *it)) {
				std::string	pName;
				if(!isDefaultHost(db, *it, pName)) {
					daq::core::Computer * ws = const_cast<daq::core::Computer *>(*it);
					if(db->destroy(*ws)) {
					    errMsg = "OK";
						return false;
					}
					else {
						errMsg = "Impossible to delete DdcWorkstation " + name;
						return true;
					}
				}
				else {
					errMsg = name + " is default host of partition " + pName;
					return true;
				}
			}
			else {
				errMsg = name + " is used by an application of this partition";
				return true;
			}
		}
	}
	
	return true;
}

bool	ddccExternHdl::isUsedByApplications(Configuration* db, const daq::core::Computer* ws)
{
	std::vector<const daq::core::Application *> 				apps;
	std::vector<const daq::core::Application *>::const_iterator	it;
	
	db->get(apps);
	for(it = apps.begin(); it != apps.end(); it++) {
		if((*it)->get_RunsOn() == ws)
			return true;
	}
	return false;
}

bool	ddccExternHdl::isDefaultHost(Configuration* db, const daq::core::Computer* ws, std::string& pName)
{
	std::vector<const daq::core::Partition *>					pList;
	std::vector<const daq::core::Partition *>::const_iterator	it;
	
	db->get(pList);
	for(it = pList.begin(); it != pList.end(); it++) {
		if((*it)->get_DefaultHost() == ws) {
			pName = (*it)->get_Name();
			return true;
		}
	}
	return false;
}


bool	ddccExternHdl::getApplList(Configuration * db, const daq::core::Segment* s, const std::string applClass,
									std::vector<std::string>& names, std::vector<std::string>& targetWs, std::string& errMsg)
{
	errMsg = "OK";
	const daq::ddc::PvssWorkstation* ws = 0;
	
	const std::vector<const daq::core::Application *>& apList = s->get_Applications();
	std::vector<const daq::core::Application *>::const_iterator it;
	
	names.erase(names.begin(), names.end());
	targetWs.erase(targetWs.begin(), targetWs.end());
	if(apList.size() != 0) {
		for(it = apList.begin(); it != apList.end(); it++) {
			if((*it)->class_name() == applClass) {
				names.push_back((*it)->get_Name());
				if(applClass.find("DdcDt") != std::string::npos) {
					const daq::ddc::DdcDtApplication* ap = db->cast<daq::ddc::DdcDtApplication, daq::core::Application>(*it);
					ws = ap->get_DCSStationToConnect();
				}
				else
					if(applClass.find("DdcMt") != std::string::npos) {
						const daq::ddc::DdcMtApplication* ap = db->cast<daq::ddc::DdcMtApplication, daq::core::Application>(*it);
						ws = ap->get_DCSStationToConnect();
					}
					else
						if(applClass.find("DdcCt") != std::string::npos) {
							const daq::ddc::DdcCtApplication* ap = db->cast<daq::ddc::DdcCtApplication, daq::core::Application>(*it);
							ws = ap->get_DCSStationToConnect();
						}
						else {
							errMsg = "Inner error of getApplList for " + (*it)->class_name();
							return true;
						}
				if(ws)
					targetWs.push_back(ws->get_Name());
				else
					targetWs.push_back("NOT DEFINED");
			}
		}
	}
	return false;
}

bool	ddccExternHdl::getDdcApObj(Configuration* db, const daq::core::Segment* s, const std::string applClass, 
							const std::string name, void ** apRef, std::string& errMsg)
{
	errMsg = "OK";
		
	const std::vector<const daq::core::Application *>& apList = s->get_Applications();
	std::vector<const daq::core::Application *>::const_iterator it;
	
	for(it = apList.begin(); it != apList.end(); it++) {
		if((*it)->class_name() == applClass) {
			if((*it)->get_Name() == name) {
				if(applClass.find("DdcDt") != std::string::npos) {
					const daq::ddc::DdcDtApplication* ap = db->cast<daq::ddc::DdcDtApplication, daq::core::Application>(*it);
					*apRef = (void *)ap;
					return false;
				}
				else {
					if(applClass.find("DdcMt") != std::string::npos) {
						const daq::ddc::DdcMtApplication* ap = db->cast<daq::ddc::DdcMtApplication, daq::core::Application>(*it);
						*apRef = (void *)ap;
						return false;
					}
					else
						if(applClass.find("DdcCt") != std::string::npos) {
							const daq::ddc::DdcCtApplication* ap = db->cast<daq::ddc::DdcCtApplication, daq::core::Application>(*it);
							*apRef = (void *)ap;
							return false;
						}
						else {
							errMsg = "Inner error of getApplObj for " + (*it)->class_name();
							std::cerr << errMsg << std::endl;
							return true;
						}
				}
			}
		}
	}
	errMsg = "No DDc application " + name;
	return true;
}

bool	ddccExternHdl::setStationToConnect(void* apv, const std::string applClass,
								const daq::ddc::PvssWorkstation* myPvssWs, std::string& errMsg)
{
	errMsg = "OK";
	bool	success = true;
		
	if(applClass.find("DdcDt") != std::string::npos) {
		daq::ddc::DdcDtApplication* ap = static_cast<daq::ddc::DdcDtApplication *>(apv);
		success = ap->set_DCSStationToConnect(myPvssWs);
		if(!success) {
			errMsg = "Cannot set PvssWorkstation for " + ap->get_Name();
			return true;
		}
	}
	else {
		if(applClass.find("DdcMt") != std::string::npos) {
			daq::ddc::DdcMtApplication* ap = static_cast<daq::ddc::DdcMtApplication *>(apv);
			success = ap->set_DCSStationToConnect(myPvssWs);
			if(!success) {
				errMsg = "Cannot set PvssWorkstation for " + ap->get_Name();
				return true;
			}
		}
		else {
			if(applClass.find("DdcCt") != std::string::npos) {
				daq::ddc::DdcCtApplication* ap = static_cast<daq::ddc::DdcCtApplication *>(apv);
				success = ap->set_DCSStationToConnect(myPvssWs);
				if(!success) {
					errMsg = "Cannot set PvssWorkstation for " + ap->get_Name();
					std::cerr << errMsg << std::endl;
					return true;
				}
			}
		}
	}
	return false;
}


bool	ddccExternHdl::getConnOps(void* apv, const std::string applClass,
							std::vector<std::string>& ops, std::string& errMsg)
{
	errMsg = "OK";
	char		value_char[8];
	string		value_str;
		
	if(applClass.find("DdcDt") != std::string::npos) {
		daq::ddc::DdcDtApplication* ap = static_cast<daq::ddc::DdcDtApplication *>(apv);
		sprintf(value_char, "%lu", ap->get_connectDelay());
		value_str = value_char;
		ops.push_back(value_str);
		sprintf(value_char, "%lu", ap->get_connectRetries());
		value_str = value_char;
		ops.push_back(value_str);
	}
	else {
		if(applClass.find("DdcMt") != std::string::npos) {
			daq::ddc::DdcMtApplication* ap = static_cast<daq::ddc::DdcMtApplication *>(apv);
			sprintf(value_char, "%lu", ap->get_connectDelay());
			value_str = value_char;
			ops.push_back(value_str);
			sprintf(value_char, "%lu", ap->get_connectRetries());
			value_str = value_char;
			ops.push_back(value_str);
		}
		else {
			if(applClass.find("DdcCt") != std::string::npos) {
				daq::ddc::DdcCtApplication* ap = static_cast<daq::ddc::DdcCtApplication *>(apv);
				sprintf(value_char, "%lu", ap->get_connectDelay());
				value_str = value_char;
				ops.push_back(value_str);
				sprintf(value_char, "%lu", ap->get_connectRetries());
				value_str = value_char;
				ops.push_back(value_str);
			}
		}
	}
	return false;
}


bool	ddccExternHdl::setConnOps(void* apv, const std::string applClass,
							std::vector<std::string>& ops, std::string& errMsg)
{
	errMsg = "OK";
	bool	success = true;
		
	if(applClass.find("DdcDt") != std::string::npos) {
		daq::ddc::DdcDtApplication* ap = static_cast<daq::ddc::DdcDtApplication *>(apv);
		success = ap->set_connectDelay(atoi(ops[0].c_str()));
		if(success)
			success = ap->set_connectRetries(atoi(ops[1].c_str()));
		if(!success)
			errMsg = "Could not set connection options";
	}
	else {
		if(applClass.find("DdcMt") != std::string::npos) {
			daq::ddc::DdcMtApplication* ap = static_cast<daq::ddc::DdcMtApplication *>(apv);
			success = ap->set_connectDelay(atoi(ops[0].c_str()));
			if(success)
				success = ap->set_connectRetries(atoi(ops[1].c_str()));
			if(!success)
				errMsg = "Could not set connection options";
		}
		else {
			if(applClass.find("DdcCt") != std::string::npos) {
				daq::ddc::DdcCtApplication* ap = static_cast<daq::ddc::DdcCtApplication *>(apv);
				success = ap->set_connectDelay(atoi(ops[0].c_str()));
				if(success)
					success = ap->set_connectRetries(atoi(ops[1].c_str()));
				if(!success)
					errMsg = "Could not set connection options";
			}
		}
	}
	return(!success);
}


bool	ddccExternHdl::removeDdcAppl(Configuration* db, daq::core::Segment* s, void* apv, 
										std::string applClass, std::string& errMsg)
{
	bool	success = true;
	errMsg = "OK";
	daq::core::Application* thisAp = (daq::core::Application *)apv;
	
	const std::vector<const daq::core::Application *>&	objRefList = s->get_Applications();
	std::vector<const daq::core::Application *>::iterator	it;
	// remove DDC appl from Segment
	std::vector<const daq::core::Application *> new_appls(objRefList);
	for(it = new_appls.begin(); it != new_appls.end(); it++) {
		if((*it)->UID() == thisAp->UID()) {
			new_appls.erase(it);
			break;
		}
	}
	success = ((daq::core::Segment *)s)->set_Applications(new_appls);
	if(!success) {
		errMsg = "Cannot remove DDC application from segment";
		return true;
	}
	
	if(applClass.find("DdcDt") != std::string::npos) {
		daq::ddc::DdcDtApplication* ap = static_cast<daq::ddc::DdcDtApplication *>(apv);
		success = db->destroy(*ap);
	}
	else {
		if(applClass.find("DdcMt") != std::string::npos) {
			daq::ddc::DdcMtApplication* ap = static_cast<daq::ddc::DdcMtApplication *>(apv);
			success = db->destroy(*ap);
		}
		else {
			if(applClass.find("DdcCt") != std::string::npos) {
				daq::ddc::DdcCtApplication* ap = static_cast<daq::ddc::DdcCtApplication *>(apv);
				success = db->destroy(*ap);
			}
			else {
				errMsg = "Improper application class " + applClass;
				return true;
			}
		}
	}
	if(!success) 
		errMsg = "Cannot destroy " + applClass;
	return(!success);
}

bool	ddccExternHdl::getApplDdcWs(void * apv, std::string applClass, std::string& name, std::string& errMsg)
{
	errMsg = "OK";
	const daq::core::Computer*	ws;
	
	if(applClass.find("DdcDt") != std::string::npos) {
		daq::ddc::DdcDtApplication* ap = static_cast<daq::ddc::DdcDtApplication *>(apv);
		ws = ap->get_RunsOn();
	}
	else {
		if(applClass.find("DdcMt") != std::string::npos) {
			daq::ddc::DdcMtApplication* ap = static_cast<daq::ddc::DdcMtApplication *>(apv);
			ws = ap->get_RunsOn();
		}
		else {
			if(applClass.find("DdcCt") != std::string::npos) {
				daq::ddc::DdcCtApplication* ap = static_cast<daq::ddc::DdcCtApplication *>(apv);
				ws = ap->get_RunsOn();
			}
			else {
				errMsg = "Improper application class " + applClass;
				return true;
			}
		}
	}
	if(ws)
		name = ws->get_Name();
	else
		name = "";
	return false;
}


bool	ddccExternHdl::assignDdcWs(Configuration* db, daq::core::Application* ap, std::string name, std::string& errMsg)
{
	std::vector<const daq::core::Computer *> wsList;
	std::vector<const daq::core::Computer *>::const_iterator it;
	
	db->get(wsList);
	for(it = wsList.begin(); it != wsList.end(); it++) {
		if((*it)->get_Name() == name) {
			if(ap->set_RunsOn(*it)) {
				errMsg = "OK";
				return false;
			}
			else {
				errMsg = "Cannot assign DdcWs for " + ap->get_Name();
				return true;
			}
		}
	}
	errMsg = "Could not find object for " + name;
	return true;
}
