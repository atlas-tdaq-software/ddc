//
// This file contains definition of DdcDirectCommander class methods. 
// An object of this class is able to send a command to a certain
// PVSS system and get feedback
//							S. Khomoutnikov 
//							23.07.07
// See DdcDirectCommander.hxx to follow the history
//
//////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include <time.h>
#include <vector>

#include <dic.hxx>

using namespace std;

#include "DdcDirectCommander.hxx"


void DdcDirectCommanderInfo::rpcInfoHandler()
{
	int cmdResult = getInt();
	ddcCmdr->cmdrNotify(pvssHost, commandName, cmdResult, commandKeys);
	delete this;
	return;
}

	
void DdcDirectCommander::cmdrNotify(std::string host, std::string cmd, int response,
											std::vector<std::string> keyWords)
{
	time_t 		now = time(&now);
	cerr << "Command " << cmd;
	if(host != "")
		cerr << " for " << host;
	cerr << " is completed with result " 
		 << response << " at " << ctime(&now) << endl;
	for(unsigned int i=0; i<keyWords.size(); i++) 
		if(i==0) 
			cerr << keyWords[i];
		else
			cerr << " " << keyWords[i];
	cerr << endl;
}

	
bool DdcDirectCommander::launchCommand(DdcDirectCommand& command, vector<string>& keys)
{
	DimBrowser	browser;
	string 		cName = command.getCommName();
	string		pvssHost = command.getHostName();
	string		pvssHostName, pvssHostNameAlt;
	time_t 		now = time(&now);
	DdcDirectCommanderInfo* ntRpcInfo;
	
	string	dimDnsNode;
	bool	conf_error = false;
	if(getenv((const char*)"DIM_DNS_NODE") == 0) 
		conf_error = true;
	else {
		dimDnsNode = getenv((const char*)"DIM_DNS_NODE");
		if(dimDnsNode.size() == 0) 
			conf_error = true;
	}

	if(conf_error) {
		cerr << "DirectCommander_FATAL: Node for DIM Name Server (DIM_DNS_NODE environment) is not defined" << endl;
		return(false);
	}
	
	if(pvssHost != "") {
		if(pvssHost.find('.') != string::npos)
			pvssHostName = pvssHost.substr(0, pvssHost.find('.'));
		else
			pvssHostName = pvssHost;
		pvssHostNameAlt = pvssHostName;
		for(unsigned int i=0; i<pvssHostName.size(); i++) {
			pvssHostName[i] = tolower(pvssHostName[i]);
			pvssHostNameAlt[i] = toupper(pvssHostNameAlt[i]);
		}		
		// Alternative server names
		std::cerr << "Looking for " << pvssHostName << " or " 
				  << pvssHostNameAlt << std::endl;
		browser.getServers();
	
		// List of DIM servers found
//		std::cerr << "List of servers registered in DIM Name Server:" << std::endl;
		char*	server;
		char*	node;
		string	nodeString, serverString;
		bool	dcsFound = false;
		while(browser.getNextServer(server, node)) {
			nodeString = node;
			serverString = server;
//			cerr << "Server " << serverString << " at " << nodeString << endl;
			if(nodeString.find('.') != string::npos)
				nodeString = nodeString.substr(0, nodeString.find('.'));
			if(((nodeString == pvssHostName) || (nodeString == pvssHostNameAlt)) 
			&& (serverString != "DIS_DNS"))  {
				dcsFound = true;
				break;
			}
		}
		if(!dcsFound) {
			cerr << "DirectCommander_ERROR: DIM manager is not found on "
				 << pvssHost << endl ;
			cerr << "------ Check also DIM Name Server node (DIM_DNS_NODE) and DIM Name Server" << endl;
			return(false);
		}
	}
	
	cerr << "DirectCommander_INFO: Start execution of command " << cName;
	if(pvssHost != "")
		cerr << " for " << pvssHost;
	cerr << " at " << ctime(&now) << endl;
		
	// Set parameter values
	string	allPars = command.getParValue();
	string	dimCommandName = cName + ".parameters";
	if(DimClient::sendCommand((char *)dimCommandName.c_str(), (char *)allPars.c_str())) {
		// Set trigger (activating RPC DIM service)
		ntRpcInfo = new DdcDirectCommanderInfo(cName,command.getTimeout(),COMM_ERR_TIMEOUT,
											   pvssHost, keys, this);
		int	triggerFlag = 1;
		ntRpcInfo->setData(triggerFlag);
//		cerr << "Command " << cName << " is sent to " << pvssHost << endl;
		return(true);
	}
	else {
		cerr << "DirectCommand_ERROR: Could not set parameters for " << cName
			 << " command" << endl;
		return(false);
	}
}


void DdcDirectCommander::decodeError(int code, std::string &msg)
{
	char	msgText[32];
	
	switch(code) {
	case COMM_ERR_OK:
		msg = "OK";
		break;
	case COMM_ERR_UNDEF:
		msg = "Undefined state";
		break;
	case COMM_ERR_NAME:
		msg = "Invalid command name";
		break;
	case COMM_ERR_DCSDP:
		msg = "Invalid a DCS DP (DIM Service) definition";
		break;
	case COMM_ERR_TIMEOUT:
		msg = "Command execution timeout";
		break;
	case COMM_ERR_CONNECT:
		msg = "Communication fault (no connection to project)";
		break;
	case COMM_ERR_CONNBROKEN:
		msg = "Communication fault (connection broken)";
		break;
	case COMM_ERR_CONFIG:
		msg = "Invalid command configuration";
		break;
	case COMM_ERR_NTDESC:
		msg = "Invalid definition of an nt-command";
		break;
	case COMM_ERR_BUSY:
		msg = "Previous command has not ended";
		break;
	case COMM_ERR_DCSNOTREADY:
		msg = "DCS is not ready for data taking";
		break;
	default:
		sprintf(msgText, "user defined error %d", code);
		msg = msgText;
	}
}


