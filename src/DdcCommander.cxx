///////////////////////////////////////////////////////////////////////////
// This file contains definition of functions to issue 
// a non-transition command
//					
//									Created 17.01.03
//									by S.Khomutnikov
//
///////////////////////////////////////////////////////////////////////////

#include <string>

#include <ipc/core.h>
#include <is/namedinfo.h>
#include <ers/ers.h>
#include <owl/semaphore.h>

using namespace std;

#include "DdcCommander.hxx"
#include "DdcBenchmark.hxx"

/*
 * static map containing command response value
 * is accessed within ddcExecCommand and the command callback
 * execCommandCallback
 * has to be protected by mutex
 */
std::map<std::string,int> DdcCommander::commandResponse;


DdcCommander::DdcCommander(IPCPartition& p)
	: partition(p)	//, commandRunning(false) 
{ 
	receiver = new ISInfoReceiver(p);
}


DdcCommander::~DdcCommander()
{
  std::map<std::string,OWLSemaphore*>::iterator lock = commandLock.begin();
  for (;lock!=commandLock.end(); ++lock) {
    delete lock->second;
  }
}


void DdcCommander::notify(std::string ctrl, std::string cmd, int result)
{
//	std::cerr << "NT-Command " << cmd << " returned " << result << std::endl;
	;		// Default notify is dummy
}


/*
 * callback function for IS signal from DDC controller
 * if the DCS response was received
 */
void DdcCommander::commanderCallback(ISCallbackInfo* cbInfo)
{
	DdcCommander* 	commandSender = (DdcCommander*)(cbInfo->parameter());
	
	ddc::DdcNtCommandResponseInfoNamed	result(commandSender->getPartition(),(char*)cbInfo->name());
	result.value = 0;
	string	infoName(cbInfo->name());
	string	ctrlName;
	int 	response;
	string  ctrl;
	string	cmd = commandSender->cmdFromResponse(infoName, ctrl);
	
	if(cbInfo->reason() != ISInfo::Deleted) {
		if(cbInfo->type() == result.ISInfo::type()) {
			cbInfo->value(result);
			
//			switch(result.getValue()) {
			switch(result.value) {
		    case COMM_ERR_OK:
				std::cerr << "OK" << std::endl;
				break;
			case COMM_ERR_NAME:
				std::cerr << "INVALIDE COMMAND NAME" << std::endl;
				break;
			case COMM_ERR_DCSDP:
				std::cerr << "COMMAND NOT DEFINED PROPERLY IN DCS" << std::endl;
				break;
			case COMM_ERR_TIMEOUT:
				std::cerr << "TIMEOUT" << std::endl;
				break;
			case COMM_ERR_CONNECT:
				std::cerr << "PVSS IS UNAVAILABLE" << std::endl;
				break;
			case COMM_ERR_CONNBROKEN:
				std::cerr << "CONNECTION TO DCS LOST" << std::endl;
				break;
			case COMM_ERR_NTDESC:
				std::cerr << "INVALID COMMAND DEFINITION IN IS" << std::endl;
				break;
			case COMM_ERR_BUSY:
				std::cerr << "NOT ALLOWED WHILE INITIAL STATE or OTHER CMD EXCUTION" << std::endl;
				break;
			default:
				std::cerr << "USER'S ERROR STATUS" << std::endl;
			}
			
			try {
				commandSender->getReceiver()->unsubscribe(infoName);
			}
			catch(daq::is::Exception) {
				cerr << "Unsuccessful 'unsubscribe' for " << infoName << endl;
			}
//			response = result.getValue();			
			response = result.value;			
			try {
				result.remove();
			}
			catch(daq::is::Exception) {
				cerr << "DdcCommander: Could not remove NT-response" << endl;
			}
			
			commandSender->commandMutex.lock();
			commandResponse[infoName] = response;
			commandSender->commandMutex.unlock();

			commandSender->notify(ctrl, cmd, response);
		}
		else 
			cerr << "DdcCommander: Incorrect response. Assume "
				 << "an internal error" << endl;
	}
}


// To send command for an individual DDC controller
bool DdcCommander::ddcSendDaqCommand(Configuration* confDb, std::string ctrl, string daqCommandId)
{
	bool	success = true;
	
	string	commandName;
	string	commParameters;
	unsigned int timeout;
	
    const daq::ddc::DdcCommandDef* commDef = confDb->get<daq::ddc::DdcCommandDef>(daqCommandId);

	if(commDef == 0) {
		std::cerr << "DdcCommander_ERROR: Command " << daqCommandId << " is NOT found" << std::endl;
		success = false;
	}
	else {
		commandName = commDef->get_DcsCommand();
		commParameters = commDef->get_CommandParameters();
		timeout = commDef->get_Timeout();
		success = ddcSendCommand(ctrl, commandName, commParameters, timeout);
	}
	return(success);
}


bool DdcCommander::ddcSendCommand(string ctrl, string commandName, string commParameters,
								  unsigned int timeout, bool BM)
{
	string	commEntryName(COMMAND_SERVER);
	
	if(commandName != BM_COMMAND) {
		commEntryName = commEntryName + ".DdcNtCommand." + ctrl + "." + commandName;
	}
	else {
		commEntryName += ".";
		commEntryName += BM_COMMAND;
	}
	ERS_LOG("Sending direct command " << commEntryName);
	return(__ddcSendCommand(commEntryName, commandName, commParameters, timeout, BM)); 
}


// Technical sending nt-command with low level interface
bool DdcCommander::__ddcSendCommand(string& isEntryName, string& commandName, 
								   string& commParameters, unsigned int timeout,
								   bool BM)
{
	bool	ok = true;
	bool    notRemoved;
	
	if(BM) {
		DdcBenchmark::getControllerTimer().stop();
		DdcBenchmark::getIsTimer().start();
	}
 		
//	DdcNtCommand* command = new DdcNtCommand(partition, (char*)isEntryName.c_str());
	ddc::DdcNtCommandInfoNamed* command = new ddc::DdcNtCommandInfoNamed(partition, (char*)isEntryName.c_str());
	try {
		notRemoved = command->isExist();
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG("Looking 0 for " << isEntryName <<  " exception " << ex);
		ERS_LOG("Problem with IS server!");
		notRemoved = true;
	}
	if(notRemoved) {
		std::cerr << "DdcCommander_ERROR: Previous command for the same controller(s)" 
			 	  << " seems to be still executing" << std::endl;
		ok = false;
	}
	else {
//		command->setCommName(commandName);
		command->commandName = commandName;
		command->parameters = commParameters;
		command->timeout = timeout;
//		ERS_LOG("Checkin NtCommand " << isEntryName);
		try {
			command->checkin();
		}
		catch(daq::is::Exception &ex) {
			std::cerr << "DdcCommander_ERROR: " << ex << std::endl;
			std::cerr << "DdcCommander_ERROR: " << isEntryName
				 	  << " command sending fault. Be sure that command server is running "
					  << std::endl;
			ok = false;
		}
	}
	delete command;
	return(ok);
}


// Sending command with recieving response . This hides IS from the user
// Option 'wait' allowes waiting of completion of the previous command with 
// the same command name
int DdcCommander::ddcExecCommand(string ctrl, string commandName, string commParameters,
								  unsigned int timeout, bool wait)
{
	string isEntryName(COMMAND_SERVER);
	bool notRemoved;
	
	isEntryName = isEntryName + ".DdcNtCommand." + ctrl + "." + commandName;

	ddc::DdcNtCommandInfoNamed* command
					= new ddc::DdcNtCommandInfoNamed(partition, (char*)isEntryName.c_str());

	// use the semaphore to queue incoming commands
	// within the same Commander object
  
	if (wait) {
    	commandMutex.lock();
   		if (commandLock.find(isEntryName) == commandLock.end()) {
//    		ERS_LOG("DdcCommander_INFO: Inserting lock for " << commandName);
    		commandLock.insert(std::make_pair(isEntryName, (new OWLSemaphore())));
    		commandMutex.unlock();
    	}
    	else 
			if (commandLock[isEntryName]->try_wait()!=0) {
      			// check for previous ongoing command
      			ERS_LOG("DdcCommander_WARNING: previous command " << commandName << " did not finish"
						  << ", waiting for completion.");
      			commandMutex.unlock();
      			commandLock[isEntryName]->wait(); // wait for previous command to finish
    		}
    		else 
				commandMutex.unlock();
	}

	// check for running commands from different processes
	// 
	try {
		notRemoved = command->isExist();
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG("Looking for " << isEntryName <<  " exception " << ex);
		notRemoved = true;
	}
	if (notRemoved) {
    	ERS_LOG("DdcCommander_ERROR: Previous command " << commandName 
	    		  << " seems to be still executing");
  		delete command;
    	if(wait) {
      		commandLock[isEntryName]->post();
    	}
   		return COMM_ERR_BUSY;
	}

	// subscribe to IS callbacks to receive the answer to the command
	string responseName = makeResponseName(ctrl, commandName);
	std::string cmdServer(COMMAND_SERVER);

	commandMutex.lock();
	if (commandResponse.find(responseName) == commandResponse.end()) {
//		std::cerr << "DdcCommander__INFO: Creating response for "
//	      		  << responseName << std::endl;
    	commandResponse.insert(std::make_pair(responseName, COMM_ERR_UNDEF));
  	}
	else
		commandResponse[responseName] = COMM_ERR_UNDEF;
	commandMutex.unlock();
	
//	ERS_LOG("Subscribing for response of " << commandName << ": " << responseName);
   	try {
   		receiver->subscribe(responseName, commanderCallback, (void*)this);
   	}
   	catch(daq::is::Exception) {
   		ERS_LOG("DdcCommander_ERROR: Could not subscribe to response "
				  << responseName << " in IS!");
	    delete command;
    	return(COMM_ERR_CONNECT);
   	}
  
	// send command to IS
	//
	command->commandName = commandName;
	command->parameters = commParameters;
	command->timeout = timeout;
//	ERS_LOG("Checkin to IS " << isEntryName);
	try {
    	command->checkin();
	}
	catch(daq::is::Exception) {
    	ERS_LOG("DdcCommander_ERROR: " << isEntryName
	    		  << " command sending fault. Is the IS server running?");
	    delete command;
    	return(COMM_ERR_CONNECT);
	}
	
	// wait for answer,
	// the commandResponse is to be set within the callback function
	// execCommandCallback()
  
	int response = COMM_ERR_UNDEF;

    unsigned int timeoutCounter = (timeout+10)*100; // in units of 10 millisecs
    ERS_LOG(commandName << ": Waiting for reply..");
    while (response == COMM_ERR_UNDEF && timeoutCounter) {
      commandMutex.lock();
      response = commandResponse[responseName];
      commandMutex.unlock();

      usleep(10000);
      --timeoutCounter;
    }
//    ERS_LOG("..Received " << response << " for " << commandName);

    // Removing command 
    //
//   	ERS_LOG("DdcCommander_INFO: Removing " << commandName
//			  << " command from IS!");
	if (!ddcRemoveCommand(ctrl, commandName)) {
   		ERS_LOG("DdcCommander_ERROR: Could not remove " << commandName
				  << " command from IS!");
	}

    // if no response, callback has not been triggered,
    // removing response which is normally done in callback
    if (!timeoutCounter) {
      	ERS_LOG("DdcCommander_ERROR: No response from DDC controller for " << commandName
	   			  << " : command timed out.");
      	try {
			receiver->unsubscribe(responseName);
      	}
     	catch(daq::is::Exception) {}

// 		ERS_LOG("DdcCommander_INFO: Removing response of " << commandName << " from IS!");
     	ddc::DdcNtCommandResponseInfoNamed cmdResult(partition, responseName.c_str());
      	try {
			cmdResult.remove();
      	}
      	catch(daq::is::Exception) {
			ERS_LOG("DdcCommander_ERROR: Could not remove response from IS!");
		}
		notify(ctrl, commandName, COMM_ERR_TIMEOUT);
		response = COMM_ERR_TIMEOUT;
    }
	
	unsigned int waitingCounter = 200;
    if(wait) {
		
		while(waitingCounter) {
			try {
				notRemoved = command->isExist();
			}
			catch(daq::is::Exception &ex) {
				ERS_LOG("Looking for " << isEntryName <<  " exception " << ex);
				notRemoved = true;
			}
			if(notRemoved) {
				ERS_LOG("DdcCommander_WARNING: Wait removing " << commandName
				  << " command from IS");
				usleep(10000);
				--waitingCounter;
			}
			else
				break;
		}
		if(!waitingCounter)
      		ERS_LOG("DdcCommander_ERROR: Command " << commandName
	   			  << " is not removed from IS for 2 sec.");
 		
		commandMutex.lock();
  		commandLock[isEntryName]->post(); // unlock
  		commandMutex.unlock();
	}

	delete command;
	return(response);
}


bool DdcCommander::ddcRemoveCommand(string ctrl, string commName)
{
	bool			ok = true;
	string	commEntryName(COMMAND_SERVER);
	
	commEntryName = commEntryName + ".DdcNtCommand." + ctrl + '.' + commName;
	ddc::DdcNtCommandInfoNamed* command = new ddc::DdcNtCommandInfoNamed(partition, (char*)commEntryName.c_str());
//	ERS_LOG("Removing " << commEntryName << " from IS");
	try {
		command->remove();
	}
	catch(daq::is::RepositoryNotFound) {
		ERS_LOG("DdcCommander_ERROR: " << COMMAND_SERVER
			 	  << " IS server seems not to be running in " << partition.name());
		ok = false;
	}
	catch(daq::is::InvalidName) {
		ERS_LOG("DdcCommander_ERROR: InvalidName for removing "
				  << commEntryName);
		ok = false;
	}
	catch(daq::is::InfoNotFound) {}
	delete command;
	return(ok);
}


bool DdcCommander::ddcRemoveBMCommand()
{
	bool			ok = true;
	string	commEntryName(COMMAND_SERVER);
	
	commEntryName = commEntryName + ".";
	commEntryName += BM_COMMAND;
	ddc::DdcNtCommandInfoNamed* command = new ddc::DdcNtCommandInfoNamed(partition, (char*)commEntryName.c_str());
	try {
		command->remove();
	}
	catch(daq::is::RepositoryNotFound) {
		std::cerr << "DdcCommander_ERROR: " << COMMAND_SERVER
			 	  << " seems not to be running in " << partition.name() << std::endl;
		ok = false;
	}
	catch(daq::is::InvalidName) {
		std::cerr << "DdcCommander_ERROR: 'InvalidName" << std::endl;
		ok = false;
	}
	catch(daq::is::InfoNotFound) {}
	delete command;
	return(ok);
}


string	DdcCommander::makeResponseName(string ctrlName, string commandName)
{
	string	isEntry(COMMAND_SERVER);
	
	isEntry += '.'+ctrlName+'.'+commandName + ".response";
	return(isEntry);
}


string DdcCommander::makeResponseName(Configuration* confDb, string ctrlName, string dbCommandName)
{
	string	respName("");
	string	commandName;
	
    if(confDb != 0) {
		const daq::ddc::DdcCommandDef* commDef = confDb->get<daq::ddc::DdcCommandDef>(dbCommandName);	
	
		if(commDef == 0) 
			std::cerr << "DdcCommander_ERROR: Command " << dbCommandName 
					  << " is NOT found in configuration" << std::endl;
		else {
			commandName = commDef->get_DcsCommand();
			respName = makeResponseName(ctrlName, commandName);
		}
	}
	return(respName);
}


string DdcCommander::cmdFromResponse(std::string response, std::string& ctrl)
{
	string cmd = response.substr(response.find('.')+1);
	ctrl = cmd.substr(0, cmd.find('.'));
	cmd = cmd.substr(cmd.find('.')+1);
	return(cmd.substr(0, cmd.find('.')));
}


