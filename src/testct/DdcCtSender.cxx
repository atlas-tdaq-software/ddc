#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <unistd.h>

#include <ipc/core.h>
#include <ipc/partition.h>
#include <owl/semaphore.h>
#include <is/namedinfo.h>
#include <is/inforeceiver.h>
#include <cmdl/cmdargs.h>

using namespace std;

#include "../DdcCommand.hxx"
#include "../DdcCommander.hxx"
#include "DdcCtSender.hxx"


DdcCtSender::DdcCtSender(IPCPartition& p, ::Configuration* db) 
			: partition(p), commandAllowed(true), confDb(db)
{}


void callback(ISCallbackInfo* cbInfo)
{
	DdcCtSender* 	commandSender = (DdcCtSender*)(cbInfo->parameter());
	
	ddc::DdcNtCommandResponseInfoNamed	result(commandSender->getPartition(),(char*)cbInfo->name());
	string	infoName(cbInfo->name());
//	cerr << "DdcCtSender: Info name = " << infoName << endl;
	string	cmdName = commandSender->getCommand();
	string	ctrl = commandSender->getController();
	
	if(cbInfo->reason() != ISInfo::Deleted) {
		if(cbInfo->type() == result.ISInfo::type()) {
			cbInfo->value(result);
			cerr << "DdcCtSender: Result of the "
				 << cmdName << " command is ";
			switch(result.value) {
			case COMM_ERR_OK:
				cerr << "OK" << endl;
				break;
			case COMM_ERR_NAME:
				cerr << "INVALIDE COMMAND NAME" << endl;
				break;
			case COMM_ERR_DCSDP:
				cerr << "COMMAND NOT DEFINED PROPERLY IN DCS" << endl;
				break;
			case COMM_ERR_TIMEOUT:
				cerr << "TIMEOUT" << endl;
				break;
			case COMM_ERR_CONNECT:
				cerr << "PVSS IS UNAVAILABLE" << endl;
				break;
			case COMM_ERR_CONNBROKEN:
				cerr << "CONNECTION TO DCS LOST" << endl;
				break;
			case COMM_ERR_NTDESC:
				cerr << "INVALID COMMAND DEFINITION IN IS" << endl;
				break;
			case COMM_ERR_BUSY:
				cerr << "NOT ALLOWED WHILE INITIAL STATE or RUN CONTROL TRANSITION" << endl;
				break;
			default:
				cerr << "USER'S ERROR STATUS" << endl;
			}
			
			try {
//				cerr << "Unsubscribe for " << infoName << endl;
				commandSender->getReceiver()->unsubscribe(infoName);
			}
			catch(daq::is::SubscriptionNotFound &ex) {
				cerr << "Subscription not found" << endl;
			}
			catch(daq::is::Exception &ex) {}
			try {
				result.remove();
			}
			catch(daq::is::Exception &ex) {}
			
			if(cmdName == BM_COMMAND) {
				cerr << "Removing BM-command" << endl;
				commandSender->removeBMCommand();
			}
			else
				commandSender->removeCommandFromIs(ctrl, cmdName);
		}
		else 
			cerr << "DdcCtSender: Incorrect response. Assume "
				 << "an internal error" << endl;
	}
}

void DdcCtSender::removeBMCommand()
{
	commander->ddcRemoveBMCommand();
	commandAllowed = true;
}


void DdcCtSender::removeCommandFromIs(string ctrl, string cmdName)
{
	commander->ddcRemoveCommand(ctrl, cmdName);
	commandAllowed = true;
}


void DdcCommander::notify(string ctrl, string cmd, int response)
{
	cerr << "We notify you that the command " << cmd << " for " << ctrl
		 << " is completed with code: " << response << endl;
}

void DdcCtSender::inputCommandParameters(std::string cmd, std::string& params,
								 		 int& timeout)
{
	std::string	answer;
	char		timeBuf[16];
	int			intTime;
	
	std::cout << "Has the " << cmd << " any parameters (y/n)? > ";
	std::cin >> answer;
	if((answer[0] == 'y') || (answer[0] == 'Y')) {
		std::cout << "Type the command parameters (p1|p2|...) > ";
		std::cin >>  params;
	}
	else
		params = "";
	
	// Input the timeout value
	std::cout << "Type the timeout value (sec) > ";
	std::cin >>  timeBuf;
	intTime = atoi(timeBuf);
	timeout = (intTime == 0) ? DEFAULT_TIMEOUT : intTime;
//	cerr << "Timeout set by " << timeout << endl;
	return;
}


void DdcCtSender::run(ISInfoReceiver* testReceiver)
{
	receiver = testReceiver; 
	commander = new DdcCommander(partition);
	
	cout << endl;
	cout << "#############   Simulation   ##############" << endl;
	cout << "#######       of DAQ sending        #######" << endl;
	cout << "##    non-transition commands for DCS    ##" << endl;
	cout << "###########################################" << endl;
	
	string			ynAnswer;
	string			commParameters;
	string			bmCommandName;
	string			bmCommandRepeat;
	unsigned int	timeout = 0;
	time_t			now;
	int				timeoutApplied = 0xFFFF;
	bool			directCommand = true;
	bool			exeResult;
	int				ctrlNum;
	string			respName;
	
	while(true) {
		while(!commandAllowed) {
			usleep(10000);
			now = time(&now);
			if(now - startTime > timeoutApplied) {
				respName = COMMAND_SERVER;
				respName = respName + '.' + ctrlName + '.' + commandName + ".response";
				try {
					testReceiver->unsubscribe(respName);
				}
				catch(daq::is::Exception &ex) {
					cerr << "Could not unsubscribe " << respName 
						 << " on timeout" << endl;
				}
				if(getCommand() != BM_COMMAND)
					removeCommandFromIs(ctrlName, commandName);
				else 
					removeBMCommand();
				
				cerr << "now=" << (int)now 
					 << " startTime=" << (int)startTime
					 << " timeoutApplied=" << timeoutApplied << endl;
				cerr << "DdcCtSender: Result of the "
				 	 << getCommand() << " command is TIMEOUT" << endl;
				break;
			}
		}
			
		commandAllowed = false;
		
	  // Input the command name
		cout << endl << "Type the name of a command to be issued: ";
		cin >>  commandName;
		if(commandName == EXIT_COMMAND)
			return;
			
	  // Input the controller name
		cout << endl << "Type the name of controller to be addressed to: ";
		cin >>  ctrlName;
			
		if(commandName == BM_COMMAND) {
			bmCommandName = "";
			while(true) {
				cout << "Type the DCS RPC service name >  ";
				cin >> bmCommandName;
				if(bmCommandName.find_first_not_of(" .") != string::npos)
					break;
			}
			while(true) {
				cout << "How many times to repeat? > ";
				cin >> bmCommandRepeat;
				if(atoi(bmCommandRepeat.c_str()) > 0)
					break;
			}
			directCommand = true;
			inputCommandParameters(bmCommandName, commParameters,
								   timeoutApplied);
			respName = commander->makeResponseName(ctrlName, commandName);
			commParameters = bmCommandName + "|" 
							 + bmCommandRepeat + "|" + commParameters;
			ctrlNum = 1;
			try {
				receiver->subscribe(respName, callback, (void*)this);
			}
			catch(daq::is::RepositoryNotFound &ex) {
				std::cerr << "DdcCtSender_ERROR: " << respName
					 << " DdcNtCommandResponse subscription fault " << std::endl;
				std::cerr << "        ======>    No IS server to send command" << std::endl;;
				commandAllowed = true;
				ctrlNum = 0;
			}
			catch(daq::is::InvalidName &ex) {
				std::cerr << "DdcCtSender_ERROR: " << respName
					 << " is invalid name " << std::endl;
				commandAllowed = true;
				ctrlNum = 0;
			}
			catch(daq::is::AlreadySubscribed &ex) {
				std::cerr << "DdcCtSender_ERROR: " << respName
					 << " is already subscribed " << std::endl;
				commandAllowed = true;
				ctrlNum = 0;
			}
			if(ctrlNum) {		// i.e. no Exception happend
	 		 // And now sending the command
		 		startTime = time(&startTime);
//				cerr << "Sending command " << commandName << endl;
				commander->ddcSendCommand(ctrlName, commandName, commParameters, 
										  (unsigned int)timeoutApplied);
			}
		}
		else {
			if(confDb != 0) {
			 // Configured or "direct" command selection
		 		cout << "Is the command defined in configuration (y/n)? > ";
				cin >> commParameters;
				if((commParameters[0] != 'y') && (commParameters[0] != 'Y')) {
		 		 // Input the command parameters
					directCommand = true;
					inputCommandParameters(commandName, commParameters,
										   timeoutApplied);
				}
				else
					directCommand = false;
			}
			else {
	 		 // Input the command parameters
				directCommand = true;
				inputCommandParameters(commandName, commParameters,
									   timeoutApplied);
			}
				
			cout << "Using ddcExecCommand()? [y/n] > ";
			cin >> ynAnswer;
			if((ynAnswer[0] == 'y') || (ynAnswer[0] == 'Y')) {
				exeResult = commander->ddcExecCommand(ctrlName, commandName,
							                          commParameters, timeoutApplied);
				cerr << "Executing command completed ";
				if(exeResult == 0)
					cerr << "successfully" << endl;
				else
					cerr << "with error" << endl; 
				commandAllowed = true;
			}
			else {
			 // First subscription for the command result
				if(!directCommand) {
					respName = commander->makeResponseName(confDb, ctrlName, commandName);
					if(respName.size() == 0) {
						commandAllowed = true;
						std::cerr << "DdcCtSender_ERROR: No response is subscribed" << std::endl;
						continue;
					}
				}
				else
					respName = commander->makeResponseName(ctrlName, commandName);
		
				try {
					receiver->subscribe(respName, callback, (void*)this);
	 			 // And now sending the command
		 			startTime = time(&startTime);
					if(!directCommand)
						commander->ddcSendDaqCommand(confDb, ctrlName, commandName);
					else
						commander->ddcSendCommand(ctrlName, commandName, commParameters, timeoutApplied);
					ctrlNum = 1;
				}
				catch(daq::is::Exception &ex) {
					std::cerr << "DdcCtSender_ERROR: " << respName
						 << " DdcNtCommandResponse subscription fault " << std::endl;
					std::cerr << "        ======>    No IS server to send command" << std::endl;;
					commandAllowed = true;
					ctrlNum = 0;
				}
			}
		}
	}
}


int main(int argc, char *argv[])
{
	DdcCtSender* myDaq = 0;

	string	db_ref, data;
	bool	conf_error = false;

	if((getenv((const char*)"TDAQ_DB") != 0)
	&&	((db_ref = getenv((const char*)"TDAQ_DB")).size() != 0))
		conf_error = false;
	else {
		if(getenv((const char*)"TDAQ_DB_DATA") == 0) 
			conf_error = true;
		else {
			data = getenv((const char*)"TDAQ_DB_DATA");
			if(data.size() == 0) 
				conf_error = true;
		}
		if(!conf_error) {
			db_ref = "oksconfig:" + data;
			setenv((const char*)"TDAQ_DB", db_ref.c_str(), 1);
		}
		else {
			cerr << "DDC_Sender_FATAL: neither TDAQ_DB_DATA nor TDAQ_DB is not defined" << endl;
			return(1);
		}
	}
	
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmdLine.parse(arg_iter);
	
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
		return(1);
    }
    
	::Configuration* confDb;
	try {
		confDb = new Configuration("");
	}
	catch(daq::config::Exception &ex) {
		std::cerr << "Exception while initialization DB: " << ex << std::endl;
		return(1);
	}

	IPCPartition testPartition((const char*)partition_name);
	
	ISInfoReceiver* testReceiver = new ISInfoReceiver(testPartition);
	
	myDaq = new DdcCtSender(testPartition, confDb);
	myDaq->run(testReceiver);
	
	delete myDaq;
	delete testReceiver;
	
	return(0);	
}
