#ifndef _COMMAND_SIMULATOR_H_
#define _COMMAND_SIMULATOR_H_

#define EXIT_COMMAND	"doExit"
#define DEFAULT_TIMEOUT  20

class DdcDirectSender
{
public:
	DdcDirectSender() {}
	void run(DdcDirectCommander* commander);
private:
	void	inputCommandParameters(std::string cmd, std::string& params,
								   int& timeout);
};

#endif
