#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <unistd.h>

#include <ipc/core.h>
#include <ipc/partition.h>
//#include <ipc/pipeline.h>
#include <owl/semaphore.h>
#include <is/namedinfo.h>
#include <is/inforeceiver.h>
#include <cmdl/cmdargs.h>

using namespace std;

#include "../DdcCommand.hxx"
#include "../DdcCommander.hxx"
#include "DdcCtSerialSender.hxx"


struct job
{
	job(DdcCommander* cmdr, string ctrl, string num, int tout, int* errRef) :
		commander(cmdr), 
		ctrlName(ctrl),
		cmdName(num),
		timeout(tout),
		cmdErr(errRef)
	{;}
	
    void operator()()
    {
        {
//            boost::mutex::scoped_lock lock( mutex );
//            std::cerr << "Job " << id_ << " has been started and will lasts for " << delay_ << " seconds" << std::endl;
			string commParams("");
			int exeResult = commander->ddcExecCommand(ctrlName, cmdName,
						                          commParams, timeout, true);
			cerr << cmdName << ": Executing command completed ";
			if(exeResult == 0)
				cerr << "successfully" << endl;
			else {
				cerr << endl << "WITH ERROR " << exeResult << endl; 
				*cmdErr +=1;
			}
        }
        
//        sleep( delay_ );
        
//        {
//            boost::mutex::scoped_lock lock( mutex );
//            std::cerr << "Job " << id_ << " has been finished after " << delay_ << " seconds" << std::endl;
//        }
    }

	private:
	    DdcCommander* commander;
		string ctrlName;
		string cmdName;
		int    timeout;
		int*   cmdErr;
};


DdcCtSerialSender::DdcCtSerialSender(IPCPartition& p) 
			: partition(p)
{}



void DdcCtSerialSender::run()
{
//	receiver = testReceiver; 
	commander = new DdcCommander(partition);
	
	cout << endl;
	cout << "#############   Simulation   ##############" << endl;
	cout << "#######  of DAQ sending a series of #######" << endl;
	cout << "##    non-transition commands for DCS    ##" << endl;
	cout << "###########################################" << endl;
	
	unsigned int	timeout = 0;
	time_t			now;
	string			inBuf;
	int 			nCommands;
	int				nSeries;
	int				inTimeout;
	int 			i, N;
	char 			sNum[12];
	string			cmdBase;
	string 			cmdName;
	string 			ctrlName;
	int				seriesDelay;
	int 			errNum;
	
	while(true) {
	  // Input the command name
		cout << endl << "Type the controller name to be addressed: ";
		cin >>  ctrlName;
		
		cout << endl << "Type the base name of a command to be issued: ";
		cin >>  cmdBase;
		if(cmdBase == EXIT_COMMAND)
			return;
			
		// Input the timeout value
		std::cout << "Type the timeout value (sec) > ";
		std::cin >>  inBuf;
		inTimeout = atoi(inBuf.c_str());
		timeout = (inTimeout == 0) ? DEFAULT_TIMEOUT : inTimeout;
		seriesDelay = timeout/2+1;
		// Input the number of commands
		std::cout << "Type the number of commands > ";
		std::cin >>  inBuf;
		nCommands = atoi(inBuf.c_str());
		// Input the number of series
		std::cout << "Type the number of command series > ";
		std::cin >>  inBuf;
		nSeries = atoi(inBuf.c_str());
		
		errNum = 0;
		{
			IPCPipeline pipline(120);
			for(N=1; N<=nSeries; N++) {
				cerr << endl << "Series N " << N << endl;
				for(i=1; i<=nCommands; i++) {
					sprintf(sNum, "%d", i);
					cmdName = cmdBase+"_"+sNum;
					pipline.addJob(job(commander, ctrlName, cmdName, timeout, &errNum));
				}
				sleep(seriesDelay);
			}
		}
		cerr << "TEST COMPLETED WITH " << errNum << " ERRORS" << endl;
	}
}


int main(int argc, char *argv[])
{
	DdcCtSerialSender* myDaq = 0;

    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmdLine.parse(arg_iter);
	
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
		return(1);
    }
    
	IPCPartition testPartition((const char*)partition_name);
	
	myDaq = new DdcCtSerialSender(testPartition);
	myDaq->run();
	
	delete myDaq;
	
	return(0);	
}
