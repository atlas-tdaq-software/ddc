#ifndef _COMMAND_SIMULATOR_H_
#define _COMMAND_SIMULATOR_H_

#include "../DdcDal.hxx"

#define EXIT_COMMAND	"doExit"
#ifndef BM_COMMAND
#define BM_COMMAND		"BMCommand"
#endif

class DdcCtSerialSender
{
public:
	DdcCtSerialSender(IPCPartition& p);

	void run();
	IPCPartition&	getPartition() 	{ return(partition); }
//	std::string&	getCommand()	{ return(commandName); }
//	bool 	ddcSendControllerCommand(std::string& commandName, 
//									 std::string& commParameters, unsigned int timeout);
	
	static int doExit;
private:
//	bool 	ddcSendCommand(std::string& isEntryName, std::string& commandName, 
//						   std::string& commParameters, unsigned int timeout);
//	void	inputCommandParameters(std::string cmd, std::string& params,
//								   int& timeout);
	IPCPartition	partition;
	DdcCommander*	commander;
	time_t			startTime;
//	std::string		commandName;
};

#endif
