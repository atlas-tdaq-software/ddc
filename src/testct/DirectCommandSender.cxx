#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <unistd.h>

using namespace std;

#include <dic.hxx>						// for user application replace this
										// by <ddc/dim/dic.hxx>
#include "../DdcDirectCommander.hxx" 	// for user application replace this
										// by <ddc/ddcDirectCommander.hxx>
#include "DirectCommandSender.hxx"

// This is the user callback function of a direct command sent to a PVSS project
void DdcDirectCommander::cmdrNotify(std::string host, std::string cmd, int response,
							std::vector<std::string> keyWords)
{
	int			i;
	std::string	errMsg;
	decodeError(response, errMsg);
	std::cerr << "Command " << cmd;
	if(host != "")
		std::cerr << " for " << host;
	std::cerr << " is completed with code "
			  << response << " (" << errMsg << ")" << std::endl;
	std::cerr << "The command keys: ";
	if(keyWords.size() > 0) {
		for(i=0; i<keyWords.size(); i++) 
			std::cerr << keyWords[i] << " "; 
	}
	else
		std::cerr << "NO KEYS";
	std::cerr << std::endl;		 	
}

// User application methods
void DdcDirectSender::inputCommandParameters(std::string cmd, std::string& params,
								 		 int& timeout)
{
	std::string	answer;
	char		timeBuf[16];
	int			intTime;
	
	std::cout << "Has the command " << cmd << " any parameters (y/n)? ";
	std::cin >> answer;
	if((answer[0] == 'y') || (answer[0] == 'Y')) {
		std::cout << "Type the command parameters (p1|p2|...): ";
		std::cin >>  params;
	}
	else
		params = "";
	
	// Input the timeout value
	std::cout << "Type the timeout value (sec): ";
	std::cin >>  timeBuf;
	intTime = atoi(timeBuf);
	timeout = (intTime == 0) ? DEFAULT_TIMEOUT : intTime;
	return;
}


void DdcDirectSender::run(DdcDirectCommander* commander)
{
	cout << endl;
	cout << "#############   Simulation   ##############" << endl;
	cout << "#######       of DAQ sending        #######" << endl;
	cout << "##        direct commands for DCS        ##" << endl;
	cout << "###########################################" << endl;
	
	string			host;
	string			commandName;
	string			inputString = "";
	string			commParameters;
	unsigned int	timeout = 0;
	int				timeoutApplied = 0xFFFF;
	DdcDirectCommand* command;
	vector<string> keyWords;
	
	while(true) {
	  // Input the command destination host
	  	cout << endl << "Do you want to check first that the project is running? [y/n] ";
		cin >> inputString;
		if((inputString[0] == 'Y') || (inputString[0] == 'y')) {
			inputString = "";
			cout << "Type the host name to send the command: ";
			cin >> inputString;
			host = inputString;
		}
		inputString = "";
	  // Input the command name
		cout << "Type the name of a command to be issued: ";
	  	while(inputString == "") {
			cin >> inputString;
			if(inputString == "")
				cout << "Not-empty command name is mandatory! Type it here: ";
	 	}
		commandName = inputString;
		inputString = "";
		if(commandName == EXIT_COMMAND)
			return;
		
	 // Input the command parameters
		inputCommandParameters(commandName, commParameters,
							   timeoutApplied);
		timeout = timeoutApplied;
		
		command = new DdcDirectCommand(commandName, commParameters, timeout, host);
		
	 // Input the keyWords of the command
		keyWords.clear();
		while(inputString != ".") {
			cout << "Type a key word for this command ('.' to close input): " << endl;
			cin >> inputString;
			if(inputString == ".")
				break;
			else
				keyWords.push_back(inputString);
		}
		inputString = "";
		
 	 // And now sending the command
		if(commander->launchCommand(*command, keyWords))
			cerr << "Command " << command->getCommName() << " is sent" << endl;
		else
			cerr << "Could not send command " << command->getCommName() << endl;
	}
}


int main()
{
	string	db_ref, data;
	bool	conf_error = false;

	if(getenv((const char*)"DIM_DNS_NODE") == 0) 
		conf_error = true;
	else {
		data = getenv((const char*)"DIM_DNS_NODE");
		if(data.size() == 0) 
			conf_error = true;
	}
	if(conf_error) {
		cerr << "DirectCommandSender_FATAL: DIM Name Server host (DIM_DNS_NODE environment) is not defined" << endl;
		return(1);
	}

	DdcDirectCommander* commander = new DdcDirectCommander();
	
	DdcDirectSender* myDaqApplication = new DdcDirectSender();
	myDaqApplication->run(commander);
	
	delete myDaqApplication;
	delete commander;
	
	return(0);	
}
