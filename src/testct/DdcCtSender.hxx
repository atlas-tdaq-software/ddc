#ifndef _COMMAND_SIMULATOR_H_
#define _COMMAND_SIMULATOR_H_

#include "../DdcDal.hxx"

#define EXIT_COMMAND	"doExit"
#ifndef BM_COMMAND
#define BM_COMMAND		"BMCommand"
#endif

class DdcCtSender
{
public:
	DdcCtSender(IPCPartition& p, ::Configuration* db);

	void run(ISInfoReceiver *);
	IPCPartition&	getPartition() 	{ return(partition); }
	std::string&	getCommand()	{ return(commandName); }
	std::string&	getController()	{ return(ctrlName); }
	ISInfoReceiver* getReceiver()	{ return(receiver); }
	void			removeCommandFromIs(std::string ctrl, std::string cmdlName);
	void			removeBMCommand();
//	bool 	ddcSendControllerCommand(std::string& commandName, 
//									 std::string& commParameters, unsigned int timeout);
	
	static int doExit;
private:
//	bool 	ddcSendCommand(std::string& isEntryName, std::string& commandName, 
//						   std::string& commParameters, unsigned int timeout);
	void	inputCommandParameters(std::string cmd, std::string& params,
								   int& timeout);
	IPCPartition	partition;
	DdcCommander*	commander;
	time_t			startTime;
	std::string		ctrlName;
	std::string		commandName;
	ISInfoReceiver* receiver;
	bool			commandAllowed;
	Configuration*	confDb;
};

#endif
