//
// DDC message receiver for testing DDC-MT subsystem
//					S.Khomoutnikov		07.07.01
//

#include <ipc/core.h>
#include <ipc/partition.h>
//#include <mrs/message.h>

#include <ers/InputStream.h>
#include <ers/SampleIssues.h>
#include <ers/ers.h>
#include <semaphore.h>

using namespace std;

/* Code for MRS is completely replaced by SKolos's example
void callback(MRSCallbackMsg* msg)
{
	string	msgReceived;
	
	msgReceived = msg->getMessage();
	cout << "Message received by ddc_msg_receiver: " 
		 << endl << "     " << msgReceived << endl;
}

int main(int argc, char** argv)
{
	char	*partitionName = 0;
	
	if(argc <= 2) {
		cerr << "-p <partition_name> option is mandatory! Exiting..." << endl;
		exit(1);
	}
	else {
		if(!strncmp(argv[1], "-p", 2)) { 
			partitionName = (char*)argv[2];
			cout << "IPC partition for receiving messages is " << partitionName << endl;
		
			IPCCore::init(argc, argv);
			IPCPartition  p(partitionName);
			MRSReceiver receiver(p);
			try {
				receiver.subscribe("ALL", callback);
			}
			catch (daq::mrs::RouterNotFound) {
				cerr << "MRSReceiver.subscribe ERROR: RouterNotFound" << endl;
				exit(1);
			}
			catch (daq::mrs::InvalidExpression) {
				cerr << "MRSReceiver.subscribe ERROR: InvalidExpression" << endl;
				exit(1);
			}
			catch (daq::mrs::SubscriptionAlreadyExist) {
				cerr << "MRSReceiver.subscribe WARNING: SubscriptionAlreadyExist" << endl;
			}
			cout << "Test receiver is subscribed for DCS messages" << endl;
//			receiver.run();		// At 2013 there is already no this method
								// Arrange "loop forever" instead
			while(true) {
				sleep(1);
			}
		}
		else {
			cerr << "-p key must be applied! Exiting..." << endl;
		}
	}
}
*/

struct MyIssueReceiver : public ers::IssueReceiver
{
    void receive( const ers::Issue & issue )
    {
        std::cout << issue << std::endl;
    }
};

int main(int , char** )
{

    try {
        ers::StreamManager::instance().add_receiver( "mts", "*", new MyIssueReceiver );
    }
    catch( ers::Issue & ex ) {
        ers::fatal( ex );
        return 1;
    }

    sem_t semaphore;
    sem_init( &semaphore, 0, 0 );
    sem_wait( &semaphore );

    return 0;
}

