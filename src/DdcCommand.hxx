#ifndef _DDCCOMMAND_H_
#define _DDCCOMMAND_H_

//
// This file contains definition of DdcCommand classes that is to 
// represent a DAQ command and the current status of its execution.
// 
//							S. Khomoutnikov 
//							12.03.01
// Modified:
//		14.03.01	S.Kh.	Command definition is suppressed.
// 		26.03.01	S.Kh	Conciderably changed the command representation in order to
//							1) Represent all command parameters within a single string
//							2) Construct commands by the config database definitions,
//							   based on mapping of RC transitions
// 		25.11.01	S.Kh	Non-transition (Nt) command related classes are
//							introduced
//
////////////////////////////////////////////////////////////////////////////////

#include <is/infoT.h>
#include <is/namedinfo.h>

// Definition of default time interval (sec)  
// for polling Not_Ready_for_DAQ flag
#define DCS_POLL_TIME_DEFAULT	5

// Definition of Benchmark command  
#define BM_COMMAND	"BMCommand"

// Definition of DCS response value until a command has finished,
// which all DCS responces on a command MUST differ from
#define NO_COMMAND_RESPONSE		-1

// Error codes used by DdcCtManager while command execution
//---------------------------------------------------------
//!! DCS developer must define command error codes 
//   taking into account that the negative codes  
//   listed below may not be assigned by DCS
//=========================================================
#define COMM_ERR_OK		 	 0	// no errors
#define COMM_ERR_UNDEF		-1	// undefined status
#define COMM_ERR_NAME		-2	// invalid command name
#define COMM_ERR_DCSDP		-3	// invalid a DCS' datapoint definition
#define COMM_ERR_TIMEOUT	-4	// timeout expired
#define COMM_ERR_CONNECT	-5	// SCADA communication fault (no connection)
#define COMM_ERR_CONNBROKEN	-6	// SCADA communication fault (connection broken)
#define COMM_ERR_CONFIG		-7	// invalid configuration
#define	COMM_ERR_NTDESC		-8	// invalid definition of an nt-command
#define COMM_ERR_BUSY		-9	// previous command has not ended
#define	COMM_ERR_DCSNOTREADY	-10	// DCS is not ready for data taking
//=========================================================

// Default timeout for command execution (in seconds)
const int DEFAULT_TIMEOUT = 20;
// The name of IS server for issuing nt-commands
#define	COMMAND_SERVER	"DDC"
//================================

class DdcCommand
{ 
public:	
	DdcCommand(std::string commandId) 
		: timeout(0), commName(commandId) {}
	int 	getTimeout()		{ return(timeout); }
	time_t	getStartTime()		{ return(startTime); }
	std::string	getCommName()		{ return(commName); }
	void	setStartTime()		{ startTime = time(&startTime); }
	void 	setTimeout(int t) 	{ timeout = t; }

private:
	time_t		startTime;
	int 		timeout;
	std::string		commName;
};

// Generated classes
#include "ddc/DdcNtCommandInfo.h"
#include "ddc/DdcNtCommandInfoNamed.h"
#include "ddc/DdcNtCommandResponseInfo.h"
#include "ddc/DdcNtCommandResponseInfoNamed.h"


/*class DdcNtCommand : public ISNamedInfo
{
public:
	DdcNtCommand(IPCPartition& p, const std::string& obName) 
		: ISNamedInfo(p, obName.c_str()), commandName("") {}
		
	std::string getCommName()			{ return(commandName); }
	void	setCommName(std::string& s)	{ commandName = s; }
	std::string	getParValue()			{ return(parameters); }
	void	setParValue(std::string& pr){ parameters = pr; }
	int		getTimeout()				{ return(timeout); }
	void	setTimeout(unsigned int t)	{ timeout = t; }
	
	void publishGuts(ISostream &ostrm); 
	void refreshGuts(ISistream &istrm);
	
private:
	std::string		commandName;
	std::string		parameters;
	unsigned int 	timeout;	
};

class DdcNtCommandResponse : public ISNamedInfo
{
public: 
	DdcNtCommandResponse(IPCPartition& p, const char* name, int resp)
		: ISNamedInfo(p, name), value(resp) {}
		
	int getValue()						{ return(value); }
	void publishGuts(ISostream &ostrm) 	{ ostrm << getValue(); }
	void refreshGuts(ISistream &istrm) 	{ int r; istrm >> r; setValue(r); }
private:
	void setValue(int res)	{ value = res; }
	
	int	value;
};
*/

// ========= inline member functions ============================
/*
inline void DdcNtCommand::publishGuts(ISostream &ostrm) 
{
	ostrm << commandName << parameters << timeout;
}

inline void DdcNtCommand::refreshGuts(ISistream &istrm) 
{
	istrm >> commandName >> parameters >> timeout;
}
*/
#endif
