#include <string>
#include <iostream>

#include "DdcBenchmark.hxx"

long DdcBenchmark::fNumNtCommands = 0;
OWLTimer DdcBenchmark::fDimTimer;
OWLTimer DdcBenchmark::fIsTimer;
OWLTimer DdcBenchmark::fControllerTimer;

void DdcBenchmark::TimerReport()
{
  std::cout << "Number of executed NT commands: " << fNumNtCommands << std::endl
	    << "Time needed for DIM transactions and DCS actions: CPU "
	    << fDimTimer.userTime()+fDimTimer.systemTime() << "s, Total: "
	    << fDimTimer.totalTime() << " Total time per command: "
	    << fDimTimer.totalTime()/fNumNtCommands << std::endl
	    << "Time needed for IS: CPU "
	    << fIsTimer.userTime()+fIsTimer.systemTime() << "s, Total: "
	    << fIsTimer.totalTime() << " Total time per command: "
	    << fIsTimer.totalTime()/fNumNtCommands << std::endl
	    << "Time needed for Controller: CPU "
	    << fControllerTimer.userTime()+fControllerTimer.systemTime() << "s, Total: "
	    << fControllerTimer.totalTime() << " Total time per command: "
	    << fControllerTimer.totalTime()/fNumNtCommands << std::endl;
}
