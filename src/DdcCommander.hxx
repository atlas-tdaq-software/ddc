///////////////////////////////////////////////////////////////////////////////////////
// This file contains the class definition to issue 
// a non-transition command
//					
//									Created 17.01.03
//									by S.Khomutnikov
// Modifications:
//		S.Kh	01.11.06	Methods accept daq::is::Exception 
//		S.Kh	12.09.07	Methods 'execDdcCommand()' and notify() are introduced
//							on top of previously existing. This required also introducing
//							ISInfoReceiver and its callback
//		S.Kh	26.09.07	Benchmark facilities in _ddcSendCommand()
//							on top of previously existing. This required also introducing
// Schlenk,S.Kh 25.02.08	Parallel execution of commands,
//      					Encapsulation of send/waiting for response
//      					Dropping invalide command "for all controllers"
// 
///////////////////////////////////////////////////////////////////////////////////////

#ifndef _DDC_COMMANDER_H_
#define _DDC_COMMANDER_H_

#include <mutex>
#include <ipc/partition.h>
#include <is/inforeceiver.h>

#include "DdcCommand.hxx"
#include "DdcDal.hxx"

class DdcCommander
{
public:
	DdcCommander(IPCPartition& p); 
	virtual ~DdcCommander();

	// For a command defined in the configuration
	//---------------------------------------------
	bool ddcSendDaqCommand(Configuration* confDb, std::string ctrlName, std::string daqCommandName);
	
	// For "direct" command defined by known PVSS DIM-RPC-service
	//------------------------------------------------------------
	bool ddcSendCommand(std::string ctrlName, std::string commandName, std::string commParameters,
						unsigned int timeout, bool BM = false);

	// For "direct" command with receiving response (high level interface)
	//--------------------------------------------------------------------
	int ddcExecCommand(std::string ctrlName, std::string command, std::string params, 
					   unsigned int timeout, bool queued = false);
	
	// To remove last command from the RunCtrl IS server
	//----------------------------------------------------
	bool ddcRemoveCommand(std::string ctrlName, std::string cmdName);
	bool ddcRemoveBMCommand();
	
	// To build the IS entry name for the command response 
	//-----------------------------------------------------
	std::string	makeResponseName(std::string ctrlName,std::string commandName);
	std::string	makeResponseName(Configuration* confDb, std::string ctrlName, std::string commandName);

	// For informing user aplication about the command result (high level interface)
	//--------------------------------------------------------------------
	virtual void notify(std::string ctrl, std::string cmd, int response);
	
	IPCPartition&	getPartition() 	{ return(partition); }
//	std::string&	getCommand()	{ return(cmdName); }
	ISInfoReceiver* getReceiver()	{ return(receiver); }
//	void			release()		{ cmdName = ""; commandRunning = false; }
	std::string 	cmdFromResponse(std::string response, std::string& ctrl);	
	
private:
	bool __ddcSendCommand(std::string& isEntryName, std::string& commandName, 
						std::string& commParameters, unsigned int timeout, bool BM = false);
	// For ddcExecCommand()
	std::mutex commandMutex;
	std::map<std::string,OWLSemaphore*> commandLock;
	static void commanderCallback(ISCallbackInfo* cbInfo);
	static std::map<std::string,int> commandResponse;

	IPCPartition	partition;
	ISInfoReceiver* receiver;
//	bool			commandRunning;
//	std::string		cmdName;
};

#endif
