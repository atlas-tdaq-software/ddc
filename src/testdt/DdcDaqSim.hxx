#ifndef _SYMULATOR_H_
#define _SYMULATOR_H_


#define DAQSIM_TYPE_STRUCT	13

typedef struct
{
unsigned int	type;
unsigned int	dimension;
std::string		name;
void*			data;
}	DataDef;

//
// Template class to represent DAQ single data value of type 'D' without time stamp
// 
template<class D> class DaqSingleData : public ISNamedInfo
{
public:
	DaqSingleData(IPCPartition &p, const std::string& obName) 
		: ISNamedInfo(p, obName.c_str()) {}
	D	 getValue() { return(value); }
	void setValue(D data) { value = data; }
	void printValue() { cout << value; }
	void publishGuts(ISostream &ostrm); 
	void refreshGuts(ISistream &istrm);
private:
	D	value;
};

//
// Class to represent DAQ data of type 'dynamic array of D' without time stamp
// where D means one of <bool, float, int, unsigned int, char, string>
// 
template<class D> class DaqDataArray : public ISNamedInfo
{
public:
	DaqDataArray(IPCPartition &p, const std::string& obName) 
		: ISNamedInfo(p, obName.c_str()) {}
	vector<D>* 	getValue() const { return(&value); }
	void addValue(D data) 		{ value.push_back(data); }
	void publishGuts(ISostream &ostrm);
	void refreshGuts(ISistream &istrm);
private:
	vector<D>	value;
};

class DaqStruct : public ISNamedInfo
{
public:
	DaqStruct(IPCPartition &p, const std::string& obName) 
		: ISNamedInfo(p, obName.c_str()) {}
	~DaqStruct();
	void* 	getValue(unsigned int n) const 		{ return(value[n]); }
	unsigned int	getType(unsigned int n) const	{ return(type[n]); }
	unsigned int	getSize(unsigned int n) const	{ return(length[n]); }
	void 	addValue(unsigned int itemType, unsigned int itemSize, void* data) 		
			{ length.push_back(itemSize);
			  type.push_back(itemType);
			  value.push_back(data);	 }
	void publishGuts(ISostream &ostrm);
	void refreshGuts(ISistream &istrm)	{}	//Assumed to be never used
private:
	vector<unsigned int>	length;
	vector<unsigned int>	type;
	vector<void*>			value;
};

class Simulator
{
public:
	Simulator(DdcDaqsimConfig* dcsImportConfig, DdcDaqsimConfig* daqRequestConfig,
			  IPCPartition& p) 
		: exportConfig(dcsImportConfig), requestConfig(daqRequestConfig),
		  partition(p) { }
	~Simulator() { delete exportConfig; delete requestConfig; }
	void run();

private:
	void printTypes();
	void presentData(DataDef& dataDef);
	void presentStruct(DataDef& dataDef);
	void updateStruct(DataDef& dataDef);
	void getFieldData(DataDef& fieldDef);

	vector<DataDef> 		table;
	vector<DataDef>			structDef;
	DdcDaqsimConfig*		exportConfig;
	DdcDaqsimConfig*		requestConfig;
	IPCPartition			partition;
};

template<class D> inline void DaqSingleData<D>::publishGuts(ISostream &ostrm) 
{
	ostrm << value;
}

template<class D> inline void DaqSingleData<D>::refreshGuts(ISistream &istrm) 
{
	D	newValue;
	
	istrm >> newValue;
	value = newValue;
}

template<class D> inline void DaqDataArray<D>::publishGuts(ISostream &ostrm) 
{
	D* ptr = new D[value.size()];
	for(unsigned int i = 0; i < value.size(); i++) {
		ptr[i] = value[i];
	}
	ostrm.put(ptr, value.size());
	delete[] ptr;
}

template<class D> inline void DaqDataArray<D>::refreshGuts(ISistream &istrm) 
{
	D * ptr;
	size_t size;
	istrm.get( &ptr, size );
	value.erase(value.begin(), value.end());
	for(size_t i = 0; i < size; i++)
		value.push_back(ptr[i]);
	delete[] ptr;
}
	
#endif
