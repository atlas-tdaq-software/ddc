#include <string>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include <ipc/core.h>
#include <is/namedinfo.h>

#include <owl/time.h>
#include <cmdl/cmdargs.h>

using namespace std;

#include "../DdcData.hxx"
#include "../DdcRequest.hxx"
#include "DdcDaqSimConfig.hxx"
#include "DdcDaqSim.hxx"


DaqStruct::~DaqStruct()
{
	unsigned int i;
	
	for(i=0; i < length.size(); i++) {
		switch(type[i]) {
		case 1:
			if(length[i])
				delete[] (bool*)value[i];
			else
				delete (bool*)value[i];
			break;
		case 2:
		case 3:
			if(length[i])
				delete[] (char*)value[i];
			else
				delete (char*)value[i];
			break;
		case 4:
		case 5:
			if(length[i])
				delete[] (short*)value[i];
			else
				delete (short*)value[i];
			break;
		case 6:
		case 7:
			if(length[i])
				delete[] (int*)value[i];
			else
				delete (int*)value[i];
			break;
		case 8:
			if(length[i])
				delete[] (float*)value[i];
			else
				delete (float*)value[i];
			break;
		case 9:
			if(length[i])
				delete[] (double*)value[i];
			else
				delete (double*)value[i];
			break;
		case 10:
			if(length[i])
				delete[] (string*)value[i];
			else
				delete (string*)value[i];
			break;
		case 11:
			if(length[i])
				delete[] (OWLTime *)value[i];
			else
				delete (OWLTime*)value[i];
			break;
		default: ;
		}
	}
}

void DaqStruct::publishGuts(ISostream &ostrm)
{
	unsigned int j;
	int		tstInt;
	
	for(unsigned int i=0; i < length.size(); i++ ) {
		switch(type[i]) {
		case 1:
			if(length[i] == 0)	// not array
				ostrm << *((bool*)(value[i]));
			else {				// an array
				bool* ptr = new bool[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((bool*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 2:
			if(length[i] == 0)	// not array
				ostrm << *((char*)(value[i]));
			else {				// an array
				char* ptr = new char[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((char*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 3:
			if(length[i] == 0)	// not array
				ostrm << *((unsigned char*)(value[i]));
			else {				// an array
				unsigned char* ptr = new unsigned char[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((unsigned char*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 4:
			if(length[i] == 0)	// not array
				ostrm << *((short*)(value[i]));
			else {				// an array
				short* ptr = new short[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((short*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 5:
			if(length[i] == 0)	// not array
				ostrm << *((unsigned short*)(value[i]));
			else {				// an array
				unsigned short* ptr = new unsigned short[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((unsigned short*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 6:
			if(length[i] == 0) {	// not array
				tstInt = *((int*)value[i]);
				ostrm << tstInt;
			}
			else {				// an array
				int* ptr = new int[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((int*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 7:
			if(length[i] == 0)	// not array
				ostrm << *((unsigned int*)(value[i]));
			else {				// an array
				unsigned int* ptr = new unsigned int[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((unsigned int*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 8:
			if(length[i] == 0)	// not array
				ostrm << *((float*)(value[i]));
			else {				// an array
				float* ptr = new float[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((float*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 9:
			if(length[i] == 0)	// not array
				ostrm << *((double*)(value[i]));
			else {				// an array
				double* ptr = new double[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((double*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 10:
			if(length[i] == 0)	// not array
				ostrm << *((string*)(value[i]));
			else {				// an array
				string* ptr = new string[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((string*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		case 11:
			if(length[i] == 0)	// not array
				ostrm << *((OWLTime*)(value[i]));
			else {				// an array
				OWLTime* ptr = new OWLTime[length[i]];
				for(j=0; j < length[i]; j++)
					ptr[j] = ((OWLTime*)(value[i]))[j];
				ostrm.put(ptr, length[i]);
				delete[] ptr;
			}
			break;
		default: ;		// unreachable
		}
	}	
}

void Simulator::presentData(DataDef& dataDef)
{
	void*			data;
	unsigned int	i;
	short			shortData = -1;
	unsigned short	ushortData;
	int				intData = -1;
	unsigned int	uintData;
	char			charData;
	unsigned char	ucharData;
	float			floatData;
	double			doubleData;
	string			stringData;
	OWLTime			timeData();
	OWLTime*		newTimeData;
	char			buf[256];
	string			isServer = exportConfig->findIsServer(dataDef.name);
	
	string	isName = isServer + '.' + dataDef.name; 
	
	if(dataDef.dimension == 1) {
		switch(dataDef.type) {
		case 1:
			data = new DaqSingleData<bool>(partition, isName);
			while((intData != 0) && (intData != 1)) {
				cout << "Type value [0/1]: ";
				cin >> buf;
				intData = atoi(buf);
			}
			((DaqSingleData<bool>*)data)->setValue((intData == 1) ? true : false);
			try {
				((DaqSingleData<bool>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer  << std::endl;
			}
	 		delete (DaqSingleData<bool>*)data;
			break;
		case 2:
			data = new DaqSingleData<char>(partition, isName);
			cout << "Type value <char>: ";
			cin >> charData;
			((DaqSingleData<char>*)data)->setValue(charData);
			try {
				((DaqSingleData<char>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<char>*)data;
			break;
		case 3:
			data = new DaqSingleData<unsigned char>(partition, isName);
			cout << "Type value <unsigned char>: ";
			cin >> ucharData;
			((DaqSingleData<unsigned char>*)data)->setValue(ucharData);
			try {
				((DaqSingleData<unsigned char>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<unsigned char>*)data;
			break;
		case 4:
			data = new DaqSingleData<short>(partition, isName);
			cout << "Type value <short>: ";
			cin >> shortData;
			((DaqSingleData<short>*)data)->setValue(shortData);
			try {
				((DaqSingleData<short>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<short>*)data;
			break;
		case 5:
			data = new DaqSingleData<unsigned short>(partition, isName);
			cout << "Type value <unsigned short>: ";
			cin >> ushortData;
			((DaqSingleData<unsigned short>*)data)->setValue(ushortData);
			try {
				((DaqSingleData<unsigned short>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<unsigned short>*)data;
			break;
		case 6:
			data = new DaqSingleData<int>(partition, isName);
			buf[0] = '\0';
			while(buf[0] == '\0') {
				cout << "Type value <int>: ";
				cin >> buf;
				intData = atoi(buf);
			}
			((DaqSingleData<int>*)data)->setValue(intData);
			try {
				((DaqSingleData<int>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<int>*)data;
			break;
		case 7:
			data = new DaqSingleData<unsigned int>(partition, isName);
			buf[0] = '\0';
			while(buf[0] == '\0') {
				cout << "Type value <unsigned int>: ";
				cin >> buf;
				uintData = atoi(buf);
			}
			((DaqSingleData<unsigned int>*)data)->setValue(uintData);
			try {
				((DaqSingleData<unsigned int>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<unsigned int>*)data;
			break;
		case 8:
			data = new DaqSingleData<float>(partition, isName);
			buf[0] = '\0';
			while(buf[0] == '\0') {
				cout << "Type value <float>: ";
				cin >> buf;
				floatData = atof(buf);
			}
			((DaqSingleData<float>*)data)->setValue(floatData);
			try {
				((DaqSingleData<float>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<float>*)data;
			break;
		case 9:
			data = new DaqSingleData<double>(partition, isName);
			buf[0] = '\0';
			while(buf[0] == '\0') {
				cout << "Type value <double>: ";
				cin >> buf;
				doubleData = atof(buf);
			}
			((DaqSingleData<double>*)data)->setValue(doubleData);
			try {
				((DaqSingleData<double>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<double>*)data;
			break;
		case 10:
			data = new DaqSingleData<string>(partition, isName);
			cout << "Type value <string>: ";
			cin >> stringData;
			((DaqSingleData<string>*)data)->setValue(stringData);
			try {
				((DaqSingleData<string>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqSingleData<string>*)data;
			break;
		case 11:
			data = new DaqSingleData<OWLTime>(partition, isName);
			cout << "Type time value (DD/MM/YY HH:MM:SS): ";
			cin >> stringData;
			newTimeData = new OWLTime(stringData.c_str());
			((DaqSingleData<OWLTime>*)data)->setValue(*newTimeData);
			try {
				((DaqSingleData<OWLTime>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
			delete newTimeData;
		 	delete (DaqSingleData<OWLTime>*)data;
			break;
		default:		// this could not be
			std::cerr << "ddc_daqsim_ERROR: data type " << dataDef.type << " is incorrect" << std::endl;
		}
	}
	else {		// Array of data
		switch(dataDef.type) {
		case 1:
			data = new DaqDataArray<bool>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				intData = -1;
				while((intData != 0) && (intData != 1)) {
					cout << "Type value [0/1]: ";
					cin >> buf;
					intData = atoi(buf);
				}
				((DaqDataArray<bool>*)data)->addValue((intData == 1) ? true : false);
			}
			try {
				((DaqDataArray<bool>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<bool>*)data;
			break;
		case 2:
			data = new DaqDataArray<char>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				cout << "Type value <char>: ";
				cin >> charData;
				((DaqDataArray<char>*)data)->addValue(charData);
			}
			try {
				((DaqDataArray<char>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<char>*)data;
			cerr << "Deleted" << endl;
			break;
		case 3:
			data = new DaqDataArray<unsigned char>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				cout << "Type value <unsigned char>: ";
				cin >> ucharData;
				((DaqDataArray<unsigned char>*)data)->addValue(ucharData);
			}
			try {
				((DaqDataArray<unsigned char>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<unsigned char>*)data;
			break;
		case 4:
			data = new DaqDataArray<short>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				cout << "Type value <short>: ";
				cin >> shortData;
				((DaqDataArray<short>*)data)->addValue(shortData);
			}
			try {
				((DaqDataArray<short>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<short>*)data;
			break;
		case 5:
			data = new DaqDataArray<unsigned short>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				cout << "Type value <unsigned short>: ";
				cin >> ushortData;
				((DaqDataArray<unsigned short>*)data)->addValue(ushortData);
			}
			try {
				((DaqDataArray<unsigned short>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<unsigned short>*)data;
			break;
		case 6:
			data = new DaqDataArray<int>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				buf[0] = '\0';
				while(buf[0] == '\0') {
					cout << "Type value <int>: ";
					cin >> buf;
					intData = atoi(buf);
				}
				((DaqDataArray<int>*)data)->addValue(intData);
			}
			try {
				((DaqDataArray<int>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<int>*)data;
			break;
		case 7:
			data = new DaqDataArray<unsigned int>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				buf[0] = '\0';
				while(buf[0] == '\0') {
					cout << "Type value <unsigned int>: ";
					cin >> buf;
					uintData = atoi(buf);
				}
				((DaqDataArray<unsigned int>*)data)->addValue(uintData);
			}
			try {
				((DaqDataArray<unsigned int>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<unsigned int>*)data;
			break;
		case 8:
			data = new DaqDataArray<float>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				buf[0] = '\0';
				while(buf[0] == '\0') {
					cout << "Type value <float>: ";
					cin >> buf;
					floatData = atoi(buf);
				}
				((DaqDataArray<float>*)data)->addValue(floatData);
			}
			try {
				((DaqDataArray<float>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<float>*)data;
			break;
		case 9:
			data = new DaqDataArray<double>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				buf[0] = '\0';
				while(buf[0] == '\0') {
					cout << "Type value <double>: ";
					cin >> buf;
					doubleData = atoi(buf);
				}
				((DaqDataArray<double>*)data)->addValue(doubleData);
			}
			try {
				((DaqDataArray<double>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<double>*)data;
			break;
		case 10:
			data = new DaqDataArray<string>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				cout << "Type value <string>: ";
				cin >> stringData;
				((DaqDataArray<string>*)data)->addValue(stringData);
			}
			try {
				((DaqDataArray<string>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<string>*)data;
			break;
		case 11:
			data = new DaqDataArray<OWLTime>(partition, isName);
			for(i=0; i < dataDef.dimension; i++) {
				cout << "Type time value (DD/MM/YY HH:MM:SS): ";
				cin >> stringData;
				newTimeData = new OWLTime(stringData.c_str());
				((DaqDataArray<OWLTime>*)data)->addValue(*newTimeData);
				delete newTimeData;
			}
			try {
				((DaqDataArray<OWLTime>*)data)->checkin();
			}
			catch(daq::is::RepositoryNotFound) {
				std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
					 << " is refused " << std::endl;
				std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
			}
			catch(daq::is::InvalidName) {
				std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
					 << isServer << std::endl;
			}
			catch(daq::is::InfoNotCompatible) {
				std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
					 << " for " << isServer << std::endl;
			}
		 	delete (DaqDataArray<OWLTime>*)data;
			break;
		default:		// this could not be
			std::cerr << "ddc_daqsim_ERROR: data type " << dataDef.type << " is incorrect" << std::endl;
		}
	}
}

void Simulator::getFieldData(DataDef& fieldDef)
{
	unsigned int i;
	short			boolBuf;
	bool*			boolData;
	short*			shortData;
	unsigned short*	ushortData;
	int*			intData;
	unsigned int*	uintData;
	char*			charData;
	unsigned char*	ucharData;
	float*			floatData;
	double*			doubleData;
	string*			stringData;
	string			timeTime;
	OWLTime*		timeData;
	OWLTime*		newTimeData;
	string			prompt01("Type value [0/1]: ");
	string			promptChar("Type value (char): ");
	string			promptUChar("Type value (unsigned char): ");
	string			promptShort("Type value (short): ");
	string			promptUShort("Type value (unsigned short): ");
	string			promptInt("Type value (int): ");
	string			promptUInt("Type value (unsigned int): ");
	string			promptFloat("Type value (float): ");
	string			promptDouble("Type value (double): ");
	string			promptString("Type value (string): ");
	string			promptTime("Type time value (DD/MM/YY HH:MM:SS): ");
	
	switch(fieldDef.type) {
	case 1:
		if(fieldDef.dimension == 0) {
			boolData = new bool;
			cout << prompt01;
			while((boolBuf != 0) && (boolBuf != 1))
				cin >> boolBuf;
			if(boolBuf)
				*boolData = true;
			else
				*boolData = false;
		}
		else {
			boolData = new bool[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				boolBuf = -1;
				cout << prompt01;
				while((boolBuf != 0) && (boolBuf != 1))
					cin >> boolBuf;
				if(boolBuf)
					boolData[i] = true;
				else
					boolData[i] = false;
			}
		}
		fieldDef.data = boolData;
		cerr << "Bool data: " << ((*((bool*)fieldDef.data)) ? "true" : "false") << endl;
		break;
	case 2:
		if(fieldDef.dimension == 0) {
			charData = new char;
			cout << promptChar;
			cin >> *charData;
		}
		else {
			charData = new char[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptChar;
				cin >> charData[i];
			}
		}
		fieldDef.data = charData;
		break;
	case 3:
		if(fieldDef.dimension == 0) {
			ucharData = new unsigned char;
			cout << promptUChar;
			cin >> *ucharData;
		}
		else {
			ucharData = new unsigned char[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptUChar;
				cin >> ucharData[i];
			}
		}
		fieldDef.data = ucharData;
		break;
	case 4:
		if(fieldDef.dimension == 0) {
			shortData = new short;
			cout << promptShort;
			cin >> *shortData;
		}
		else {
			shortData = new short[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptShort;
				cin >> shortData[i];
			}
		}
		fieldDef.data = shortData;
		break;
	case 5:
		if(fieldDef.dimension == 0) {
			ushortData = new unsigned short;
			cout << promptUShort;
			cin >> *ushortData;
		}
		else {
			ushortData = new unsigned short[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptUShort;
				cin >> ushortData[i];
			}
		}
		fieldDef.data = ushortData;
		break;
	case 6:
		if(fieldDef.dimension == 0) {
			intData = new int;
			cout << promptInt;
			cin >> *intData;
		}
		else {
			intData = new int[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptInt;
				cin >> intData[i];
			}
		}
		fieldDef.data = intData;
		cout << "Int data: " << *((int*)fieldDef.data) << endl;
		break;
	case 7:
		if(fieldDef.dimension == 0) {
			uintData = new unsigned int;
			cout << promptUInt;
			cin >> *uintData;
		}
		else {
			uintData = new unsigned int[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptUInt;
				cin >> uintData[i];
			}
		}
		fieldDef.data = uintData;
		break;
	case 8:
		if(fieldDef.dimension == 0) {
			floatData = new float;
			cout << promptFloat;
			cin >> *floatData;
		}
		else {
			floatData = new float[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptFloat;
				cin >> floatData[i];
			}
		}
		fieldDef.data = floatData;
		cout << "Float data: " << *((float*)fieldDef.data) << endl;
		break;
	case 9:
		if(fieldDef.dimension == 0) {
			doubleData = new double;
			cout << promptDouble;
			cin >> *doubleData;
		}
		else {
			doubleData = new double[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptDouble;
				cin >> doubleData[i];
			}
		}
		fieldDef.data = doubleData;
		break;
	case 10:
		if(fieldDef.dimension == 0) {
			stringData = new string;
			cout << promptString;
			cin >> *stringData;
		}
		else {
			stringData = new string[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptString;
				cin >> stringData[i];
			}
		}
		fieldDef.data = stringData;
		break;
	case 11:
		stringData = new string;
		if(fieldDef.dimension == 0) {
			cout << promptTime;
			cin >> *stringData;
			cin >> timeTime;
			*stringData += " ";
			*stringData += timeTime;
			timeData = new OWLTime((*stringData).c_str());
		}
		else {
			timeData = new OWLTime[fieldDef.dimension];
			for(i=0; i < fieldDef.dimension; i++) {
				cout << promptTime;
				cin >> *stringData;
				cin >> timeTime;
				*stringData += " ";
				*stringData += timeTime;
				newTimeData = new OWLTime((*stringData).c_str());
				timeData[i] = *newTimeData;
				delete newTimeData;
			}
		}
		fieldDef.data = timeData;
		break;
	}
}

void Simulator::presentStruct(DataDef& dataDef)
{
	DataDef			fieldDef;
	string			isServer = exportConfig->findIsServer(dataDef.name);
	
	string	isName = isServer + '.' + dataDef.name; 
	DaqStruct* 		structData = new DaqStruct(partition, isName);
	
	cout << dataDef.name << " is a record of different type fields" << endl;
	cout << "Let us prepare data field by field" << endl;
	for(unsigned i=0; i < dataDef.dimension; i++) {
		cout << "Field " << i << ": " << endl;
		fieldDef.type = 100;
		while(!((fieldDef.type > 0) && (fieldDef.type < 11))) {
			cout << "Field.type ( 0 < type < 11 ): ";
			cin >> fieldDef.type;
		}
		fieldDef.dimension = 0;
		while(!(fieldDef.dimension > 0)) {
			cout << "Field.dimension ( >0 ): ";
			cin >> fieldDef.dimension;
		}
		if(fieldDef.dimension == 1)
			fieldDef.dimension = 0;
		getFieldData(fieldDef);
		structData->addValue(fieldDef.type, fieldDef.dimension, fieldDef.data);	
		structDef.push_back(fieldDef);
	}
	try {
		structData->checkin();
	}
	catch(daq::is::RepositoryNotFound) {
		std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
			 << " is refused " << std::endl;
		std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
	}
	catch(daq::is::InvalidName) {
		std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
			 << isServer << std::endl;
	}
	catch(daq::is::InfoNotCompatible) {
		std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
			 << " for " << isServer << std::endl;
	}
	delete structData;
}

void Simulator::updateStruct(DataDef& dataDef)
{
	DataDef*		fieldDef;
	string			isServer = exportConfig->findIsServer(dataDef.name);

	string	isName = isServer + '.' + dataDef.name; 
	DaqStruct* 		structData = new DaqStruct(partition, isName);
	
	cout << "Updating " << isName << endl;

	for(unsigned i=0; i < dataDef.dimension; i++) {
		cout << "Field " << i << ": " << endl;
		fieldDef = &structDef[i];
		getFieldData(*fieldDef);
		structData->addValue(fieldDef->type, fieldDef->dimension, fieldDef->data);	
	}
	try {
		structData->checkin();
	}
	catch(daq::is::RepositoryNotFound) {
		std::cerr << "ERROR: DAQ data " << dataDef.name << " for " << isServer 
			 << " is refused " << std::endl;
		std::cerr << "Most probably, " << isServer << " is not running" << std::endl;
	}
	catch(daq::is::InvalidName) {
		std::cerr << "ERROR: InvalidName " << dataDef.name << " for " 
			 << isServer << std::endl;
	}
	catch(daq::is::InfoNotCompatible) {
		std::cerr << "ERROR: 'InfoNotCompatible' for " << dataDef.name 
			 << " for " << isServer << std::endl;
	}
	delete structData;
}

void Simulator::printTypes()
{
	cout << "Allowed type codes are the following:" << endl;
	cout << "	1- bool, 2 - char, 3 - unsigned char, 4 - short," << endl;
	cout << "	5- unsigned short, 6 - int, 7 - unsigned int, " << endl;
	cout << "	8- float, 9 - double, 10 - string, 11 - time, 13 - struct" << endl;
}

void Simulator::run()
{
	DataDef			exportData;
	string			dpName;
	string			baseName("ForDAQ_");
	unsigned int	dataNumber = 0;
	unsigned int	reqNumber = 0;
	vector<string>	reqDp;
	vector<string>	reqSet;
	DdcRequest*		sender = 0;
	vector<DdcNameItem>	dpList;
	vector<DdcNameItem>::const_iterator it;
	char			buf[32];
	string 			errMsg;
	int 			howMany;
	int 			i;
		
  // First build the table of controlled (simulated) systems
  	exportConfig->getConfig(dpList);
	for(it = dpList.begin(); it != dpList.end(); it++) {
		dpName = it->getDpName();
		exportData.name = dpName;
		exportData.type = 0;
		exportData.dimension = 0;
		cout << "PVSS client name: " << dpName << endl;
		printTypes();
		while(!((exportData.type > 0) && (exportData.type < 14))) {
			cout << dpName << ".type ( 1-10, 13 ): ";
			cin >> exportData.type;
		}
		if((exportData.type == 13) && !structDef.empty())
			cerr << "Only one struct data is allowed => Ignored!" << endl;
		else {
			while(!(exportData.dimension > 0)) {
				cout << dpName << ".dimension ( >0 ): ";
				cin >> buf;
				exportData.dimension = atoi(buf);
			}
			cout << "Making initial value of " << dpName << endl;
			if(exportData.type == 13) 		// a struct
				presentStruct(exportData);
			else
				presentData(exportData);
			table.push_back(exportData);
		}
	}
	dataNumber = table.size();
		
  	requestConfig->getConfig(dpList);
	if(dpList.size() != 0) {
		dpName = dpList[0].getDpName();
		reqDp.push_back(dpName);
		const string dataReqName(dpName);
		string reqServerName = requestConfig->findIsServer(dataReqName);
		sender = new DdcRequest(partition, reqServerName);
	}
	reqNumber = reqDp.size();
	if(reqNumber)
		reqNumber += 2;		// + subscribe/unsubscribe requests
	
	if(dataNumber + reqNumber == 0) {
		cout << "Simulator: No any data defined for simulation!" << endl;
		cerr << "Simulator: Exiting... !" << endl;
		return;
	}
	
	cout << endl;
	cout << "#############   Simulation   ##############" << endl;
	cout << "#######   of DAQ parameter export   #######" << endl;
	cout << "##   and issuing requests for DCS data   ##" << endl;
	cout << "###########################################" << endl << endl;
	
  // Now print the table and expose (where has not been yet done)
  // the initial data to the IS server
	cout << "DAQ parameters are being simulated:" << endl;
	int n = 0;
	for(vector<DataDef>::iterator it = table.begin(); it != table.end(); ++it) {
		cout << ++n << ". " << it->name << "  Type: ";
		switch(it->type) {
		case 1: 
			cout << "bool ";
			break;
		case 2: 
			cout << "char ";
			break;
		case 3: 
			cout << "unsigned char ";
			break;
		case 4: 
			cout << "short ";
			break;
		case 5: 
			cout << "unsigned short ";
			break;
		case 6: 
			cout << "int ";
			break;
		case 7: 
			cout << "unsigned int ";
			break;
		case 8: 
			cout << "float ";
			break;
		case 9: 
			cout << "double ";
			break;
		case 10: 
			cout << "string ";
			break;
		case 11: 
			cout << "time ";
			break;
		case 13: 
			cout << "struct ";
			break;
		}
		cout << "Dimension: " << it->dimension;
		if(it->type == 13) 
			cout << " fields";
		cout << endl;
	}
	if(reqNumber != 0) {
		cout << "DCS data requests are being simulated" << endl;
		cout << ++n << ". " << "READ_DATA_REQUEST" << endl;
		cout << ++n << ". " << "SUBSCRIBE_REQUEST" << endl;
		cout << ++n << ". " << "UNSUBSCRIBE_REQUEST" << endl;
	}
	
  // And finally - cycle of changing status parameters or requests
	int	overTable;
	string reqTypeName;
	while(0 == 0) {
		cout << endl << "Type a parameter number : ";
		cin >> buf;
		n = atoi(buf);
		if(n <= 0)
			break;
		if((unsigned int)n <= dataNumber) {
			cout << "Preparing new value of " << table[n-1].name << endl;
			if(table[n-1].type != DAQSIM_TYPE_STRUCT)
				presentData(table[n-1]);
			else
				updateStruct(table[n-1]);
		}
		else {
			overTable = n - dataNumber;
			if((overTable == 3) || (overTable == 2)) {
				string	oneName = "++";
				switch(overTable) {
				case 2:
					cout << "How many data like 'ForDAQ_xxx' do you want to subscribe: ";
					reqTypeName = DAQ_SUBSCRIBE_REQUEST;
					cin >> buf;
					cerr << buf << " data items to be subscribed" << endl;
					break;
				case 3:
					cout << "How many data like 'ForDAQ_xxx' do you want to unsubscribe: ";
					reqTypeName = DAQ_UNSUBSCRIBE_REQUEST;
					cin >> buf;
					cerr << buf << " data items to be unsubscribed" << endl;
					break;
				default: ;		// Impossible case
				}
				
				howMany = atoi(buf);
				
				for(i=1; i <= howMany; i++) {
					sprintf(buf, "%d", i);
					if(overTable == 2) {
						oneName = baseName + buf + "=>DDC";
					}
					else {
						oneName = baseName + buf;
					}
					reqSet.push_back(oneName);
				}
				
				cout << "One after the other (alternativly - all together) [Y/N]?: ";
				cin >> buf;
				if(buf[0] == 'Y' or buf[0] == 'y') {
					for(i=0; i<howMany; i++) {
						switch(overTable) {
						case 1: 
							sender->ddcReadDcsData(reqSet[i], errMsg);
							break;
						case 2: 
							sender->ddcMoreImportOnChange(reqSet[i], errMsg);
							break;
						case 3: 
							sender->ddcCancelImport(reqSet[i], errMsg);
							break;
						default: ;
						}
						if(errMsg.size())
							std::cerr << errMsg << std::endl;
					}
				}
				else {
				
					cout << "Request accepted: ";
//					for(unsigned int i=0; i < reqSet.size(); i++)
//						cout << reqSet[i] << " ";
					cout << endl;
				
					if(reqSet.size() > 1) {
						switch(overTable) {
						case 1: 
							sender->ddcReadDcsData(reqSet, errMsg);
							break;
						case 2: 
							sender->ddcMoreImportOnChange(reqSet, errMsg);
							break;
						case 3: 
							sender->ddcCancelImport(reqSet, errMsg);
							break;
						default: ;
						}
						if(errMsg.size())
							std::cerr << errMsg << std::endl;
					}
					else {
						switch(overTable) {
						case 1: 
							sender->ddcReadDcsData(reqSet[0], errMsg);
							break;
						case 2: 
							sender->ddcMoreImportOnChange(reqSet[0], errMsg);
							break;
						case 3: 
							sender->ddcCancelImport(reqSet[0], errMsg);
							break;
						default: ;
						}
						if(errMsg.size())
							std::cerr << errMsg << std::endl;
					}
				}
				reqSet.erase(reqSet.begin(), reqSet.end());
			}
			else {
				cerr << "This request is not handled by this application" << endl;
			}
		}
	}
	return;
}


int main(int argc, char *argv[])
{
	Simulator* myDaq = NULL;
	DdcDaqsimConfig* dcsImportConfig = 0;
	DdcDaqsimConfig* daqRequestConfig = 0;
	string 	ddc_conf;

    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);
    CmdArgStr	appl_name('n', "this_appl", "application-name", "name of this application.", CmdArg::isREQ);
	CmdArgStr 	ddc_config_path('C', "DDC_config", "config-path", "path of daqsim.config.", CmdArg::isREQ);

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, &appl_name, &ddc_config_path, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmdLine.parse(arg_iter);
	
	dcsImportConfig = new DdcDaqsimConfig((const string)ddc_config_path, (const string)appl_name, TO_DCS);
	daqRequestConfig = new DdcDaqsimConfig((const string)ddc_config_path, (const string)appl_name, DAQ_REQUESTS);

	IPCCore::init(argc, argv);				
	IPCPartition partition((const char*)partition_name);
	
	myDaq = new Simulator(dcsImportConfig, daqRequestConfig, partition);
	myDaq->run();
	
	return(0);	
}
