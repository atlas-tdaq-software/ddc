#ifndef _DDCDAQSIMCONFIG_H_
#define _DDCDAQSIMCONFIG_H_

//
// This header file contains definitions of DDC-daqsim subsystem 
// configuration data related classes  (on the base of DDC-DT' config)
//							S. Khomoutnikov 
//							26.02.03
//	
//=========================================================================	

/*
// These are definitions and includes sitting in DdcRequest.hxx
#ifndef _DDCDAQREQTYPES_H_
#define _DDCDAQREQTYPES_H_
// IS entry name of DAQ request for single read DCS data
#define	DAQ_DATA_REQUEST		"daqDataRequest"
// IS entry name of DAQ request for subscription of DCS data
#define	DAQ_SUBSCRIBE_REQUEST	"daqSubscribeRequest"
// IS entry name of DAQ request for unsubscription of DCS data
#define	DAQ_UNSUBSCRIBE_REQUEST	"daqUnsubscribeRequest"
#endif

// Generated header for DAQ requests
#include <ddc/DdcDataRequestInfo.h>
#include <ddc/DdcDataRequestInfoNamed.h>
*/
#include "../DdcRequest.hxx"

#ifndef _DDCDTSECTIONSID_
#define _DDCDTSECTIONSID_
#define DDCDTSECTION1 		"[DCS-to-DAQ]"
#define DDCDTSECTION2 		"[DAQ-to-DCS]"
#define DDCDTSECTION3 		"[DAQ-REQUESTS]"
#define IS_SERVER_KEY		"IS_SERVER"
#define UNNECESSARY_END_CHARS	" \n\r"
#endif

using namespace std;

enum DdcDtDirection {
	FROM_DCS,
	TO_DCS,
	DAQ_REQUESTS
};

class DdcNameItem
{
public:
	DdcNameItem(const string& name, const string& server) 
		: NameString(name), isServer(server) {}
	DdcNameItem(const DdcNameItem& item)
		{ NameString = item.getDpName(); 
		  isServer = item.getIsServer(); }

	string getIsServer() const		{ return(isServer); }
	string getDpName() const		{ return(NameString); }
private:
	string	NameString;
	string	isServer;
};

class DdcDaqsimConfig
{

public:
	DdcDaqsimConfig(const string& config_path, 
				const string& name, DdcDtDirection dir);
	~DdcDaqsimConfig()
		{ parList.erase(parList.begin(), parList.end()); }
	string findIsServer(const string& dpeName);
	void getConfig(vector<DdcNameItem>& config)	{ config = parList; }
	int	getItemNum() { return(parList.size()); }
protected:
	void addItem(DdcNameItem* item)	{ parList.push_back(*item); }
private:
	string	applName;
	vector<DdcNameItem> parList;
};

#endif
