//
// Defnitions of methods of DAQ data simulator Configuration class
//							V. Khomoutnikov 
//							26.02.03
// See DdcDaqsimConfig.hxx to follow modifications
//

#include <string>
#include <vector>
#include <fstream>
#include <ctype.h>

#include "DdcDaqSimConfig.hxx"

DdcDaqsimConfig::DdcDaqsimConfig(const string& config_path, 
						 const string& name, DdcDtDirection dir)
	: applName(name)
{
	string 		mBuf;
	string 		section;
	ifstream 	fConfig(config_path.c_str());
	string		isServer;
	bool 		server_flag = false;
	bool		error = false;

	if(fConfig) {
	  // looking for config section
		fConfig >> mBuf;

		if(dir == DAQ_REQUESTS)
			section = DDCDTSECTION3;
		else
			section = (dir == FROM_DCS) ? DDCDTSECTION1 : DDCDTSECTION2;
		
		while ((mBuf != section) && (!fConfig.eof()))
			fConfig >> mBuf;
		
		if(!fConfig.eof()) {
		  // looking for IS servers and corresponding data
			getline(fConfig, mBuf);
			
			if(!fConfig.eof())
				do {
					if(mBuf.find('[') != string::npos) {
						unsigned char symb = mBuf[mBuf.find('[')+1];
						if(!isdigit(symb))
							break;
					}
					
					if((mBuf[0] != '#') && (mBuf[0] != ' ') && !mBuf.empty()) {	// neither comment nor empty
						if(mBuf.find(IS_SERVER_KEY) != string::npos) {
							string::size_type pos = mBuf.find('=');
							if(pos != string::npos) {
								pos = mBuf.find_first_not_of(" ", pos+1);
								if(pos != string::npos) {
									isServer = mBuf.substr(pos);
									isServer = isServer.substr(0, isServer.find_last_not_of(UNNECESSARY_END_CHARS)+1);
									server_flag = true;
								}
								else 
									error = true;
							}
							else 
								error = true;
								
							if(error) {
								cerr << applName << "_ERROR: in the server definition" << endl;
								cerr << "###### " << mBuf  << endl;
								parList.erase(parList.begin(), parList.end());
								break;
							}
							else {
								if(dir == DAQ_REQUESTS) {		// Only server name is used
									DdcNameItem* item = new DdcNameItem(DAQ_DATA_REQUEST, isServer);
									parList.push_back(*item);
									cerr << applName << "_INFO: DAQ request for data is configured" << endl;
									delete item;
									item = new DdcNameItem(DAQ_SUBSCRIBE_REQUEST, isServer);
									parList.push_back(*item);
									cerr << applName << "_INFO: DAQ request for subscription configured" << endl;
									delete item;
									item = new DdcNameItem(DAQ_UNSUBSCRIBE_REQUEST, isServer);
									parList.push_back(*item);
									cerr << applName << "_INFO: DAQ request for unsubscription configured" << endl;
									delete item;
									break;
								}
							}
						}
						else {
							if(!server_flag) {
								cerr << applName << "_ERROR: No IS server definition for " 
									 << mBuf << endl;
								break;
							}
							else {
								mBuf = mBuf.substr(0, mBuf.find_last_not_of(UNNECESSARY_END_CHARS)+1);
								DdcNameItem* item = new DdcNameItem(mBuf, isServer); 
								parList.push_back(*item);
								delete item;
							}
						}
					}
					getline(fConfig, mBuf);
				} while(!fConfig.eof() );
		}
		if(parList.size() == 0) {
			if(dir == DAQ_REQUESTS)
				cerr << applName << "_INFO: No DAQ request references specified" << endl;
			else {
				cerr << applName << "_INFO: None DCS parameter is configured for " << "DCS import" << endl;
			}
		}
		fConfig.close();
	}
	else {
		cerr << applName << "_ERROR: Can't open config file " << config_path << endl;
	}
}

string DdcDaqsimConfig::findIsServer(const string& dpeName)
{
	vector<DdcNameItem>::iterator current;

	for(current = parList.begin(); current != parList.end(); current++) {
		if(dpeName == current->getDpName())
			return(current->getIsServer());
	}
	return((string)"");
}
