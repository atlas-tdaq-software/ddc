#include <string>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

#include "DdcRecordPublisher.hxx"
void RecordPublisher::run()
{
	std::string		buf("EXIT");
	string 			errMsg;
	int				i;
	int      		dynstringSize;
	int64_t			bufI64;
	
	TestRecord rec(partition, "DDC.testRecord:Main:Example");
	
	rec.state = "INITIAL";
	rec.numX = 14294967295;	// MAX int32+10000000000
		
	cout << std::endl;
	cout << "#############   Simulation   ##############" << std::endl;
	cout << "#######   of export of DAQ record   #######" << std::endl;
	cout << "## with int64t and a string array and    ##" << std::endl;
	cout << "## a int64_t array (both possibly empty) ##" << std::endl;
	cout << "##<string, int64_t, string[], int64_t[]> ##" << std::endl;
	cout << "###########################################" << std::endl << std::endl;
	
  // Cycle of changing record fields
	while(0 == 0) {
		cout << "State now is " << rec.state << std::endl << "Type new state: ";
		cin >> buf;
		
		if((buf == "EXIT") || (buf == "exit") || (buf == "Exit")) 
			break;
			
		rec.state = buf;
		cout << "New state is " << rec.state << std::endl;
		
		cout << "numX now is " << rec.numX << std::endl << "Type new numX: ";
		cin >> buf;
		
		if(buf != " ")
			rec.numX = atoll(buf.c_str());
		cout << "New numX is " << rec.numX << std::endl << std::endl;

		cout << "dyn_string contains now " << rec.dyn_string.size() << " elements";
		if(rec.dyn_string.size() > 0) {
			cout << ":" << std::endl;
			for(i=0; i<rec.dyn_string.size(); i++)
				cout << rec.dyn_string[i] << std::endl;
		}
		else 
			cout << std::endl;
		cout << "Type new number of dyn_string elements: ";
		cin >> buf;
		dynstringSize = atoi(buf.c_str());
		
		rec.dyn_string.clear();
		if(dynstringSize != 0) {
			for(i=0; i<dynstringSize; i++) {
				cout << "Type dyn_string[" << i << "]: ";
				cin >> buf;
				rec.dyn_string.push_back(buf);
			}
			cout << std::endl;
			cout << "New dyn_string contains now " << rec.dyn_string.size() << " elements:" << std::endl;
			for(i=0; i<rec.dyn_string.size(); i++)
				cout << rec.dyn_string[i] << std::endl;
		}
		
		cout << "dyn_int64 contains now " << rec.dyn_int64.size() << " elements";
		if(rec.dyn_int64.size() > 0) {
			cout << ":" << std::endl;
			for(i=0; i<rec.dyn_int64.size(); i++)
				cout << rec.dyn_int64[i] << std::endl;
		}
		else 
			cout << std::endl;
		cout << "Type new number of dyn_int64 elements: ";
		cin >> buf;
		dynstringSize = atoi(buf.c_str());
		
		rec.dyn_int64.clear();
		if(dynstringSize != 0) {
			for(i=0; i<dynstringSize; i++) {
				cout << "Type dyn_int64[" << i << "]: ";
				cin >> buf;
				bufI64 = atoll(buf.c_str());
				rec.dyn_int64.push_back(bufI64);
			}
			cout << std::endl;
			cout << "New dyn_int64 contains now " << rec.dyn_int64.size() << " elements:" << std::endl;
			for(i=0; i<rec.dyn_int64.size(); i++)
				cout << rec.dyn_int64[i] << std::endl;
		}
		
		try {
			rec.checkin();
		}
		catch(daq::is::RepositoryNotFound) {
			std::cerr << "ERROR: DAQ testRecord for " << "DDC" 
			 		<< " is refused " << std::endl;
			std::cerr << "Most probably, " << "DDC" << " is not running" << std::endl;
		}
		catch(daq::is::InvalidName) {
			std::cerr << "ERROR: InvalidName DDC.testRecord for " 
				 	<< "DDC" << std::endl;
		}
		catch(daq::is::InfoNotCompatible) {
			std::cerr << "ERROR: 'InfoNotCompatible' for DDC.testRecord for " << "DDC" << std::endl;
		}
		
		cout << "New record published" << std::endl << std::endl;
	}
	rec.remove();
}


void RecordPublisher::runAutomatic(int cycleNum, int delay)
{
	std::string		buf("EXIT");
	string 			errMsg;
	int				i;
	int      		size;
	int 			count = cycleNum;
	char			stringBuf[16];
	
	TestRecord rec(partition, "DDC.testRecord_Example");
	
	rec.state = "INITIAL";
	rec.numX = 14294967295;	// MAX int32+10000000000
	rec.Char = '0';
	rec.UChar = ' ';
	rec.Short = 0;
	rec.UShort = 1;
	rec.Num = -222;
	rec.uNum = 1;
	rec.unumX = 14294967296;	// rec.numX + 1
	rec.fNum = 0.;
	rec.dNum = 1.;
	rec.flag = true;
		
	cout << std::endl;
	cout << "#############   Simulation   ##############" << std::endl;
	cout << "##   of automatic export of DAQ record   ##" << std::endl;
	cout << "## with int64t and a string array and    ##" << std::endl;
	cout << "## a int64_t array (10 and 20 elements)  ##" << std::endl;
	cout << "##<string, int64_t, string[], int64_t[]> ##" << std::endl;
	cout << "###########################################" << std::endl << std::endl;
	
  // Cycle of changing record fields
	while(count > 0) {
		if((buf == "INITIAL")||(buf == "START"))
			buf = "STOP";
		else 
			buf = "START";
			
		if(rec.flag == true)
			rec.flag = false;
		else
			rec.flag = true;
			
		rec.state = buf;
		rec.Char += 1;
		rec.UChar += 1;
		rec.Short += 1;
		rec.UShort += 1;
		rec.Num += 1;
		rec.uNum += 1;
		rec.fNum += 0.5;
		rec.dNum += 1.2;

		rec.numX += 2;
		rec.unumX += 2;
		
		size = 10;
		
		rec.dyn_string.clear();
		for(i=0; i<size; i++) {
			sprintf(stringBuf,"%d%c",i,'\0');
			rec.dyn_string.push_back(stringBuf);
		}

		rec.dyn_int64.clear();
		for(i=0; i<size; i++) {
			rec.dyn_int64.push_back(rec.numX+i);
		}
		
		rec.vChar.clear();
		rec.vUChar.clear();
		rec.vShort.clear();
		rec.vUShort.clear();
		rec.vInt.clear();
		rec.vUInt.clear();
		rec.vFloat.clear();
		rec.vDouble.clear();
		rec.vUint64.clear();
		
		for(i=0; i<size; i++) {
			rec.vChar.push_back(rec.Char+i);
			rec.vUChar.push_back(rec.UChar+i);
			rec.vShort.push_back(rec.Short+i);
			rec.vUShort.push_back(rec.UShort+i);
			rec.vInt.push_back(rec.Num+i);
			rec.vUInt.push_back(rec.uNum+i);
			rec.vFloat.push_back(rec.fNum+i);
			rec.vDouble.push_back(rec.dNum+i);
			rec.vUint64.push_back(rec.unumX+i);
		}
		
		try {
			rec.checkin();
		}
		catch(daq::is::RepositoryNotFound) {
			std::cerr << "ERROR: DAQ testRecord for " << "DDC" 
			 		<< " is refused " << std::endl;
			std::cerr << "Most probably, " << "DDC" << " is not running" << std::endl;
		}
		catch(daq::is::InvalidName) {
			std::cerr << "ERROR: InvalidName DDC.testRecord for " 
				 	<< "DDC" << std::endl;
		}
		catch(daq::is::InfoNotCompatible) {
			std::cerr << "ERROR: 'InfoNotCompatible' for DDC.testRecord for " << "DDC" << std::endl;
		}
		count--;
		if(delay>0)
			sleep(delay);
		else
			usleep(500);
	}
	cout << cycleNum << " records published" << std::endl << std::endl;
	rec.remove();
}


int main(int argc, char *argv[])
{
	RecordPublisher* myDaq = NULL;
	string 	ddc_conf;
	std::string  cbuf;
	int cycles, pause;

    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);
    CmdArgStr	appl_name('n', "this_appl", "application-name", "name of this application.", CmdArg::isREQ);

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, &appl_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmdLine.parse(arg_iter);
	
	IPCCore::init(argc, argv);				
	IPCPartition partition((const char*)partition_name);
	
	myDaq = new RecordPublisher(partition);
	cout << "Run in automatic mode? [y/n]>";
	cin >> cbuf;
	if(cbuf[0] != 'n') {
		cout  << "Cycle number: [10] >";
		cin >> cbuf;
		cycles = atoi(cbuf.c_str());
		if(cycles == 0) cycles = 10;
		cout << "Delay between cycles (sec) [2] >";
		cin >> cbuf;
		pause = atoi(cbuf.c_str());
		if(pause == 0) pause = -1;
		myDaq->runAutomatic(cycles,pause);
	}
	else
		myDaq->run();
	
	return(0);	
}
