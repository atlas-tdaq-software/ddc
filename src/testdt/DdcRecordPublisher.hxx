#ifndef _REC_SYMULATOR_H_
#define _REC_SYMULATOR_H_


#include <ipc/core.h>
#include <is/namedinfo.h>
#include <cmdl/cmdargs.h>


class TestRecord : public ISNamedInfo
{
public:
	TestRecord(IPCPartition &p, const std::string& obName)
		: ISNamedInfo(p, obName.c_str()) {}
	
	string	state;
	vector<float> vFloat;
	char Char;
	bool flag;
	vector<char> vChar;
	unsigned char UChar;
	vector<unsigned char> vUChar;
	short Short;
	vector<short> vShort;
	unsigned short UShort;
	vector<unsigned short> vUShort;
	int Num;
	vector<int> vInt;
	unsigned int uNum;
	vector<unsigned int> vUInt;
	int64_t numX;
	uint64_t unumX;
	float fNum;
	double dNum;
	vector<double> vDouble;
	vector<std::string> dyn_string;
	vector<int64_t> dyn_int64;
	vector<uint64_t> vUint64;
	
	void publishGuts(ISostream &ostrm); 
	void refreshGuts(ISistream &istrm);
};


inline void TestRecord::publishGuts(ISostream &ostrm)
{
	int j;
	int dynlen;
	ostrm << state;
	dynlen = vFloat.size();
	float* fptr = new float[dynlen];
	for(j=0; j < dynlen; j++)
		fptr[j] = vFloat[j];
	ostrm.put(fptr, dynlen);
	delete[] fptr;
	
	ostrm << Char;
	ostrm << flag;
	
	dynlen = vChar.size();
	char* chptr = new char[dynlen];
	for(j=0; j < dynlen; j++)
		chptr[j] = vChar[j];
	ostrm.put(chptr, dynlen);
	delete[] chptr;
	
	ostrm << UChar;
	dynlen = vUChar.size();
	unsigned char* uchptr = new unsigned char[dynlen];
	for(j=0; j < dynlen; j++)
		uchptr[j] = vUChar[j];
	ostrm.put(uchptr, dynlen);
	delete[] uchptr;
	
	ostrm << Short;
	dynlen = vShort.size();
	short* shptr = new short[dynlen];
	for(j=0; j < dynlen; j++)
		shptr[j] = vShort[j];
	ostrm.put(shptr, dynlen);
	delete[] shptr;
	
	ostrm << UShort;
	dynlen = vUShort.size();
	unsigned short* ushptr = new unsigned short[dynlen];
	for(j=0; j < dynlen; j++)
		ushptr[j] = vUShort[j];
	ostrm.put(ushptr, dynlen);
	delete[] ushptr;
	
	ostrm << Num;
	dynlen = vInt.size();
	int* iptr = new int[dynlen];
	for(j=0; j < dynlen; j++)
		iptr[j] = vInt[j];
	ostrm.put(iptr, dynlen);
	delete[] iptr;
	
	ostrm << uNum;
	dynlen = vUInt.size();
	unsigned int* uiptr = new unsigned int[dynlen];
	for(j=0; j < dynlen; j++)
		uiptr[j] = vUInt[j];
	ostrm.put(uiptr, dynlen);
	delete[] uiptr;
	
	ostrm << numX;
	ostrm << unumX;

	ostrm << fNum;
	
	ostrm << dNum;
	dynlen = vDouble.size();
	double* dptr = new double[dynlen];
	for(j=0; j < dynlen; j++)
		dptr[j] = vDouble[j];
	ostrm.put(dptr, dynlen);
	delete[] dptr;
	
	dynlen = dyn_string.size();
	std::string* ptr = new std::string[dynlen];
	for(j=0; j < dynlen; j++)
		ptr[j] = dyn_string[j];
	ostrm.put(ptr, dynlen);
	delete[] ptr;	
	
	dynlen = dyn_int64.size();
	int64_t* ptr64 = new int64_t[dynlen];
	for(j=0; j < dynlen; j++)
		ptr64[j] = dyn_int64[j];
	ostrm.put(ptr64, dynlen);

	dynlen = vUint64.size();
	uint64_t* uptr64 = new uint64_t[dynlen];
	for(j=0; j < dynlen; j++)
		uptr64[j] = vUint64[j];
	ostrm.put(uptr64, dynlen);
}

inline void TestRecord::refreshGuts(ISistream &istrm) {}	// Dummy function (no need)

class RecordPublisher
{
public:
	RecordPublisher(IPCPartition& p) 
		: partition(p) { }
	~RecordPublisher() { }
	void run();
	void runAutomatic(int cyclec, int delay);

private:
	IPCPartition			partition;
};


#endif
