#ifndef _DDCDIRCOMM_H_
#define _DDCDIRCOMM_H_

//
// This header file contains definition of object class
// DdcDirectCommander and relatives
//							S. Khomoutnikov 
//							23.07.07
// Modifications:
//		S.Kh	
//
////////////////////////////////////////////////////////////////////////////////

// Error codes used by DirectCommander while command execution
//---------------------------------------------------------
//!! DCS developer must define command error codes 
//   taking into account that the negative codes  
//   listed below may not be assigned by DCS
//=========================================================
#define COMM_ERR_OK		 	 0	// no errors
#define COMM_ERR_UNDEF		-1	// undefined status
#define COMM_ERR_NAME		-2	// invalid command name
#define COMM_ERR_DCSDP		-3	// invalid a DCS' datapoint definition
#define COMM_ERR_TIMEOUT	-4	// timeout expired
#define COMM_ERR_CONNECT	-5	// SCADA communication fault (no connection)
#define COMM_ERR_CONNBROKEN	-6	// SCADA communication fault (connection broken)
#define COMM_ERR_CONFIG		-7	// reserved
#define	COMM_ERR_NTDESC		-8	// reserved
#define COMM_ERR_BUSY		-9	// reserved
#define	COMM_ERR_DCSNOTREADY	-10	// reserved
//=========================================================

const std::string DIM_DNS_NAME("DIS_DNS");

class DdcDirectCommand
{
public:
	DdcDirectCommand() {}
	DdcDirectCommand(std::string host = "") : pvssHost(host) {}
	DdcDirectCommand(std::string name, std::string params, unsigned int t, 
					 std::string host = "")
		: pvssHost(host), commandName(name), parameters(params), timeout(t) {}
		
	std::string getHostName()			{ return(pvssHost); }
	std::string getCommName()			{ return(commandName); }
	std::string	getParValue()			{ return(parameters); }
	int		getTimeout()				{ return(timeout); }
	void	setHostName(std::string& hn){ pvssHost = hn; }
	void	setCommName(std::string& s)	{ commandName = s; }
	void	setParValue(std::string& pr){ parameters = pr; }
	void	setTimeout(unsigned int t)	{ timeout = t; }
private:
	std::string		pvssHost;
	std::string		commandName;
	std::string		parameters;
	unsigned int 	timeout;	
};


class DdcDirectCommander;

class DdcDirectCommanderInfo : public DimRpcInfo
{
public:
	DdcDirectCommanderInfo(std::string& name, int timeout, int nolink, 
						   std::string host, std::vector<std::string>& keys, 
						   DdcDirectCommander* father)
		: DimRpcInfo((char *)name.c_str(), timeout, nolink),
		  pvssHost(host), commandName(name), commandKeys(keys), ddcCmdr(father) {}
	void 	rpcInfoHandler();
private:
	std::string			pvssHost;
	std::string			commandName;
	std::vector<std::string>	commandKeys;
	DdcDirectCommander*	ddcCmdr;
};


class DdcDirectCommander : public DimClient
{

public:
	DdcDirectCommander()
	{ std::cerr << "DDC Direct Commander is constructed" << std::endl;}
		  
	~DdcDirectCommander() {}
	virtual void cmdrNotify(std::string host, std::string cmd, int response,
							std::vector<std::string> keyWords);
	bool launchCommand(DdcDirectCommand& cmd, std::vector<std::string>& keys);
	void decodeError(int commError, std::string& msg);
private:

};

#endif
