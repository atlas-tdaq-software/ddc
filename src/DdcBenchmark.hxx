#ifndef _DDCBENCHMARK_H_
#define _DDCBENCHMARK_H_

//
// benchmark timers, Schlenk 09.08.2007
//

#include <owl/timer.h>

class DdcBenchmark
{

public:
  static void TimerReport();
  static OWLTimer& getDimTimer() {return DdcBenchmark::fDimTimer;}; // time between trigger sending and dim answer
  static OWLTimer& getIsTimer() {return DdcBenchmark::fIsTimer;}; // time for checkin of command result
  static OWLTimer& getControllerTimer() {return DdcBenchmark::fControllerTimer;}; // time for internal controller processing
  static void   nextCommand() {++fNumNtCommands;};
  static void	clean()	{ fDimTimer.reset(); fIsTimer.reset(); fControllerTimer.reset(); 
  						  fNumNtCommands = 0; }


private:

  static OWLTimer fDimTimer; // time between trigger sending and dim answer
  static OWLTimer fIsTimer; // time for checkin of command result
  static OWLTimer fControllerTimer; // time for internal controller processing
  static long     fNumNtCommands;

};

#endif
