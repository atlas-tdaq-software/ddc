///////////////////////////////////////////////////////////////////////////
// This file contains definition of DdcRequest class to produce DAQ
// requests for DCS data single read or change DCS-export configuration
//					
//									Created 10.01.03
//									by S.Khomutnikov
// Modifications:
//		S.Kh	23.11.03	Methods to subscribe more DCS data and to
//							unsubscribe some DCS data are introduced;
//		S.Kh	01.11.06	Methods accept daq::is::Exception and return boolean
// 
///////////////////////////////////////////////////////////////////////////

#ifndef _DDC_REQUEST_H_
#define _DDC_REQUEST_H_

#ifndef _DDCDAQREQTYPES_H_
#define _DDCDAQREQTYPES_H_
// IS entry name of DAQ request for single read DCS data
#define	DAQ_DATA_REQUEST		"daqDataRequest"
// IS entry name of DAQ request for subscription of DCS data
#define	DAQ_SUBSCRIBE_REQUEST	"daqSubscribeRequest"
// IS entry name of DAQ request for unsubscription of DCS data
#define	DAQ_UNSUBSCRIBE_REQUEST	"daqUnsubscribeRequest"
#endif

// Generated header for DAQ requests
#include <ddc/DdcDataRequestInfo.h>
#include <ddc/DdcDataRequestInfoNamed.h>


class DdcRequest
{
public:
	DdcRequest(IPCPartition& p, std::string& isServer);
	bool ddcReadDcsData(std::vector<std::string>& request, std::string& errMsg);
	bool ddcReadDcsData(std::string& request, std::string& errMsg);
	bool ddcMoreImportOnChange(std::vector<std::string>& request, std::string& errMsg);
	bool ddcMoreImportOnChange(std::string& request, std::string& errMsg);
	bool ddcCancelImport(std::vector<std::string>& request, std::string& errMsg);
	bool ddcCancelImport(std::string& request, std::string& errMsg);

private:
	bool 	ddcSendRequest(std::vector<std::string>& request, std::string reqName, 
										std::string& errMsg);
	bool 	ddcSendRequest(std::string& request, std::string reqName, std::string& errMsg);
	
	IPCPartition	partition;
	std::string		requestServer;
};

#endif
