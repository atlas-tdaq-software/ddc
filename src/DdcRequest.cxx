///////////////////////////////////////////////////////////////////////////
// This file contains definition of functions to produce 
// DAQ request for DCS data single read 
//					
//									Created 08.01.03
//									by S.Khomutnikov
///////////////////////////////////////////////////////////////////////////

#include 	<string>
#include 	<vector>
#include 	<algorithm>

#include <ipc/partition.h>
#include <is/namedinfo.h>

#include <ers/ers.h>

using namespace std;

#include "DdcRequest.hxx"

DdcRequest::DdcRequest(IPCPartition& p, string& isServer)
	: partition(p), requestServer(isServer)
{ }


bool DdcRequest::ddcReadDcsData(vector<string>& request, string& errMsg)
{
	ERS_LOG("Complex Read Data request");
	return(ddcSendRequest(request, DAQ_DATA_REQUEST, errMsg));
}


bool DdcRequest::ddcReadDcsData(string& request, string& errMsg)
{
	ERS_LOG("Simple Read Data request");
	return(ddcSendRequest(request, DAQ_DATA_REQUEST, errMsg));
}


bool DdcRequest::ddcMoreImportOnChange(vector<string>& request, string& errMsg)
{
	ERS_LOG("Complex Subscription request");
	return(ddcSendRequest(request, DAQ_SUBSCRIBE_REQUEST, errMsg));
}


bool DdcRequest::ddcMoreImportOnChange(string& request, string& errMsg)
{
	ERS_LOG("Simple Subscription request");
	return(ddcSendRequest(request, DAQ_SUBSCRIBE_REQUEST, errMsg));
}


bool DdcRequest::ddcCancelImport(vector<string>& request, string& errMsg)
{
	ERS_LOG("Complex Unsubscription request");
	return(ddcSendRequest(request, DAQ_UNSUBSCRIBE_REQUEST, errMsg));
}


bool DdcRequest::ddcCancelImport(string& request, string& errMsg)
{
	ERS_LOG("Simple Unsubscription request");
	return(ddcSendRequest(request, DAQ_UNSUBSCRIBE_REQUEST, errMsg));
}


bool DdcRequest::ddcSendRequest(vector<string>& request, string reqId, string& errMsg)
{
	string 			isObName;
	time_t			currTime;
	bool			success = true;
	unsigned int 	i;
	bool			incompatible = false;
	ISInfoDictionary	infoDict(partition);
	
	isObName = requestServer + '.';
	isObName += reqId;
	errMsg = "";

	if(request.size() == 0) {
		success = false;
		errMsg = "Empty request is illegal";
	}
	else {
		for(i=0; i < request.size(); i++)
			if(request[i].size() == 0) {
				success = false;
				errMsg = "Empty service name is illegal";
				break;
			}
	}
	if(!success) {
		ERS_LOG("DdcRequest_ERROR: Empty request is illegal");
		return(false);
	}

	ddc::DdcDataRequestInfoNamed* info = new ddc::DdcDataRequestInfoNamed(partition, isObName);
	info->serviceNames = request;
	success = false;
	try {
		info -> checkin();
		success = true;
	}
	catch(daq::is::RepositoryNotFound) {
		ERS_LOG("DdcRequest_ERROR: DAQ data request " << reqId << " for " << requestServer 
			 << " in partition " << partition.name() << " is refused");
		errMsg = requestServer + " not found";
	}
	catch(daq::is::InvalidName) {
		ERS_LOG("DdcRequest_ERROR: 'InvalidName' is::Exception in " << reqId << " for " 
			 << requestServer << " in partition " << partition.name());
		errMsg = "Invalid name in " + reqId;
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG("DdcRequest_ERROR: Could not checkin " << reqId << " " << ex);
	}

	return(success);	
}


bool DdcRequest::ddcSendRequest(string& request, string reqId, string& errMsg)
{
	std::vector<std::string> vectorRequest;
	
	vectorRequest.push_back(request);
	return(ddcSendRequest(vectorRequest, reqId, errMsg));
}
	
