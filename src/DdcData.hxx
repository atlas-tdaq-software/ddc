#ifndef _DDCDATA_H_
#define _DDCDATA_H_

//
// This header file contains definitions of DDC Data classes. Objects of these 
// classes represent time stamped values of DCS data to be stored in DAQ/DCS
// information system as well as of DAQ data to be exported for DCS
//							S. Khomoutnikov (Viatcheslav.Khomoutnikov@cern.ch)
//							26.10.00
// Modified:
//		S.Kh.	12.07.01 	The RunParams data class is defined to transfer
//							the number and other DAQ run charachteristics to DCS
//		S.Kh.	25.09.01 	The time stamp is now represented by an OWLTime
//							object. Constructors with 'null' time stamp
//							are suppressed.
//		S.Kh.	31.10.01 	The valueType field is removed from the DdcData
//							class definition.
//		S.Kh.	05.11.01 	Template classes for both single data and arrays 
//							to be transferrable DCS -> DAQ are defined. The
//							specified DdcData subclasses are suppressed.
//		S.Kh.	20.11.03 	DAQ run-time requests to subscribe/unsubscribe DCS
//							data (to be used in DAQ request' API)
//
//############################################################################
//
// Since release ddc-05-08-05, Mar.2014, this header is not in use any more
//
//
//############################################################################

#include <is/namedinfo.h>

#include <owl/time.h>

// IS entry name of DAQ request for single read DCS data
#define	DAQ_DATA_REQUEST		"daqDataRequest"
// IS entry name of DAQ request for subscription of DCS data
#define	DAQ_SUBSCRIBE_REQUEST	"daqSubscribeRequest"
// IS entry name of DAQ request for unsubscription of DCS data
#define	DAQ_UNSUBSCRIBE_REQUEST	"daqUnsubscribeRequest"


//
// An abstract class for any DCS data to be exported to IS  
// as a "value" with time stamp
// 
class DdcData
{	
public:
	DdcData(time_t sec) : eventTime(sec) {}
	
	time_t getTime() 		{ return (eventTime.c_time()); }
	OWLTime* getOWLTime()	{ return (&eventTime); }
	void updateTime(ISistream & isis)	{ isis >> eventTime; }
	void printTimeStamp() { std::cout << eventTime; }
private:
	OWLTime		eventTime;
};

//
// Template class to represent DCS single data value of type 'D' with time stamp
// where D means one of <bool, bit32, float, int, unsigned int, char, string>
// 
template<class D> class DcsSingleData : public DdcData, public ISNamedInfo
{
public:
	DcsSingleData(time_t sec, IPCPartition &p, const std::string& obName) 
		: DdcData(sec), ISNamedInfo(p, obName.c_str()) {}
	D	 getValue() { return(value); }
	void setValue(D data) { value = data; }
	void printValue() { std::cout << value; }
	void publishGuts(ISostream &ostrm); 
	void refreshGuts(ISistream &istrm);
private:
	D	value;
};

//
// Class to represent DCS data of type 'dynamic array of D' with time stamp
// where D means one of <bool, bit32, float, int, unsigned int, char, string>
// 
template<class D> class DcsDataArray : public DdcData, public ISNamedInfo
{
public:
	DcsDataArray(time_t sec, IPCPartition &p, const std::string& obName) 
		: DdcData(sec), ISNamedInfo(p, obName.c_str()) {}
	std::vector<D>* 	getValue() const { return(&value); }
	void addValue(D data) 		{ value.push_back(data); }
	void publishGuts(ISostream &ostrm);
	void refreshGuts(ISistream &istrm);
private:
	std::vector<D>	value;
};


//
// Class to represent dynamic array of 'blob' data with time stamp
// 
template<class D> class DcsDataArray< std::vector<D> > : public DdcData, public ISNamedInfo
{
public:
	DcsDataArray(time_t sec, IPCPartition &p, const std::string& obName) 
		: DdcData(sec), ISNamedInfo(p, obName.c_str()) {}
	std::vector< std::vector<D> >* 	getValue() const { return(&value); }
	void addValue(std::vector<D> data) 		{ value.push_back(data); }
	void publishGuts(ISostream &ostrm);
	void refreshGuts(ISistream &istrm);
private:
	std::vector< std::vector<D> >	value;
};


// ========= inline member functions ============================

template<class D> inline void DcsSingleData<D>::publishGuts(ISostream &ostrm) 
{
	ostrm << *getOWLTime() << value;
}

template<class D> inline void DcsSingleData<D>::refreshGuts(ISistream &istrm) 
{
	D	newValue;
	
	updateTime(istrm);
	istrm >> newValue;
	value = newValue;
}

template<class D> inline void DcsDataArray<D>::publishGuts(ISostream &ostrm) 
{
	ostrm << *getOWLTime();
	D* ptr = new D[value.size()];
	for(unsigned int i = 0; i < value.size(); i++) {
		ptr[i] = value[i];
	}
	ostrm.put(ptr, value.size());
	delete[] ptr;
}

template<class D> inline void DcsDataArray<D>::refreshGuts(ISistream &istrm) 
{
	updateTime(istrm);
	D * ptr;
	size_t size;
	istrm.get( &ptr, size );
	value.erase(value.begin(), value.end());
	for(size_t i = 0; i < size; i++)
		value.push_back(ptr[i]);
	delete[] ptr;
}
	
template<class D> inline void DcsDataArray< std::vector<D> >::publishGuts(ISostream &ostrm)
{
	ostrm << *getOWLTime();
	for(unsigned int i = 0; i < value.size(); i++) {
		D* line = new D[value[i].size()];
		for(unsigned int j = 0; j < value[i].size(); j++) {
			line[j] = value[i][j];
		}
		ostrm.put(line, value[i].size());
		delete[] line;
	}
}

template<class D> inline void DcsDataArray< std::vector<D> >::refreshGuts(ISistream &istrm)
{
	updateTime(istrm);
	std::cout << "The recovering of DcsDataArray< vector<D> > is not implemented"
		 << std::endl
		 << "===== Use ISInfoAny class to do this" << std::endl;
}

#endif
