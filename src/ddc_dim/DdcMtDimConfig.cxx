//
// Defnitions of methods of DDC Message Transferring Configuration class
// for DIM based version of the aplication
//							V. Khomoutnikov 
//							28.04.05
// See DdcMtDimConfig.hxx to follow modifications
//
//////////////////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <fstream>

#include <ers/ers.h>
#include <ers/Severity.h>
//#include <mrs/message.h>

using namespace std;

#include "DdcMtDimConfig.hxx"

DdcMesItem::DdcMesItem(const string& name)
	: nameString(name)
{
	msgSeverity = ers::Information;
}

DdcMesItem::DdcMesItem(const DdcMesItem& item)
	: nameString(item.nameString)
{
	nameString = item.nameString;
	msgSeverity = item.msgSeverity;
}

DdcMesItem::DdcMesItem(const string& name, const string& sever, 
											const string& applName)
	: nameString(name)
{
	if(sever == "FATAL")
		msgSeverity = ers::Fatal;
	else 
		if(sever == "ERROR")
			msgSeverity = ers::Error;
		else
			if(sever == "WARNING")
				msgSeverity = ers::Warning;
			else
				if(sever == "LOG")
					msgSeverity = ers::Log;
				else
					if(sever == "INFORMATION")
						msgSeverity = ers::Information;
					else
						if(sever == "DEBUG")
							msgSeverity = ers::Debug;
						else {
							msgSeverity = ers::Error;
							ERS_LOG(applName << "_WARNING: Invalid severity definition for "
								 << name << " " << sever);
							ERS_LOG("         ===> severity `ERROR` is set");
						}
}


void DdcMtDimConfig::createAlarmConfig(std::vector<const daq::ddc::DdcTargetList *>& targetLists)
{
	std::vector<const daq::ddc::DdcTargetList *>::const_iterator	i = targetLists.begin();
	std::vector<std::string>								datapoints;
	std::vector<std::string>::const_iterator				j;

	for(; i != targetLists.end(); ++i) {
		datapoints = (*i)->get_Datapoints();
		if(datapoints.size() != 0) {
			for(j = datapoints.begin(); j != datapoints.end(); j++) {
				DdcMesItem* item = new DdcMesItem((*j)+"/ALARM_INFO"); 
				alarmList.push_back(*item);
				ERS_LOG(applName << "_INFO: DP included: " << (*j));
				delete item;
			}
		}
	}
}


void DdcMtDimConfig::createMessageConfig(std::vector<const daq::ddc::DdcTextMessage*>& dpLists)
{
	std::vector<const daq::ddc::DdcTextMessage*>::const_iterator		i = dpLists.begin();

	for(; i != dpLists.end(); ++i) {
		DdcMesItem* item = new DdcMesItem((*i)->get_PvssDpeName(), (*i)->get_MessageSeverity(), applName);
		parList.push_back(*item);
		ERS_LOG(applName << "_INFO: DP included: (" 
			 << (*i)->get_PvssDpeName() << ", " << (*i)->get_MessageSeverity() << ")");
		delete item;
		if((*i)->get_SendWhileConnection() != "YES") {
			ignoreFirst.push_back((*i)->get_PvssDpeName());			
			ERS_LOG(applName << "_INFO: Message " << (*i)->get_PvssDpeName()
					  << " will NOT be reported on connection to DCS"); 
		}
	}
}


DdcMtDimConfig::DdcMtDimConfig(::Configuration& confDb, const string& mt_name)
	: applName(mt_name)
{
	ERS_LOG("MtDimConfig constructor .."); 
	std::vector<const daq::ddc::DdcMtApplication *> mtApplList;
	confDb.get(mtApplList);
	ERS_LOG("Applist got"); 
	
	std::vector<const daq::ddc::DdcMtApplication *>::const_iterator i = mtApplList.begin();
	const daq::ddc::DdcMtApplication* application = 0;
	
	ERS_LOG("Looking for application"); 
	for(; i != mtApplList.end(); ++i) {
		if((*i)->UID() == applName) {
			application = (*i);
			break;
		}
	}
	ERS_LOG("Finished"); 

	
	if(application) {
		ERS_LOG("Application found"); 
		if(application->get_DCSStationToConnect() == 0) {
			ERS_LOG(applName << "_FATAL: DCS station is not defined in configuration");
			pvssHost = "";
			return;
		}
		else
			pvssHost = application->get_DCSStationToConnect()->UID();
		ERS_LOG("PVSS Host = " << pvssHost); 
		std::vector<const daq::ddc::DdcTargetList*>		targetLists = application->get_DcsAlarms();
		std::vector<const daq::ddc::DdcTextMessage*>	messageList = application->get_DcsTextMessages();
		
		if(targetLists.size() == 0) {
			ERS_LOG(applName << "_WARNING: No DCS alarm specified");
		}
		else {
			ERS_LOG(applName << "_INFO: Configuring alarms");
			createAlarmConfig(targetLists);
		}

		if(messageList.size() == 0) {
			ERS_LOG(applName << "_WARNING: No DCS non-alarm messages specified");
		}
		else {
			ERS_LOG(applName << "_INFO: Configuring non-alarm messages");
			createMessageConfig(messageList);
		}
	}
	else {
		ERS_LOG(applName << "_Config_ERROR: There is no this application in confDb");
	}
}


bool DdcMtDimConfig::isInList(string& name, DdcMtListId configList)
{
	unsigned int i;
	vector<DdcMesItem>*	itemList;
	
	if(configList == DDC_ALARM_MSG) {
//		ERS_LOG(applName << "_ALARM msg");
		itemList = &alarmList;
	}
	else 
		if(configList == DDC_TEXT_MSG) {
//			ERS_LOG(applName << "_TEXT msg");
			itemList = &parList;
		}
		else 
			return(false);
//	ERS_LOG(applName << "_Looking in the list of size "<<itemList->size());
	for(i=0; i<itemList->size(); i++) {
//		ERS_LOG(i);
//		ERS_LOG((*itemList)[i].getServiceName());
		if(name == (*itemList)[i].getServiceName()) {
//			ERS_LOG("Found!");
			return(true);
		}
	}
//	ERS_LOG("NOT Found!");
	return(false);
}


DdcMesItem* DdcMtDimConfig::findMsgItem(string& name)
{
	unsigned int i;
	unsigned int l = parList.size();
	
	for(i=0; i<l; i++) {
		if(name == parList[i].getServiceName()) {
			DdcMesItem* ptr = &parList[i];
			return(ptr);
		}
	}
	return(0);
}
