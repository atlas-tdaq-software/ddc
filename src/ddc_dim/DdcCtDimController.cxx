//
// This file contains the definition of DdcCtRunController class
// method functions for sending DAQ command into DCS.
//							S. Khomoutnikov 
//							15.03.01
//
// See the history at .hxx file
//
/////////////////////////////////////////////////////////////////

#include <string>
#include <vector>
#include <time.h>

using namespace std;

#include <ipc/partition.h>
#include <ipc/alarm.h>
//#include <mrs/message.h>

//#include <RunControl/Common/UserRoutines.h>

#include <ers/ers.h>

#include "../DdcCommand.hxx"
#include "DdcCtDimConfig.hxx"
#include "DdcCtDimController.hxx"
#include "DdcCtDim.hxx"

ERS_DECLARE_ISSUE( ddc, AppWarning, message, ((const char*)message) )

void DdcCtRoutines::configureAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_CONFIG);
	if(!transitionError)
		manager->setRcState(DDC_CONFIGURED);
	return;
}

void DdcCtRoutines::connectAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_CONNECT);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::prepareAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_PREPARERUN);
	if(!transitionError)
		manager->setRcState(DDC_PREPARED);
	return;
}

void DdcCtRoutines::stopROIBAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_STOPROIB);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::stopDCAction(const daq::rc::TransitionCmd& cmd)		// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
{
	transitionError = manager->launchCommand(CMD_STOPDC);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::stopHLTAction(const daq::rc::TransitionCmd& cmd)	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
{
	transitionError = manager->launchCommand(CMD_STOPHLT);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::stopRecordingAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_STOPRECORD);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::stopGatheringAction(const daq::rc::TransitionCmd& cmd)	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
{
	transitionError = manager->launchCommand(CMD_STOPGATHER);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::stopArchivingAction(const daq::rc::TransitionCmd& cmd)	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
{
	transitionError = manager->launchCommand(CMD_STOPARCHIVE);
	if(!transitionError)
		manager->setRcState(DDC_CONNECTED);
	return;
}

void DdcCtRoutines::disconnectAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_DISCONNECT);
	if(!transitionError)
		manager->setRcState(DDC_CONFIGURED);
	return;
}

void DdcCtRoutines::unconfigureAction(const daq::rc::TransitionCmd& cmd)
{
	transitionError = manager->launchCommand(CMD_UNCONFIG);
	if(!transitionError)
		manager->setRcState(DDC_INITIAL);
	return;
}


void DdcCtRoutines::introduceMyself()
{ 
	manager->setRoutines(this);
}

void DdcCtDimController::introduceMyself()
{ 
	manager->referController(this); 
}
