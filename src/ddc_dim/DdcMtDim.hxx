#ifndef _DDCMTSERVICE_H_
#define _DDCMTSERVICE_H_

//
// This header file contains definition of DdcMtService object class. 
// An object of this class (based on DIM classes) 
// is responsible for receiving alarm messages and specified text
// data (non-alarm messages) from a certain PVSS DIM manager,
// and then send them into MRS server of a specified DAQ partition
//  
//							S. Khomoutnikov 
//							28.04.05
// Modifications:
//		S.Kh.	25.10.05	ConfDb converter of environment variables is introduced
//		S.Kh.	27.03.08	Attribute "SendWhileConnection" of DdcTextMessage (default = NO)
//							is introduced. Value YES of that indicate that the message should be 
//							sent to DAQ while connection. Another case it will be transferred
//							on change only. 'ignoreList' member contains initially the list
//							of those messages
//		S.Kh.	19.04 10	Mechanism of trying connection to DIM-DNS changed
//		S.Kh.	22.05 10	No direct MRS use, only ERS
//							
//===============================================================================

#include <dic.hxx>

#include "../DdcDal.hxx"
#include "DdcMtDimConfig.hxx"

#define CONNECTION_WAIT_TIME 60		// delay between series of connection
									// attempts (in sec)
#define PVSS_CONN_TIMEOUT  15		// Timeout for DimCurrentInfo reading DIS_DNS
#define PVSS_CONN_ATTEMPTS 6		// Number of attempts to read DIS_DNS									
									
const std::string	UNAVAILABLE_INFO("Unavailable");
const std::string	ALARM_SUFFIX("ALARM_INFO");

typedef struct			// Structure of PVSS alert information
{
	bool	active;
	int 	state;
	char	priority;
	std::string	text;
} AlarmInfo;
	

class MessageContent
{
public:
	MessageContent(std::string dp, std::string m) 
		: msgService(dp), msgText(m)	{}
	std::string getServiceName() { return(msgService); }
	std::string getText() 		{ return(msgText); }
private:
	std::string msgService;			//The name of DP being the message source
	std::string msgText;			// Message text
};


class DdcMtService : public DimClient	
{
public:
	DdcMtService(IPCPartition p, const std::string& name);
	~DdcMtService() { 	delete dcsMessageConfig; }
	
	bool doConfig(::Configuration&);
	void setPvssHost() { pvssHost = dcsMessageConfig->getPvssHost(); }
	bool doInitialize();
	void run();
	void exportDcsMessage(char* msgName, char* text, ers::severity msgSev);
	ers::severity convertPriority(char priority);
	
	static bool doExit;
protected:
	void infoHandler();
private:
	bool initExport();
	void handleWildcards(std::vector<DdcMesItem>&);
	void addActiveMsg(std::string dp, std::string txt)
		{	MessageContent* msgRef = new MessageContent(dp, txt);
			activeMsgs.push_back(*msgRef);
			delete msgRef;
		}
	bool isToInvalidateMsg(std::string dp, std::string* txt);
	
	std::vector<MessageContent>	activeMsgs;
	
	std::string			applName;
	std::string			pvssHost;
	IPCPartition		daqPartition;
	DdcMtDimConfig*		dcsMessageConfig;
	std::vector<std::string>	ignoreList;
//	MRSStream			mout;
};

#endif
