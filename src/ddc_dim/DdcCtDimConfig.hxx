#ifndef _DDCCTDIMCONFIG_H_
#define _DDCCTDIMCONFIG_H_

//
// This header file contains definitions of DDC-CT-DIM  
// configuration data related classes 
//							S. Khomoutnikov 
//							31.03.05 - 15.04.05
// Modified:
//				S.Kh.	
//
/////////////////////////////////////////////////////////////////////////////////

#include "../DdcDal.hxx"

enum CommandAttrRole {
	DDC_COMM_PARAMETERS,
	DDC_COMM_TRIGGER,
	DDC_COMM_RESPONSE
};

// Command or alarm service definition class
class DdcCtDimAlarmDef
{
public:
	DdcCtDimAlarmDef(std::string name) : serviceName(name) {}
	DdcCtDimAlarmDef(const DdcCtDimAlarmDef& parDef)
		: serviceName(parDef.serviceName) {}
	std::string	getServiceName() { return(serviceName); }

private:
	std::string				serviceName;
};

// Command definition class
class DdcCtDimCommandDef
{
public:
	DdcCtDimCommandDef(std::string name, std::string service, std::string parsValue, int timeout)
		: cmdName(name), cmdService(service), cmdParams(parsValue), cmdTimeout(timeout) {} 
	std::string 	getCmdName() const	{ return(cmdName); }
	std::string		getService() const	{ return(cmdService); }
	std::string 	getParString() const { return(cmdParams); }
	void			setTimeout(int t)	{ cmdTimeout = t; }
	int				getTimeout() const	{ return(cmdTimeout); }
private:
	std::string		cmdName;
	std::string		cmdService;
	std::string		cmdParams;
	int				cmdTimeout;
};

// DDC-CT configuration class
class DdcCtDimConfig
{
public:
	DdcCtDimConfig(Configuration&, std::string& ddcCtrlName);
	~DdcCtDimConfig() 			{ commList.erase(commList.begin(), commList.end()); }
	bool			emptyList() 		{ return(commList.empty()); }
	int				itemNum() 			{ return((int)commList.size()); }
	std::string 	getIpcPartition() 	{ return(ipcPartition); }
	std::string 	getAlarmDp() 		{ return(alarmDpService); }
	std::string		getPvssHost() const	{ return(pvssHost); }
	const std::vector<DdcCtDimCommandDef>& getCommList()	{ return(commList); }
	std::vector<DdcCtDimCommandDef>::iterator findCommDef(std::string commName);
	void print_config();
private:
	std::string		ipcPartition;
	std::string		pvssHost;
	std::string		alarmDpService;
	std::vector<DdcCtDimCommandDef> commList;
};

//==============================================================================
// Inline print configuration method (for the debugging purpose)

inline void DdcCtDimConfig::print_config()
{
	vector<DdcCtDimCommandDef>::const_iterator cPtr = getCommList().begin();
	int i, numOfCommands;
	
	std::cout << "DDC command transfer configuration" << std::endl;
	std::cout << "=====================================" << std::endl;
	std::cout << "IPC partition " << ipcPartition << std::endl;
	std::cout << "Commands:" << std::endl << "==========" << std::endl;
	if((numOfCommands = itemNum()) != 0) {
		i = 0;
		for( ; i < numOfCommands; cPtr++) {
			std::cout << "Command = " << cPtr->getCmdName() << std::endl;
			std::cout << "Parameters' string variable = " << cPtr->getParString() << std::endl;
			i++;
			std::cout << std::endl;
		}
	}
	else
		std::cout << "None command is defined" << std::endl;
}

#endif
