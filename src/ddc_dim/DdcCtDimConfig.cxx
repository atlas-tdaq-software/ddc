//
// Defnitions of methods of DDC Command Transfer Configuration class
//
//							S. Khomoutnikov 
//							31.03.05
// See DdcCtDimConfig.hxx to follow the history
//

#include <string>
#include <vector>
#include <fstream>
#include <list>

using namespace std;

#include "../DdcCommand.hxx"
#include "DdcCtDimConfig.hxx"


DdcCtDimConfig::DdcCtDimConfig(Configuration& confDb, string& ddcCtrlId)
		: alarmDpService("")
{
    const daq::ddc::DdcCtApplication * ddcController = confDb.get<daq::ddc::DdcCtApplication>(ddcCtrlId);

	if(ddcController == 0) {
		ERS_LOG(ddcCtrlId << "_FATAL: Controller " << ddcCtrlId
				  << " is NOT defined in DB");
		ddcCtrlId = "";			// flag of insucessful search
		return;
	}
	else {
		pvssHost = ddcController->get_DCSStationToConnect()->UID();
		
		const daq::ddc::PvssDatapoint* detectorAlarmDp = ddcController->get_detectorAlarmDef();
		const std::vector<const daq::ddc::DdcCommandDef *>& commandList = ddcController->get_TransitionCommands();
		
		string	commandId;
		string 	rpcService;
		string 	commandParameters;
		int		timeout;
		DdcCtDimCommandDef*	command;
		
		std::vector<const daq::ddc::DdcCommandDef *>::const_iterator i = commandList.begin();
		for(; i != commandList.end(); ++i) {
			commandId = (*i)->get_RunControlTransition();
			rpcService = (*i)->get_DcsCommand();
			commandParameters = (*i)->get_CommandParameters();
			timeout = (*i)->get_Timeout();
			command = new DdcCtDimCommandDef(commandId, rpcService, commandParameters, timeout);
			commList.push_back(*command);
			delete command;			
		}
	
		if(detectorAlarmDp) {
			alarmDpService = detectorAlarmDp->get_DpName();
		}
		else {
			ERS_LOG(ddcCtrlId << "_WARNING: TTCPartition state flag is not configured!");
			ERS_LOG("====> TDAQ will not be informed about TTC partition state");
		}
	}
	ERS_LOG(ddcCtrlId << "_INFO: Configuration of Run Control commands is finished");
}


vector<DdcCtDimCommandDef>::iterator DdcCtDimConfig::findCommDef(const string name)
{
	vector<DdcCtDimCommandDef>::iterator curItemPtr = commList.begin();
	
	for(; curItemPtr != commList.end(); curItemPtr++) {
		if(curItemPtr->getCmdName() == name)
			return(curItemPtr);
	}
	return(commList.end());
}
