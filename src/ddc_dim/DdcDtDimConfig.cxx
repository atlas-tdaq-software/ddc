//
// Defnitions of methods of DDC Data Transfer Configuration class
// on the base of DIM
//							V. Khomoutnikov 
//							08.03.05 - 
// See DdcDtDimConfig.hxx to follow modifications
//

#include <string>
#include <vector>
#include <fstream>
#include <ctype.h>

using namespace std;

#include <ers/ers.h>

#include "DdcDtDimConfig.hxx"


void DdcServiceNameItem::buildListOfSons(string& sons)
{
	if(fields.size())
		fields.erase(fields.begin(), fields.end());
	
	string oneSon;
	string::size_type pos = sons.find_first_not_of(", ;");
	string::size_type endPos;
	string::size_type dummyPos = sons.find_first_of(",;");
	while(dummyPos < pos) {
		oneSon = "";
		fields.push_back(oneSon);
		dummyPos = sons.find_first_of(",;", dummyPos+1);
	}
	while(pos != string::npos) {
		endPos = sons.find_first_of(", ;", pos);
		if(endPos == string::npos) 
			oneSon = sons.substr(pos);
		else
			oneSon = sons.substr(pos, endPos - pos);
		fields.push_back(oneSon);
		if(endPos == string::npos) {
			break;
		}
		else {
			sons = sons.substr(endPos+1);
			pos = sons.find_first_not_of(", ;");
			if(pos != string::npos) {
				dummyPos = sons.find_first_of(",;");
				while(dummyPos < pos) {
					oneSon = "";
					fields.push_back(oneSon);
					dummyPos = sons.find_first_of(",;", dummyPos+1);
				}
			}
		}
	}
}


DdcDtDimConfig::DdcDtDimConfig(const vector<string>& request, const string& name)
	: applName(name)
{
	string	isServer;
	string	dpeName;
	int		i, len;
	
	len = request.size();
	for(i=0; i < len; i++) {
		if(request[i].find("=>") == string::npos) {
			ERS_LOG(applName << "_ERROR: Incorrect syntax - no IS server name preceded by =>"
				 << " =======> Request " << request[i] << " is ignored");
			continue;
		}
		isServer = request[i].substr(request[i].find("=>")+2);
		isServer = isServer.substr(isServer.find_first_not_of(" "));
		isServer = isServer.substr(0, isServer.find_first_of(" "));
		dpeName = request[i].substr(0, request[i].find_first_of(" =>"));		// Drop IS server name
		if((isServer.size() == 0) || (dpeName.size() == 0)) {
			ERS_LOG(applName << "_ERROR: Incorrect syntax - IS server and service name must be not empty"
				 << " =======> Request " << request[i] << " is ignored");
			continue;
		}
		DdcServiceNameItem* item = new DdcServiceNameItem(dpeName, isServer); 
		parList.push_back(*item);
		delete item;
	}
}


DdcDtDimConfig::DdcDtDimConfig(Configuration& confDb, const string& name, DdcDtDirection dir)
	: applName(name)
{
	std::vector<const daq::ddc::DdcDtApplication *> dtApplicationList;
	confDb.get(dtApplicationList);
	
	std::vector<const daq::ddc::DdcDtApplication *>::const_iterator i = dtApplicationList.begin();
	const daq::ddc::DdcDtApplication* application = 0;
	
	for(; i != dtApplicationList.end(); ++i) {
		if((*i)->UID() == applName) {
			application = (*i);
			break;
		}
	}
	
	if(application) {
		if(application->get_DCSStationToConnect() == 0) {
			ERS_LOG(applName << "_FATAL: DCS station is not defined in configuration");
			pvssHost = "";
			return;
		}
		pvssHost = application->get_DCSStationToConnect()->UID();
		string 	isServer;
		if(dir == DAQ_REQUESTS) {
			isServer = application->get_DaqDataReqServer();
			if(isServer == "") {
				ERS_LOG(applName << "_WARNING: DAQ request server is not specified");
			}
			else {
				DdcServiceNameItem* item = new DdcServiceNameItem(DAQ_DATA_REQUEST, isServer);
				parList.push_back(*item);
				ERS_LOG(applName << "_INFO: DAQ request' server configured is " << isServer);
				delete item;
				item = new DdcServiceNameItem(DAQ_SUBSCRIBE_REQUEST, isServer);
				parList.push_back(*item);
				delete item;
				item = new DdcServiceNameItem(DAQ_UNSUBSCRIBE_REQUEST, isServer);
				parList.push_back(*item);
				delete item;
			}
		}
		else {
			std::vector<const daq::ddc::DdcDataTargetList *>	targetLists;
			if(dir == FROM_DCS) {
				targetLists = application->get_DcsExport();
				if(targetLists.size() == 0) {
					ERS_LOG(applName << "_WARNING: No DCS export specified");
				}
				else {
					ERS_LOG(applName << "_INFO: Configuring export");
					getDatapointsOfList(targetLists, dir);
				}
			}
			if(dir == TO_DCS) {
				targetLists = application->get_DcsImport();
				if(targetLists.size() == 0) {
					ERS_LOG(applName << "_WARNING: No DCS import specified");
				}
				else {
					ERS_LOG(applName << "_INFO: Configuring import");
					getDatapointsOfList(targetLists, dir);
				}
			}
		}
		if(dir == FROM_DCS) {
			
		}
	}
	else 
		ERS_LOG(applName << "_Config_ERROR: There is no this application in confDb");
}

						
void DdcDtDimConfig::getDatapointsOfList(std::vector<const daq::ddc::DdcDataTargetList *>& targetLists,
											DdcDtDirection dir)
{
	std::string isServer;
	std::vector<const daq::ddc::DdcDataTargetList *>::const_iterator	i = targetLists.begin();
	std::vector<std::string>::const_iterator j;
	size_t foundPos;
	size_t startPos;
	std::string itemName;
	int count = 0;		// number of found "\:"
	
	for(; i != targetLists.end(); ++i) {
		isServer = (*i)->get_ISServer();
		const std::vector<std::string>&	datapoints = (*i)->get_Datapoints();
		if(datapoints.size() != 0) {
			if(isServer == "") {
				ERS_LOG(applName << "_Config_ERROR: IS server is not specified in target list " << (*i)->UID());
			}
			else {
				DdcServiceNameItem* item;
				for(j = datapoints.begin(); j != datapoints.end(); ++j) {
				  // Finding of "\:" withing the itemName and replacing it by ':'
				  // (':' within the top name, not ':' indicating the list of structure elements)
				  // Introduced by SK 16.03.15
				  	itemName = "";
					startPos = 0;
					count = 0;
					foundPos = j->find("\\:", 0);
					while(foundPos != string::npos) {
						count += 1;
						itemName = itemName + j->substr(startPos,foundPos - startPos) + ":"; 
						startPos = foundPos+2; 
						foundPos = j->find("\\:", startPos); 
					}
					itemName = itemName + j->substr(startPos);
//					ERS_LOG("Finally: " << itemName << " " << startPos << " " << *j);
				  // End of addition of 16.03.15
				  // Next 5 lines replace 4 commented after them: the start position of search introduced
				    foundPos = j->find(':',startPos);
					if(foundPos != string::npos) {
						string srv = itemName.substr(0, foundPos-count);
						item = new DdcServiceNameItem(srv, isServer);
						string sons = j->substr(foundPos+1);
/*					if(j->find(':') != string::npos) {					// Peace of code before 16.03.15
						string srv = j->substr(0, j->find(':'));
						item = new DdcServiceNameItem(srv, isServer);
						string sons = j->substr(j->find(':')+1);
*/
						item->buildListOfSons(sons);

					// Debug print
//						const vector<string>& cs = item->getSons();
//						ERS_LOG("Number of sons = " << cs.size());
//						if(cs.size()) {
//							ERS_LOG("They are: ");
//							for(unsigned int k=0; k < cs.size()-1; k++)
//								ERS_LOG("------------" << cs[k]);
//							ERS_LOG("------------" << cs[cs.size()-1]);
//						}
					// end of debug print
					}
					else {
						item = new DdcServiceNameItem(itemName, isServer); 
//						ERS_LOG(applName << "_INFO: Config item for " << itemName);
					}
					
					parList.push_back(*item);
					ERS_LOG(applName << "_INFO: Item introduced: " << item->getServiceName());
					string dirSymb = (dir == FROM_DCS) ? " -> " : " <- ";
					ERS_LOG(applName << "_INFO: DP included: (" << itemName 
						 << dirSymb << isServer << ")");
					delete item;
				}
			}
		}
	}
}


string DdcDtDimConfig::findIsServer(const string& serviceName)
{
	vector<DdcServiceNameItem>::iterator current;

	for(current = parList.begin(); current != parList.end(); current++) {
		if(serviceName == current->getServiceName())
			return(current->getIsServer());
	}
	return((string)"");
}


DdcServiceNameItem*	DdcDtDimConfig::findItem(const string& name)
{
	vector<DdcServiceNameItem>::iterator current;
	
	for(current = parList.begin(); current != parList.end(); current++) {
		if(name == current->getServiceName())
			return(&*current);
	}
	return(0);
}


void DdcDtDimConfig::removeItem(string& name)
{
	vector<DdcServiceNameItem>::iterator	it;
	
	for(it = parList.begin(); it != parList.end(); it++)
		if(it->getServiceName() == name) {
			parList.erase(it);
			break;
		}
}
