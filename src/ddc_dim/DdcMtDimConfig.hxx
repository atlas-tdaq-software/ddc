#ifndef _DDCMTDIMCONFIG_H_
#define _DDCMTDIMCONFIG_H_

//
// This header file contains definitions of DDC-MT-DIM subsystem 
// configuration data related classes 
//							S. Khomoutnikov 
//							28.04.05 - 29.04.05
// Modifications:
//		S.Kh.	27.03.08	Handling the "SendWhileConnection" attribute of DdcTextMessage;
//							the ignoreFirst member is the list of messages, which will not be 
//							transferrred to DAQ while connection (on further change only)
//
//////////////////////////////////////////////////////////////////////////////////	

#include "../DdcDal.hxx"

// Ranges of DDC alarm priorities
#define	RANGE_INFO 		10
#define	RANGE_DIAGNOS 	20
#define	RANGE_WARNING 	50
#define	RANGE_ERROR 	80

typedef enum
{
	DDC_ALARM_MSG,
	DDC_TEXT_MSG
} DdcMtListId;

class DdcMesItem
{
public:
	DdcMesItem(const std::string& name, const std::string& sev, const std::string& appl_name);
	DdcMesItem(const std::string& name);
	DdcMesItem(const DdcMesItem&);
	std::string getServiceName() 	{ return(nameString); }
	ers::severity getSeverity()			{ return(msgSeverity); }
	void	setSeverity(ers::severity s)	{ msgSeverity = s; }
private:
	std::string	nameString;
	ers::severity	msgSeverity;
};


class DdcMtDimConfig
{
public:
	DdcMtDimConfig(Configuration& confDb, const std::string& mt_name);
	~DdcMtDimConfig() 
		{ 	alarmList.erase(alarmList.begin(), alarmList.end());
			parList.erase(parList.begin(), parList.end());		}
	std::string getPvssHost()	{ return(pvssHost); }
	int	getAlarmNum() 			{ return(alarmList.size()); }
	ers::severity getSeverity(std::string& name) 
		{ return(findMsgItem(name) ? findMsgItem(name)->getSeverity() : ers::Error); }
	int	getItemNum() { return(parList.size()); }
	std::vector<DdcMesItem>&	getAlarmList()	{ return(alarmList); }
	std::vector<DdcMesItem>&	getTxtVarList()	{ return(parList); }
	bool isInList(std::string&, DdcMtListId);
	std::vector<std::string>&	getIgnoreFirst() { return(ignoreFirst); }  
	void print_config();
private:
	void createMessageConfig(std::vector<const daq::ddc::DdcTextMessage*>& dpLists);
	void createAlarmConfig(std::vector<const daq::ddc::DdcTargetList*>& targetLists);
	DdcMesItem* findMsgItem(std::string& name);
	
	std::string			pvssHost;
	std::string			applName;
	std::vector<DdcMesItem>	alarmList;
	std::vector<DdcMesItem>	parList;
	std::vector<std::string>	ignoreFirst;
};

//=================== inline print_config() function ===============

inline void DdcMtDimConfig::print_config()
{
	std::vector<DdcMesItem>::iterator item;
	
	cout << "Configuration object of DDC-MT subsystem: " << endl;
	for(item = parList.begin(); item != parList.end(); item++) {
		cout << item->getServiceName() << " ";
		switch(item->getSeverity()) {
		case ers::Information:
			cout << "Information:" << endl;
			break;
		case ers::Warning:
			cout << "WARNING" << endl;
			break;
		case ers::Error:
			cout << "ERROR" << endl;
			break;
		case ers::Fatal:
			cout << "FATAL" << endl;
			break;
		default:
			cout << "UNDEFINED Severity!!" << endl;
		}
	}
}

#endif
