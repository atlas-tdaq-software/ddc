#ifndef _DDCDTDIM_H_
#define _DDCDTDIM_H_

//
// This header file contains definition of object classes 
// responsible for data excange with a certain PVSS system,
// as well as to export/import DCS data into IS servers of a DAQ partition.
// The application is produced from DdcDtManager by removing of direct 
// connection with PVSS run time database and by introducing instead
// communication with PVSS DIM manager
//							S. Khomoutnikov 
//							04.03.05 - 28.03.05
// Modifications:
//		S.Kh.	25.10.05	ConfDb converter of environment variables is introduced
//		S.Kh.	30.11.06	IS calls modified to accept Exceptions
//		S.Kh.	19.04 10	Mechanism of trying connection to DIM-DNS changed
//		S.Kh.	12.05 10	Bugs of ddc-05-06-150 with uninitialized ptr and not-catching 
//							exception fixed 
//			
////////////////////////////////////////////////////////////////////////////////

#include <is/namedinfo.h>

#include <dis.hxx>
#include <dic.hxx>

#include "../DdcDal.hxx"
//#include "../DdcData.hxx"
#include "DdcDtDimConfig.hxx"

#define CONNECTION_WAIT_TIME 60		// delay between series of connection
									// attempts (in sec)
#define PVSS_CONN_TIMEOUT  15		// Timeout for DimCurrentInfo reading DIS_DNS
#define PVSS_CONN_ATTEMPTS 6		// Number of attempts to read DIS_DNS	
#define MAXINT32 4294967295		// 2**32-1						
									
const std::string	UNAVAILABLE_INFO("Unavailable");

typedef union {
	int		intValue;
	unsigned int uintValue;
	float	floatValue;
	void*	anyValue;
} DdcDimData;

typedef struct
{
	std::string		name;
	DdcDimDataType	type;
	std::string		dimFormat;
	DdcDimData 		data;
	size_t			length;
	DimService*		dimService;
} IsService;

typedef struct
{
	std::string name;
	ISInfoDynAny* isObject;
} TdaqDataEntry;


class DdcDataService : public DimClient, public DimServer		// In turn, Client of PVSS DIM manager
{
public:
	DdcDataService(IPCPartition&, const std::string& application);
	~DdcDataService();
	bool doConfig(Configuration&);
	bool doInitialize();
	void run();

	void buildSourceServerList();
	void startDataReceivers();
	void stopImport();
	
	void handleDaqDataRequest(std::vector<std::string>&);
	void handleDaqSubscribeRequest(std::vector<std::string>&);
	void handleDaqUnsubscribeRequest(std::vector<std::string>&);
	void updateIsRecord(IsService* serviceDef, ISInfoDynAny& value);
	void publishIsRecord(std::string& serviceName, ISInfoDynAny& value);
	void publishIsNonStructService(std::string& serviceName, ISInfoDynAny& value, int position = 0);
	bool isServicePublished(std::string& serviceName, IsService** service);
	void updateIsNonStructService(IsService* service, ISInfoDynAny& value, int position = 0);
	bool isIsRecord(ISInfoDynAny& value);
//	void reportForDaq(const char* text, severity msgSev);
	const std::string& getApplName()		{ return(applName); }
	bool importConfigured() { return(dcsImportConfig->getItemNum()
									 + daqRequestConfig->getItemNum() == 0 ? false : true); }
	void putIsService(std::string serviceName, ISInfoDynAny& value);
	static bool doExit;
	void exitHandler(int code);


protected:
	void infoHandler();

private:
	void exportDcsData(const std::string& srvName, DdcServiceNameItem* item, DimStampedInfo* info);
	bool exportDynInt(const std::string& isObName, int* data, unsigned int length, time_t updateTime);
	bool exportDynFloat(const std::string& isObName, float* data, unsigned int length, time_t updateTime);
	bool exportDynString(const std::string& isObName, char* data, int length, time_t updateTime);
	bool exportStruct(const std::string& isObName, void* data, const std::string& dataFormat, int length, 
								time_t updateTime, const std::vector<std::string>& sons);
	void handleExportConfigWildcards(DdcDtDimConfig *);
	void initExport();
	void exportNoService(const std::string& isServer, const std::string& name, time_t);
	void buildDataType(DdcServiceNameItem *, DimInfo *, std::string& isName);
	void removeIsData();
	void removeIsEntry(std::vector<DdcServiceNameItem>::iterator);
	void openConfigStructures(DdcDtDimConfig* config);
	void moreForExport(DdcDtDimConfig* moreConfig);
	void initMoreExport(unsigned int howMany);
	void removeClient(std::string& serviceName);
	void wildcardsRemoveClients(std::string& serviceName);
	void handleDirectDataRequest(std::string& dpName, std::string& serverName);
	bool prepareDataBuf(ISInfoDynAny& value, int position, DdcDimData* data, 
						   ISType::Basic type, bool is_array, size_t& length, DdcDimDataType*);
	void arrangeImport();
	void publishCompleteIsRecord(std::string& serviceName, ISInfoDynAny& value);
	void publishIsRecordFields(std::string& serviceName, const std::vector<std::string>& fields, ISInfoDynAny& value);
	void __publishSimpleService(IsService* serviceDef);
	void releaseDataBuf(IsService* serviceDef);
	void updateCompleteIsRecord(IsService* serviceDef, ISInfoDynAny& value);
	void updateIsRecordFields(IsService* serviceDef, const std::vector<std::string>& fields, ISInfoDynAny& value);
	void singleStringToChar(std::string*, size_t& length, DdcDimData* charArray);
	void manyStringsToChar(std::vector<std::string> *, size_t& length, DdcDimData* charArray);
	void buildStructService(std::vector<IsService *>& structureFields, IsService* serviceDef, unsigned int length);
	void stopServices();
	void stopDdcDimService(IsService *);
	void removeStructFields(std::vector<IsService *>& fields);
	void acceptRequestedData(DdcServiceNameItem* configItem, DimStampedInfo* info);
	DdcServiceNameItem* findActiveRequest(std::string& serviceName);
	bool isUnavailableServer(std::string server);
	void markAvailable(std::string server);
	void removeTreatedIsService();
	void treatIsDataObject();
	bool isAlreadyPublished(std::string serviceName, std::string& publisher, std::string& comp);
	
	std::string			pvssHost;
	std::string			applName;
	DdcDtDimConfig*		dcsExportConfig;
	DdcDtDimConfig*		dcsImportConfig;
	DdcDtDimConfig*		daqRequestConfig;
	std::vector<DdcServiceNameItem *> activeRequests;

	IPCPartition		daqPartition;
//	MRSStream			mout;
	std::vector<std::string>	sourceIsServers;
	std::vector<void*>			dataReceivers;
	std::vector<IsService *> 	isServices;
	std::vector<DimStampedInfo *> activeClients;
	std::mutex					configMutex;
	std::mutex					requestMutex;
	std::mutex					serverAvailableMutex;
	std::vector<std::string>	unavailableServers;
	std::vector<TdaqDataEntry *>  tdaqDataQueue;
	std::mutex    				tdaqQueueMutex;
	std::string 				beingPublished;
};

#endif
