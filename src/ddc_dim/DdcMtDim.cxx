//
// This file contains definition of DdcMtService class methods. 
// An object of this class (based on DIM classes) 
// is responsible for receiving alarm messages and specified text
// data (non-alarm messages) from a certain PVSS system (its DIM manager),
// and then send them into MRS server of a specified DAQ partition
//							S. Khomoutnikov 
//							28.04.05 - 29.04.05
// See DdcMtService.hxx to follow the history
//
////////////////////////////////////////////////////////////////////////////

#include <string>
#include <time.h>
#include <vector>
#include <stdlib.h>

using namespace std;

#include <ipc/core.h>
#include <ipc/partition.h>

#include <is/exceptions.h>

#include <dal/Partition.h>
#include <dal/Variable.h>
#include <dal/util.h>

#include <ers/ers.h>
#include <ers/Severity.h>

//#include <mrs/message.h>
#include <cmdl/cmdargs.h>

#include <pmg/pmg_initSync.h>

#include "DdcMtDimConfig.hxx"
#include "DdcMtDim.hxx"


static	CmdArgStr	manager_num('M', "Manager", "manager-number", "number of API manager");

bool DdcMtService::doExit = false;

ERS_DECLARE_ISSUE( ddc, AppWarning, message, ((const char*)message) )
ERS_DECLARE_ISSUE( ddc, AppError, message, ((const char*)message) )
ERS_DECLARE_ISSUE( ddc, AppFatal, message, ((const char*)message) )


DdcMtService::DdcMtService(IPCPartition p, const string& name) 
	: applName(name), daqPartition(p), dcsMessageConfig(0)		// , mout(p)
{ 	
	ERS_LOG(name << "_INFO: DDC_MT_DIM client constructed");
}

  
ers::severity DdcMtService::convertPriority(char priority)
{
	ers::severity msgSeverity;
	
	if(priority > RANGE_ERROR)
		msgSeverity = ers::Fatal;
	else
		if(priority > RANGE_WARNING)
			msgSeverity = ers::Error;
		else
			if(priority > RANGE_DIAGNOS)
				msgSeverity = ers::Warning;
			else
				msgSeverity = ers::Information;
	return(msgSeverity);
}

void DdcMtService::infoHandler()
{
	string			svcName;
	string			msgText;
	AlarmInfo		alarmInfo;
	AlarmInfo*		alarmService;
	ers::severity	msgSeverity;
	std::vector<std::string>::iterator	ignoreMsg;
	bool			publish = true;
	int 			alarmInfoLength;
	int 			count;

//	ERS_LOG(applName << " infoHandler call");
	
	DimStampedInfo* info = (DimStampedInfo *)getInfo();
	svcName = info->getName();

  // Check communication error
	if(info->getSize() == (int)UNAVAILABLE_INFO.size()+1) {
		ERS_LOG(applName << " About UNAVAILABLE");
		string strErr(info->getString());
		if(strErr == UNAVAILABLE_INFO) {
			strErr = "Messages of " + svcName + " unavailable";
			ers::error(ddc::AppError(ERS_HERE, strErr.c_str()));
			for(ignoreMsg = ignoreList.begin(); ignoreMsg != ignoreList.end(); ignoreMsg++)
				if(*ignoreMsg == svcName) {
					ignoreList.erase(ignoreMsg);	// If/when will be published => will be distributed
					ERS_LOG("Unavailable " << *ignoreMsg << " dropped form ignore list");
					break;
				}
			return;
		}
	}
	else {
		alarmInfoLength = info->getSize();
//		ERS_LOG(applName << " " << info->getSize() << " bytes received");
//		ERS_LOG(applName << " Format: " << info->getFormat());
	}
	if(svcName.find(ALARM_SUFFIX) == string::npos) {	// Message from a text variable (DP)
//		ERS_LOG(applName << " About ALARM_SUFFIX");
		if(dcsMessageConfig->isInList(svcName, DDC_TEXT_MSG)) {
	  		if(ignoreList.size()) {
				for(ignoreMsg = ignoreList.begin(); ignoreMsg != ignoreList.end(); ignoreMsg++) {
					if(*ignoreMsg == svcName) {
						ERS_LOG("First coming " << *ignoreMsg << " dropped it form ignore list");
						ignoreList.erase(ignoreMsg);	// If/when will be published => will be distributed
						publish = false;
						break;
					}
				}
			}
			if(publish) {
				// Making the message components 
				msgText = info->getString();
				msgSeverity = dcsMessageConfig->getSeverity(svcName);
				
				if(isToInvalidateMsg(svcName, &msgText)) {	// Invalidating message
					msgText += " NOT VALID ANY MORE";
				}
				else {	  // Exporting new DCS message 
					if((msgText != "") && (msgText != " ")) {
						addActiveMsg(svcName, msgText);
					}
				}
				exportDcsMessage((char *)svcName.c_str(), (char *)msgText.c_str(), ers::Log);
			}
		}
	}
	else {												// Alert message
//		ERS_LOG(applName << " isInList? " << svcName << "," << DDC_ALARM_MSG);
		if(dcsMessageConfig->isInList(svcName, DDC_ALARM_MSG)) {
		  // Making the message components 
	  		alarmService = (AlarmInfo *)info->getData(); // Format C:1,I:1,C:1,C is assumed

//			ERS_LOG(applName << " building message");
			alarmInfo.active = *((bool*)alarmService);
//			ERS_LOG(applName << " active = " << alarmInfo.active);
			count = sizeof(bool);
//			sleep(1);
			alarmInfo.state = *((int*)alarmService+count);
//			ERS_LOG(applName << " state = " << alarmInfo.state);
			count += sizeof(int);
//			sleep(1);
			alarmInfo.priority = *((char*)alarmService+count);
			count += sizeof(char);
			msgSeverity = convertPriority(alarmInfo.priority);
//			ERS_LOG(applName << " Severity " << msgSeverity);
//			sleep(1);
//			ERS_LOG(applName << " alarmInfo.text = " << (char*)alarmService+count);
			alarmInfo.text = (char*)alarmService+count;
//			sleep(1);
				
			ERS_LOG(applName << " exporting message " << svcName << " " << alarmInfo.text << " with severity " << msgSeverity);
			exportDcsMessage((char *)svcName.c_str(), (char *)alarmInfo.text.c_str(), msgSeverity);
//			ERS_LOG(applName << " exporting message - DONE");
		}
	}
}


bool DdcMtService::isToInvalidateMsg(string dp, string* text)
{
	vector<MessageContent>::iterator	msgRef;
	
	for(msgRef = activeMsgs.begin(); msgRef != activeMsgs.end(); msgRef++) {
		if(msgRef->getServiceName() == dp) {
			if((*text == "") || (*text == " ")) { 
				*text = msgRef->getText();
				activeMsgs.erase(msgRef);
				return(true);
			}
			else {
				activeMsgs.erase(msgRef);
				return(false);
			}
		}
	}
	return(false);
}


void DdcMtService::exportDcsMessage(char* msgName, char* text, ers::severity msgSev)
{
	string msg;
	
	msg = pvssHost + msgName + ": " + text;
	switch(msgSev)	{
	case ers::Fatal:
		ers::fatal(ddc::AppFatal(ERS_HERE, msg.c_str()));
		break;
	case ers::Error:
		ers::error(ddc::AppError(ERS_HERE, msg.c_str()));
		break;
	case ers::Warning:
		ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
		break;
	default: 
		ERS_INFO(applName << " message: " << msg.c_str());
	}

	return;
}

void DdcMtService::handleWildcards(vector<DdcMesItem>& itemList)
{
	vector<DdcMesItem>	listCopy;
	DdcMesItem*			abcItem;
	unsigned int 		i, j;
	string				serviceName;
	char*				service;				// on the base of wildcards
	string				realName;
	unsigned int		toConvertSize;
	vector<DdcMesItem>::iterator	it;
	vector<DdcMesItem>::iterator	itLook;
	bool				skipDp;
	DimBrowser			browser;
	int					serviceClass;
	char*				format;
	string				msg;
	
  // Copying items with Service names with wildcards
	for(i=0; i<itemList.size(); i++) {
		if(itemList[i].getServiceName().find('*') != string::npos) {
			listCopy.push_back(itemList[i]);
		}
	}
	
	toConvertSize = listCopy.size();
	if(toConvertSize != 0) {
	  // Removing from the source list the items with DP names with wildcards
		for(i=0; i<toConvertSize; i++) {
			serviceName = listCopy[i].getServiceName();
			for(j=0; j<itemList.size(); j++) {
				if(itemList[j].getServiceName() == serviceName) {
					itemList.erase(itemList.begin()+j);
					break;
				}
			}
		}
	  // Extending names with wildcards and storing them at the source list
		for(it = listCopy.begin(); it != listCopy.end(); it++) {
			serviceName = it->getServiceName();
			
			browser.getServices(serviceName.c_str());
			serviceClass = browser.getNextService(service, format);
			if(serviceClass == 0) {
				msg = "There is no published services like " + serviceName;
				ERS_LOG(applName << "_WARNING: " << msg);
				continue;
			}
			while(serviceClass) {
				if(serviceClass == DimSERVICE) {						// Otherwise nothing to do
					serviceName = service;
					skipDp = false;
					for(itLook=itemList.begin(); itLook != itemList.end(); itLook++) 
						if(itLook->getServiceName() == serviceName) {
							skipDp = true;
							break;
						}
					if(!skipDp) {
						abcItem = new DdcMesItem(serviceName); 
						itemList.push_back(*abcItem);
						delete abcItem;
					}
				}
				serviceClass = browser.getNextService(service, format);		
			}
		}	
	}
}


bool DdcMtService::initExport()
{
	string 		serviceName;
	string		attrName;
	string 		connectDpName;
	string 		errMsg;
	vector<DdcMesItem>	serviceList;
	vector<DdcMesItem>::iterator it;

  // First extend the list by replacing wildcards' names
	handleWildcards(dcsMessageConfig->getAlarmList());
  // Subscribe for ALARM_INFOs
  	serviceList = dcsMessageConfig->getAlarmList();
	for(it = serviceList.begin(); it != serviceList.end(); it++) {
		serviceName = it->getServiceName();
		DimStampedInfo* info = new DimStampedInfo(serviceName.c_str(), (char *)UNAVAILABLE_INFO.c_str(), this);
	}
		
	// Subscribe for the text parameters to be DCS messages
	
	// First extend the list by replacing wildcards' names
	handleWildcards(dcsMessageConfig->getTxtVarList());
	
	// And now subscribe
  	serviceList = dcsMessageConfig->getTxtVarList();
	for(it = serviceList.begin(); it != serviceList.end(); it++) {
		serviceName = it->getServiceName();
		DimStampedInfo* info = new DimStampedInfo(serviceName.c_str(), (char *)UNAVAILABLE_INFO.c_str(), this);
	}

// for debugging (remained for the users)
	serviceList = dcsMessageConfig->getAlarmList();
	ERS_LOG(applName << "_INFO: Alarm list");
	for(it = serviceList.begin(); it != serviceList.end(); it++) {
		ERS_LOG("    DIM Service: " << it->getServiceName());
	}
  	serviceList = dcsMessageConfig->getTxtVarList();
	ERS_LOG(applName << "_INFO: MSG list");
	for(it = serviceList.begin(); it != serviceList.end(); it++) {
		ERS_LOG("    DIM Service: " << it->getServiceName());
	}

	if((dcsMessageConfig->getItemNum() > 0) 
	|| (dcsMessageConfig->getAlarmNum() > 0))
		return(true);
	else
		return(false);
}


bool DdcMtService::doInitialize()
{
	string	msg;
	string	pvssHost = dcsMessageConfig->getPvssHost();
	string 	dnsSrvHost;	
		
	ERS_LOG(applName << "_INFO: Starting connection to PVSS");

	dic_disable_padding();			// Deny any padding in clients (necessary for using getFormat() )
	dis_disable_padding();			// Deny any padding in servers (necessary for packing structures)
	
	// Find/wait DIM DNS server
	bool	dnsFound = false;
	int		srvVersion;
	int 	count = 0;
	int		timeSum = 0;
	int	    timeoutValue = (PVSS_CONN_TIMEOUT+5)*PVSS_CONN_ATTEMPTS;
	char    duration[128];
	
	while(!dnsFound) {
		dnsSrvHost = getenv((const char*)"DIM_DNS_NODE");	// Already known, that it is set
		DimCurrentInfo*	dnsSrv = new DimCurrentInfo("DIS_DNS/VERSION_NUMBER", PVSS_CONN_TIMEOUT, -1);
		srvVersion = dnsSrv->getInt();
		count++;
		if(srvVersion < 0) {
			if(count == PVSS_CONN_ATTEMPTS) {
				timeSum += timeoutValue;
				sprintf(duration, "%d", timeSum/60);
				msg = "DIM Name Server on " + dnsSrvHost + " is not available since " + duration + " min";
				ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
//				ers::error(ddc::AppError(ERS_HERE, msg.c_str()));
				if(timeSum < timeoutValue*2) {
					msg = "Be sure that DIM Name Server runs or correct DIM_DNS_NODE environment. Still waiting...";
					ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
				}
				else {
					msg = "DIM Name Server does NOT run on "+dnsSrvHost+". Exiting...";
					ers::fatal(ddc::AppFatal(ERS_HERE, msg.c_str()));
					exit(1);
				}
				count = 0;;
			}
			sleep(5);
		}
		else {
			dnsFound = true;
			sleep(2);
		}
		delete dnsSrv;
	}
	
// This part is commented: it is time consuming and can be "noisy" on this reason.
// On other hand, while subscription for the PVSS DIM services will appear an error message,
// if it is not published that also allowed avoiding this checking here

//	// Check/wait PVSS DIM server 
//	bool srvFound = false;
//	string	pvssHostName;
//	string	pvssHostNameAlt;
//	int		i;
//	if(pvssHost.find('.') != string::npos)
//		pvssHostName = pvssHost.substr(0, pvssHost.find('.'));
//	else
//		pvssHostName = pvssHost;
//	pvssHostNameAlt = pvssHostName;
//	for(i=0; i<pvssHostName.size(); i++) {
//		pvssHostName[i] = tolower(pvssHostName[i]);
//		pvssHostNameAlt[i] = toupper(pvssHostNameAlt[i]);
//	}
//	if(srvList.find('|') != string::npos) {
//		srvList = srvList.substr(srvList.find('|')+1);
//		srvFound = ((srvList.find(pvssHostName) != string::npos)
//					|| (srvList.find(pvssHostNameAlt) != string::npos));
//	}
//	
//	if(!srvFound) {
//		msg = "PVSS DIM manager on " + pvssHost + " is not (yet) registered. Waiting...";	
//		ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
//		msg = "Be sure that DIM_DNS_NODE is correct on PVSS machine!";	
//		ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
//	}

//	while(!srvFound) {		// waiting until PVSS DIM server is running
//		DimCurrentInfo* servers = new DimCurrentInfo("DIS_DNS/SERVER_LIST", "NOT_RUNNING");
//		srvList = servers->getString();
//		if(srvList.find('|') != string::npos) {
//			srvList = srvList.substr(srvList.find('|')+1);
//			srvFound = ((srvList.find(pvssHostName) != string::npos)
//						|| (srvList.find(pvssHostNameAlt) != string::npos));
//		}
//		else
//			srvFound = false;
//		delete servers;
//		if(srvFound) {
//			msg = "DIM manager is running now on " + pvssHost;
//			ERS_LOG(applName << "_INFO: " << msg);
//		}
//		else {
//			if(doExit)				// to exit on a signal
//				return(false);
//			sleep(3);
//		}
//	}
// End of commented piece of code
	
  // Initialization of exporting DCS messages and alarms
	if(!initExport()) {
		ers::fatal(ddc::AppFatal(ERS_HERE, "No effective configuration defined => Exiting..."));
		ERS_LOG(applName << "_FATAL: No effective configuration defined => Exiting...");
		return(false);
	}
	else {
		if(dcsMessageConfig->getItemNum() == 0) {
			ERS_LOG("_INFO: None DCS text message for DAQ configured!");
		}
		if(dcsMessageConfig->getAlarmNum() == 0)      {
			ERS_LOG("_INFO: DCS alarms are not configured!");
		}
		ERS_LOG(applName << "_INFO: DCS message export initialized");
		return(true);
	}
}


void DdcMtService::run()
{
	ERS_LOG(applName << "_INFO: In run()");
//	for(;;)
	while(true)
	{
//		ERS_LOG("_INFO: Running MT");
		if (doExit) {
			ERS_LOG(applName << "_WARNING: Exit command received");
			return;
		}
		
//		ERS_LOG("_INFO: Sleeping 10");
//		sleep(10);
//		ERS_LOG("_INFO: Once more..");
		sleep(1);
	}
}

void handleSignal(int)
{
	time_t now = time(&now);
	ERS_LOG("Killing signal received by DDC_MT_DIM");
	DdcMtService::doExit = true;
}

bool DdcMtService::doConfig(::Configuration& confDb)
{
	ERS_LOG("DDC_MT_DIM reads its configuration");
	dcsMessageConfig = new DdcMtDimConfig(confDb, applName); 
	ERS_LOG("Done");
	
	ignoreList = dcsMessageConfig->getIgnoreFirst();
	if((dcsMessageConfig->getItemNum() != 0)
	|| (dcsMessageConfig->getAlarmNum() != 0) )
		return(true);
	else
		return(false);
}


int main(int argc, char *argv[])
{
/*    ERS_LOG("Dummy 'do forever'");
	while(true) {
		sleep(3);
	}
*/	
	
	CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);
    CmdArgStr	appl_name('n', "application", "appl-name", "name of application.", CmdArg::isREQ);

//
// Initialize command line parameters with default values
//
	manager_num = "0";

//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &manager_num, &partition_name, &appl_name, 0);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
    cmdLine.parse(arg_iter);

	signal(SIGINT, handleSignal);
	signal(SIGTERM, handleSignal);
	signal(SIGKILL, handleSignal);
	
  // Initial references for configuring	
	bool conf_error = false;
	
	string	dimDnsNode;
	if(getenv((const char*)"DIM_DNS_NODE") == 0) 
		conf_error = true;
	else {
		dimDnsNode = getenv((const char*)"DIM_DNS_NODE");
		if(dimDnsNode.size() == 0) 
			conf_error = true;
	}

	if(conf_error) {
		ERS_LOG(appl_name << "_FATAL: DIM Name Server host (DIM_DNS_NODE environment) is not defined");
		return(1);
	}
	else {
		ERS_LOG(appl_name << "_INFO: DIM_DNS_NODE environment = " << dimDnsNode);
	}
	
	IPCPartition partition((const char*)partition_name);

	// Initialization of CORBA broker omniORB4	
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
		return(1);
    }
	
	::Configuration * confDb;
	try {
		confDb = new Configuration("");
	}
	catch(daq::config::Exception &ex) {
		ERS_LOG(appl_name << "_ERROR: Exception while initialization DB: " << ex);
		return(1);
	}

	// Install Environment converter
	std::string pName((const char*)partition_name);
	const daq::core::Partition * confPartition = daq::core::get_partition(*confDb, pName);
    if(!confPartition) {
		ERS_LOG(appl_name << "_ERROR: Failed to find " << pName
				  << " partition in configuration database");
		delete confDb;
		return 1;
	}
	// Replaced 25.02.19 as deprecated
    //confDb->register_converter(new daq::core::SubstituteVariables(*confDb, *confPartition));
    confDb->register_converter(new daq::core::SubstituteVariables(*confPartition));
	
	const string apName((const char*)appl_name);
	ERS_LOG(apName << "_INFO: Creating DDC-DIM object for DCS=>DAQ message transfer");
	DdcMtService* ddcMtService = new DdcMtService(partition, apName);
	
	if(ddcMtService->doConfig(*confDb))		{	// Configuring from confDB 	
		ERS_LOG("Doing pmg_initSync");
		pmg_initSync();
		ERS_LOG("Done initSync");
		sleep(1);
		if(ddcMtService->doInitialize()) {
			time_t 	justnow = time(&justnow);
			ERS_LOG(apName << ": DCS is connected");
			ERS_LOG(apName << "_INFO: Going to run()");
			ddcMtService->run();
		}
		else {
			// Error message is already issued in doInitialize()
		}
	}
	else {
		ers::fatal(ddc::AppFatal(ERS_HERE, "NO DCS message is configured! Exiting..."));
	}
	
	delete ddcMtService;
	 	
	return 0;
}
