#ifndef _DDCCTDIMCONTROLLER_H_
#define _DDCCTDIMCONTROLLER_H_

//
// This file contains the definition of DdcCtDimController class
// that is responsible for sending DAQ command into DCS.
//							S. Khomoutnikov 
//							31.03.05 - 15.04.05
// Modified:
//		S.Kh.	19.03.06	Virtual Routines functions changed according to
//							new DAQ FSM
//		S.Kh.	13.02.14	Virtual Routines functions changed (all void with input 
//							cmd-parameter according to new DAQ RC
//
/////////////////////////////////////////////////////////////////////////

#include <RunControl/Common/UserRoutines.h>
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Controller/Controller.h>

// Command identification for DdcCtService
const std::string	CMD_CONFIG("configure");
const std::string	CMD_CONNECT("connect");
const std::string	CMD_PREPARERUN("prepareforrun");
const std::string	CMD_STOPROIB("stoproib");
const std::string	CMD_STOPDC("stopdc");
const std::string	CMD_STOPHLT("stophlt");
const std::string	CMD_STOPRECORD("stoprecording");
const std::string	CMD_STOPGATHER("stopgathering");
const std::string	CMD_STOPARCHIVE("stoparchiving");
const std::string	CMD_DISCONNECT("disconnect");
const std::string	CMD_UNCONFIG("unconfigure");

const std::string empty("");

class DdcCtService;

class DdcCtRoutines : public daq::rc::UserRoutines
{

friend class DdcCtService;

public:
	DdcCtRoutines(DdcCtService* man) 
		: manager(man), errorReported(false), transitionError(0) {}

	virtual void configureAction(const daq::rc::TransitionCmd &cmd);
	virtual void connectAction(const daq::rc::TransitionCmd &cmd);
	virtual void prepareAction(const daq::rc::TransitionCmd &cmd);
	virtual void stopROIBAction(const daq::rc::TransitionCmd &cmd);
	virtual void stopDCAction(const daq::rc::TransitionCmd &cmd);	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
	virtual void stopHLTAction(const daq::rc::TransitionCmd &cmd);	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
	virtual void stopRecordingAction(const daq::rc::TransitionCmd &cmd);
	virtual void stopGatheringAction(const daq::rc::TransitionCmd &cmd);	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
	virtual void stopArchivingAction(const daq::rc::TransitionCmd &cmd);	// added 25.11.13, as appears in RunControl/Common/UserRoutines.h
	virtual void disconnectAction(const daq::rc::TransitionCmd &cmd);
	virtual void unconfigureAction(const daq::rc::TransitionCmd &cmd);
	
	void introduceMyself();

private:
	DdcCtService*	manager;
	bool			errorReported;
	int				transitionError;
};


class DdcCtDimController : public daq::rc::Controller
{
public:
	DdcCtDimController(DdcCtService* man, const std::string& ctrlName, IPCPartition& p, 
						 const std::string& parent, const std::string& segment, const std::shared_ptr<DdcCtRoutines>& routines)
		  : daq::rc::Controller(ctrlName, parent, p.name(), segment, routines), manager(man) {}
	void introduceMyself(); 

	
private:
	DdcCtService*	manager;
};

#endif
