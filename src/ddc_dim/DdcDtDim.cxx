//
// This file contains definition of DdcDataService class methods. 
// An object of this class 
// is responsible for data exchange between a certain PVSS system
// and a DAQ partition
// 
//							S. Khomoutnikov 
//							07.08.05 - 11.03.06
//
// See DdcDtDimManager.hxx to follow the history
//

#include <string>
#include <time.h>
#include <vector>
#include <algorithm>

#include <signal.h>

using namespace std;

#include <mutex>
#include <owl/time.h>

#include <ipc/core.h>
#include <ipc/alarm.h>
#include <ipc/partition.h>

#include <dal/Partition.h>
#include <dal/Variable.h>
#include <dal/util.h>

#include <is/infodictionary.h>
#include <is/namedinfo.h>
#include <is/infodynany.h>

#include <pmg/pmg_initSync.h>

#include <ers/ers.h>

#include <cmdl/cmdargs.h>

#include "../DdcDal.hxx"
//#include "../DdcData.hxx"
#include "dis.hxx"

#include "ddc/DdcBoolArrayInfoNamed.h"
#include "ddc/DdcBoolInfoNamed.h"
#include "ddc/DdcFloatArrayInfoNamed.h"
#include "ddc/DdcFloatInfoNamed.h"
#include "ddc/DdcIntArrayInfoNamed.h"
#include "ddc/DdcIntInfoNamed.h"
#include "ddc/DdcStringArrayInfoNamed.h"
#include "ddc/DdcStringInfoNamed.h"
#include "ddc/DdcCharInfoNamed.h"

#include "DdcDtDimConfig.hxx"
#include "DdcDtDimReceiver.hxx"
#include "DdcDtDim.hxx"


bool DdcDataService::doExit = false;

ERS_DECLARE_ISSUE( ddc, AppWarning, message, ((const char*)message) )
ERS_DECLARE_ISSUE( ddc, AppError, message, ((const char*)message) )
ERS_DECLARE_ISSUE( ddc, AppFatal, message, ((const char*)message) )

DdcDataService::DdcDataService(IPCPartition& p, const string& name) 
	: applName(name), dcsExportConfig(0), daqRequestConfig(0), daqPartition(p)		//, mout(p)
{
	ERS_LOG(name << "_INFO: DDC_DT_DIM Client constructed");
}

void DdcDataService::infoHandler()
{	
	string				svcName;
	string				isServerName;
	DdcServiceNameItem*	configItem;
	time_t				now;

	DimStampedInfo* info = (DimStampedInfo *)getInfo();
	svcName = info->getName();

	now = time(&now);
//	ERS_LOG(applName << "_INFO: Data came " << svcName);
	configMutex.lock();
//	ERS_LOG(applName << "_INFO: Mutex locked for " << svcName);
	configItem = dcsExportConfig->findItem(svcName);
	if(configItem != 0) {		// this DP is in persistent configuration
	  // IS server name
	 	isServerName = configItem->getIsServer();
	  // Check communication error
		if(info->getSize() == (int)UNAVAILABLE_INFO.size()+1) {
			string strErr(info->getString());
			if(strErr == UNAVAILABLE_INFO) {
				strErr = "Service " + svcName + " unavailable";
				ERS_LOG(applName << "_ERROR:  " << strErr);
//				ers::warning(ddc::AppWarning(ERS_HERE, strErr.c_str()));
				configItem->setDataType(DDC_DIM_NOTYPE);
				now = time(&now);
				exportNoService(isServerName, svcName, now);	// Does nothing 29.01.14 on request TDAQ
				configMutex.unlock();
				return;
			}
		}
		if(configItem->getDataType() == BAD_TYPE) {
			configMutex.unlock();
			return;
		}
	  // Does come for the first time?
		if(configItem->getDataType() == DDC_DIM_NOTYPE) {
			buildDataType(configItem, info, isServerName);
		}
	  // Exporting DCS data for DAQ information service
		if(configItem->getDataType() != BAD_TYPE) {
			now = time(&now);
//			ERS_LOG(applName << ": Exporting " << svcName << " to " << isServerName);
			exportDcsData(isServerName, configItem, info);
		}
		else
			ERS_LOG(applName << "_ERROR: Bad data type of " << svcName);
		configMutex.unlock();
	}
	else {
		configMutex.unlock();
		requestMutex.lock();
		configItem = findActiveRequest(svcName);
		if(configItem != 0) 
			acceptRequestedData(configItem, info);
		requestMutex.unlock();
	}
}

void DdcDataService::exportNoService(const string& isServer, const string& name, time_t now)
{
/* This functionality blocked 29.01.14 on request TDAQ in order to avoid
   sending string instead of any type of unavailable data - this crashes some TDAQ
   applications
	ddc::DdcStringInfoNamed*	isInfo = 0;
	string isName = isServer + '.' + name;
	string	txtNo("Service unavailable");
	
	// Removing DCS data from IS
	isInfo = new ddc::DdcStringInfoNamed(daqPartition, isName);
	try {isInfo->remove();}
	catch(daq::is::Exception) {;}	 // Ignore
	isInfo->value = txtNo;
	isInfo->ts = now;
	try { isInfo->checkin(); }
	catch(daq::is::Exception) {;}	 // Ignore
	delete isInfo;
*/
	return;
}


void DdcDataService::buildDataType(DdcServiceNameItem* item, DimInfo* info, string& isServer)
{
	string 		srvFormat;
	string 		stringOfData;
	string		serviceName = info->getName();
	string		isName = isServer + '.' + serviceName;
	ISInfoDictionary infoDict(daqPartition);
	int dimQuality;
	
	try {infoDict.remove(isName);}
	catch(daq::is::Exception &ex) {;}	//Ignore
	
	srvFormat = info->getFormat();
	dimQuality = info->getQuality();
	ERS_LOG(applName << "_INFO: Service name = " << serviceName << ", format: " << srvFormat);
	
	// Analyzing DIM format and creating DDC-DIM type definition
	if(srvFormat.size() == 1) {		// Dynamic array
		switch(srvFormat[0]) {
		case 'I':
			item->setDataType(IDA);
			break;
		case 'F':
			item->setDataType(FDA);
			break;
		case 'C':
			stringOfData = info->getString();
			if( (dimQuality == -2)
			|| (((int)stringOfData.size() != info->getSize())
			    && ((int)stringOfData.size() != info->getSize() - 1)) 	// There is '/0' inside
			)
				item->setDataType(SDA);
			else
				item->setDataType(CDA);
			break;
		default:	
			ERS_LOG(applName << "_ERROR: Unknown data format " << srvFormat[0] 
			                 << " of service " << serviceName << ": Will be ignored");
			item->setDataType(BAD_TYPE);
		}
	}
	else {
		if(srvFormat.find(';') == string::npos)			// Single value
			switch(srvFormat[0]) {
			case 'I':
				item->setDataType(I);
				break;
			case 'F':
				item->setDataType(F);
				break;
			case 'C':
				item->setDataType(C);
				break;
			default:
				ERS_LOG(applName << "_ERROR: Unknown data format " << srvFormat
								 << " of service " << serviceName << ": Will be ignored");
				item->setDataType(BAD_TYPE);
			}
		else
			item->setDataType(R);
	}
	
	return;
}


bool DdcDataService::exportDynInt(const string& isObName, int* data, 
											unsigned int arraySize, time_t sec)
{
	bool			success = false;
	unsigned int 	i;

//	DcsDataArray<int>* 			intDataArray = 0;
	ddc::DdcIntArrayInfoNamed* 	intDataArray = 0;

	intDataArray = new ddc::DdcIntArrayInfoNamed(daqPartition, isObName);
	if(arraySize) {
		for(i=0; i < arraySize; i++) {
//			intDataArray->addValue(data[i]);
			intDataArray->value.push_back(data[i]);
		}
	}
	intDataArray->ts = sec;
	try {
		intDataArray->checkin();
		success = true;
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG("daq::is::Exception" << ex);
	}
	catch(std::exception &ex) {
		ERS_LOG("std::exception while put to IS " << isObName);
	}
	catch(...) {
		ERS_LOG("Unknown exception");
	}
	delete intDataArray;
	return(success);
}


bool DdcDataService::exportDynFloat(const string& isObName, float* data, 
											  unsigned int arraySize, time_t sec)
{
	bool			success = false;
	unsigned int 	i;

//	DcsDataArray<float>* 			floatDataArray = 0;
	ddc::DdcFloatArrayInfoNamed* 	floatDataArray = 0;

	floatDataArray = new ddc::DdcFloatArrayInfoNamed(daqPartition, isObName);
	if(arraySize) {
		for(i=0; i < arraySize; i++) {
//			floatDataArray->addValue(data[i]);
			floatDataArray->value.push_back(data[i]);
		}
	}
	floatDataArray->ts = sec;
	try {
		floatDataArray->checkin();
		success = true;
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG("daq::is::Exception" << ex);
	}
	catch(std::exception &ex) {
		ERS_LOG("std::exception while put to IS " << isObName);
	}
	catch(...) {
		ERS_LOG("Unknown exception");
	}
	delete floatDataArray;
	return(success);
}


bool DdcDataService::exportDynString(const string& isObName, char* data, 
												int length, time_t sec)
{
	bool	success = false;
	string*	memString;
	char* 	pos = data;

	ddc::DdcStringArrayInfoNamed* 	txtDataArray = 0;

	txtDataArray = new ddc::DdcStringArrayInfoNamed(daqPartition, isObName);
	if(length) {
		while(1) {
			memString = new string(pos);
			txtDataArray->value.push_back(*memString);
			pos += memString->size() + 1;
			delete memString;
			if(pos >= data + length)
				break;
		}
	}
	txtDataArray->ts = sec;
	
	try {
		txtDataArray->checkin();
		success = true;
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG("daq::is::Exception" << ex);
	}
	catch(std::exception &ex) {
		ERS_LOG("std::exception while put to IS " << isObName);
	}
	catch(...) {
		ERS_LOG("Unknown exception");
	}
	delete txtDataArray;
	return(success);
}


bool DdcDataService::exportStruct(const string& isObName, void* data, const string& format,
										int length, time_t sec, const vector<string>& sons)
{
	bool			success = false;
	void* 			pos = data;
	unsigned int	amount;
	string::size_type 	frmpos = 0;
	string::size_type	fend = format.find(';');
	int*			ipos = (int *)data;
	float*			flpos = (float *)data;
	char*			cpos = (char *)data;
	string			currentFormat;
	char			buf[32];
	
	int				sonAmount = sons.size();
	int				sonNum = 0;
	string			sonName;
	ddc::DdcIntInfoNamed*			intDataPtr  = 0;
	ddc::DdcFloatInfoNamed* 		fltDataPtr  = 0;
	ddc::DdcStringInfoNamed*		txtDataPtr	= 0;
	ddc::DdcCharInfoNamed*			charDataPtr  = 0;
	char*							stringData;
	string							firstString;
	
	vector<string>	fields;
	int				numOfFields = sonAmount;
	if(numOfFields == 0) {						// Export all fields
		snprintf(buf, 31, "%d", numOfFields);
		sonName = buf;
		fields.push_back(sonName);
		numOfFields = 1;
		while(fend != string::npos) {
			snprintf(buf, 31, "%d", numOfFields);
			sonName = buf;
			fields.push_back(sonName);
			numOfFields +=1;
			fend = format.find(';', fend+1);
		}
		fend = format.find(';');
	}
	else
		fields = sons;
	
	currentFormat = format.substr(0, fend);
	while(sonNum < numOfFields) {
		sonName = isObName + '.' + fields[sonNum];
		
		amount = atoi(currentFormat.c_str() + 2);
		switch(currentFormat[0]) {
		case 'I':
			if(fields[sonNum] != "") {
				if(amount == 1) {
					intDataPtr = new ddc::DdcIntInfoNamed(daqPartition, sonName);
					intDataPtr->value = *((int*)pos);
					intDataPtr->ts = sec;
					try {
						intDataPtr->checkin();
						success = true;
					}
					catch(daq::is::Exception &ex) {
						ERS_LOG("daq::is::Exception" << ex);
					}	
					catch(std::exception &ex) {
						ERS_LOG("std::exception while put to IS " << sonName);
					}
					catch(...) {
						ERS_LOG("Unknown exception");
					}
					delete intDataPtr;
				}
				else {
					success = exportDynInt(sonName, (int *)pos, amount, sec);
				}
			}
			ipos = (int*)pos + amount;
			pos = ipos;
			break;
		case 'F':
			if(fields[sonNum] != "") {
				if(amount == 1) {
					fltDataPtr = new ddc::DdcFloatInfoNamed(daqPartition, sonName);
					fltDataPtr->value = *((float*)pos);
					fltDataPtr->ts = sec;
					try {
						fltDataPtr->checkin();
						success = true;
					}
					catch(daq::is::Exception &ex) {
						ERS_LOG("daq::is::Exception" << ex);
					}	
					catch(std::exception &ex) {
						ERS_LOG("std::exception while put to IS " << sonName);
					}
					catch(...) {
						ERS_LOG("Unknown exception");
					}
					delete fltDataPtr;
				}
				else {
					success = exportDynFloat(sonName, (float *)pos, amount, sec);
				}
			}
			flpos = (float*)pos + amount;
			pos = flpos;
			break;
		case 'C':
			if(fields[sonNum] != "") {
				if(amount == 1) {
					charDataPtr = new ddc::DdcCharInfoNamed(daqPartition, sonName);
					charDataPtr->value = *((char*)pos);
					charDataPtr->ts = sec;
					try {
						charDataPtr->checkin();
						success = true;
					}
					catch(daq::is::Exception &ex) {
						ERS_LOG("daq::is::Exception" << ex);
					}	
					catch(std::exception &ex) {
						ERS_LOG("std::exception while put to IS " << sonName);
					}
					catch(...) {
						ERS_LOG("Unknown exception");
					}
					delete charDataPtr;
				}
				else {
					firstString = (char *)pos;
					if(firstString.size() != amount) 	// dyn_string
						exportDynString(sonName, (char *)pos, amount, sec);
					else {
						stringData = strndup((const char*)pos, amount); 
						txtDataPtr = new ddc::DdcStringInfoNamed(daqPartition, sonName);
						txtDataPtr->value = stringData;
						txtDataPtr->ts = sec;
						try {
							txtDataPtr->checkin();
							success = true;
						}
						catch(daq::is::Exception &ex) {
							ERS_LOG("daq::is::Exception" << ex);
						}	
						catch(std::exception &ex) {
							ERS_LOG("std::exception while put to IS " << sonName);
						}
						catch(...) {
							ERS_LOG("Unknown exception");
						}
						delete stringData;
						delete txtDataPtr;
					}
				}
			}
			cpos = (char*)pos + amount;
			pos = cpos;
			break;
		default:
			// This is unlikely to happen
			ERS_LOG(applName << "_ERROR: Internal error of DIM, giving format " << currentFormat);
			frmpos = fend+1;
			success = true;		// in the sense that not checkin() failed
		}
		
		if(!success)
			break;
			
		sonNum++;
		
		if(fend == string::npos)
			break;
		else {				// Still not last currentFormat handled
			frmpos = fend+1;
			fend = format.find(';', frmpos);
			if(fend != string::npos) 							// this is not last format group
				currentFormat = format.substr(frmpos, fend-frmpos);
			else {
				currentFormat = format.substr(frmpos);
				if(currentFormat.size() == 1) {			// i.e. I,F or C
					long lRem;
					switch(currentFormat[0]) {
					case 'I':
						lRem = ((long)data+length-(long)pos)/sizeof(int);
						break;
					case 'F':
						lRem = ((long)data+length-(long)pos)/sizeof(float);
						break;
					case 'C':
						lRem = ((long)data+length-(long)pos)/sizeof(char);
						break;
					default: 
						lRem = 0;
					}
					sprintf(buf, "%ld", lRem);
					string calcAmount(buf);
					currentFormat = currentFormat + ':' + calcAmount;
				}
			}
		}
	}
	return(success);
}


void DdcDataService::exportDcsData(const string& server, DdcServiceNameItem* item, DimStampedInfo* info)
{
	ddc::DdcIntInfoNamed*			intDataPtr  = 0;
	ddc::DdcFloatInfoNamed* 		fltDataPtr  = 0;
	ddc::DdcStringInfoNamed*		txtDataPtr	= 0;
	ddc::DdcCharInfoNamed*			charDataPtr  = 0;
	const string 					obName = item->getServiceName();
	
	DdcDimDataType dataType = item->getDataType();
	time_t 		updateTime = (time_t)(info->getTimestamp());
	void*		data = info->getData();
	int			length = info->getSize();
	const vector<string>&	structSons = item->getSons();
	const string			dataFormat = info->getFormat();

	string 		isObName = server + "." + obName;
	
	bool success = false;
	string tmpString;
	string errType = "";
	int limit = 3;		// Max number of publishing attemts
	
	while(limit > 0) {
		switch(dataType) {
			case I:
				intDataPtr = new ddc::DdcIntInfoNamed(daqPartition, isObName);
				intDataPtr->value = info->getInt();
				intDataPtr->ts = updateTime;
				try {
					intDataPtr->checkin();
					success = true;
				}
				catch(daq::is::Exception &ex) {
					ERS_LOG("daq::is::Exception" << ex);
				}
				catch(std::exception &ex) {
					ERS_LOG("std::exception while put to IS " << isObName);
				}
				catch(...) {
					ERS_LOG("Unknown exception");
				}
				delete intDataPtr;
				break;
			case IDA:
				length = length/sizeof(int);
				success = exportDynInt(isObName, (int *)data, length, updateTime);
				break;
			case F:
				fltDataPtr = new ddc::DdcFloatInfoNamed(daqPartition, isObName);
				fltDataPtr->value = info->getFloat();
				fltDataPtr->ts = updateTime;
				try {
					fltDataPtr->checkin();
					success = true;
				}
				catch(daq::is::Exception &ex) {
					ERS_LOG("daq::is::Exception" << ex);
				}
				catch(std::exception &ex) {
					ERS_LOG("std::exception while put to IS " << isObName);
				}
				catch(...) {
					ERS_LOG("Unknown exception");
				}
				delete fltDataPtr;
				break;
			case FDA:
				length = length/sizeof(float);
				success = exportDynFloat(isObName, (float *)data, length, updateTime);
				break;
			case C:
				charDataPtr = new ddc::DdcCharInfoNamed(daqPartition, isObName);
				charDataPtr->value = (info->getString())[0];
				charDataPtr->ts = updateTime;
				try {
					charDataPtr->checkin();
					success = true;
				}
				catch(daq::is::Exception &ex) {
					ERS_LOG("daq::is::Exception" << ex);
				}
				catch(std::exception &ex) {
					ERS_LOG("std::exception while put to IS " << isObName);
				}
				catch(...) {
					ERS_LOG("Unknown exception");
				}
				delete charDataPtr;
				break;
			case CDA:
				txtDataPtr = new ddc::DdcStringInfoNamed(daqPartition, isObName);
				txtDataPtr->value = info->getString();
			
               // This test is unreasonable/mistaken here because has been done already while the
			   // data type definition: it would be SDA, not CDA under this condition
			   // ==> Removing the test. SK, 30.05.22
			   /*	// run-time check that it is not a dyn_string; introduced 09.09.14 by SK
				tmpString = txtDataPtr->value;
				if( ((int)tmpString.size() != info->getSize())
				 && ((int)tmpString.size() != (info->getSize() - 1)) ) {	// There is '/0' inside
					string strErr = obName+" came as dyn_string. Check DIM version in DCS project";
					ers::warning(ddc::AppWarning(ERS_HERE, strErr.c_str()));
				}
				*/	
				txtDataPtr->ts = updateTime;
				try {
					txtDataPtr->checkin();
					success = true;
				}
				catch(daq::is::Exception &ex) {
					ERS_LOG("daq::is::Exception" << ex);
				}
				catch(std::exception &ex) {
					ERS_LOG("std::exception while put to IS " << isObName);
				}
				catch(...) {
					ERS_LOG("Unknown exception");
				}
				delete txtDataPtr;
				break;
			case SDA:
				success = exportDynString(isObName, (char *)data, length, updateTime);
				break;
			case R:
				success = exportStruct(isObName, data, dataFormat, length, updateTime, structSons);
				break;
			default:
				ERS_LOG(applName << "_ERROR: Internal error of DdcDimDataType configured as " << (int)(dataType)
								 << " for " << obName << " service");
				ERS_LOG("   =====> " << obName << " has not been exported!");
				success = true;		// in the sense "not a problem of checkin()"
				return;
		}
		serverAvailableMutex.lock();
		string errText;
		if(!success) {
			limit--;
			ERS_LOG(applName << "_ERROR: " << ctime((const time_t*)&updateTime) << " IS server "
				 << server << " in partition " << daqPartition.name() << " did not accept " 
				 << isObName);
			if(limit>0) {
				errText = "Unsuccessful attempt of publishing for TDAQ "+isObName;
				ers::warning(ddc::AppWarning(ERS_HERE, errText.c_str()));
				sleep(2);
			}
			if(!isUnavailableServer(server)) {
				string msgText = "IS server " + server + " does not run, it seems!";
				ERS_LOG(applName << "_ERROR: " << msgText);
				ers::warning(ddc::AppWarning(ERS_HERE, msgText.c_str()));
				unavailableServers.push_back(server);
			}
			if(limit==0) {
				errText = "Unsuccessful final attempt of publishing for TDAQ "+isObName;
				errText = errText+" Take care of accessibility of "+server+" IS server, ";
				errText = errText+"then restart "+applName+" if not running";
				ers::error(ddc::AppError(ERS_HERE, errText.c_str()));
			}
		}
		else {
//			ERS_LOG("Successful publishing");
			limit = 0;			// Attempt of publishing was successful: break from WHILE
			markAvailable(server);
		}
		serverAvailableMutex.unlock();
	}		// End of WHILE
	return;
}


bool DdcDataService::isUnavailableServer(std::string server)
{
	std::vector<std::string>::iterator it;
	
	for(it = unavailableServers.begin(); it != unavailableServers.end(); it++) {
		if(*it == server)
			return(true);
	}
	return(false);
}


void DdcDataService::markAvailable(std::string server)
{
	std::vector<std::string>::iterator it;
	
	for(it = unavailableServers.begin(); it != unavailableServers.end(); it++) {
		if(*it == server) {
			unavailableServers.erase(it);
			return;
		}
	}
}
	

void DdcDataService::initExport()
{
	handleExportConfigWildcards(dcsExportConfig);
	
	vector<DdcServiceNameItem>::iterator serviceItemPtr;
	string						serviceName;
	vector<DdcServiceNameItem>&	itemList = dcsExportConfig->getItems();

	for(serviceItemPtr = itemList.begin(); serviceItemPtr != itemList.end(); serviceItemPtr++) {
		serviceName = serviceItemPtr->getServiceName();
		DimStampedInfo* info = new DimStampedInfo(serviceName.c_str(), (char *)UNAVAILABLE_INFO.c_str(), this);
		activeClients.push_back(info);
	}
}


void DdcDataService::removeClient(string& serviceName)
{
	vector<DdcServiceNameItem>&				itemList = dcsExportConfig->getItems();
	
	vector<DdcServiceNameItem>::iterator 	it;
	vector<DimStampedInfo *>::iterator		clientIt;
//	time_t	now;
	
	ERS_LOG(applName << "_INFO: removeClient() for " << serviceName);
	for(it = itemList.begin(); it != itemList.end(); it++) {
		if(it->getServiceName() == serviceName) {
			removeIsEntry(it);
			itemList.erase(it);
//			now = time(&now);
//			ERS_LOG(applName << "_WARNING: Transfer for DAQ of " << serviceName 
//				 << " is stopped on a user request";
			for(clientIt = activeClients.begin(); clientIt != activeClients.end(); clientIt++)
				if(serviceName == (*clientIt)->getName()) {
					delete *clientIt;
					activeClients.erase(clientIt);
					break;
				}
			break;
		}
	}
}


void DdcDataService::wildcardsRemoveClients(string& serviceName)
{
	vector<DdcServiceNameItem>&				itemList = dcsExportConfig->getItems();
	vector<DdcServiceNameItem>::iterator 	it;
	vector<string>							patterns;
	vector<string>							toRemove;
	string::size_type						pos = 0;
	string::size_type						endpos = serviceName.find('*');
	unsigned int							i;
	unsigned int							patternNum;
	bool 									found;

	while(endpos != string::npos) {
		patterns.push_back(serviceName.substr(pos, endpos-pos));
		if(endpos == serviceName.size()-1)
			break;
		pos = endpos+1;
		endpos = serviceName.find('*', pos);
	}
	if(endpos != serviceName.size()-1) 
		patterns.push_back(serviceName.substr(pos));
	
	patternNum = patterns.size();
	for(it = itemList.begin(); it != itemList.end(); it++) {
		found = true;
		for(i=0; i<patternNum; i++) {
			if((it->getServiceName()).find(patterns[i]) == string::npos) {
				found = false;
				break;
			}
		}
		if(found) 
			toRemove.push_back(it->getServiceName());
	}
	for(i=0; i<toRemove.size(); i++)
		removeClient(toRemove[i]);
}


void DdcDataService::handleDaqUnsubscribeRequest(vector<string>& request)
{
	unsigned int	i, len = request.size();
	string			isServer;
	ISInfoDictionary	infoDict(daqPartition);

	ERS_LOG(applName << "_INFO: Unsubscribe request for " << request.size() << " services");
	for(i=0; i<len; i++) {
		if(request[i].find(':') != string::npos)
			request[i] = request[i].substr(0, request[i].find(':'));
		else
			request[i] = request[i].substr(0, request[i].find_first_of(" =>"));
//		ERS_LOG(" " << request[i]);
	}
	
	configMutex.lock();
	for(i=0; i<len; i++) {
		if(request[i].find('*') == string::npos)		//no wildcards
			removeClient(request[i]);
		else
			wildcardsRemoveClients(request[i]);
	}
	configMutex.unlock();
//	ERS_LOG(applName << "_INFO: Unsubscribe request executed");
}	
	

void DdcDataService::initMoreExport(unsigned int howMany)
{
	int										count = howMany;
	vector<DdcServiceNameItem>::iterator 	itemPtr;
	string									serviceName;
	vector<DdcServiceNameItem>&				itemList = dcsExportConfig->getItems();
//	time_t	now;

	ERS_LOG(applName << "_INFO: initMoreExport(): " << howMany);
	if(itemList.size() != 0) {
		itemPtr = itemList.begin();
		do {
			serviceName = itemPtr->getServiceName();
//			now = time(&now);
//			ERS_LOG(applName << "_INFO: Initiating transfer for DAQ of " << serviceName);
			DimStampedInfo* info = new DimStampedInfo(serviceName.c_str(), (char *)UNAVAILABLE_INFO.c_str(), this);
			activeClients.push_back(info);
			itemPtr++;
		}  while(--count);
	}
}


void DdcDataService::moreForExport(DdcDtDimConfig* moreConfig)
{
	unsigned int 			i;
	DdcServiceNameItem*		existent;
	vector<DdcServiceNameItem>::iterator toJoin;
	vector<string>			toRemove;
	string					isName;
	vector<DdcServiceNameItem>&	itemList = moreConfig->getItems();
	ISInfoDictionary	infoDict(daqPartition);
	
//	ERS_LOG("moreForExport(): " << itemList.size());
	if(itemList.size() != 0) 
		for(toJoin = itemList.begin(); toJoin != itemList.end(); toJoin++) {
			existent = dcsExportConfig->findItem(toJoin->getServiceName());
			if(!existent) {
				dcsExportConfig->addFirst(&*toJoin);
			}
			else {
//				ERS_LOG("Still exists in config: " << existent->getServiceName());
				if(existent->getIsServer() != toJoin->getIsServer()) {
					// Removing value from former IS server
					isName = existent->getIsServer();
					isName += ".";
					isName += existent->getServiceName();
					try {infoDict.remove(isName);}
					catch(daq::is::Exception &ex) 
						{ ERS_LOG(applName << "_ERROR: Could not remove " << isName << " " << ex);}

					existent->setIsServer(toJoin->getIsServer());
				}
				toRemove.push_back(toJoin->getServiceName());
			}
		}
	
	for(i=0; i<toRemove.size(); i++) 
		moreConfig->removeItem(toRemove[i]);
}


void DdcDataService::openConfigStructures(DdcDtDimConfig* config)
{
	vector<DdcServiceNameItem>::iterator 	item;
	vector<DdcServiceNameItem>&				itemList = config->getItems();
	string									serviceName;
	
//	ERS_LOG("openConfigStructures());
	for(item = itemList.begin(); item != itemList.end(); item++) {
		serviceName = item->getServiceName();
		if(serviceName.find(':') != string::npos) {
			string srv = serviceName.substr(0, serviceName.find(':'));
			item->changeName(srv);
			string sons = serviceName.substr(serviceName.find(':')+1);
			item->buildListOfSons(sons);
		}
	}
}


void DdcDataService::handleExportConfigWildcards(DdcDtDimConfig* dtConfig)
{
	vector<DdcServiceNameItem>::iterator item;
	DdcServiceNameItem*				abcItem;
	string 							serviceName;
	unsigned int 					i;
	vector<string>					toRemove;
	vector<DdcServiceNameItem>		toJoin;
	string							isServer;
	vector<DdcServiceNameItem>&		itemList = dtConfig->getItems();
	DimBrowser						browser;
	int								serviceClass;
	string							msg;
	char*							service;
	char*							format;
	string							sons;

	if(itemList.size() != 0) {
		for(item = itemList.begin(); item != itemList.end(); item++) {
			serviceName = item->getServiceName();
			isServer = dtConfig->findIsServer(serviceName);
			
			if(serviceName.find('*') != string::npos) {					// Name with wildcards
				toRemove.push_back(serviceName);
				browser.getServices(serviceName.c_str());
				serviceClass = browser.getNextService(service, format);
				if(serviceClass == 0) {
					msg = "There is no published services like " + serviceName;
					ERS_LOG(applName << "_WARNING: " << msg);
					time_t now = time(&now);
					exportNoService(isServer, serviceName, now);
					continue;
				}
				while(serviceClass) {
					if(serviceClass == DimSERVICE) {						// Otherwise nothing to do
						serviceName = service;
						abcItem = new DdcServiceNameItem(serviceName, isServer); 
						toJoin.push_back(*abcItem);
						delete abcItem;
					}
					serviceClass = browser.getNextService(service, format);		
				}
			}
		}
	
		// Removing names with wildcards from exportConfig
		for(i=0; i<toRemove.size(); i++) 
			dtConfig->removeItem(toRemove[i]);

		// Joining a-b-c-named items (if any) to the exportConfig
		for(i=0; i<toJoin.size(); i++) 
			dtConfig->addItem(&toJoin[i]);
	}
}


void DdcDataService::handleDaqSubscribeRequest(vector<string>& request)
{
	unsigned int 	number;
	unsigned int 	i;
	
	ERS_LOG(applName << "_INFO: Subscribe request for ");
	for(i=0; i<request.size(); i++) 
		ERS_LOG("        " << request[i]);
	
	DdcDtDimConfig* moreConfig = new DdcDtDimConfig(request, applName);
	// replacing wildcards
	handleExportConfigWildcards(moreConfig);
	// replacing structures by their root and leaves
	openConfigStructures(moreConfig);
	// extend configuration (may reduce moreConfig)
	configMutex.lock();
	moreForExport(moreConfig);
	number = moreConfig->getItemNum();
	configMutex.unlock();
	delete moreConfig;	
	// and finally make additional DIM subscription
	if(number > 0) {
//		configMutex.lock();
		initMoreExport(number);
//		configMutex.unlock();
	}
}


DdcServiceNameItem* DdcDataService::findActiveRequest(string& serviceName)
{
	vector<DdcServiceNameItem *>::iterator	it = activeRequests.begin();
	
//	ERS_LOG("findActiveRequest(): " << serviceName);
	for(; it != activeRequests.end(); it++) 
		if((*it)->getServiceName() == serviceName)
			return(*it);
	return(0);
}


void DdcDataService::acceptRequestedData(DdcServiceNameItem* configItem, DimStampedInfo* info)
{
	string		msg;
	string		serviceName = info->getName();
	string		serverName = configItem->getIsServer();
	
//	ERS_LOG("acceptRequestedData(): " << serviceName << " for " << serverName);
	if(info->getSize() == (int)UNAVAILABLE_INFO.size()+1) 
		if(info->getString() == UNAVAILABLE_INFO) {
			msg = "Unsuccessful DAQ request for " + serviceName + " (Seems not be published)";
			ERS_LOG(applName << "_ERROR: " << msg);
			time_t now = time(&now);
			exportNoService(serverName, serviceName, now);	
			delete info;
			return;
		}
	buildDataType(configItem, info, serverName);
//	ERS_LOG(Format built: " << (int)(configItem->getDataType()));
	if(configItem->getDataType() != BAD_TYPE)
		exportDcsData(serverName, configItem, info);
	else {
		time_t now = time(&now);
		exportNoService(serverName, serviceName, now);		
	}
	delete info;
}


void DdcDataService::handleDirectDataRequest(string& serviceName, string& serverName)
{
	DdcServiceNameItem* configItem;
	string				sons = "";
	
//	ERS_LOG("handleDirectDataRequest(): " << serviceName);
	string fatherService = serviceName.substr(0, serviceName.find(':'));
	requestMutex.lock();
//	LOG("requestMutex locked");
	if(!(configItem = findActiveRequest(fatherService))) {
		configItem = new DdcServiceNameItem(fatherService, serverName);
		activeRequests.push_back(configItem);
	}
	else {
		configItem->setIsServer(serverName);
	}
	
	if(serviceName.find(':') != string::npos) 
		sons = serviceName.substr(serviceName.find(':')+1);
	configItem->buildListOfSons(sons);
	
	DimStampedInfo*	info = new DimStampedInfo(fatherService.c_str(), (char *)UNAVAILABLE_INFO.c_str(), this);
//	ERS_LOG("New DimInfo " << fatherService);
	requestMutex.unlock();
}


void DdcDataService::handleDaqDataRequest(vector<string>& request)
{
	string 			serviceName;
	string 			serverName;
	DimBrowser		browser;
	char*			service;
	char*			format;
	int				serviceClass;
	string 			msg;
	string			fatherService;
	string			sonNames;
	unsigned int	pos;

	unsigned int i;

	ERS_LOG("handleDaqDataRequest() " << request.size());
	for(i=0; i<request.size(); i++) {
		if(request[i].find("=>") == string::npos) {
			ERS_LOG(applName << "_ERROR: IS server is not defined => "
				 << request[i] << " ignored");
			continue;
		}
		ERS_LOG("handleDaqDataRequest(): request[" << i << "] = " << request[i]);
		serverName = request[i].substr(request[i].find("=>")+2);		// Extract IS server name
		serverName = serverName.substr(serverName.find_first_not_of(" "));
		serverName = serverName.substr(0, serverName.find_first_of(" "));
		serviceName = request[i].substr(0, request[i].find_first_of(" =>"));	// Drop IS server name
		if((serverName.size() == 0) || (serviceName.size() == 0)) {
			ERS_LOG(applName << "_ERROR: IS server or service name absents => "
				 << request[i] << " ignored");
			continue;
		}
		
		if(serviceName.find('*') != string::npos) {						// Name with wildcards
			if(serviceName.find(':') != string::npos) {
				pos = serviceName.find(':');
				sonNames = serviceName.substr(pos+1);					// Struct field names
				serviceName = serviceName.substr(0, pos);
			}
			else
				sonNames = "";

			browser.getServices(serviceName.c_str());
//			ERS_LOG("Browser result: " << (int)service);
			serviceClass = browser.getNextService(service, format);
			if(serviceClass == 0) {
				msg = "There is no published services like " + serviceName;
				ERS_LOG(applName << "_ERROR: " << msg);
				time_t now = time(&now);
				exportNoService(serverName, serviceName, now);
				continue;
			}
			while(serviceClass) {
				if(serviceClass == DimSERVICE) {						// Otherwise nothing to do
					serviceName = service;
					if(sonNames != "") 
						serviceName = serviceName + ':' + sonNames;
					handleDirectDataRequest(serviceName, serverName);
				}
				serviceClass = browser.getNextService(service, format);		
			}
		}
		else {
			handleDirectDataRequest(serviceName, serverName);
		}
	} 
}


void DdcDataService::buildStructService(vector<IsService *>& fields, IsService* serviceDef,
																		unsigned int length)
{
	unsigned int 					i;
	unsigned int					pos = 0;
	unsigned int					fieldLen;
	void*							dataPtr;
	char							fBuf[16];
	vector<IsService *>::iterator	it;
	char* 							buf = new char[length];
	
	for(it = fields.begin(); it != fields.end(); it++) {
		// packing data
		switch((*it)->type) {
		case I:
			dataPtr = (void *)&((*it)->data.intValue);
			break;
		case F:
			dataPtr = (void *)&((*it)->data.floatValue);
			break;
		default:
			dataPtr = (*it)->data.anyValue;
		}
		fieldLen = (*it)->length;
		for(i=0; i<fieldLen; i++) {
			buf[pos+i] = *(((char *)dataPtr) + i);
		}
		pos += fieldLen;
		
		// making format
		if(it != fields.begin())
			serviceDef->dimFormat += ';';
		switch((*it)->type) {
		case I:
		case IDA:
			snprintf(fBuf, 15, "I:%ld", fieldLen/sizeof(int));
			(*it)->dimFormat = fBuf;
			break;
		case F:
		case FDA:
			snprintf(fBuf, 15, "F:%ld", fieldLen/sizeof(float));
			(*it)->dimFormat = fBuf;
			break;
		case C:
		case CDA:
		case SDA:
			snprintf(fBuf, 15, "C:%ld", fieldLen/sizeof(char));
			(*it)->dimFormat = fBuf;
			break;
		default: ;			// cannot be R
		}
		serviceDef->dimFormat += (*it)->dimFormat;
	}
	serviceDef->data.anyValue = (void *)buf;
	string::size_type lr = (serviceDef->dimFormat).rfind(':');
	serviceDef->dimFormat = (serviceDef->dimFormat).substr(0, lr);
}


void DdcDataService::removeStructFields(vector<IsService *>& fields)
{
	vector<IsService *>::iterator	it;
	
	for(it = fields.begin(); it != fields.end(); it++) {
		switch((*it)->type) {
		case C:
			delete (char *)((*it)->data.anyValue);
			break;
		case IDA:
			delete[] (int *)((*it)->data.anyValue);
			break;
		case FDA:
			delete[] (float *)((*it)->data.anyValue);
			break;
		case CDA:
		case SDA:
			delete[] (char *)((*it)->data.anyValue);
			break;
		default: ; 			// nothing to do for I, F
		}
		delete *it;
	}
}


void DdcDataService::updateIsRecord(IsService* serviceDef, ISInfoDynAny& value)
{
	const vector<string>&	fields = dcsImportConfig->findItem(serviceDef->name)->getSons();
	if(fields.size())		// Fields specified
		updateIsRecordFields(serviceDef, fields, value);
	else
		updateCompleteIsRecord(serviceDef, value);
}


void DdcDataService::updateIsRecordFields(IsService* serviceDef, const vector<string>& fields,
										  ISInfoDynAny& value)
{
	OWLTime			whenSet;
	unsigned int	attrNum = value.getAttributesNumber();
	unsigned int	i;
	bool			fieldSuccess;
	unsigned int	fieldNum = fields.size();
	
/*	// Why the Time should be skipped? Commenting 28.05.15 SK
	if(value.getAttributeType(0) == ISType::Time) {
		value >> whenSet;
		attrNum--;
	}
*/	
	IsService* 	fieldDef;
	string		fieldName;
	// since 28.05.15 i - the attribute absolute number (index) in DynAny
	for(i=0; i<attrNum; i++) {
		if(i < fieldNum) {
			if(fields[i] != "") {
				fieldName = serviceDef->name + '.' + fields[i];
				if(isServicePublished(fieldName, &fieldDef)) {
					updateIsNonStructService(fieldDef, value, i);
					continue;
				}
			}
		// the lines from here included into if() 06.07.10 by SK)
			fieldDef = new IsService;
			fieldSuccess = false;
			fieldSuccess = prepareDataBuf(value, i, &(fieldDef->data), value.getAttributeType(i),
									 value.isAttributeArray(i), fieldDef->length, &(fieldDef->type));
			if(fieldSuccess) 
				releaseDataBuf(fieldDef);
			delete fieldDef;
		}
		else
			break;
	}
}


void DdcDataService::updateCompleteIsRecord(IsService* serviceDef, ISInfoDynAny& value)
{
	OWLTime			whenSet;
	unsigned int	attrNum = value.getAttributesNumber();
	unsigned int	i;
	vector<IsService *>  structureFields;
	bool			success = false;
	
/*	// Why the Time should be skipped? Commenting 28.05.15 SK
	if(value.getAttributeType(0) == ISType::Time)
		value >> whenSet;
*/		
	IsService* newServiceDef = new IsService;
	newServiceDef->name = serviceDef->name;
	newServiceDef->type = R;
	newServiceDef->length = 0;
	
	IsService* fieldDef;
	for(i=0; i<attrNum; i++) {
		fieldDef = new IsService;
		success = prepareDataBuf(value, i, &(fieldDef->data), value.getAttributeType(i),
								 value.isAttributeArray(i), fieldDef->length, &(fieldDef->type));
		if(!success) {
// ERS message commented to avoid multiplication of the same message
// An error mesage has been already issued during first publication of that data
//			ERS_LOG(applName << "_ERROR: " << serviceDef->name
//				 << " Cannot be transferred for DCS due to unacceptable format " << value.getAttributeType() 
//				 << " of record field " << i);
			break;
		}
		structureFields.push_back(fieldDef);
		newServiceDef->length += fieldDef->length;
	}
	
	if(success) {
		int clients;
		buildStructService(structureFields, newServiceDef, newServiceDef->length);
		if(newServiceDef->dimFormat != serviceDef->dimFormat) {
			stopDdcDimService(serviceDef);
			ERS_LOG("Existing service " << serviceDef->name << " is stopped for changing");
			*serviceDef = *newServiceDef;
			delete newServiceDef;
			serviceDef->dimService = new DimService((char *)((serviceDef->name).c_str()), 
													(char *)((serviceDef->dimFormat).c_str()), 
													serviceDef->data.anyValue, serviceDef->length);
		}
		else {
			newServiceDef->dimService = serviceDef->dimService;
			*serviceDef = *newServiceDef;
			delete newServiceDef;
			clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
// Commented 13.08.10: If no client subscribed, and the update rate is high => log file is being filled in soon => DT crashes
//			if(!clients)
//				ERS_LOG(applName << "_ERROR: " << serviceDef->name << " is not updated in DCS (probably, not defined)");
		}
	}
	
	removeStructFields(structureFields);
}


void DdcDataService::publishIsRecord(string& serviceName, ISInfoDynAny& value)
{
	const vector<string>&	fields = dcsImportConfig->findItem(serviceName)->getSons();
	if(fields.size())	{						// Fields specified
//		ERS_LOG(applName << "_DEBUG: Publishing fields of " << serviceName);
		publishIsRecordFields(serviceName, fields, value);
	}
	else {
		publishCompleteIsRecord(serviceName, value);
	}
}


void DdcDataService::publishIsRecordFields(string& serviceName, const vector<string>& fields, 
											ISInfoDynAny& value)
{
	OWLTime			whenSet;
	unsigned int	attrNum = value.getAttributesNumber();
	unsigned int	i;
//	for(i=0; i<attrNum; i++)
//		ERS_LOG("_DEBUG: Field from config " + fields[i]);
	bool			success = false;
	bool			fieldSuccess = false;
	unsigned int	fieldNum = fields.size();
	string			errMsg;
	string 			publisher;
	string 			node;	

	IsService* serviceDef = new IsService;
	serviceDef->name = serviceName;
	serviceDef->type = R;
	serviceDef->length = 0;
	
/*	// Why the Time should be skipped? Commenting 28.05.15 SK
	if(value.getAttributeType(0) == ISType::Time) {
		value >> whenSet;
		attrNum--;
	}
*/	
	
	IsService* fieldDef;
	for(i=0; i<attrNum; i++) {
		if(i < fieldNum) {
			fieldDef = new IsService;
			fieldDef->name = serviceName + '.' + fields[i];
			beingPublished = fieldDef->name;
//			ERS_LOG("_DEBUG: Field "+serviceName + '.' + fields[i]);

			fieldSuccess = prepareDataBuf(value, i, &(fieldDef->data), value.getAttributeType(i),
						            	  value.isAttributeArray(i), fieldDef->length, &(fieldDef->type));
			if((fields[i] != "") && (fieldSuccess)) {
				success = true;		// At least one field to be published
//				ERS_LOG("_DEBUG: Publishing this field");
				__publishSimpleService(fieldDef);
				isServices.push_back(fieldDef);
				continue;
			}
			else {
//				ERS_LOG("_DEBUG: NOT publishing this empty field");
				if(!fieldSuccess && (fields[i] != ""))
					ERS_LOG("_WARNING: " << fieldDef->name << " Cannot be transferred for DCS due to unacceptable format");
				if(fieldSuccess)
					releaseDataBuf(fieldDef);
				delete fieldDef;
			}
		}
		else
			break;
	}

	if(success)
		isServices.push_back(serviceDef);		// this is for quiet skiping empty fields in the future
}


void DdcDataService::publishCompleteIsRecord(string& serviceName, ISInfoDynAny& value)
{
	OWLTime			whenSet;
	unsigned int	attrNum = value.getAttributesNumber();
	unsigned int	i;
	vector<IsService *>  structureFields;
	bool			success = false;
	string			errMsg;
	string 			publisher;
	string 			node;
	
	IsService* serviceDef = new IsService;
	serviceDef->name = serviceName;
	serviceDef->type = R;
	serviceDef->length = 0;
	
/*	// Why the Time should be skipped? Commenting 28.05.15 SK
	if(value.getAttributeType(0) == ISType::Time) {
		value >> whenSet;
		attrNum--;
	}
*/	
//	if(isAlreadyPublished(serviceName,publisher,node))
//	std::cerr << serviceName << "Published by " << publisher << "@" << node << std::endl;
	IsService* fieldDef;
	for(i=0; i<attrNum; i++) {
		fieldDef = new IsService;
		success = prepareDataBuf(value, i, &(fieldDef->data), value.getAttributeType(i),
								 value.isAttributeArray(i), fieldDef->length, &(fieldDef->type));
		if(!success) {
			ERS_LOG("_WARNING: " << serviceName << " Cannot be transferred for DCS due to unacceptable format "
						         << value.getAttributeType(i) << " of record field " << i);
			break;
		}
		structureFields.push_back(fieldDef);
		serviceDef->length += fieldDef->length;
	}
	
	if(success) {
		buildStructService(structureFields, serviceDef, serviceDef->length);
		ERS_LOG("Publishing: " << serviceName << ", " << serviceDef->dimFormat
				 << ", length " << serviceDef->length);
		beingPublished = serviceName;
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), (char *)((serviceDef->dimFormat).c_str()), 
																serviceDef->data.anyValue, serviceDef->length);
		isServices.push_back(serviceDef);
	}
	removeStructFields(structureFields);
	
	return;
}


bool DdcDataService::isServicePublished(string& serviceName, IsService** service)
{
	vector<IsService *>::iterator it;
	
	for(it = isServices.begin(); it !=isServices.end(); it++) {
//		ERS_LOG("_INFO: " << (*it)->name << " is published");
		if((*it)->name == serviceName) {
			*service = *it;
			return(true);
		}
	}
	return(false);
}


void DdcDataService::updateIsNonStructService(IsService* serviceDef, ISInfoDynAny& value, int position)
{
	OWLTime			whenSet;
	ISType::Basic	isType;
	bool			is_array = false;
	DdcDimDataType	ddcDimType = serviceDef->type;
	int*			oldIntArray = 0;
	float*			oldFloatArray = 0;
	char*			oldCharArray = 0;
	char*			oldChar = 0;
	int64_t*		oldXArray = 0;

	//Keep in mind the previous buffers	
	switch(ddcDimType) {
	case C:
		oldChar = (char *)(serviceDef->data.anyValue);
		break;
	case IDA:
		oldIntArray = (int *)(serviceDef->data.anyValue);
		break;
	case X:		// X and XDA aren't possible (see comment in next switch())
	case XDA:
		oldXArray = (int64_t *)(serviceDef->data.anyValue);
		break;
	case FDA:
		oldFloatArray = (float *)(serviceDef->data.anyValue);
		break;
	case CDA:
	case SDA:
		oldCharArray = (char *)(serviceDef->data.anyValue);
		break;
	default: ;		// Nothing to delete
	}
	
/*	// Why the Time should be skipped? Commenting 28.05.15 SK
	if((isType = value.getAttributeType()) == ISType::Time)
		value >> whenSet;
*/
	isType = value.getAttributeType(position);
	is_array = value.isAttributeArray(position);
	
	if(!prepareDataBuf(value, position, &(serviceDef->data), isType, is_array, serviceDef->length, &ddcDimType)) {
// ERS message commented to avoid multiplication of the same message
// An error mesage has been already issued during first publication of that data
//		ERS_LOG(applName << "_ERROR: " << serviceDef->name
//			 << " Cannot be transferred for DCS due to unacceptable format " << isType);
		return;
	}
	
	int	clients;
//	std::cerr << "Updating DIM service of the type " << ddcDimType << std::endl;
	switch(ddcDimType) {
	case I:
	case F:
		clients = serviceDef->dimService->updateService();
		break;
	case X:  // prepareDataBuf() does NOT set type X or XDA (at least now, 15.07.14)
	         // because PVSS does not accept int64. It is passed now as int[2] 
			 // with LSB in[0] and MSB in[1] element. DdcDimType is IDA.
		ERS_LOG("_INFO: Updating X-service " << serviceDef->name);
		clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
		delete oldXArray;
		break;
	case C:
		clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
		delete oldChar;
		break;
	case IDA:
		clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
		delete[] oldIntArray;
		break;
	case XDA:	// Does not exist: see X above
		clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
		delete[] oldXArray;
		break;
	case FDA:
		clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
		delete[] oldFloatArray;
		break;
	case CDA:
	case SDA:
		clients = serviceDef->dimService->updateService(serviceDef->data.anyValue, serviceDef->length);
		delete[] oldCharArray;
		break;
	default: ;			// Nothing to do
	}
// Commented 13.08.10: If no client subscribed, and the update rate is high => log file is being filled in soon => DT crashes
//	if(!clients)
//		ERS_LOG(applName << "_WARNING: "<< serviceDef->name << " is not updated in DCS (probably, not defined)");
}


void DdcDataService::__publishSimpleService(IsService* serviceDef)
{
	string	serviceName = serviceDef->name;
	
//	ERS_LOG("_DEBUG: Publishing " << serviceName << " type " << serviceDef->type);
	switch(serviceDef->type) {
	case I:
		serviceDef->dimFormat = "I:1";
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), serviceDef->data.intValue);
		break;
	case IDA:
		serviceDef->dimFormat = "I";
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), (char *)((serviceDef->dimFormat).c_str()), 
																serviceDef->data.anyValue, serviceDef->length);
		break;
	case F:
		serviceDef->dimFormat = "F:1";
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), serviceDef->data.floatValue);
		break;
	case FDA:
		serviceDef->dimFormat = "F";
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), (char *)(serviceDef->dimFormat).c_str(), 
																serviceDef->data.anyValue, serviceDef->length);
		break;
	case C:
		serviceDef->dimFormat = "C:1";
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), (char *)(serviceDef->data.anyValue));
		break;
	case CDA:
	case SDA:
		serviceDef->dimFormat = "C";
		serviceDef->dimService = new DimService((char *)serviceName.c_str(), (char *)(serviceDef->dimFormat).c_str(), 
																serviceDef->data.anyValue, serviceDef->length);
		break;
	default: ;	// No default format
	}
}


// This (expensive for time) function is used whithin exitHandler in the case of duplication of a publication
bool DdcDataService::isAlreadyPublished(std::string serviceName, std::string& publisher, std::string& comp)
{
	string			errMsg;

	DimBrowser		browser;
	char*			server;
	char*			node;
	char*			service;
	char*			format;
	string 			serverString;
	string			nodeString;
	string			serviceString;
	
//	std::cerr << "We are in isAlreadyPublished()" << std::endl;
	browser.getServers();
//	std::cerr << "Servers collected" << std::endl;
	// List of DIM servers found
	while(browser.getNextServer(server, node)) {
		nodeString = node;
		serverString = server;
		browser.getServerServices(serverString.c_str());
		while(browser.getNextServerService(service, format)) {
			serviceString = service;
			if(serviceString == serviceName) {
				publisher = serverString;
				comp = nodeString;
				return(true);
			}
		}
	}	
	return(false);
}


void DdcDataService::publishIsNonStructService(string& serviceName, ISInfoDynAny& value, int position)
{
	OWLTime			whenSet;
	DdcDimDataType	ddcDimType = DDC_DIM_NOTYPE;
	string			errMsg;
	string 			publisher;
	string 			node;
	
	IsService* serviceDef = new IsService;
	serviceDef->name = serviceName;

	beingPublished = serviceName;
	if(!prepareDataBuf(value, position, &(serviceDef->data), value.getAttributeType(0),
					   value.isAttributeArray(0), serviceDef->length, &ddcDimType)) {
		ERS_LOG("_WARNING: " << serviceName << " Cannot be transferred for DCS due to unacceptable format "
					     << value.getAttributeType(0));
		return;
	}
	
	serviceDef->type = ddcDimType;
	__publishSimpleService(serviceDef);
	isServices.push_back(serviceDef);

	return;
}


void DdcDataService::arrangeImport()
{
	string			serviceName;
	vector<string>	toCancel;
	vector<DdcServiceNameItem>::iterator 	item;
	vector<DdcServiceNameItem>&				itemList = dcsImportConfig->getItems();
	
	if(itemList.size() != 0) {
// If this would stay at the end of function, massive publishing in DIM would be provided
		ERS_LOG(applName << "_INFO: Starting DimServer " << applName);
		DimServer::addExitHandler(this);
		DimServer::start(applName.c_str());
		for(item = itemList.begin(); item != itemList.end(); item++) {
			serviceName = item->getServiceName();
			
			// If DAQ data does already exist it should be published also for DCS
			ISInfoDictionary	infoDict(daqPartition);
			ISInfoDynAny		value;
			string				isName;
			isName = item->getIsServer() + '.' + serviceName;
			try {
				infoDict.findValue(isName.c_str(), value);
				ERS_LOG(applName << "_INFO: Publishing " << serviceName);
				beingPublished = serviceName;
				if(isIsRecord(value)) {
					publishIsRecord(serviceName, value);
				}
				else {
					publishIsNonStructService(serviceName, value);
				}
			}
			catch(daq::is::Exception &ex) {}	// There is no yet this service published in IS:
											// Building format of DIM service is postponed
		}
// Moved to begin of function for possible diagnostics, which service has been refused
//		ERS_LOG(applName << "_INFO: Starting DimServer " << applName);
//		DimServer::addExitHandler(this);
//		DimServer::start(applName.c_str());
	}
	return;
}

void DdcDataService::exitHandler(int code)
{
	string		msg;
	string 		publisher;
	string		node;
	
//	std::cerr << "Exit code " << code << std::endl;
	if(code == DIMDNSDUPLC) {
		std::cerr << "Going to isAlreadyPublished()" << std::endl;
//		if(isAlreadyPublished(beingPublished, publisher, node)) {	// Does not work (hangs in browser.getServers()0
//			msg = beingPublished+" could not be published, as it has been already published by "+publisher+" running on "+node;
			msg = beingPublished+" could not be published, as it has been already published by another application";
			ers::error(ddc::AppWarning(ERS_HERE, msg.c_str()));
//		}		
	}
	std::cerr << "Exiting with code " << code << std::endl;
	exit(code);
}


bool DdcDataService::doInitialize()
{
	string		msg;
	
	pvssHost = dcsExportConfig->getPvssHost();
	string dnsSrvHost;	
		
	ERS_LOG(applName << "_INFO: Starting connection to PVSS");
	
	dic_disable_padding();			// Deny any padding in clients (necessary for using getFormat() )
	dis_disable_padding();			// Deny any padding in servers (necessary for packing structures)
	
	// Find/wait DIM DNS server
	bool	dnsFound = false;
	int		srvVersion;
	int 	count = 0;
	int		timeSum = 0;
	int	    timeoutValue = (PVSS_CONN_TIMEOUT+5)*PVSS_CONN_ATTEMPTS;
	char    duration[128];
	
	while(!dnsFound) {
		dnsSrvHost = getenv((const char*)"DIM_DNS_NODE");	// Already known, that it is set
		DimCurrentInfo*	dnsSrv = new DimCurrentInfo("DIS_DNS/VERSION_NUMBER", PVSS_CONN_TIMEOUT, -1);
		srvVersion = dnsSrv->getInt();
		count++;
		if(srvVersion < 0) {
			if(count == PVSS_CONN_ATTEMPTS) {
				timeSum += timeoutValue;
				sprintf(duration, "%d", timeSum/60);
				msg = "DIM Name Server on " + dnsSrvHost + " is not available since " + duration + " min";
				ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
				if(timeSum < timeoutValue*2) {
					msg = "Be sure that DNS server runs or correct DIM_DNS_NODE environment. Still waiting...";
					ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
				}
				else {
					msg = "DIM Name Server does NOT run on "+dnsSrvHost+". Exiting...";
					ers::fatal(ddc::AppFatal(ERS_HERE, msg.c_str()));
					exit(1);
				}
				count = 0;
			}
			sleep(5);
		}
		else {
			dnsFound = true;
			sleep(2);
		}
		delete dnsSrv;
	}
	

// This part is commented: it is time consuming and can be "noisy" on this reason.
// On other hand, while subscription for the PVSS DIM services will appear an error message,
// if it is not published that also allowed avoiding this checking here

// Check/wait PVSS DIM server 
//	bool srvFound = false;
//	count = 0;		
//	string	pvssHostName;
//	string	pvssHostNameAlt;
//	int		i;
//	if(pvssHost.find('.') != string::npos)
//		pvssHostName = pvssHost.substr(0, pvssHost.find('.'));
//	else
//		pvssHostName = pvssHost;
//	pvssHostNameAlt = pvssHostName;
//	for(i=0; i<pvssHostName.size(); i++) {
//		pvssHostName[i] = tolower(pvssHostName[i]);
//		pvssHostNameAlt[i] = toupper(pvssHostNameAlt[i]);
//	}
//	if(srvList.find('|') != string::npos) {
//		srvList = srvList.substr(srvList.find('|')+1);
//		srvFound = ((srvList.find(pvssHostName) != string::npos)
//					|| (srvList.find(pvssHostNameAlt) != string::npos));
//	}
//	
//	if(!srvFound) {
//		msg = "PVSS DIM manager on " + pvssHost + " is not (yet) registered. Waiting...";	
//		ERS_LOG(applName << "_WARNING: " << msg);
//		ers::error(ddc::AppError(ERS_HERE, msg.c_str()));
//		msg = "Be sure that DIM_DNS_NODE is correct on PVSS machine!";	
//		ERS_LOG(applName << "_ERROR: " << msg);
//	}
//
//	while(!srvFound) {		// waiting until PVSS DIM server is running
//		DimCurrentInfo* servers = new DimCurrentInfo("DIS_DNS/SERVER_LIST", PVSS_CONN_TIMEOUT, "NOT_RUNNING");
//		srvList = servers->getString();
//		if(srvList.find('|') != string::npos) {
//			srvList = srvList.substr(srvList.find('|')+1);
//			srvFound = ((srvList.find(pvssHostName) != string::npos)
//						|| (srvList.find(pvssHostNameAlt) != string::npos));
//		}
//		else
//			srvFound = false;
//		delete servers;
//		if(srvFound) {
//			msg = "DIM manager is running now on " + pvssHost;
//			ERS_LOG(applName << "_ERROR: " << msg);
//			ERS_INFO(msg);
//		}
//		else {
//			if(doExit)				// to exit on a signal
//				return(false);
//			sleep(3);
//		}
//	}
//
// End of commented piece of code
	
	 // Initialization of exporting/importing DCS data 
	if((dcsExportConfig->getItemNum()) != 0)
		initExport();
	// Arrangement of importing DAQ data
	if((dcsImportConfig->getItemNum()) != 0) 
		arrangeImport();
	if(dcsExportConfig->getItemNum()
	   + dcsImportConfig->getItemNum()
	   	+ daqRequestConfig->getItemNum() > 0)
		return(true);	
	else {
		ERS_LOG(applName << "_FATAL: No effective configuration defined => Exiting...");
		ers::fatal(ddc::AppFatal(ERS_HERE, "No effective configuration defined => Exiting..."));
		return(false);
	}

	return(true);
}	


void DdcDataService::removeIsData()
{
	ISInfoDictionary 						infoDict(daqPartition);
	vector<DdcServiceNameItem>::iterator 	item;
	vector<DdcServiceNameItem>& 			itemList = dcsExportConfig->getItems();
	
	for(item = itemList.begin(); item != itemList.end(); item++) {
		removeIsEntry(item);
	}
	// Removing DAQ requests
	string	isName, isNameBase;
	isNameBase = daqRequestConfig->findIsServer(DAQ_UNSUBSCRIBE_REQUEST);
	isNameBase += ".";
	isName = isNameBase + DAQ_UNSUBSCRIBE_REQUEST;
	try {infoDict.remove(isName);}
	catch(daq::is::Exception &ex) 
		{ ERS_LOG(applName << "_WARNING: Could not remove " << isName << " " << ex);}
		
	isName = isNameBase + DAQ_SUBSCRIBE_REQUEST;
	try {infoDict.remove(isName);}
	catch(daq::is::Exception &ex) 
		{ ERS_LOG(applName << "_WARNING: Could not remove " << isName << " " << ex);}
		
	isName = isNameBase + DAQ_DATA_REQUEST;
	try {infoDict.remove(isName);}
	catch(daq::is::Exception &ex) 
		{ ERS_LOG(applName << "_WARNING: Could not remove " << isName << " " << ex);}
}


void DdcDataService::removeIsEntry(vector<DdcServiceNameItem>::iterator item)
{
	string									isName;
	ISInfoDictionary infoDict(daqPartition);
	
	isName = item->getIsServer() + '.' + item->getServiceName();
	if(item->getDataType() != R) {		// Simple service
		try {infoDict.remove(isName);}
		catch(daq::is::Exception &ex) 
			{ ERS_LOG(applName << "_ERROR: Could not remove " << isName << " " << ex);}
		catch(...) 
			{ ERS_LOG(applName << "_ERROR: Could not remove " << isName);}
	}
	else {
		const vector<string>& sons = item->getSons();
		unsigned int num = sons.size();
		string sonIsName;
		for(unsigned int i = 0; i < num; i++) {
			sonIsName = isName + '.' + sons[i];
			try {infoDict.remove(sonIsName);}
			catch(daq::is::Exception &ex) {;}	//ignore
			catch(...) { ERS_LOG(applName << "_ERROR: Could not remove (unknown exception) " << sonIsName);}
		}
		char buf[8];
		while(true) {
			snprintf(buf, 7, "%d", num);
			sonIsName = isName + '.' + buf;
			try {infoDict.remove(sonIsName);}
			catch(daq::is::InfoNotFound &ex) { break; }
			catch(daq::is::InvalidName &ex) { ERS_LOG(applName << "_ERROR: Could not remove (invalid name) " << isName); }
			catch(daq::is::RepositoryNotFound &ex) { ERS_LOG(applName << "_ERROR: Could not remove (repository not found) " << isName); }
			catch(...) { ERS_LOG(applName << "_ERROR: Could not remove (unknown exception) " << isName);}
			num++;
		}
	}
}


void DdcDataService::singleStringToChar(string* dataString, size_t & length, DdcDimData* data)
{
	unsigned int 	i;
	char* 			charString;
	const char* 	symbols = dataString->c_str();
	
	length = dataString->size();
	charString = new char[length+1];
	
	for(i=0; i<length; i++)
		charString[i] = *(symbols + i);
	charString[length] = '\0';
	length++;
	
	data->anyValue = charString;
	length = length*sizeof(char);
}


void DdcDataService::manyStringsToChar(std::vector<std::string>* dataString, size_t & length, DdcDimData* data)
{
	unsigned int 	i;
	char* 			charString;
	char* 			symbols;
	unsigned int	strl;
//	size_t			snum = length;  // number of strings - not needed for vector
	std::vector<std::string>::iterator it; 
	
	unsigned int 	curlen = 0;
	length = 0;
	
//	for(j=0; j<snum; j++)
//		length += (*dataString)[j].size() + 1;

	for(it = dataString->begin(); it != dataString->end(); it++)
		length += it->size()+1;
		
	charString = new char[length];
//	for(j=0; j<snum; j++) { 
	for(it = dataString->begin(); it != dataString->end(); it++) {
//		symbols = (char *)(dataString[j].c_str());
		symbols = (char *)(it->c_str());
//		strl = dataString[j].size();
		strl = it->size();
		for(i=0; i<strl; i++)
			charString[curlen+i] = *(symbols + i);
		curlen += strl;
		charString[curlen] = '\0';
		curlen++;
	}
	// Introduced 08.07.14 by SK to work arooung the case of string[0] as a record field
	if(length == 0)
		charString[0] = '\0';
	data->anyValue = charString;
	length = length*sizeof(char);
}

// Substantial revision for DynAny: attribute number introduced. 28.05.15 SK
bool DdcDataService::prepareDataBuf(ISInfoDynAny& value, int pos, DdcDimData* data, ISType::Basic type,
									 bool is_array, size_t & length, 
									 DdcDimDataType* ddcDimType)
{
	std::vector<bool>* vDataBool;
	char*	dataChar;
	unsigned char*	dataUChar;
	std::vector<char>* vDataChar;
	std::vector<unsigned char>* vDataUChar;
	std::vector<short>* vDataShort;
	std::vector<unsigned short>* vDataUShort;
	int*	dataInt;
	unsigned int*	dataUInt;
	std::vector<int>*  vDataInt;
	std::vector<unsigned int>*  vDataUInt;
	float*	dataFloat;
	std::vector<float>*  vDataFloat;
	std::vector<double>*  vDataDouble;
	string*	dataString;
	std::vector<std::string>* vDataString;
	std::vector<OWLTime>* vDataTime;
	OWLTime timeBuf;
	std::vector<int64_t>*  vDataInt64t;
	std::vector<uint64_t>*  vDataUInt64t;
	unsigned int i;
	bool	success = true;
	string  errMsg;
	int intBuf;
	int64_t dataInt64;
	uint64_t dataUInt64;
	
//	ERS_LOG("_DEBUG: prepareDataBuf, pos " << pos);
	switch(type) {
	case ISType::Boolean:
		if(is_array) {
			vDataBool = new std::vector<bool>;
			std::vector<bool>::iterator itBo;
			*vDataBool = value.getAttributeValue<std::vector<bool>>(pos);
			dataInt = new int[vDataBool->size()];
			i = 0;
			for(itBo = vDataBool->begin(); itBo != vDataBool->end(); itBo++) {
				intBuf = *itBo;
				dataInt[i] = intBuf;
				i++;
			}
			length = (vDataBool->size())*sizeof(int);
			delete vDataBool;
			*ddcDimType = IDA;
			data->anyValue = (void *)dataInt;
		}
		else {
			bool dataB;
			int dataI;
			dataB = value.getAttributeValue<bool>(pos);
			dataI = dataB;
			length = sizeof(int);
			*ddcDimType = I;
			data->intValue = dataI;
		}
		break;
	case ISType::S8:
		if(is_array) {
			vDataChar = new std::vector<char>;
			std::vector<char>::iterator itCh;
			*vDataChar = value.getAttributeValue<std::vector<char>>(pos);
			length = (vDataChar->size())*sizeof(char);
			dataChar = new char[vDataChar->size()];
			i = 0;
			for(itCh = vDataChar->begin(); itCh != vDataChar->end(); itCh++) {
				dataChar[i] = *itCh;
				i++;
			}
			delete vDataChar;
			*ddcDimType = CDA;
			data->anyValue = (void *)dataChar;
		}
		else {
			dataChar = new char;
			*dataChar = value.getAttributeValue<char>(pos);
			length = sizeof(char);
			*ddcDimType = C;
			data->anyValue = (void *)dataChar;
		}
		break;
	case ISType::U8:
		if(is_array) {
			vDataUChar = new std::vector<unsigned char>;
			std::vector<unsigned char>::iterator itUCh;
			*vDataUChar = value.getAttributeValue<std::vector<unsigned char>>(pos);
			length = (vDataUChar->size())*sizeof(unsigned char);
			dataUChar = new unsigned char[vDataUChar->size()];
			i = 0;
			for(itUCh = vDataUChar->begin(); itUCh != vDataUChar->end(); itUCh++) {
				dataUChar[i] = *itUCh;
				i++;
			}
			delete vDataUChar;
			*ddcDimType = CDA;
			data->anyValue = (void *)dataUChar;
		}
		else {
			dataUChar = new unsigned char;
			*dataUChar = value.getAttributeValue<unsigned char>(pos);
			length = sizeof(unsigned char);
			*ddcDimType = C;
			data->anyValue = (void *)dataUChar;
		}
		break;
	case ISType::S16:
		if(is_array) {
			vDataShort = new std::vector<short>;
			std::vector<short>::iterator itSh;
			*vDataShort = value.getAttributeValue<std::vector<short>>(pos);
			length = (vDataShort->size()) * sizeof(int);
			dataInt = new int[vDataShort->size()];
			i = 0;
			for(itSh = vDataShort->begin(); itSh != vDataShort->end(); itSh++) {
				dataInt[i] = *itSh;
				i++;
			}
			delete vDataShort;
			*ddcDimType = IDA;
			data->anyValue = (void *)dataInt;
		}
		else {
			short dataSh;
			int   dataI;
			dataSh = value.getAttributeValue<short>(pos);
			dataI = dataSh;
			length = sizeof(int);
			*ddcDimType = I;
			data->intValue = dataI;
		}
		break;
	case ISType::U16:
		if(is_array) {
			vDataUShort = new std::vector<unsigned short>;
			std::vector<unsigned short>::iterator itUSh;
			*vDataUShort = value.getAttributeValue<std::vector<unsigned short>>(pos);
			length = (vDataUShort->size()) * sizeof(unsigned int);
			dataUInt = new unsigned int[vDataUShort->size()];
			i = 0;
			for(itUSh = vDataUShort->begin(); itUSh != vDataUShort->end(); itUSh++) {
				dataUInt[i] = *itUSh;
				i++;
			}
			delete vDataUShort;
			*ddcDimType = IDA;
			data->anyValue = (void *)dataUInt;
		}
		else {
			unsigned short dataUSh;
			unsigned int   dataUI;
			dataUSh = value.getAttributeValue<unsigned short>(pos);
			dataUI = dataUSh;
			length = sizeof(unsigned int);
			*ddcDimType = I;
			data->uintValue = dataUI;  
		}
		break;
	case ISType::S32:
		if(is_array) {
			vDataInt = new std::vector<int>;
			std::vector<int>::iterator itI;
			*vDataInt = value.getAttributeValue<std::vector<int>>(pos);
			length = (vDataInt->size())*sizeof(int);
			dataInt = new int[vDataInt->size()];
			i = 0;
			for(itI = vDataInt->begin(); itI != vDataInt->end(); itI++) {
				dataInt[i] = *itI;
				i++;
			}
			delete vDataInt;
			*ddcDimType = IDA;
			data->anyValue = (void *)dataInt;
		}
		else {
			int dataI;
			dataI = value.getAttributeValue<int>(pos);
			length = sizeof(int);
			*ddcDimType = I;
			data->intValue = dataI;
		}
		break;
	case ISType::U32:
		if(is_array) {
			vDataUInt = new std::vector<unsigned int>;
			std::vector<unsigned int>::iterator itUI;
			*vDataUInt = value.getAttributeValue<std::vector<unsigned int>>(pos);
			length = (vDataUInt->size())*sizeof(unsigned int);
			dataUInt = new unsigned int[vDataUInt->size()];
			i = 0;
			for(itUI = vDataUInt->begin(); itUI != vDataUInt->end(); itUI++) {
				dataUInt[i] = *itUI;
				i++;
			}
			delete vDataUInt;
			*ddcDimType = IDA;
			data->anyValue = (void *)dataUInt;
		}
		else {
			unsigned int dataUI;
			dataUI = value.getAttributeValue<unsigned int>(pos);
			length = sizeof(unsigned int);
			*ddcDimType = I;
			data->uintValue = dataUI;
		}
		break;
	case ISType::Float:
		if(is_array) {
			vDataFloat = new std::vector<float>;
			std::vector<float>::iterator itF;
			*vDataFloat = value.getAttributeValue<std::vector<float>>(pos);
			length = (vDataFloat->size())*sizeof(float);
			dataFloat = new float[vDataFloat->size()];
			i=0;
			for(itF = vDataFloat->begin(); itF != vDataFloat->end(); itF++) {
				dataFloat[i] = *itF;
				i++;
			}
			delete vDataFloat;
			*ddcDimType = FDA;
			data->anyValue = (void *)dataFloat;
		}
		else {
			float dataF;
			dataF = value.getAttributeValue<float>(pos);
			length = sizeof(float);
			*ddcDimType = F;
			data->floatValue = dataF;
		}
		break;
	case ISType::Double:
		if(is_array) {
			vDataDouble = new std::vector<double>;
			std::vector<double>::iterator itD;
			*vDataDouble = value.getAttributeValue<std::vector<double>>(pos);
			dataFloat = new float[vDataDouble->size()];
			i=0;
			for(itD = vDataDouble->begin(); itD != vDataDouble->end(); itD++) {
				dataFloat[i] = *itD;
				i++;
			}
			length = (vDataDouble->size())*sizeof(float);
			delete vDataDouble;
			*ddcDimType = FDA;
			data->anyValue = (void *)dataFloat;
		}
		else {
			double dataD;
			float  dataF;
			dataD = value.getAttributeValue<double>(pos);
			dataF = dataD;
			length = sizeof(float);
			*ddcDimType = F;
			data->floatValue = dataF;
		}
		break;
	case ISType::String:
		if(is_array) {
			vDataString = new std::vector<std::string>;
			*vDataString = value.getAttributeValue<std::vector<std::string>>(pos);
			*ddcDimType = SDA;
			length = vDataString->size();
			manyStringsToChar(vDataString, length, data);
			delete vDataString;
		}
		else {
			dataString = new string;
			*dataString = value.getAttributeValue<std::string>(pos);
			*ddcDimType = CDA;
			singleStringToChar(dataString, length, data);
			delete dataString;
		}
		break;
	case ISType::Time:
		if(is_array) {
			vDataTime = new std::vector<OWLTime>;
			std::vector<OWLTime>::iterator itT;
			*vDataTime = value.getAttributeValue<std::vector<OWLTime>>(pos);
			dataInt = new int[vDataTime->size()];
			i=0;
			for(itT = vDataTime->begin(); itT != vDataTime->end(); itT++) {
				dataInt[i] = itT->c_time();
				i++;
			}
			length = (vDataTime->size())*sizeof(int);
			delete vDataTime;
			*ddcDimType = IDA;
			data->anyValue = (void *)dataInt;
		}
		else {
			timeBuf = value.getAttributeValue<OWLTime>(pos);
			dataInt = new int;
			*dataInt = timeBuf.c_time();
			length = sizeof(int);
			*ddcDimType = I;
			data->intValue = *dataInt;
			delete dataInt;
		}
		break;
	case ISType::S64:
		if(is_array) {
			vDataInt64t = new std::vector<int64_t>;
			std::vector<int64_t>::iterator it64;
			*vDataInt64t = value.getAttributeValue<std::vector<int64_t>>(pos);
			dataInt = new int[2*(vDataInt64t->size())];
			i=0;
			for(it64=vDataInt64t->begin(); it64!=vDataInt64t->end(); it64++) {
				dataInt64 = *it64;
				dataInt[i] = (int)(dataInt64 - (dataInt64/(MAXINT32+1))*(MAXINT32+1));	// Calculated lower half
				i++;
				dataInt[i] = (int)(dataInt64/(MAXINT32+1));			// Calculated upper half
				i++;
			}
			length = 2*(vDataInt64t->size())*sizeof(int);
			delete vDataInt64t;
			data->anyValue = (void *)dataInt;
			*ddcDimType = IDA;
		}
		else {
			dataInt64 = value.getAttributeValue<int64_t>(pos);
			length = 2*sizeof(int);
			dataInt = new int[2];
			dataInt[0] = (int)(dataInt64 - (dataInt64/(MAXINT32+1))*(MAXINT32+1));	
			dataInt[1] = (int)(dataInt64/(MAXINT32+1));	
			data->anyValue = dataInt;
			*ddcDimType = IDA;
		}
		break;
	case ISType::U64:
		if(is_array) {
			vDataUInt64t = new std::vector<uint64_t>;
			std::vector<uint64_t>::iterator itu64;
			*vDataUInt64t = value.getAttributeValue<std::vector<uint64_t>>(pos);
			dataUInt = new unsigned int[2*(vDataUInt64t->size())];
			i=0;
			for(itu64=vDataUInt64t->begin(); itu64!=vDataUInt64t->end(); itu64++) {
				dataUInt64 = *itu64;
				dataUInt[i] = (int)(dataUInt64 - (dataUInt64/(MAXINT32+1))*(MAXINT32+1));	// Calculated lower half
				i++;
				dataUInt[i] = (int)(dataUInt64/(MAXINT32+1));			// Calculated upper half
				i++;
			}
			length = 2*(vDataUInt64t->size())*sizeof(unsigned int);
			delete vDataUInt64t;
			data->anyValue = (void *)dataUInt;
			*ddcDimType = IDA;
		}
		else {
			uint64_t dataUInt64 = value.getAttributeValue<uint64_t>(pos);
			length = 2*sizeof(unsigned int);
			dataUInt = new unsigned int[2];
			dataUInt[0] = (unsigned int)(dataUInt64 - (dataUInt64/(MAXINT32+1))*(MAXINT32+1));	
			dataUInt[1] = (unsigned int)(dataUInt64/(MAXINT32+1));	
			data->anyValue = dataUInt;
			*ddcDimType = IDA;
		}
		break;
	default:	// What comes here?
//		ERS_LOG(applName << "_ERROR: In position " << pos << " unknown data type"); 
		success = false;
	}
	return(success);
}


bool DdcDataService::isIsRecord(ISInfoDynAny& value)
{
	size_t			attrCount;
	
	if((attrCount = value.getAttributesNumber()) >= 2)
		return(true);
	else
 		return(false);
}

void DdcDataService::buildSourceServerList()
{
	vector<DdcServiceNameItem>::iterator	reqPtr;
	string									serverName;
	vector<DdcServiceNameItem>&				itemList = dcsImportConfig->getItems();
	
	for(reqPtr = itemList.begin(); reqPtr != itemList.end(); reqPtr++) {
		serverName = reqPtr->getIsServer();
		if(find(sourceIsServers.begin(), sourceIsServers.end(), serverName)
													== sourceIsServers.end()) {
			sourceIsServers.push_back(serverName);
			ERS_LOG(applName << "_INFO: DAQ data server is " << serverName);
		}
	}
	
	vector<DdcServiceNameItem>&	itemList1 = daqRequestConfig->getItems();
	for(reqPtr = itemList1.begin(); reqPtr != itemList1.end(); reqPtr++) {
		serverName = reqPtr->getIsServer();
		if(find(sourceIsServers.begin(), sourceIsServers.end(), serverName)
													== sourceIsServers.end()) {
			sourceIsServers.push_back(serverName);
			ERS_LOG(applName << "_INFO: DAQ request server is " << serverName);
		}
	}
}


void DdcDataService::startDataReceivers()
{
	unsigned int i;
	unsigned int n = sourceIsServers.size();
	DdcDtDimReceiver*	receiver;
	
	for(i=0; i<n; i++) {
		receiver = new DdcDtDimReceiver(sourceIsServers[i], daqPartition, this);
		dataReceivers.push_back(receiver);
		receiver->doSubscriptions(dcsImportConfig, daqRequestConfig);
	}
}


void DdcDataService::stopImport()
{
	unsigned int i;
	unsigned int n = dataReceivers.size();
	
	for(i=0; i<n; i++) {
		((DdcDtDimReceiver*)(dataReceivers[i]))->stopImport();
		delete (DdcDtDimReceiver*)(dataReceivers[i]);
	}
}


void DdcDataService::run()
{
	for(;;)
	{
		while(tdaqDataQueue.size() > 0) {
			treatIsDataObject();
		}
		
		if (doExit) {
			ERS_LOG(applName << "_WARNING: Exit command received");
			removeIsData();
			return;
		}
		
		usleep(10000);
	}
}

void DdcDataService::treatIsDataObject()
{
	std::vector<TdaqDataEntry*>::iterator it = tdaqDataQueue.begin();
	
	IsService*  isService;
	std::string obName = (*it)->name;
	ISInfoDynAny* info = (*it)->isObject;
	if(!isIsRecord(*info)) {
		if(!isServicePublished(obName, &isService)) {
//			ERS_LOG("_INFO: " << obName << " is a NOT yet published service: publishing it");
			publishIsNonStructService(obName, *info);
		}
		else {
			updateIsNonStructService(isService, *info);
		}
	}
	else {
		if(!isServicePublished(obName, &isService)) {
//			ERS_LOG("_INFO: " << obName << " is NOT yet published record: publishing it");
			publishIsRecord(obName, *info);
		}
		else {
			updateIsRecord(isService, *info);
		}
	}
	removeTreatedIsService();
}


void DdcDataService::putIsService(std::string serviceName, ISInfoDynAny& value)
{
	ISInfoDynAny* info = new ISInfoDynAny(value);
	TdaqDataEntry* dataEntry = new TdaqDataEntry;
	
	dataEntry->name = serviceName;
	dataEntry->isObject = info;
	
	// control with mutex here
	tdaqQueueMutex.lock();
	tdaqDataQueue.push_back(dataEntry);
//	ERS_LOG("_DEBUG: " << serviceName << " put to the queue");
	tdaqQueueMutex.unlock();
}


// Function removes from the queue always the first element of the queue
// of IS Services to be updated in DIM
void DdcDataService::removeTreatedIsService()
{
	// control with mutex here
	tdaqQueueMutex.lock();
	if(tdaqDataQueue.size() > 0) {		// Insurance, probably redundant
		delete((tdaqDataQueue[0])->isObject);
		delete(*(tdaqDataQueue.begin()));
		tdaqDataQueue.erase(tdaqDataQueue.begin());		
//		ERS_LOG("_DEBUG: Service removed from the queue");
	}
	tdaqQueueMutex.unlock();	
}


bool DdcDataService::doConfig(Configuration& confDb)
{
	
	dcsExportConfig  = new DdcDtDimConfig(confDb, applName, FROM_DCS);
	dcsImportConfig  = new DdcDtDimConfig(confDb, applName, TO_DCS);
	daqRequestConfig = new DdcDtDimConfig(confDb, applName, DAQ_REQUESTS);

	if(dcsExportConfig->getItemNum() 
		+ dcsImportConfig->getItemNum()
			+ daqRequestConfig->getItemNum() != 0) 
		return(true);
	else
		return(false);
}


void DdcDataService::stopServices()
{
	unsigned int 	i;

	for(i=0; i<isServices.size(); i++) 
		stopDdcDimService(isServices[i]);
}


void DdcDataService::stopDdcDimService(IsService* isService)
{
	if(isService->length) {
		delete isService->dimService;
		releaseDataBuf(isService);
	}
}
	
void DdcDataService::releaseDataBuf(IsService* isService)
{
	switch(isService->type) {
	case I:
	case F:
		break;
	case C:
		delete (char *)(isService->data.anyValue);
		break;
	case IDA:
		delete[] (int *)(isService->data.anyValue);
		break;
	case FDA:
		delete[] (float *)(isService->data.anyValue);
		break;
	default: 
		delete[] (char *)(isService->data.anyValue);
	}
}


DdcDataService::~DdcDataService()
{
	stopServices();
	
	delete dcsExportConfig;
	delete dcsImportConfig;
	delete daqRequestConfig;
}


void handleSignal(int)
{
//	time_t now = time(&now);
	ERS_LOG("Killing signal received by DDC_DT_DIM");
	DdcDataService::doExit = true;
}


int main(int argc, char *argv[])
{
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);
    CmdArgStr	appl_name('n', "application", "appl-name", "name of application.", CmdArg::isREQ);


//
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, &appl_name, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
	cmdLine.parse(arg_iter);

	signal(SIGINT, handleSignal);
	signal(SIGTERM, handleSignal);
	signal(SIGKILL, handleSignal);
	
	IPCPartition partition((const char*)partition_name);
	
	// Initial reference for configuration
	bool conf_error = false;

	string	dimDnsNode;
	if(getenv((const char*)"DIM_DNS_NODE") == 0) 
		conf_error = true;
	else {
		dimDnsNode = getenv((const char*)"DIM_DNS_NODE");
		if(dimDnsNode.size() == 0) 
			conf_error = true;
	}

	if(conf_error) {
		ERS_LOG(appl_name << "_FATAL: DIM Name Server node (DIM_DNS_NODE environment) is not defined");
		return(1);
	}
	
	// Initialization of CORBA broker omniORB4	
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
		return(1);
    }
    
	::Configuration * confDb;
	try {
		confDb = new Configuration("");
	}
	catch(daq::config::Exception &ex) {
		ERS_LOG(appl_name << "_ERROR: Exception while initialization DB: " << ex);
		return(1);
	}
 
	// Install Environment converter
	std::string pName((const char*)partition_name);
	const daq::core::Partition * confPartition = daq::core::get_partition(*confDb, pName);
    if(!confPartition) {
		ERS_LOG(appl_name << "_ERROR: Failed to find " << pName <<" partition in configuration database");
		delete confDb;
		return 1;
	}
	// Replaced 25.02.19 as deprecated
    //confDb->register_converter(new daq::core::SubstituteVariables(*confDb, *confPartition));
    confDb->register_converter(new daq::core::SubstituteVariables(*confPartition));
	
	const string apName((const char*)appl_name);
	ERS_LOG(apName << "_INFO: Creating DDC-DIM object for DCS<=>DAQ data transfer");
	DdcDataService* ddcDtDimService = new DdcDataService(partition, apName);
	
	if(ddcDtDimService->doConfig(*confDb)) 	{	// Configuring data transfer
		ERS_LOG("Config made");
		pmg_initSync();
		sleep(1);
		if(ddcDtDimService->doInitialize()) {
			time_t 	justnow = time(&justnow);
			ERS_INFO(apName << "_INFO: DCS is connected");
			
			if(ddcDtDimService->importConfigured()) {				
				ddcDtDimService->buildSourceServerList();
				ddcDtDimService->startDataReceivers();
			}
			ddcDtDimService->run();
		}
		else 
			return 1;
		
		if(ddcDtDimService->importConfigured()) {
			ddcDtDimService->stopImport();
		}
	}	
	else {
		ERS_LOG(apName << "_FATAL: No data exchange configured properly => Exiting");
		ers::fatal(ddc::AppFatal(ERS_HERE, "No data exchange configured properly => Exiting"));
	}
	
	ERS_LOG(apName << "_WARNING: Exiting ddc_dt_dim data exchanger");
	delete ddcDtDimService;

	return 0;
}
