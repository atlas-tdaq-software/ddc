//
// This file contains definition of DdcCtService class methods. 
// An object of this class  
// is responsible for communication of DDC-CT sub-system with a certain
// PVSS system to transmit commands of a DAQ partition (with feedback) 
//							S. Khomoutnikov 
//							30.03.05
// See DdcCtDim.hxx to follow the history
//
//////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
#include <time.h>
#include <vector>
#include <signal.h>

using namespace std;

#include <ipc/core.h>
#include <ipc/alarm.h>
#include <ipc/partition.h>

#include <owl/semaphore.h>

#include <dal/Partition.h>
#include <dal/Variable.h>
#include <dal/util.h>

#include <is/namedinfo.h>
#include <is/criteria.h>
#include <is/infoiterator.h>
#include <cmdl/cmdargs.h>
//#include <mrs/message.h>

#include <ers/ers.h>

#include <RunControl/Common/UserRoutines.h>
#include <RunControl/Controller/Controller.h>

#include <config/ConfigObject.h>
#include <config/Configuration.h>

//#include "../DdcData.hxx"
//#include "../DdcCommand.hxx"
#include "../DdcCommander.hxx"
#include "../DdcBenchmark.hxx"
#include "DdcCtDimConfig.hxx"
#include "DdcCtDimController.hxx"
#include "DdcCtDim.hxx"

bool DdcCtService::doExit = false;

//ERS_DECLARE_ISSUE( ddc, DCS_NotReady, "!! " << name << ": DCS is NOT READY for data taking", ((std::string)name ) )
ERS_DECLARE_ISSUE( ddc, AppWarning, message, ((const char*)message) )
ERS_DECLARE_ISSUE( ddc, AppError, message, ((const char*)message) )
ERS_DECLARE_ISSUE( ddc, AppFatal, message, ((const char*)message) )


void DdcRpcInfo::rpcInfoHandler()
{
	std::string	cmdName;
	time_t	now = time(&now);
	
	cmdName = getName();
	int cmdResult = getInt();
//	ERS_LOG("DIM RPC Callback: Command " << cmdName << " result = " << cmdResult);
	ddcct->acceptCommandResult(cmdName, transitionCommand, cmdResult);
	return;
}

	
void DdcCtService::infoHandler()
{	
	string			svcName;
	int				dcsReadyStatus;
	string			msg;
	time_t 			updateTime;

	DimStampedInfo* info = (DimStampedInfo *)getInfo();
	svcName = info->getName();

	if(svcName == alarmDpService) {		// only for 'Not_Ready_for_DAQ'
										// Since 2010 this is the TTCPartitionState flag of DCS 
										// of a TTC partition that can have the values:
										// 1 - READY, 2 - STANDBY, 3 - everything else | 0 - not defined or no connection
		updateTime = (time_t)(info->getTimestamp());
		dcsReadyStatus = info->getInt();
		updatePartitionState(dcsReadyStatus, updateTime);
		if(dcsReadyStatus == ALRM_CONNECT_UNDEF) {
			if(flagAlarmConnected != ALRM_CONNECT_ERROR) {		// i.e. it was connected earlier
				msg = svcName + " (TTCPartitionState) is not available any more!";
				ers::error(ddc::AppError(ERS_HERE, msg.c_str()));
				flagAlarmConnected = ALRM_CONNECT_ERROR;
			}
			else {
				msg = svcName + " (TTCPartitionState) is unavailable!";
				ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
			}
		}
		else {
			if(flagAlarmConnected == ALRM_CONNECT_ERROR) {
				msg = "Since now, " + svcName + " (TTCPartitionState) is available";
				ERS_INFO(svcName << " (TTCPartitionState) is available");
			}
			flagAlarmConnected = ALRM_CONNECT_OK;
			prevTTCPartitionState = TTCPartitionState;
			TTCPartitionState = dcsReadyStatus;
			if(TTCPartitionState != prevTTCPartitionState) {
				// Here there were messages for DAQ, which are dropped
				// 12.05.10, as there is a separate mechanism for those
				// messages in fwAtlasDDC DU
				// Probably, one can drop out also the 'prevTTCPartitionState'
				// member variable...
			}
		}
	}
}


void DdcCtService::updatePartitionState(int state, time_t updateTime)
{
	string	isDataName;
	
	if(partitionStatePtr == 0) {
		isDataName = TTCPARTITION_STATE_SERVER;
		isDataName += "."+controllerName+".partitionState";
		partitionStatePtr = new ddc::DdcIntInfoNamed(daqPartition, isDataName.c_str());
	}
	
	ERS_LOG("Sending to IS the partition state " << isDataName << " = " << state);
	partitionStatePtr->value = state;
	partitionStatePtr->ts = updateTime;
	try {
		partitionStatePtr->checkin();
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG(" _ERROR: " << isDataName
		 		  << ": sending to IS fault. Be sure that " << TTCPARTITION_STATE_SERVER
				  << " IS server is running ");
		ERS_LOG(ex);
	}
	catch(std::exception &ex) {
		ERS_LOG("std::exception while put to IS " << isDataName);
	}
	catch(...) {
		ERS_LOG("Unknown exception while put to IS " << isDataName);
	}
	return; 
}


int DdcCtService::buildDdcCtConfig(Configuration& confDb)
{
	int success = COMM_ERR_OK;
	
	// Build the RC transition command configuration
	string	tmpName = controllerName;
	commandConfig = new DdcCtDimConfig(confDb, tmpName);
	
	if(tmpName == "") {
		ERS_LOG(" _FATAL: Invalid configuration! Exiting ...");
		success = COMM_ERR_CONFIG;
	}
	else {
		if(commandConfig->emptyList()) {
			ERS_LOG(" _WARNING: None run control transition command is configured!");
		}
	}
	
	return(success);
}


int DdcCtService::launchCommand(string rcCommand)
{
	string		errMsg;
	time_t 		now = time(&now);

	DimBrowser		browser;
	char*			server;
	char*			node;
	string 			msg;
	string			nodeString;
	string			serverString;
	string			pvssHostName;
	string			pvssHostNameAlt;
	char 			stateNum[8];	
	unsigned int	i;
	bool 			parametersSent = true;
	
		
	ERS_LOG(controllerName << "_INFO: " << rcCommand << " is starting");
	
	commError = COMM_ERR_UNDEF;
	if(rcCommand == CMD_CONFIG) {
		// Loading configuration DB
		::Configuration * confDb;
		try {
			confDb = new Configuration("");
		}
		catch(daq::config::Exception &ex) {
			ERS_LOG(controllerName << "_ERROR: Exception while initialization DB: " << ex);
			return(COMM_ERR_CONFIG);
		}
		ERS_LOG(controllerName << "_INFO: building config");
		commError = buildDdcCtConfig(*confDb);
//		commandConfig->print_config();
		delete confDb;
		ERS_LOG(controllerName << "_INFO: Configuring done with code :" << commError);
		if(commError != COMM_ERR_OK) {
			return(commError);
		}
		else {
	 		pvssHost = commandConfig->getPvssHost();
			if(pvssHost.find('.') != string::npos)
				pvssHostName = pvssHost.substr(0, pvssHost.find('.'));
			else
				pvssHostName = pvssHost;
			pvssHostNameAlt = pvssHostName;
			for(i=0; i<pvssHostName.size(); i++) {
				pvssHostName[i] = tolower(pvssHostName[i]);
				pvssHostNameAlt[i] = toupper(pvssHostNameAlt[i]);
			}		
			
			browser.getServers();
			// List of DIM servers found
//			ERS_LOG(controllerName << "_INFO: List of servers registered in DIM Name Server:");
			while(browser.getNextServer(server, node)) {
				nodeString = node;
				serverString = server;
//				ERS_LOG(controllerName << "_INFO: DIM server " << serverString << " at " << nodeString);
				if(nodeString.find('.') != string::npos)
					nodeString = nodeString.substr(0, nodeString.find('.'));
				if(((nodeString == pvssHostName) || (nodeString == pvssHostNameAlt)) 
				&& (serverString != "DIS_DNS"))  {
					if(!ntcAllowedOnBoot) {
						commandAllowed.post();
//						ERS_LOG(controllerName << "_INFO: Transition command allowed");
					}
					dcsRuns = true;
					break;
				}
			}
			if(!dcsRuns) {
				msg = "DIM manager on " + pvssHost + " is not found";
				ers::error(ddc::AppError(ERS_HERE, msg.c_str()));
				ERS_LOG("------ Check also node of DIM Name Server (DIM_DNS_NODE) and Server itself");
				return(COMM_ERR_CONNECT);
			}
		}
	}

/*	The transition does not exist any more since Feb.2014 (tdaq-05-03-00)
	if(rcCommand == CMD_FINAL) {
		if(commandConfig != 0) {
			ERS_LOG(controllerName << "_INFO: Deleting command configuration");
			delete commandConfig;
			commandConfig = 0;
		}
		if(alarmInfo) {
			ERS_LOG(controllerName << "_INFO: Deleting alarmInfo");
			delete alarmInfo;
			alarmInfo = 0;
		}
		if(partitionStatePtr) {
			try {
				ERS_LOG("Trying to remove " << TTCPARTITION_STATE_SERVER
						<< "." << controllerName << ".partitionState");
				ISInfoDictionary infoDict(daqPartition);
				infoDict.remove(TTCPARTITION_STATE_SERVER+"."+controllerName+".partitionState");
				ERS_LOG("..Done");
			}
			catch(daq::is::Exception &ex) {
				ERS_LOG(" _ERROR: TTC Partition State " << TTCPARTITION_STATE_SERVER
						<< "." << controllerName << ".partitionState could not be removed from IS");
				ERS_LOG(ex);
			}
			delete partitionStatePtr;
			partitionStatePtr = 0;
		}
//		runController->stop();
		return(COMM_ERR_OK);	
	}
*/
	
	ERS_LOG(controllerName << "_INFO: Looking for Command Definition");
	vector<DdcCtDimCommandDef>::iterator cDef = commandConfig->findCommDef(rcCommand);
	
	if(cDef == (commandConfig->getCommList()).end())	{ //this RC command does not require DDC
		ERS_LOG(controllerName << "_INFO: Dummy " << rcCommand << " is being executed");
		commError = COMM_ERR_OK;
	}
	else {
		ERS_LOG(controllerName << "_INFO: Command Definition for " << rcCommand << " found");	
		if(!dcsRuns) {
			errMsg = "DCS is unavailable while " + rcCommand + " transition";
			ers::error(ddc::AppError(ERS_HERE, errMsg.c_str()));
			ERS_LOG(controllerName << "_ERROR: DIM manager is not running on " << pvssHost);
			return(COMM_ERR_CONNECT);
		}
		
		// Now start the command execution
		string	rpcServiceName = cDef->getService();
		if(commandAllowed.try_wait() != 0) {
			errMsg = rpcServiceName + ": Waiting until DCS is unlocked";
			ers::warning(ddc::AppWarning(ERS_HERE, errMsg.c_str()));
			commandAllowed.wait();				// wait/take semaphore
			errMsg = rpcServiceName + ": Starting execution";
			ers::warning(ddc::AppWarning(ERS_HERE, errMsg.c_str()));
		}
		rcCommandActive = true;
		
		// Set parameter values
		bool	paramConfigErr = false;
		string	allPars = cDef->getParString();
		string	dimCommandName = rpcServiceName + ".parameters";
		string	dimFsmCommandName = rpcServiceName + ".fsmParameters";
		string	userPars;
		string	fsmPars;

		unsigned int		pos1 = allPars.find('|');
		unsigned int		pos2;
		if(pos1 == string::npos)
			paramConfigErr = true;
		else {
			userPars = allPars.substr(pos1+1);
			pos2 = userPars.find('|');
			if(pos2 == string::npos) {		// No parameters of procedure 
				fsmPars = allPars + "|";	// "FSM_NODE|FSM_COMMAND|
				userPars = "";
				ERS_LOG(controllerName << "_INFO: No parameters of transition procedure");
			}
			else { 
				fsmPars = allPars.substr(0, pos1+pos2+2);		// "FSM_NODE|FSM_COMMAND|
				if(fsmPars.size() < allPars.size()) {
					userPars = userPars.substr(pos2+1);
					ERS_LOG(controllerName << "_INFO: Parameters of transition: "+userPars);
				}
				else {
					userPars = "";
					ERS_LOG(controllerName << "_INFO: No parameters of transition procedure");
				}
			}
		}
		if(paramConfigErr) {
			errMsg = "Parameters of " + rcCommand + " transition are invalid";
			ers::error(ddc::AppError(ERS_HERE, errMsg.c_str()));
			commError = COMM_ERR_CONFIG;
		}
		else {
			int ddcTimeout = cDef->getTimeout();
			char cTimeout[32];
			sprintf(cTimeout, "%d", ddcTimeout); 
			string sTimeout(cTimeout);
			fsmPars += sTimeout;					// timeout as a string for DCS FSM
	
			if(ddcTimeout == 0)
				ddcTimeout = DEFAULT_TIMEOUT;
			if(!DimClient::sendCommand((char *)dimFsmCommandName.c_str(), (char *)fsmPars.c_str())) {
				errMsg = "DCS is unavailable for " + rcCommand + " transition FSM parameters";
				ers::error(ddc::AppError(ERS_HERE, errMsg.c_str()));
				commError = COMM_ERR_CONNECT;
			}
			else {
				if(userPars != "") {
					ERS_LOG(controllerName << "_INFO: Sending parameters of " << (char *)dimCommandName.c_str() 
							  << ": " << (char *)userPars.c_str());
					parametersSent = DimClient::sendCommand((char *)dimCommandName.c_str(), (char *)userPars.c_str());
				}
				if(parametersSent) {
					rpcMutex.lock();
					rpcInfo = findCommand(rpcServiceName);
					if(!rpcInfo) {
						ERS_LOG(controllerName << "_INFO: New ddcRpcInfo " << rpcServiceName 
								  << " Timeout = " << ddcTimeout+3);
						rpcInfo = new DdcRpcInfo(rpcServiceName, ddcTimeout+3, COMM_ERR_TIMEOUT,
											     true, this);
						now = time(&now);
						ERS_LOG(controllerName << "_INFO: ..Created");
//						sleep(1);
						commands.push_back(rpcInfo);
					}
					rpcMutex.unlock();
					// Set trigger (activating RPC DIM service)
					int	triggerFlag = 1;
					ERS_LOG(controllerName << "_INFO: Setting command trigger");
					commError = COMM_ERR_UNDEF;
					now = time(&now);
					rpcInfo->setData(triggerFlag);
					rpcInfo->setActive();
//					ERS_LOG(controllerName << "_INFO: Waiting command result");
					while(commError == COMM_ERR_UNDEF) {	// waiting DCS response
						usleep(10000);
					}
				}
				else {
					errMsg = "DCS or DPE is unavailable for " + rcCommand + " transition parameters";
					ers::error(ddc::AppError(ERS_HERE, errMsg.c_str()));
					commError = COMM_ERR_CONNECT;
				}
			}
		}
	
		rcCommandActive = false;
		commandAllowed.post();			// free semaphor
	}
	now = time(&now);
	if(commError == COMM_ERR_OK) {
		if(rcCommand == CMD_CONFIG) {
			if((alarmDpService = commandConfig->getAlarmDp()) != "") {		// Observing TTC partition state
				// Get current TTC partition state and subscribe for it
				
				flagAlarmConnected = ALRM_CONNECT_UNDEF;
				alarmInfo = new DimStampedInfo((char *)alarmDpService.c_str(),
												ALRM_CONNECT_UNDEF, this);
				
				while(flagAlarmConnected == ALRM_CONNECT_UNDEF) 	// waiting DCS response
					usleep(100000);
				
				if(flagAlarmConnected == ALRM_CONNECT_ERROR) {
					msg = alarmDpService + " TTC Partition State is likely not published in PVSS => will not be reported";
					ers::warning(ddc::AppError(ERS_HERE, msg.c_str()));
				}
			}
			else {
				msg = "TTC Partition State is not defined in configuration => will not be reported";
				ers::warning(ddc::AppError(ERS_HERE, msg.c_str()));
			}
			controllerState = DDC_CONFIGURED;
		}
		if(rcCommand == CMD_PREPARERUN)
			controllerState = DDC_PREPARED;
		else
			controllerState = DDC_RUNNING;
		
// This message is dropped 15.07.10, as any change of the partition state is reported by DDC DU via ddc_mt
//			if(TTCPartitionState != TTCPARTITION_STATE_READY) {
//				snprintf(stateNum, 3, "%d", TTCPartitionState);
//				msg = rcCommand + " executed, but TTC partition state code is ";
//				msg = msg + stateNum;
//				msg = msg + " "; 
//				msg = msg + ((TTCPartitionState == TTCPARTITION_STATE_STANDBY) ? " (STANDBY)" : "");
//				ers::warning(ddc::AppWarning(ERS_HERE, msg.c_str()));
//			}
		
		ERS_LOG(" _INFO: " << rcCommand << " is executed successfully");
	}
	else {
		decodeError(commError, msg);
		msg = rcCommand + " failed: " + msg;
		ers::error(ddc::AppError(ERS_HERE, msg.c_str()));
	}
	
	if(rcCommand == CMD_UNCONFIG) {
		if(commandConfig != 0) {
			delete commandConfig;
			commandConfig = 0;
		}
		if(alarmInfo) {
			delete alarmInfo;
			alarmInfo = 0;
		}
		if(partitionStatePtr) {
			try {
				ERS_LOG("Trying to remove " << TTCPARTITION_STATE_SERVER
						<< "." << controllerName << ".partitionState");
				ISInfoDictionary infoDict(daqPartition);
				infoDict.remove(TTCPARTITION_STATE_SERVER+"."+controllerName+".partitionState");
				ERS_LOG("..Done");
			}
			catch(daq::is::Exception &ex) {
				ERS_LOG(" _ERROR: TTC Partition State " << TTCPARTITION_STATE_SERVER
						<< "." << controllerName << ".partitionState could not be removed from IS");
				ERS_LOG(ex);
			}
			catch(std::exception &ex) {
				ERS_LOG("std::exception while removing " << TTCPARTITION_STATE_SERVER<< "." << controllerName << ".partitionState");
			}
			catch(...) {
				ERS_LOG("Unknown exception while removing " << TTCPARTITION_STATE_SERVER<< "." << controllerName << ".partitionState");
			}
			delete partitionStatePtr;
			partitionStatePtr = 0;
		}
		controllerState = DDC_INITIAL;
		
		if(!ntcAllowedOnBoot)
			commandAllowed.try_wait();			// No any command until new CMD_CONFIG
	}
	
	return(commError);
}

void DdcCtService::executeNtCommand(ddc::DdcNtCommandInfoNamed& ntCommand)
{
	string 		cName = ntCommand.commandName;
	time_t 		now = time(&now);
	string 		errMsg;
	DdcRpcInfo*		ntRpcInfo;
	
	ERS_LOG(controllerName << "_INFO: Start execution of NT-command " << cName);

	// Set parameter values
	string	allPars = ntCommand.parameters;
	string	dimCommandName = cName + ".parameters";
	if(forBM) {
		DdcBenchmark::getControllerTimer().stop();
		DdcBenchmark::getDimTimer().start();
	}
	if(DimClient::sendCommand((char *)dimCommandName.c_str(), (char *)allPars.c_str())) {
//		ERS_LOG(controllerName << "_INFO: Sent parameters: " << dimCommandName);
		if(forBM) {
			DdcBenchmark::getDimTimer().stop();
			DdcBenchmark::getControllerTimer().start();
			
			DdcBenchmark::nextCommand(); // increase command counter
		}
		// Set timeout
		int ddcTimeout = ntCommand.timeout;
		if(!ddcTimeout)
			ddcTimeout = DEFAULT_TIMEOUT;
			
		// Set trigger (activating RPC DIM service)
		commandMutex.lock();
		ntCmdError[cName] = COMM_ERR_UNDEF;
		commandMutex.unlock();
		rpcMutex.lock();
		ntRpcInfo = findCommand(cName);
		if(!ntRpcInfo) {
			ERS_LOG(controllerName << "_WARNING: Creating DdcRpcInfo for " << cName << ", Timeout = " << ddcTimeout);
			if(forBM) {
				DdcBenchmark::getControllerTimer().stop();
				DdcBenchmark::getDimTimer().start();
			}
			ntRpcInfo = new DdcRpcInfo(cName, ddcTimeout, COMM_ERR_CONNECT, false, this);
			if(forBM) {
				DdcBenchmark::getDimTimer().stop();
				DdcBenchmark::getControllerTimer().start();
			}
			commands.push_back(ntRpcInfo);
		}
		rpcMutex.unlock();
		
//		ERS_LOG(controllerName << "_INFO: RPC trigger DP for " << cName << " is " << ntRpcInfo->getName());
		int	triggerFlag = 1;
		ntRpcInfo->setActive();
		if(forBM) {
			DdcBenchmark::getControllerTimer().stop();
			DdcBenchmark::getDimTimer().start();
		}
		
//		ERS_LOG(" _INFO: Setting trigger for " << cName << "(" << ntRpcInfo->getName() << ")");
		ntRpcInfo->setData(triggerFlag);
		ERS_LOG(controllerName << "_INFO: Trigger set for " << cName);
		
		// No any waiting here any more! Only in the API functions.
//		while(ntCmdError[cName] == COMM_ERR_UNDEF) {	// waiting DCS response // This commented to avoid
//			usleep(1000);														// long IS callback time
//		}																		// Since this time (27.08.08) benchmarking
																				// does not work properly
		if(forBM) {
			DdcBenchmark::getDimTimer().stop();
			DdcBenchmark::getControllerTimer().start();
		}
		

	}
	else {
		if(forBM) {
			DdcBenchmark::getDimTimer().stop();
			DdcBenchmark::getControllerTimer().start();
		}
		ERS_LOG(controllerName << "_ERROR: Could not set parameter for " << cName
				  << " non-transition command");
		errMsg = cName + ".parameters: DCS or DPE is unavailable";
		
		ers::error(ddc::AppError(ERS_HERE, errMsg.c_str()));
		commandMutex.lock();
		ntCmdError[cName] = COMM_ERR_CONNECT;
		commandMutex.unlock();
		setNtCommandResult(cName, COMM_ERR_CONNECT);
	}
}


DdcRpcInfo* DdcCtService::findCommand(std::string name)
{
	vector<DdcRpcInfo*>::iterator	curcmd;
	
	for(curcmd = commands.begin(); curcmd != commands.end(); curcmd++)
		if((*curcmd)->getName() == name) 
			return(*curcmd);
	return(0);
}


void DdcCtService::acceptCommandResult(std::string cmd, bool transition, int result)
{
//	ERS_LOG(controllerName << "_INFO: " << cmd << " Command result came: " << result);
	
	rpcMutex.lock();
	DdcRpcInfo* thisCommand = findCommand(cmd);
	rpcMutex.unlock();
	if(thisCommand) {
		if(thisCommand->isCommandActive())  {
			thisCommand->setInactive();
			if(transition) 
				commError = result;
			else {
				commandMutex.lock();
				ntCmdError[cmd] = result;
				commandMutex.unlock();
//				ERS_LOG(controllerName << "_INFO: Setting command result " << result);
				setNtCommandResult(cmd, ntCmdError[cmd]);
			}
		}
	}
	else
		ERS_LOG(controllerName << "_ERROR: Reply of unregistered command " << cmd);	
}


void DdcCtService::setNtCommandResult(const string& commandName, int result)
{
	string	isDataName;
	
	if (result < COMM_ERR_OK) {
   		ERS_LOG(controllerName << "_ERROR: nt-command " << commandName 
    			  << " failed with DDC error code " << result);
	}
	else 
		if(result > COMM_ERR_OK)
			ERS_LOG(controllerName << "_ERROR: nt-command " << commandName 
					  << " failed with USER error code " << result);
	
	isDataName = COMMAND_SERVER;
	isDataName += "."+controllerName+".";
	isDataName += commandName + ".response";
	
	ddc::DdcNtCommandResponseInfoNamed* resp = new ddc::DdcNtCommandResponseInfoNamed(daqPartition, isDataName.c_str());
	resp->value = result;										
	if(forBM) {
		DdcBenchmark::getControllerTimer().stop();
		DdcBenchmark::getIsTimer().start();
	}
//	ERS_LOG("Sending to IS response " << isDataName << " as " << result << " for " << commandName);
	try {
		resp->checkin();
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG(controllerName << "_ERROR: " << isDataName
			 	  << ": sending to IS fault. Be sure that " << COMMAND_SERVER
				  << " IS server is running ");
		ERS_LOG(ex);
		if(forBM) {
			DdcBenchmark::getIsTimer().stop();
		}
	}
	catch(std::exception &ex) {
		ERS_LOG("std::exception while sending " << isDataName << " to " << COMMAND_SERVER);
	}
	catch(...) {
		ERS_LOG("Unknown exception while sending " << isDataName << " to " << COMMAND_SERVER);
	}

	delete resp;
	return; 
}


void DdcCtService::decodeError(int code, std::string &msg)
{
	char	msgText[32];
	
	switch(code) {
	case COMM_ERR_UNDEF:
		msg = "Undefined state";
		break;
	case COMM_ERR_NAME:
		msg = "Invalid command name";
		break;
	case COMM_ERR_DCSDP:
		msg = "Invalid a DCS DP (DIM Service) definition";
		break;
	case COMM_ERR_TIMEOUT:
		msg = "Command execution timeout";
		break;
	case COMM_ERR_CONNECT:
		msg = "Communication fault (no connection to project)";
		break;
	case COMM_ERR_CONNBROKEN:
		msg = "Communication fault (connection broken)";
		break;
	case COMM_ERR_CONFIG:
		msg = "Invalid command configuration";
		break;
	case COMM_ERR_NTDESC:
		msg = "Invalid definition of an nt-command";
		break;
	case COMM_ERR_BUSY:
		msg = "Previous command has not ended";
		break;
	case COMM_ERR_DCSNOTREADY:
		msg = "DCS is not ready for data taking";
		break;
	default:
		sprintf(msgText, "user defined error %d", code);
		msg = msgText;
	}
}


int DdcCtService::tryLockNtCommand(std::string serviceName) 
{
	int ret;
	
	commandMutex.lock();
	if (commandLock.find(serviceName) == commandLock.end()) {
    	commandLock.insert(std::make_pair(serviceName, new OWLSemaphore(1)));
		ntCmdError.insert(std::make_pair(serviceName, COMM_ERR_UNDEF));
  	}
  	commandMutex.unlock();
	
	if(ntcAllowedInParallel) 			    // Parallelizm of NT command allowed
		if((getRcState() != DDC_INITIAL) || ntcAllowedOnBoot)  {
//			commandMutex.lock();
//			ret = commandLock[serviceName]->try_wait();
//			commandMutex.unlock();
			ret = 0;	// Any queue of the same commands is in API (execCommand())
		}
		else {
			ret = -1;
		}
	else
		ret = commandAllowed.try_wait();

  	return ret;
}


void DdcCtService::unlockNtCommand(string serviceName)
{
	if(ntcAllowedInParallel) {			    // Parallelizm of NT command allowed
//		commandMutex.lock();
//		commandLock[serviceName]->post();
//		commandMutex.unlock();
		// Previous lines commented on the same reason that in tryLock() above
		;
	}
	else
		commandAllowed.post();
}


// Callback function for NT-commands

void commandCallback(ISCallbackInfo* cbInfo)
{
	string			entryName = cbInfo->name();
	string			commandName;
	
	DdcCtService* commandManager = (DdcCtService*)(cbInfo->parameter());
	IPCPartition p = commandManager->getPartition();
	
	ddc::DdcNtCommandInfoNamed	command(p, entryName.c_str());

	if(cbInfo->reason() != ISInfo::Deleted) {
		ERS_LOG("DDC-CT: command received: " << entryName);
		if(cbInfo->type() == command.ISInfo::type()) {
			cbInfo->value(command);
			commandName = command.commandName;
			if(commandManager->ntForBM()) {
				DdcBenchmark::getIsTimer().stop();
			}
			if(entryName.find(BM_COMMAND) == string::npos) {	// Normal NT command
				if (commandManager->tryLockNtCommand(commandName) == -1) {
 		 			ERS_LOG(commandManager->getControllerName() 
						 << "_ERROR: nt-command " << entryName 
	 						  << " is not allowed (Service is active)");
					commandManager->setNtCommandResult(command.commandName, 
															COMM_ERR_BUSY);
				}
				else {
					ERS_LOG(commandManager->getControllerName() << "_INFO: Command is allowed");
					if(!commandManager->getParallelNtAllowed() 
					&& (commandManager->ntForBM())) {
						DdcBenchmark::getControllerTimer().start();
					}
					// Send command to CtManager for execution
					commandManager->executeNtCommand(command);
// It seems, locking at the controller level does not make sense - all in execCommand(), so
					commandManager->unlockNtCommand(commandName);
				}
			}
			else {
				ERS_LOG(commandManager->getControllerName()
						  << "_ERROR: Command name should not contain BMCommand: "
						  << command.commandName);
				commandManager->setNtCommandResult(commandName, COMM_ERR_NTDESC);
			}
		}
		else {
			ERS_LOG(commandManager->getControllerName()
					  << "_ERROR: Invalid nt-command definition for "
					  << command.commandName);
			commandManager->setNtCommandResult(command.commandName, COMM_ERR_NTDESC);
		}
	}
	else {
//		ERS_LOG(entryName << " is deleted from IS");
		commandManager->deactivateCommand(entryName);  
	}
}

void DdcCtService::deactivateCommand(std::string entryName) 
{
	string cmd = entryName.substr(entryName.find('.')+1);
	cmd = cmd.substr(cmd.find('.')+1);
//	ERS_LOG("Command Deleted' callback: Deactivating " << cmd);
	rpcMutex.lock();
	DdcRpcInfo* thisCommand = findCommand(cmd);
	rpcMutex.unlock();
	if(thisCommand)
		thisCommand->setInactive();
}


// Callback function for Benchmark

void bmCommandCallback(ISCallbackInfo* cbInfo)
{
	string			entryName = cbInfo->name();
	
	DdcCtService* commandManager = (DdcCtService*)(cbInfo->parameter());
	IPCPartition p = commandManager->getPartition();
	ERS_LOG("bmCommandCallback...");
	ddc::DdcNtCommandInfoNamed	command(p, entryName.c_str());

	if(cbInfo->reason() != ISInfo::Deleted) {
		if(cbInfo->type() == command.type()) {
			cbInfo->value(command);
			if(entryName.find(BM_COMMAND) == string::npos) {	// Normal NT command
				ERS_LOG(commandManager->getControllerName() << "_ERROR: "
//						  << command.getCommName() 
						  << command.commandName 
						  << " is not Benchmark command!");
//				commandManager->setNtCommandResult(command.getCommName(), 
				commandManager->setNtCommandResult(command.commandName, 
														COMM_ERR_NTDESC);
			}
			else {
				ERS_LOG(commandManager->getControllerName()
						  << "_WARNING: Benchmark are being made for "
//						  << command.getCommName());
						  << command.commandName);
				if(commandManager->makeBenchmarks(command)) {
					ERS_LOG(commandManager->getControllerName() << "_INFO: Benchmark are made");
				}
				else {
					ERS_LOG(commandManager->getControllerName()
							  << "_INFO: Benchmark are NOT made");
				}
			}
		}
		else {
			ERS_LOG(commandManager->getControllerName()
					  << "_ERROR: Invalid nt-command definition for "
//					  << command.getCommName());
					  << command.commandName);
//			commandManager->setNtCommandResult(command.getCommName(), 
			commandManager->setNtCommandResult(command.commandName, 
														COMM_ERR_NTDESC);
		}
	}
}


// Callback for a command response - to be used for benchmarks
void callback(ISCallbackInfo* cbInfo) {
  DdcCtService* commandSender = (DdcCtService*)(cbInfo->parameter());  
  ddc::DdcNtCommandResponseInfoNamed result(commandSender->getPartition(), (char*)cbInfo->name());

  string	infoName(cbInfo->name());
  
  if(cbInfo->reason() != ISInfo::Deleted) {
    if(cbInfo->type() == result.type()) {
      cbInfo->value(result);

      DdcBenchmark::getIsTimer().stop();
      DdcBenchmark::getControllerTimer().start();

//      switch(result.getValue()) {
      switch(result.value) {
      case COMM_ERR_OK:
		commandSender->setBmCommandReply("OK");
		break;
	  case COMM_ERR_NAME:
		commandSender->setBmCommandReply("INVALID COMMAND NAME");
		break;
      case COMM_ERR_DCSDP:
		commandSender->setBmCommandReply("COMMAND NOT DEFINED PROPERLY IN DCS");
		break;
      case COMM_ERR_TIMEOUT:
		commandSender->setBmCommandReply("TIMEOUT");
		break;
      case COMM_ERR_CONNECT:
		commandSender->setBmCommandReply("PVSS IS UNAVAILABLE");
		break;
      case COMM_ERR_CONNBROKEN:
		commandSender->setBmCommandReply("CONNECTION TO DCS LOST");
		break;
      case COMM_ERR_NTDESC:
		commandSender->setBmCommandReply("INVALID COMMAND DEFINITION IN IS");
		break;
      case COMM_ERR_BUSY:
		commandSender->setBmCommandReply("NOT ALLOWED WHILE INITIAL STATE or RUN CONTROL TRANSITION");
		break;
      default:
        ostringstream os;
//        os << result.getValue();
        os << result.value;
		commandSender->setBmCommandReply("USER'S ERROR STATUS: " + os.str());
      }
    } else { 
      commandSender->setBmCommandReply("DDC Internal error");
    }

    DdcBenchmark::getControllerTimer().stop();
    DdcBenchmark::getIsTimer().start();

	try {
		result.remove();
	}
	catch(...) {}

    DdcBenchmark::getIsTimer().stop();
    DdcBenchmark::getControllerTimer().start();
  }
}


bool DdcCtService::makeBenchmarks(ddc::DdcNtCommandInfoNamed& command)
{
	// Preparing to give response
	string	isDataName;
	isDataName = COMMAND_SERVER;
	isDataName += ".";
	isDataName += BM_COMMAND; 
	isDataName += '.' + controllerName + ".response";
	
	if(ntcAllowedInParallel) {	// Makes no sense to make benchmarkes
		ddc::DdcNtCommandResponseInfoNamed* resp = new ddc::DdcNtCommandResponseInfoNamed(daqPartition, isDataName.c_str());
		resp->value	= COMM_ERR_NTDESC;
		try {
			resp->checkin();
		}
		catch(...) {
			ERS_LOG(controllerName << "_ERROR: " << resp
				 	  << " sending fault. Be sure that " << COMMAND_SERVER
					  << " is running ");
		}

		delete resp;
		return(false);
	}
	
	DdcCommander ddcCommander(daqPartition);
	ISInfoDictionary isDictionary (daqPartition);
	forBM = true;

	// First translate the BMCommand's parameters
	std::string cmdName;
	std::string cmdParams;
	std::string cmdRepeat;
	int	 cmdNum;
	bool result;
	int	 sumResult = COMM_ERR_OK;
	
//	cmdParams = command.getParValue();
	cmdParams = command.parameters;
	cmdName = cmdParams.substr(0, cmdParams.find("|"));
	cmdParams = cmdParams.substr(cmdParams.find("|")+1);
	cmdRepeat = cmdParams.substr(0, cmdParams.find("|"));
	cmdNum = atoi(cmdRepeat.c_str());
	cmdParams = cmdParams.substr(cmdParams.find("|")+1);
	ERS_LOG(controllerName << "_INFO: CMD = " << cmdName
			  << " Params: " << cmdParams);
			  
    DdcBenchmark::clean();
	ERS_LOG(controllerName << "_INFO: Repeating " << cmdName << cmdNum
			  << " times for benchmark");
	//
	// try to send a number (cmdNum) of commands in a row to measure timing
	//
	for (int i=0; i<cmdNum; ++i) {

	    DdcBenchmark::getControllerTimer().start();
	    string respName = ddcCommander.makeResponseName(controllerName, cmdName);
	    DdcBenchmark::getControllerTimer().stop();
		
	    DdcBenchmark::getIsTimer().start();
	    try { 
			subscribe(respName.c_str(), callback, (void*)this);
	   		DdcBenchmark::getIsTimer().stop();
		}
		catch(...) {
			ERS_LOG(controllerName << "_ERROR: Could not subscribe response of " << cmdName);
	 		DdcBenchmark::getIsTimer().stop();
			sumResult = -1;
			break;
		}

	    DdcBenchmark::getControllerTimer().start();
		bmCommandReply = "WAITING";
		// Now sending the command
		//
		result = ddcCommander.ddcSendCommand(controllerName, cmdName, cmdParams, 60, true);
		if (!result) {
			ERS_LOG(controllerName << "_INFO: Command #" << i << " sending failed");
			DdcBenchmark::getControllerTimer().stop();
		}
		else {
//			ERS_LOG(controllerName << "_INFO: Command " << cmdName << " sent, waiting for reply..");
			unsigned int timeLeft = 60000;

			DdcBenchmark::getControllerTimer().stop();
			while (bmCommandReply == "WAITING" && timeLeft>0) {
			    usleep(1000);
			    --timeLeft;
			}

//			if (timeLeft) 
//				ERS_LOG(controllerName << "_INFO: Command #" << i << " succeeded with response " << bmCommandReply);
//		 	else {
			if (!timeLeft) {
			    DdcBenchmark::getControllerTimer().start(); // only necessary if timed out, otherwise started in callback
				ERS_LOG(controllerName << "_WARNING: Command " << cmdName << " timed out, response " << bmCommandReply);
				sumResult = COMM_ERR_UNDEF;
			}

			DdcBenchmark::getControllerTimer().stop();
			DdcBenchmark::getIsTimer().start();

			ddcCommander.ddcRemoveCommand(controllerName, cmdName);

			DdcBenchmark::getIsTimer().stop();
			DdcBenchmark::getControllerTimer().start();

			// if the command was transferred but response is not ok
			//
			if (bmCommandReply != "WAITING") {
		  		if (bmCommandReply != "OK") {
					ERS_LOG(controllerName << "_ERROR: Return status = " << bmCommandReply);
					sumResult = COMM_ERR_UNDEF;
			    }
			} 
			else {
				ERS_LOG(controllerName << "_ERROR: No response received");
				sumResult = COMM_ERR_UNDEF;
			}
 		}

		DdcBenchmark::getControllerTimer().stop();
		DdcBenchmark::getIsTimer().start();

		try {
			unsubscribe(respName);
		}
		catch(...) {
			ERS_LOG(controllerName << "_ERROR: Could not unsubscribe " << respName);
		}

		DdcBenchmark::getIsTimer().stop();
 	}
 	
	ddc::DdcNtCommandResponseInfoNamed* resp = new ddc::DdcNtCommandResponseInfoNamed(daqPartition, isDataName.c_str());
	resp->value = sumResult;
	try {
		resp->checkin();
	}
	catch(...) {
		ERS_LOG(controllerName << "_ERROR: " << resp
			 	  << " sending fault. Be sure that command server is running");
	}

	delete resp;

	forBM = false;
	DdcBenchmark::TimerReport();
 	return(true);	
}
	

void DdcCtService::doSubscription()
{
	std::string pattern("DdcNtCommand.");
	pattern = pattern + controllerName+".*";
	ISCriteria ntCriteria(pattern);
	string commandServer(COMMAND_SERVER);
	string bmarkItemName = commandServer + ".";
	bmarkItemName += BM_COMMAND;
	
//	ERS_LOG(controllerName << "_INFO: now subscription for " << "DdcNtCommand.*");
	try {
		subscribe(commandServer.c_str(), ntCriteria, commandCallback, (void*)this);
	}
	catch(daq::is::Exception &ex) {
		ERS_LOG(controllerName << "_ERROR: "
			 	  << "Could not subscribe for NT-commands");
		ERS_LOG(ex);
	}
	catch(...) {
		ERS_LOG("Unknown exception while subscribing for NT-commands");
	}
	
// benchmarking does not work any case...
//	subscribe(bmarkItemName.c_str(), bmCommandCallback, (void*)this);
	return;
}


void DdcCtService::Initialize(bool ntc_a, bool p_ntc)
{
	if(ntc_a) {
		ntcAllowedOnBoot = true;
		commandAllowed.post();
		ERS_LOG("_INFO: NT-commands allowed on boot");
	}
	else {
		ntcAllowedOnBoot = false;
		ERS_LOG("_WARNING: NT-commands NOT allowed until LOAD");
	}
	if(p_ntc) {
		ntcAllowedInParallel = true;
		ERS_LOG("_INFO: Different NT-commands may be launched in parallel");
	}
	else {
		ntcAllowedInParallel = false;
		ERS_LOG("_WARNING: Parallel NT-commands NOT allowed");
	}
	
	dim_init();
	
	cleanNtCommands();
	doSubscription();
	ERS_LOG(controllerName << "_INFO: Subscription for nt-commands done");
}


void DdcCtService::cleanNtCommands()
{
	std::string	cmdName;
	std::string pattern("DdcNtCommand.");
	pattern = pattern + controllerName+".*";
	ISCriteria ntCriteria(pattern);
	
	try {
		ISInfoIterator cmds(daqPartition, COMMAND_SERVER, ntCriteria);
		while(cmds()) {
			cmdName = cmds.name();
			ddc::DdcNtCommandInfoNamed* command = new ddc::DdcNtCommandInfoNamed(daqPartition, cmdName.c_str());
			try {
				command->remove();
			}
			catch(...) {}
			delete command;
		}
			
	}
	catch(daq::ipc::InvalidPartition &ex) {
		ERS_LOG(controllerName << "_ERROR: " << "Could not fetch NT-commands (invalid partition)");
		return;
	}
	catch(daq::is::InvalidCriteria &ex) {
		ERS_LOG(controllerName << "_ERROR: " << "Could not fetch NT-commands (invalid criteria)");
		return;
	}
			
	pattern = controllerName+".*.response";
	ntCriteria = ISCriteria(pattern);
	try {
		ISInfoIterator resps(daqPartition, COMMAND_SERVER, ntCriteria);
		while(resps()) {
			cmdName = resps.name();
 	    	ddc::DdcNtCommandResponseInfoNamed* cmdResult = new ddc::DdcNtCommandResponseInfoNamed(daqPartition, cmdName.c_str());
  	 	 	try {
				cmdResult->remove();
   		  	}
   			catch(...) {}
			delete cmdResult;
		}
	}
	catch(daq::ipc::InvalidPartition &ex) {
		ERS_LOG(controllerName << "_ERROR: " << "Could not fetch responses (invalid partition)");
		return;
	}
	catch(daq::is::InvalidCriteria &ex) {
		ERS_LOG(controllerName << "_ERROR: " << "Could not fetch responses (invalid criteria)");
		return;
	}
}


int main(int argc, char *argv[])
{
    CmdArgStr	partition_name('p', "partition", "partition-name", "partition to work in.", CmdArg::isREQ);
    CmdArgStr	controller_name('n', "controller", "controller-name", "id (also name) of DDC controller.", CmdArg::isREQ);
    CmdArgStr	parent_name('P', "parentName", "parentName", "Name of the parent.", CmdArg::isREQ);
    CmdArgStr	segment_name('s', "segmentName", "segmentName", "Name of the segment.", CmdArg::isREQ);
    CmdArgStr	ntcAllowed('A', "NTC_allowed", "NTC_allowed", "Non-transition commands allowed before configuring.");
    CmdArgStr	ntcParallel('B', "NTC_parallel", "NTC_parallel", "Nt-commands parallel execution allowed.");
    CmdArgStr	ntcServer('C', "NTC_SERVER", "NTC_SERVER", "IS server for Nt-commands.");


//
// Initialize command line parameters with default values
//
	ntcAllowed = "YES";
	ntcParallel = "NO";
	ntcServer = "DDC";
	
//	ERS_LOG("DDC_INFO: Debugging ddcct");
	ERS_LOG("DDC_INFO: Starting DDC Controller application");
//	ERS_LOG("DDC_INFO: Debugging it with print .");
	
/*	string prt, ctrl, parent, seg, ntcA, ntcP, ntcS;  // string for debugging only
	prt = partition_name;
//	std::cerr << "DDC_INFO: "<<prt << std::endl;
	ERS_LOG("DDC_INFO: "+prt);
	ctrl = controller_name;
	std::cerr << "DDC_INFO: "<<ctrl << std::endl;
//	ERS_LOG("DDC_INFO: "<<ctrl);
	parent = parent_name;
	std::cerr << "DDC_INFO: "<<parent << std::endl;
//	ERS_LOG("DDC_INFO: "<<parent);
	seg = segment_name;
	std::cerr << "DDC_INFO: "<<seg << std::endl;
//	ERS_LOG("DDC_INFO: "<<seg);
	ntcA = ntcAllowed;
	std::cerr << "DDC_INFO: "<<ntcA << std::endl;
//	ERS_LOG("DDC_INFO: "<<ntcA);
	ntcP = ntcParallel;
	std::cerr << "DDC_INFO: "<<ntcP << std::endl;
//	ERS_LOG("DDC_INFO: "<<ntcP);
	ntcS = ntcServer;
	std::cerr << "DDC_INFO: "<<ntcS << std::endl;
//	ERS_LOG("DDC_INFO: "<<ntcS);
*/	
// 
// Declare command object and its argument-iterator
//       
    CmdLine  cmdLine(*argv, &partition_name, &controller_name, &parent_name, &segment_name, &ntcAllowed, &ntcParallel, &ntcServer, NULL);
    CmdArgvIter  arg_iter(--argc, ++argv);

//
// Parse arguments
//
//	ERS_LOG("DDC_INFO: Parsing arguments");
    cmdLine.parse(arg_iter);
//	ERS_LOG("DDC_INFO: Done. Name = " << controller_name);
	
	const string ntc_Parallel((const char*)ntcParallel);
	const string ntc_Allowed((const char*)ntcAllowed);
	const string ntc_Server((const char*)ntcServer);
	
	IPCPartition partition((const char*)partition_name);
	
	ERS_LOG(controller_name << "_INFO: IPC partition " << partition.name());

	// Initialization of CORBA broker omniORB4	
    try {
        IPCCore::init( argc, argv );
    }
    catch( daq::ipc::Exception & ex ) {
    	is::fatal( ex );
		return(1);
    }
					
	// Check whether DIM_DNS_NODE is defined
	bool conf_error = false;

	string	dimDnsNode;
	if(getenv((const char*)"DIM_DNS_NODE") == 0) 
		conf_error = true;
	else {
		dimDnsNode = getenv((const char*)"DIM_DNS_NODE");
		if(dimDnsNode.size() == 0) 
			conf_error = true;
	}

	if(conf_error) {
		ers::fatal(ddc::AppFatal(ERS_HERE, "DIM Name Server node (DIM_DNS_NODE environment) is not defined"));
		return(1);
	}
	
	const string apName((const char*)controller_name);
	
	bool	ntc_a = ((ntc_Allowed == "NO")||(ntc_Allowed == "no")||(ntc_Allowed == "No")) ?
					false : true;
	bool	p_ntc = ((ntc_Parallel == "YES")||(ntc_Parallel == "yes")||(ntc_Parallel == "Yes")) ?
					true : false;
		
	DdcCtService* ddcCtService = new DdcCtService(partition, apName);
	ERS_LOG(apName << "_INFO: Starting DDC controller");

	ddcCtService->Initialize(ntc_a, p_ntc);
	
	// DDC UserRoutines of DCS
//	DdcCtRoutines		ddcRoutines(ddcCtService);
	const std::shared_ptr<DdcCtRoutines> routines(new DdcCtRoutines(ddcCtService));
//	ddcRoutines.introduceMyself();
	routines->introduceMyself();
	
	// DDC customized controller based on rc::Controller  
	// Changes into construction introduced 25.11.13 in accordance with recent RunControl package
	const std::string ctrlName((const char*)controller_name);
	const std::string parentName((const char*)parent_name);
	const std::string segmentName((const char*)segment_name);
//	const std::string expSystem("");
	
	DdcCtDimController* ddcCtRunController = new DdcCtDimController(ddcCtService, ctrlName, partition, parentName, segmentName, routines);
//	ERS_LOG("_INFO: DDC Run controller: creating w/o routines");
//	DdcCtDimController* ddcCtRunController = new DdcCtDimController(ddcCtService, ctrlName, partition, parentName, segmentName);
	ddcCtRunController->introduceMyself();	
	ERS_LOG(ctrlName << "_INFO: DDC Run controller is created. Initializing it");
	
	ddcCtRunController->init();
	ERS_LOG(ctrlName << "_INFO: Go to Run");
	ddcCtRunController->run();
	
	ERS_LOG(ctrlName << "_INFO: DDC Run controller stopped");
	
	delete ddcCtRunController;
	ERS_LOG(ctrlName << "_INFO: DDC Run controller deleted");
	delete ddcCtService;
		
	return 0;

}
