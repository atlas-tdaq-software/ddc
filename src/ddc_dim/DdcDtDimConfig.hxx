#ifndef _DDCDTDIMCONFIG_H_
#define _DDCDTDIMCONFIG_H_

//
// This header file contains definitions of DDC-DT DIM subsystem 
// configuration data related classes 
//							S. Khomoutnikov 
//							08.03.05 - 21.03.05
// Modifications:
//	
//		S.Kh.	26.01.06	Wildcards for DCS-to-DAQ configuration introduced
//		S.Kh.	12.05.10	Bug fix version (ptr, unsubscribe)
//
///=========================================================================	

#include "../DdcDal.hxx"

/*
// The definitions are in the Request.hxx
// IS entry name of DAQ request for single read DCS data
#define	DAQ_DATA_REQUEST		"daqDataRequest"
// IS entry name of DAQ request for subscription of DCS data
#define	DAQ_SUBSCRIBE_REQUEST	"daqSubscribeRequest"
// IS entry name of DAQ request for unsubscription of DCS data
#define	DAQ_UNSUBSCRIBE_REQUEST	"daqUnsubscribeRequest"
*/
#include "../DdcRequest.hxx"

enum DdcDtDirection {
	FROM_DCS,
	TO_DCS,
	DAQ_REQUESTS
};

enum DdcDimDataType {DDC_DIM_NOTYPE = 0, I, IDA, X, XDA, F, FDA, C, CDA, SDA, R, T, TDA, BAD_TYPE};

class DdcServiceNameItem		// explicit definition of import/export service
{
public:
	DdcServiceNameItem(const std::string& name, const std::string& server) 
		: type(DDC_DIM_NOTYPE), serviceName(name), isServer(server) {}
	DdcServiceNameItem(const DdcServiceNameItem& item)
		{ type = item.getDataType(); serviceName = item.getServiceName(); 
		  isServer = item.getIsServer(); fields = item.getSons(); }

	void changeName(const std::string& name)		{ serviceName = name; }
	std::string getIsServer() const				{ return(isServer); }
	void setIsServer(const std::string& name)	{ isServer = name; }
	std::string getServiceName() const			{ return(serviceName); }
	DdcDimDataType getDataType() const		{ return(type); }
	void setDataType(DdcDimDataType t)		{ type = t; }
	const std::vector<std::string>& getSons() const	{ return fields; }
	void buildListOfSons(std::string& sons);
private:
	DdcDimDataType		type;
	std::string			serviceName;
	std::string			isServer;
	std::vector<std::string>	fields;
};

class DdcDtDpPattern			// definition of imported service pattern 
{
public:
	DdcDtDpPattern(const std::string server)
		: isServer(server) {}
	std::string getIsServer() const		{ return(isServer); }
	const std::vector<std::string>& getFields() const	{ return(fields); }
private:
	std::string	isServer;
	std::vector<std::string> patterns;
	std::vector<std::string> fields;
};

class DdcDtDimConfig
{
friend class DdcDataService;
public:
	// Builds from configuration database
	DdcDtDimConfig(Configuration&,
				const std::string& dtAppl, DdcDtDirection dir);
	// Build on request via IS
	DdcDtDimConfig(const std::vector<std::string>&, const std::string& dtAppl);
	~DdcDtDimConfig()						{ parList.erase(parList.begin(), parList.end()); }
	std::string findIsServer(const std::string& serviceName);
	DdcServiceNameItem* findItem(const std::string& name);
	std::vector<DdcServiceNameItem>& getItems() 	{ return parList; }
	unsigned int getItemNum() 				{ return(parList.size()); }
	std::string getPvssHost() const				{ return(pvssHost); }
	void getDatapointsOfList(std::vector<const daq::ddc::DdcDataTargetList *>& targetLists, DdcDtDirection);
protected:
	void removeItem(std::string& name); 
	void addItem(DdcServiceNameItem* item)	{ parList.push_back(*item); }
	void addFirst(DdcServiceNameItem* item)	{ parList.insert(parList.begin(), *item); }
private:
	std::string	pvssHost;
	std::string	applName;
	std::vector<DdcServiceNameItem> parList;		// Explicit services
	std::vector<DdcDtDpPattern> patternList;		// Patterns of services (DCS->DAQ only)
};

#endif
