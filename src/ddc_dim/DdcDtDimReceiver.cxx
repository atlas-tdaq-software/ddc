//
// This file contains definition of DdcDtDimReceiver class methods. 
// An object of this class (derived from ISInfoReceiver class) 
// is responsible for subscription in IS servers of a DAQ partition
// for the data to be imported by DCS, as well as for sending them into DCS
//  
//							S. Khomoutnikov 
//							14.03.05 - 
// See DdcDtDimReceiver.hxx to follow the history
//
/////////////////////////////////////////////////////////////////////////////////

#include <string>
#include <time.h>
#include <vector>
#include <fstream>
#include <algorithm>

using namespace std;

#include <ipc/alarm.h>
#include <ipc/partition.h>

#include <owl/semaphore.h>

#include <is/namedinfo.h>

#include <ers/ers.h>

//#include <mrs/message.h>
#include <owl/time.h>
#include <mutex>

#include "../DdcRequest.hxx"

#include "DdcDtDimConfig.hxx"
#include "DdcDtDimReceiver.hxx"
#include "DdcDtDim.hxx"

/////////////////////////////////////////////////
// Interval (in seconds) of trying to re-connect
// unavailable IS server

#define RECONNECT_INTERVAL		10

/////////////////////////////////////////////////

// new callback introduced 26.05.15 for providing safe threading

void callback(ISCallbackInfo* cbInfo)
{
	string				serviceName = cbInfo->name();
	serviceName = serviceName.substr(serviceName.find('.')+1);	// drop server name

	ISInfoDynAny			newValue;	
	DdcDataService*	manager = ((DdcDtDimReceiver*)(cbInfo->parameter()))->getDtService();

	if(cbInfo->reason() != ISInfo::Deleted) {	
		cbInfo->value(newValue);
		manager->putIsService(serviceName, newValue);
	}
	else {	
	  // Nothing to do on delete value, as it is unclear what does it mean
	  // "Undefined" for an arbitrary data type
	  // THOUGH: one can think about removing corresponding (can be taken from config)
	  // DIM publications
	}
}



/* commented the obsoleete version with full data update in callback
void callback(ISCallbackInfo* cbInfo)
{
	string				serviceName = cbInfo->name();
//	ERS_LOG("_INFO: serviceName = " << serviceName);
	serviceName = serviceName.substr(serviceName.find('.')+1);	// drop server name
	
	ISInfoAny			newValue;
	IsService*			isService;

	DdcDataService*	manager = ((DdcDtDimReceiver*)(cbInfo->parameter()))->getDtService();
	
	if(cbInfo->reason() != ISInfo::Deleted) {
	
		cbInfo->value(newValue);
	
		if(!manager->isIsRecord(newValue)) {
			if(!manager->isServicePublished(serviceName, &isService)) {
				ERS_LOG("_INFO: " << serviceName << " is a NOT yet published service: publishing it");
				manager->publishIsNonStructService(serviceName, newValue);
			}
			else {
//				ERS_LOG("_INFO: " << serviceName << " is already published service");
				manager->updateIsNonStructService(isService, newValue);
			}
		}
		else {
			if(!manager->isServicePublished(serviceName, &isService)) {
				ERS_LOG("_INFO: " << serviceName << " is NOT yet published record: publishing it");
				manager->publishIsRecord(serviceName, newValue);
			}
			else {
//				ERS_LOG("_INFO: " << serviceName << " is already published record");
				manager->updateIsRecord(isService, newValue);
			}

		}
	}
	else {
	
	  // Nothing is done on delete value, as it is unclear what does it mean
	  // "Undefined" for an arbitrary data type
	}
}
*/

void requestCallback(ISCallbackInfo* cbInfo)
{
	ddc::DdcDataRequestInfoNamed request(cbInfo->partition(), cbInfo->name()); 
	string			reqName;
	vector<string>	serviceRequired;
	
	if(cbInfo->reason() != ISInfo::Deleted) {
		reqName = cbInfo->name();
		reqName = reqName.substr(reqName.find('.')+1);	// cleanin up the server name
//		ERS_LOG("_INFO: " << reqName << " request");
		cbInfo->value(request);

		serviceRequired = request.serviceNames;
		if(serviceRequired.size() > 0) {
			if(reqName == DAQ_DATA_REQUEST) {
//				ERS_LOG("_INFO: " << serviceRequired[0] << " data requested");
				((DdcDtDimReceiver*)(cbInfo->parameter()))->getDtService()
					->	handleDaqDataRequest(serviceRequired);
			}
			else {
				if(reqName == DAQ_SUBSCRIBE_REQUEST) {
//					ERS_LOG("_INFO: " << serviceRequired[0] << " subscription requested");
					((DdcDtDimReceiver*)(cbInfo->parameter()))->getDtService()
						->	handleDaqSubscribeRequest(serviceRequired);
				}
				else {
					if(reqName == DAQ_UNSUBSCRIBE_REQUEST) {
						((DdcDtDimReceiver*)(cbInfo->parameter()))->getDtService()
							->	handleDaqUnsubscribeRequest(serviceRequired);
					}
				}
			}
		}
	}
}


// per-Alarm function to subscribe in the server having been unavailable earlier
bool moreConnectionAttempt(void* receiver)
{
	string			serverName;
	string			serviceName;
	unsigned int	i;
	bool			error = false;
	string			isServer = ((DdcDtDimReceiver*)receiver)->getIsServer();

	if(((DdcDtDimReceiver*)receiver)->getConnectionOpenedFlag()) {
		((DdcDtDimReceiver*)receiver)->setConnectionClosedFlag(false);
		return(false);
	}

	// For DAQ data to be subscribed
	vector<string> daqData = ((DdcDtDimReceiver*)receiver)->getDataList();
	for(i=0; i < daqData.size(); i++) {
		if(error)
			break;
		serviceName = isServer + "." + daqData[i];
		try {
			((DdcDtDimReceiver*)receiver)->unsubscribe(serviceName.c_str());
		}
		catch(daq::is::RepositoryNotFound &ex) {
			error = true;
		}
		catch(...) {;}
		try {
			((DdcDtDimReceiver*)receiver)
				->subscribe(serviceName.c_str(), callback, receiver);
		}
		catch(daq::is::Exception &ex) {
			error = true;
		}
	}
	
	// For DAQ requests to be subscribed for
	if(!error) {
		vector<string> daqRequests = ((DdcDtDimReceiver*)receiver)->getRequestList();
		for(i=0; i < daqRequests.size(); i++) {
			if(error)
				break;
			serviceName = isServer + "." + daqRequests[i];
			try {
				((DdcDtDimReceiver*)receiver)->unsubscribe(serviceName.c_str());
			}
			catch(daq::is::RepositoryNotFound &ex) {
				error = true;
			}
			catch(...) {;}
			try {
				((DdcDtDimReceiver*)receiver)
					->subscribe(serviceName.c_str(), requestCallback, receiver);
			}
			catch(daq::is::Exception &ex) {
				error = true;
			}
		}
	}
	return(error ? true : false);
}

DdcDtDimReceiver::DdcDtDimReceiver(string& server, IPCPartition &p, DdcDataService* man)
		: ISInfoReceiver(p), 
		  dtService(man),
		  isServer(server),
		  connectionOpenedFlag(false),
		  connectionClosedFlag(false),
		  subscriptionAlarm(0) 
{
	applName = dtService->getApplName(); 
}


void DdcDtDimReceiver::connectionClosed(void* name)
{
	if(!connectionClosedFlag) {
		ERS_LOG(applName << "_WARNING: Connection closed for " << *((string*)name) << " IS server");
		connectionOpenedFlag = false;
		if(!dtService->doExit) 			// DDC-DT is not exiting
			subscriptionAlarm = new IPCAlarm(RECONNECT_INTERVAL,  moreConnectionAttempt, (void*)this);
		connectionClosedFlag = true;
	}
	return;
}

void DdcDtDimReceiver::doSubscriptions(DdcDtDimConfig* dataConfig, DdcDtDimConfig* reqConfig)
{
	vector<DdcServiceNameItem>::iterator serviceNameItem;
	string	server;
	vector<DdcServiceNameItem>&	itemList = dataConfig->getItems();

	// First build the vectors of DAQ data elements to be subscribed for
	if(itemList.size() != 0)
		for(serviceNameItem = itemList.begin(); serviceNameItem != itemList.end(); serviceNameItem++) {
//			ERS_LOG("Item to be subscribed " << serviceNameItem->getServiceName());
			server = serviceNameItem->getIsServer();
			if(server == isServer) {
				daqData.push_back(serviceNameItem->getServiceName());
			}
		}
	
	vector<DdcServiceNameItem>&	itemList1 = reqConfig->getItems();
	if(itemList1.size() != 0) {
		for(serviceNameItem = itemList1.begin(); serviceNameItem != itemList1.end(); serviceNameItem++) {
			server = serviceNameItem->getIsServer();
			if(server == isServer) {
				daqRequests.push_back(serviceNameItem->getServiceName());
			}
		}
	}
	else {
		ERS_LOG(applName << "_ERROR: No requests subscribed for");
	}

	// And now try to subscribe for them
	bool	error = false;
	unsigned int	i;
	string			serviceName;
	string			ex;
	
	for(i=0; i<daqData.size(); i++) {
		if(error)
			break;
		serviceName = isServer + "." + daqData[i];
		try {
			subscribe(serviceName.c_str(), callback, (void*)this);
		}
		catch(daq::is::Exception &ex) {
			ERS_LOG(applName << "_ERROR: IS server " << isServer 
				 << " with DAQ data is unavailable: " << ex);
			ERS_LOG(applName << "_INFO: Trying to re-connect to " << isServer
				 << " IS server every " << RECONNECT_INTERVAL << " seconds");
			subscriptionAlarm = new IPCAlarm(RECONNECT_INTERVAL,  moreConnectionAttempt, (void*)this);
			error = true;
		}
		if(!error)
			ERS_LOG(applName << "_INFO: " << serviceName << " is subscribed");
	}
	
	if(!error)
		for(i=0; i<daqRequests.size(); i++) {
			if(error)
				break;
			serviceName = isServer + "." + daqRequests[i];
			try {
				subscribe(serviceName.c_str(), requestCallback, (void*)this);
			}
			catch(daq::is::Exception &ex) {
				ERS_LOG(applName << "_ERROR: IS server " << isServer 
					 << " for DAQ requests is unavailable: " << ex);
				ERS_LOG(applName << "_INFO: Trying to re-connect to " << isServer
					 << " IS server every " << RECONNECT_INTERVAL << " seconds");
				subscriptionAlarm = new IPCAlarm(RECONNECT_INTERVAL,  moreConnectionAttempt, (void*)this);
			}
		}
}
	

void DdcDtDimReceiver::stopImport()
{
	unsigned int	i, n;
	string			fullName;
	
	if(subscriptionAlarm != 0) {
		ERS_LOG(applName << "_INFO: Unsetting alarm for re-connect IS server(s)");
		delete subscriptionAlarm;
		subscriptionAlarm = 0;
	}
	
	n = daqData.size();
	for(i=0; i<n; i++) {
		fullName = isServer + "." + daqData[i];
		try {
			unsubscribe(fullName.c_str());
		}
		catch(daq::is::Exception &ex) {
			break;
		}
	}
	
	n = daqRequests.size();
	for(i=0; i<n; i++) {
		fullName = isServer + "." + daqRequests[i];
		try {
			unsubscribe(fullName.c_str());
		}
		catch(daq::is::Exception &ex) {
			break;
		}
	}
//	stop();			// At 2013 already there is no this method. It is really redundant
	return;
}
