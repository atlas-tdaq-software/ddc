#ifndef _DDCDTDIMRECEIVER_H_
#define _DDCDTDIMRECEIVER_H_
//
// This header file contains definition of DdcDtReceiver object class. 
// An object of this class (derived from ISInfoReceiver class) 
// is responsible for subscription in IS servers of a DAQ partition
// for the data to be imported by DCS, as well as for sending them into DCS
// 
//							S. Khomoutnikov 
//							14.03.05 - 21.03.05
// Modifications:
//		S.Kh	30.11.06	IS calls modified to accept Exceptions
//
///////////////////////////////////////////////////////////////////////////

#include <is/infodynany.h>
#include <is/inforeceiver.h>

class DdcDataService;

class DdcDtDimReceiver : public ISInfoReceiver
{
public:
	DdcDtDimReceiver(std::string& server, IPCPartition &p, DdcDataService* man); 
	
	void 			stopImport();
	void 			doSubscriptions(DdcDtDimConfig* dataConfig, 
									DdcDtDimConfig* requestConfig);
	DdcDataService*				getDtService() 		{ return(dtService); }
	std::vector<std::string>&	getDataList()  		{ return(daqData); }
	std::vector<std::string>&	getRequestList() 	{ return(daqRequests); }
	std::string&				getIsServer()		{ return(isServer); }
	const std::string&			getApplName()		{ return(applName); }
	bool		getConnectionOpenedFlag() 		{ return(connectionOpenedFlag); }
	void		setConnectionClosedFlag(bool s) { connectionClosedFlag = s; }
	
private:
	void*			connectionOpened()	
						{ connectionOpenedFlag = true;
						  std::cerr << applName << "_INFO: Connection opened for "
						  	   << isServer << " IS server" << std::endl;
						  return(&isServer); }
	void			connectionClosed(void* name);
	
	DdcDataService* dtService;
	std::string		isServer;
	std::string		applName;
	bool			connectionOpenedFlag;
	bool			connectionClosedFlag;
	IPCAlarm*		subscriptionAlarm;
	std::vector<std::string>	daqData;
	std::vector<std::string>	daqRequests;
};

#endif
