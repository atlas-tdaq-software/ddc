#ifndef _DDCCTDIM_H_
#define _DDCCTDIM_H_

//
// This header file contains definition of object classes 
// responsible for DAQ command transfer to a certain PVSS system.
// The application is produced from DdcCtManager by removing of direct 
// connection with PVSS run time database and by introducing instead
// communication with PVSS DIM manager
//							S. Khomoutnikov 
//							30.03.05 - .04.05
// Modifications:
//		S.Kh	06.10.05	Implementation independent ::Configuration() constructor	
//							in launchCommand() method
//		S.Kh.	25.10.05	ConfDb converter of environment variables is introduced
//		S.Kh.	07.06.06	launchCommand() changed for integration with DCS FSM
//							(FwFsmAtlasDDC DP type with .fsmParameters)
//		S.Kh.	04.04.07	Synchronization bug for RpcInfo fixed: no timeout check in 
//							launchCommand (and Nt), delete RpcInfo before creating new one
//							insted of just after receiving reply
//		S.Kh.	12.09.07	1) NT-commands allowed just after the booting (may be denyed
//							   by optinal cmd-line parameter ntcAllowed = NO);
//							2) DdcRpcInfo is created while the first call of a command and
//							   never deleted - for avoiding subscription for result each 
//							   call. This is for both RC and NT commands;
//							3) Dropped obsolete members: activeCommand, activeNtCommand
//							   and timeExpired().
//		S.Kh.	26.09.07	1) Benchmark command is introduced (see methodmakeBenchmark(),
//							   bmCommandCallback(), changes in executeNtCommand(), 
//							   #define BM_COMMANDin DdcCommander.hxx);
//		S.Kh.	07.02.08	Option of parallel execution of non-transition commands
//		S.Kh.	17.06.08	Dropping non-transition commands from IS on finalAction
//		S.Kh.	04.03.09	Fixed bug with ntRpcInfo in executeNtCommand() - this must be
//							a local variable, not class member.
//							NT-command name includes again the controller name.
//		S.Kh.	11.03.09	Removing NT-commands "hanging" in IS since previous unsuccessful
//							run is move to Initialize(). New method cleanNtCommands(). 
//		S.Kh.	07.01.10	Replacing Not_Ready_for_DAQ flag handling by publishin in the 
//							DDC IS server an integer value indicating the TTC partition state.
//							Warning message on exit is dropped.
//		S.Kh.	13.04.10	Mechanism of trying connection to DIM-DNS changed
//
/////////////////////////////////////////////////////////////////////////////////////////

#include <ers/ers.h>
#include <is/inforeceiver.h>
#include <owl/semaphore.h>
#include <mutex>

#include <dic.hxx>

#include <map>

#include "../DdcDal.hxx"

#include "../DdcCommand.hxx"
#include "ddc/DdcIntInfoNamed.h"

#include "DdcCtDimConfig.hxx"

// Constants of connection to PVSS
#define PVSS_CONN_TIMEOUT  10		// Timeout for DimCurrentInfo reading DIS_DNS
#define PVSS_CONN_ATTEMPTS 3		// Number of attempts to read DIS_DNS									
									
// Definition of states of connection to DCS alarm
#define	ALRM_CONNECT_UNDEF	-1
#define	ALRM_CONNECT_OK		0
#define	ALRM_CONNECT_ERROR	1
#define TTCPARTITION_STATE_READY	1
#define TTCPARTITION_STATE_STANDBY	2

// Definition of TTC Partition State IS server
const std::string TTCPARTITION_STATE_SERVER("DDC");

enum DdcRcState {DDC_INITIAL, DDC_CONFIGURED, DDC_CONNECTED, DDC_PREPARED, DDC_RUNNING, DDC_PAUSED};

const std::string DIM_DNS_NAME("DIS_DNS");


class DdcCtService;

class DdcRpcInfo : public DimRpcInfo
{
public:
	DdcRpcInfo(std::string& name, int timeout, int nolink, bool tr, DdcCtService* father)
		: DimRpcInfo((char *)name.c_str(), timeout, nolink),
		  transitionCommand(tr), active(false), ddcct(father) {}
	bool	isCommandActive()		{ return(active); }
	bool    isTransitionCmd()		{ return(transitionCommand); }
	void	setActive()				{ active = true; }
	void	setInactive()			{ active = false; }
	void 	rpcInfoHandler();

private:
	bool			transitionCommand;
	bool			active;
	DdcCtService*	ddcct;
};


class DdcCtService : public ISInfoReceiver, public DimClient
{

	friend class DdcCtDimController;

public:
	DdcCtService(IPCPartition& partition, const std::string& controller)
		: ISInfoReceiver(partition, false),
		  controllerName(controller),
		  commandConfig(0), dcsRuns(false), daqPartition(partition), alarmInfo(0),
	   	  commError(COMM_ERR_UNDEF), rcCommandActive(false), 
		  rpcInfo(0), 
		  runController(0), partitionStatePtr(0)	
	{ 	ERS_LOG(controllerName << "_INFO: DDC_CT_DIM service object is constructed"); }
		  
	~DdcCtService() {}
	void Initialize(bool ntc_a, bool p_ntc);
	void infoHandler();
	int  launchCommand(std::string rc_command);
	IPCPartition& getPartition()	{ return(daqPartition); }
	std::string& getControllerName()		{ return(controllerName); }
	void setError(int err) 			{ commError = err; }
	void executeNtCommand(ddc::DdcNtCommandInfoNamed &);
	void setNtCommandResult(const std::string& commandName, int res);
	void setRcState(DdcRcState st)	{ controllerState = st; }
	DdcRcState 		getRcState()	{ return(controllerState); }
	DdcCtDimConfig*	getConfig()		{ return(commandConfig); }
	void referController(DdcCtDimController* ctrl)	{ runController = ctrl; }
	void doSubscription();				// subscribe for nt-commands
	DdcCtDimController* getController()	{ return(runController); }
	int  getPartitionState()			{ return(TTCPartitionState); }
	void setRoutines(DdcCtRoutines* rts){ myMethods = rts; }
	void acceptCommandResult(std::string cmdName, bool transRes, int result);
	int  tryLockNtCommand(std::string name);
	void unlockNtCommand(std::string name);
	bool getNtAllowedOnBoot()	{ return(ntcAllowedOnBoot); }
	bool getParallelNtAllowed()			{ return(ntcAllowedInParallel); }
	bool makeBenchmarks(ddc::DdcNtCommandInfoNamed & );
	std::string& getBmCommandReply(){ return bmCommandReply; }
	void setBmCommandReply(std::string s){ bmCommandReply = s; }
	bool ntForBM()					{ return(forBM); }
	std::string getNtServer()       { return(COMMAND_SERVER); }
	DdcRpcInfo* findCommand(std::string name);
	void deactivateCommand(std::string entryName);
	
	int	flagAlarmConnected;
	
	static bool doExit;
private:
	int  buildDdcCtConfig(Configuration &);
	void decodeError(int commError, std::string& msg);
	void cleanNtCommands();
	void updatePartitionState(int state, time_t updateTime);
	
	std::string		controllerName;
	DdcCtDimConfig*	commandConfig;
	std::string		pvssHost;
	bool			dcsRuns;
	IPCPartition&	daqPartition;
	std::string		alarmDpService;
	DimStampedInfo*	alarmInfo;
	OWLSemaphore	commandAllowed;         // lock for transition  commands
  	std::map<std::string,OWLSemaphore*> commandLock;  	// lock for NT-commands
  	std::map<std::string,int> ntCmdError;  	// Responses of non-transition commands
  	std::mutex 		commandMutex;  		// semaphore and response container protection
	std::mutex 		rpcMutex;			// rpc creation protection
	int				commError;
	bool			rcCommandActive;
	DdcRpcInfo*		rpcInfo;
	unsigned int	TTCPartitionState;		// Status of TTC partition for data taking (instead of flag "Not_Ready_for_DAQ")
	unsigned int	prevTTCPartitionState;	// previous value of TTCPartitionStatus
	DdcCtDimController* runController;
	DdcRcState		controllerState;
	DdcCtRoutines* 	myMethods;
	bool			ntcAllowedOnBoot;
	bool			ntcAllowedInParallel;
	bool			forBM;
	std::string     bmCommandReply;
	std::vector<DdcRpcInfo*> commands;
	ddc::DdcIntInfoNamed* partitionStatePtr;
};

#endif
